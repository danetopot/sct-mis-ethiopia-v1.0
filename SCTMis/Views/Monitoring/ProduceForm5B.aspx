﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ProduceForm5AModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Produce Non Compliance Form 5B
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   
<% using (Html.BeginForm("ProduceForm5B", "Monitoring", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-7">                
            <label>
                Region <%: Html.ValidationMessageFor(model => model.RegionID) %>
                <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Woreda  <%: Html.ValidationMessageFor(model => model.WoredaID) %>
                <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Kebele <%: Html.ValidationMessageFor(model => model.KebeleID) %>  
                <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
            </label>
            </div>

            <div class="col-4">                
            <label>
                Reporting Period <%: Html.ValidationMessageFor(model => model.ReportingPeriod) %>
                <%: Html.DropDownListFor(model => model.ReportingPeriod,
                    new SelectList(Model.reportgperiod, "PeriodID", "PeriodName"), new { tabindex = "4" })%>
            </label>
            </div>
          <br /><br />
            <div class="form-actions">
	            <button id="cmdProduce" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Produce Non Compliance Form 5B</button>
                <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
            </div>
            <br />
            <%: Html.ValidationSummary(true) %>          
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#KebeleID,#ReportingPeriod").prop("disabled", false);
            $("#KebeleID,#ReportingPeriod").val('0');
            $("#cmdBack,#cmdProduce").prop("disabled", false);
            $(":input").blur();
            $("#RegionID,#WoredaID").prop("disabled", true);
            $('#loading').hide();
            $('#KebeleID').focus();

            $("#cmdBack").click(function () {
                window.location.href = "/Monitoring/Form5B";
            });
        });
    </script>
</asp:Content>
