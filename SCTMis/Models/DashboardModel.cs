﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCTMis.Models
{
    public class HouseholdByAge
    {
        public int ColumnID { get; set; }
        public int Number { get; set; }
        public string Age { get; set; }
    }

    public class HouseholdByVulnerability
    {
        public int ColumnID { get; set; }
        public int Number { get; set; }
        public string Vulnerability { get; set; }
    }

    public class PeriodLineChart
    {
        public int ColumnID { get; set; }
        public int Complete { get; set; }
        public int Complied { get; set; }
        public int UnComplied { get; set; }
        public string Months { get; set; }
    }

    public class FourthLineChart
    {
        public int ColumnID { get; set; }
        public int Visits { get; set; }
        public int Cases { get; set; }
        public int Complied { get; set; }
        public string Months { get; set; }
    }

    public class TheDashboard
    {
        public string Chartdata { get; set; }
    }

    public class DashboardModel
    {
        public int ColumnID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public int FiscalYear { get; set; }
        public int ReportingIndicatorId { get; set; }
        public int PeriodFrom { get; set; }
        public int PeriodTo { get; set; }
        public string Sex { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<IntegratedServices> intgrtedservis { get; set; }
        public IEnumerable<FinancialYear> fiscalYears { get; set; }
        public IEnumerable<Gender> genders { get; set; }
        public List<ReportingPeriod> reportgperiod { get; set; }
        public IEnumerable<ReportingIndicator> reportingIndicators { get; set; }
    }

    public class DashboardSummaryList
    {
        public int ColumnID { get; set; }
        public int TotalClients { get; set; }
        public int Compliant { get; set; }
        public int NonCompliant { get; set; }
        public string PeriodMonth { get; set; }
        public decimal CompliantPercentage { get; set; }
    }

    public class DashboardSummaryListGrid
    {
        public string PeriodMonthOne { get; set; }
        public int Compliant { get; set; }
        public int TotalClients { get; set; }
        public string PeriodMonthTwo { get; set; }
        public decimal CompliantPercentage { get; set; }
    }

    public class DashboardComplianceBar
    {
        public string PeriodMonth { get; set; }
        public int Compliant { get; set; }
        public int NonCompliant { get; set; }
    }

    public class DashboardComplianceTrend
    {
        public string PeriodMonth { get; set; }
        public decimal CompliantPercentage { get; set; }
    }
}
