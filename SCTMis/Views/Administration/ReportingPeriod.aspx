﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

   
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Reporting Period Listing
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.Partial("_CommonSearch") %>
    <div id="gridbox" style="position:relative; width: 680px; height: 250px"></div>
    <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>

    <%--<div class="form-actions">
	    <button id="cmdNew" type="button" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>Add Reporting Period</button>
    </div>--%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
        var page_count = 10;
        var gridHeader = 'ColumnID,PeriodID,Period<span class="HeaderChange">_</span>Name,Start<span class="HeaderChange">_</span>Date,End<span class="HeaderChange">_</span>Date,F<span class="HeaderChange">_</span>Year,Creator';
        var gridColType = 'ro,ro,ro,ro,ro,ro,ro';
        var mygrid;

        $(document).ready(function () {
            $('#loading').hide();
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,200,120,120,120,120");
            mygrid.setColAlign("left,left,left,right,right,right,right");
            mygrid.setHeader(gridHeader);

            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");

            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();

            mygrid.loadXML("/Administration/ReportPeriodList?RecCount=" + page_count);
            dhtmlxError.catchError("ALL", my_error_handler);

            $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));

            $("#cmdNew").click(function () {
                window.location.href = "/Administration/AddReportingPeriod";
            });
        });
    </script>
</asp:Content>
