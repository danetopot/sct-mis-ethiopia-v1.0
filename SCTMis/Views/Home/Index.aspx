﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Dashboard.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.DashboardModel>" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    SCT-MIS Dashboard
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("Dashboards", "Household", FormMethod.Post, new { name = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-7">                  
            <label>
                Fiscal Year <%: Html.ValidationMessageFor(model => model.FiscalYear) %>
                <%: Html.DropDownListFor(model => model.FiscalYear,
                    new SelectList(Model.fiscalYears, "FiscalYear", "FiscalYearName"), new { tabindex = "1" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Period From
                <%: Html.DropDownListFor(model => model.PeriodFrom,
                    new SelectList(Model.reportgperiod, "PeriodID", "PeriodName"), new { tabindex = "2" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Period To
                <%: Html.DropDownListFor(model => model.PeriodTo,
                    new SelectList(Model.reportgperiod, "PeriodID", "PeriodName"), new { tabindex = "3" })%>
            </label>
            </div>
            <div class="col-4">                
            <label>
                Region <%: Html.ValidationMessageFor(model => model.RegionID) %>
                <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "4" })%>
            </label>
            </div>
            <div class="col-4">                
            <label>
                Woreda  <%: Html.ValidationMessageFor(model => model.WoredaID) %>
                <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "5" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Kebele <%: Html.ValidationMessageFor(model => model.KebeleID) %>  
                <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "6" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Indicator <%: Html.ValidationMessageFor(model => model.ReportingIndicatorId) %>  
                <%: Html.DropDownListFor(model => model.ReportingIndicatorId,
                    new SelectList(Model.reportingIndicators, "ReportingIndicatorId", "ReportingIndicatorName"), new { tabindex = "7" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Sex 
                <%: Html.DropDownListFor(model => model.Sex,
                    new SelectList(Model.genders, "GenderID", "GenderName"), new { tabindex = "8" })%>
            </label>
            </div>
        <%--Sixth Row--%>
            <button id="cmdFilterDashboard" type="button" class="btn btn-icon btn-primary glyphicons search" tabindex="4"><i></i>Filter</button>
                <%: Html.HiddenFor(model => model.ColumnID)%>
            <hr />
            <%: Html.ValidationSummary(true) %>          
    <% } %>
    <div id="columns">
        <p id="complianceChartTitle"></p>
        <div id="complianceChart" style="width:98%;height:180px;margin:1px;border:1px solid #A4BED4"></div>
        <br />
        <p id="complianceTrendTitle"></p>
        <div id="complianceTrend" style="width:98%;height:180px;margin:1px;border:1px solid #A4BED4"></div>
        <br />
        <p id="complianceGridTitle"></p>        
        <div id="gridbox" style="position:relative; width:98%; height: 300px"></div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <style>
        #columns{
            display: table;
            width: 500px;
        }
        .col1{
            display: table-cell;
            width: 55%;
            padding: 1em;
            position: relative;
            left: auto;
        }
        .col2{
            width: 35%;
            display: table-cell;
            padding: 1em;
            position: relative;
        }
    </style>

    <script src="../Scripts/assets/js/moment.min.js"></script>
    <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
    <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>

	<script type="text/javascript">
	    //grid stuff
	    var EVENTID = null;
	    var arrObj = [];
	    var SerialCount = 0;
	    var page_count = 12;
	    var mygrid;
	    //var gridHeader = 'Period,No. of Clients that Complied,Total Number<span class="HeaderChange"> </span>of Clients,Period,% of Clients that complied';
	    var gridHeader = 'Period,Complied clients,Total Clients,Period,% complied clients';
	    var gridColType = 'ro,ro,ro,ro,ro';
	    //end grid stuff    

	    $("#cmdFilterDashboard").click(function () {
	        myComplianceChart.clearAll();
	        myComplianceTrendChart.clearAll();

	        refreshDashboard();	        
	    });

        //compliance bar chart
	    var myComplianceChart = new dhtmlXChart({
	        view: "bar",
	        container: "complianceChart",
	        value: "#Compliant#",
	        radius: 0,
	        alpha: 0.5,
	        border: true,
	        width: 100,
	        label: "",
	        color: "#45abf5",
	        tooltip: {
	            template: "#Compliant#,#PeriodMonth#"
	        },
	        xAxis: {
	            title: 'Period',
	            template: "#PeriodMonth#"
	        },
	        legend: {
	            values: [
                    { text: "Compliant", color: "#45abf5" },
                    { text: "Non-Compliant", color: "#f58f45" }
	            ],
	            valign: "top",
                align: "center",
	            width: 90,
	            layout: "x"
	        },
	        yAxis: {
	            title: "Clients"
	        }
	    });

	    myComplianceChart.addSeries({
	        value: "#NonCompliant#",
	        tooltip: {
	            template: "#NonCompliant#,#PeriodMonth#"
	        },
	        color: "#f58f45"
	    });

	    //compliance %age trend
	    var myComplianceTrendChart = new dhtmlXChart({
	        view: "line",
	        container: "complianceTrend",
	        value: "#CompliantPercentage#",
	        label: "#CompliantPercentage#",
	        item: {
	            borderColor: "#447900",
	            color: "#69ba00"
	        },
	        line: {
	            color: "#69ba00",
	            width: 2
	        },
	        tooltip: {
	            template: "#CompliantPercentage#"
	        },
	        offset: 0,
	        xAxis: {
	            template: "#PeriodMonth#",
	            title: "Period"
	        },
	        yAxis: {
	            title: "Compliance %"
	        },
	        padding: {
	            left: 50,
	            bottom: 50
	        },
	        origin: 0
	    });

	    $(document).ready(function () {	        
	        $('#Sex').prop('disabled', true);

	        $('#ReportingIndicatorId').change(function () {
	            if ($('#ReportingIndicatorId').val() == 4) {
	                $('#Sex').prop('disabled', false);
	            } else {
	                $('#Sex').prop('disabled', true);
	            }
	        });                   

	        refreshDashboard();

	        $("#RegionID").change(function () {
	            regionChange();
	        });

	        $("#WoredaID").change(function () {
	            woredaChange();
	        });

	        $("#FiscalYear").change(function () {
	            fiscalYearChange();
	        });

	        function regionChange() {
	            $('#WoredaID,#KebeleID').empty();
	            $.ajax({
	                type: "POST",
	                url: "../Household/SelectDashWoreda",
	                data: "{  'RegionID' : '" + $("#RegionID").val() + "'}",
	                dataType: "json",
	                contentType: "application/json; charset=utf-8",
	                error: function (xx, yy) {
	                    //alert(yy);
	                },
	                success: function (data) {
	                    for (var i = 0; i < data.length; i++) {
	                        var d = data[i];
	                        $('#WoredaID').append($("<option></option>").attr("value", d.WoredaID).text(d.WoredaName));
	                    }
	                    woredaChange();
	                }
	            });
	        }

	        function woredaChange() {
	            $('#KebeleID').empty();
	            $.ajax({
	                type: "POST",
	                url: "../Household/SelectDashKebele",
	                data: "{  'WoredaID' : '" + $("#WoredaID").val() + "'}",
	                dataType: "json",
	                contentType: "application/json; charset=utf-8",
	                error: function (xx, yy) {
	                    alert(yy);
	                },
	                success: function (data) {
	                    for (var i = 0; i < data.length; i++) {
	                        var d = data[i];
	                        $('#KebeleCodeID').append($("<option></option>").attr("value", d.KebeleID).text(d.KebeleName));
	                    }
	                }
	            });
	        }

	        function fiscalYearChange() {
	            $('#PeriodFrom,#PeriodTo').empty();
	            $.ajax({
	                type: "POST",
	                url: "../Household/SelectReportingPeriods",
	                data: "{  'FiscalYear' : '" + $("#FiscalYear").val() + "'}",
	                dataType: "json",
	                contentType: "application/json; charset=utf-8",
	                error: function (xx, yy) {
	                    alert(yy);
	                },
	                success: function (data) {
	                    for (var i = 0; i < data.length; i++) {
	                        var d = data[i];
	                        $('#PeriodFrom').append($("<option></option>").attr("value", d.PeriodID).text(d.PeriodName));
	                        $('#PeriodTo').append($("<option></option>").attr("value", d.PeriodID).text(d.PeriodName));
	                    }
	                }
	            });
	        }
	    });

	    function refreshDashboard() {
	        var model = getModel();

	        loadDashboardSummariesList(model);
	        loadLinecharts(model);	
	        loadBarcharts(model);

	        var indicator = $("#ReportingIndicatorId option:selected").text();
	        var title = "Section B: Chart on client compliance to " + indicator + " indicator";
	        $("#complianceChartTitle").html(title);

	        title = "Section C: % of client compliance to " + indicator + " indicator";
	        $("#complianceTrendTitle").html(title);
	        
	        title = "Section D: Table on trend of client compliance to " + indicator + " indicator"
	        $("#complianceGridTitle").html(title);
	    }

	    function loadDashboardSummariesList(model) {
	        $('#loading').hide();
	        //RetargetingHeaderID
	        mygrid = new dhtmlXGridObject('gridbox');
	        mygrid.clearAll();
	        mygrid.setImagePath("../DHTMLX/codebase/imgs/");
	        mygrid.setInitWidths("100,200,180,180,180");
	        mygrid.setColAlign("right,right,right,right,right");
	        mygrid.setHeader(gridHeader);

	        mygrid.setColTypes(gridColType);
	        mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
	        mygrid.setPagingSkin("bricks");
	        mygrid.setSkin("dhx_skyblue");

	        mygrid.attachEvent("onXLE", showLoading);
	        mygrid.attachEvent("onXLS", function () { showLoading(true) });
	        mygrid.init();

	        mygrid.loadXML("/Dashboard/DashboardSummariesList?filterString=" + model);
	        dhtmlxError.catchError("ALL", my_error_handler);
	    }

	    function loadBarcharts(model) {
	        //myComplianceChart.clearAll();
	        myComplianceChart.load("/Dashboard/FetchDashboardComplianceBarData?filterString=" + model);	        
	    }

	    function loadLinecharts(model) {
	        //myComplianceTrendChart.clearAll();
	        myComplianceTrendChart.load("/Dashboard/FetchDashboardComplianceTrendData?filterString=" + model);
	    }
	    	    
	    function getModel() {
	        var periodFrom = $("#PeriodFrom").val();
	        var periodTo = $('#PeriodTo').val();

	        if (periodFrom == '' || periodFrom == null) {
	            periodFrom = 0;
	        }

	        if (periodTo == '' || periodTo == null) {
	            periodTo = 0;
	        }

	        var model = "{ RegionID: " + $('#RegionID').val() + ", WoredaID: " + $("#WoredaID").val() + ", KebeleID: " + $("#KebeleID").val() + ", ";
	        model = model + "FiscalYear: " + $('#FiscalYear').val() + ", ReportingIndicatorId: " + $("#ReportingIndicatorId").val() + ", PeriodFrom: " + periodFrom + ", ";
	        model = model + "PeriodTo: " + periodTo + ", Sex: '" + $("#Sex").val() + "'}"

	        return model;
	    }

	</script>
</asp:Content>
