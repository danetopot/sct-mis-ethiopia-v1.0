﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Financial Years
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="gridbox" style="position:relative; width: 470px; height: 320px"></div>
    <div id="gridfooter">
        <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
        var page_count = 10;
        var gridHeader = 'ColumnID,FiscalYear,Financial<span class="HeaderChange">_</span>Year,Start<span class="HeaderChange">_</span>Date,End<span class="HeaderChange">_</span>Date,Status,Edit';
        var gridColType = 'ro,ro,ro,ro,ro,ro,img';
        var mygrid;

        $(document).ready(function () {
            $('#loading').hide();
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,120,90,90,90,80");
            mygrid.setColAlign("left,left,right,right,right,right,left");
            mygrid.setHeader(gridHeader);

            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");

            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();

            mygrid.loadXML("/Administration/FinancialYearsList?RecCount=" + page_count);
            dhtmlxError.catchError("ALL", my_error_handler);
        });

        function createWindow(_fiscalYear) {
            var selectedRow = mygrid.getSelectedId();
            _fiscalYear = mygrid.cells(selectedRow, 1).getValue();

            post('../Administration/EditFinancialYear', { FiscalYear: _fiscalYear });
        }
    </script>
</asp:Content>
