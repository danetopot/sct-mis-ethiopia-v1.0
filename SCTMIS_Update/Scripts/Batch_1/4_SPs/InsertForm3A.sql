ALTER Proc [dbo].[InsertForm3A]
(
	@KebeleID				Int,
	@ReportFileName			varchar(50),
	@CreatedBy				varchar(30)
)
AS
BEGIN
SET NOCOUNT ON

	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD

		INSERT INTO CheckListDS(CheckListDSID,CoRespDSDetailID,GeneratedBy,GeneratedOn,ReportFileName,FiscalYear)
		SELECT NEWID(),CoRespDSDetailID,dbo.f_getUserIDByUserName(@CreatedBy),GETDATE(),@ReportFileName,dbo.f_getFiscalYearFromDate(GETDATE()) 
		FROM ProfileDSHeader
		INNER JOIN ProfileDSDetail ON ProfileDSHeader.ProfileDSHeaderID = ProfileDSDetail.ProfileDSHeaderID
		INNER JOIN CoResponsibilityDSDetail ON CoResponsibilityDSDetail.ProfileDSDetailID = ProfileDSDetail.ProfileDSDetailID
		WHERE KebeleID = @KebeleID 
		AND ISNULL(Form2Produced,0) = 1
		AND ISNULL(Form3Generated,0) = 0
		AND ISNULL(ApprovedBy,'') <> ''

		UPDATE ProfileDSDetail
		SET Form3Generated = 1
		FROM ProfileDSHeader
		INNER JOIN ProfileDSDetail ON ProfileDSHeader.ProfileDSHeaderID = ProfileDSDetail.ProfileDSHeaderID
		WHERE KebeleID = @KebeleID 
		AND ISNULL(Form2Produced,0) = 1
		AND ISNULL(Form3Generated,0) = 0
		AND ISNULL(ApprovedBy,'') <> ''

		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
		  --PRINT 'Error detected, all changes reversed'
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END

