﻿using SCTMis.Models;
using SCTMis.Services;
using System;
using System.Web.Mvc;

namespace SCTMis.Controllers
{
    [SessionConfig.SessionExpireFilter]
    public class HomeController : Controller
    {
        GeneralServices dbService = new GeneralServices();

        public ActionResult Index()
        {
            ViewBag.Subtitle = "Dashboard";
            ViewBag.Message = "Your app description page.";

            DashboardModel householdModel = new DashboardModel();

            var currentFY = dbService.GetCurrentFiscalYear();

            householdModel.regions = dbService.GetDashRegions();
            householdModel.woredas = dbService.GetDashWoredas(int.Parse(Session["RegionID"].ToString()));
            householdModel.kebeles = dbService.GetDashKebeles(int.Parse(Session["WoredaID"].ToString()));
            householdModel.intgrtedservis = dbService.GetAllIntegratedServices();
            householdModel.genders = dbService.GetGenders();
            householdModel.fiscalYears = dbService.GetFiscalYears(null);            
            householdModel.reportingIndicators = dbService.GetReportingIndicators();

            var reportgperiods = dbService.GetReportingPeriods(currentFY.FiscalYear);
            householdModel.reportgperiod = reportgperiods;

            if (!(reportgperiods == null) && (reportgperiods.Count > 0))
            {
                householdModel.PeriodTo = int.Parse(reportgperiods.FindLast(p => p.ColumnID != -9999).PeriodID);
            }
            
            //defaos
            householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
            householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
            
            return View(householdModel);
        }

        [Authorize]
        public FileResult downloadpdf()
        {
            try
            {
                string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + Request.QueryString["GenerationID"].ToString());
                
                switch (strReportFilePath.Substring(strReportFilePath.Length - 3).ToUpper())
                {
                    case "XLS":
                    case "CSV":
                        byte[] fileBytes = System.IO.File.ReadAllBytes(strReportFilePath);
                        return File(fileBytes, "application/vnd.ms-excel", Request.QueryString["GenerationID"].ToString());
                    default:
                        return File(strReportFilePath, "application/pdf");
                }
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            return null;
        }

        [Authorize]
        public FileResult downloadxls()
        {
            try
            {
                string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + Request.QueryString["GenerationID"].ToString());
                byte[] fileBytes = System.IO.File.ReadAllBytes(strReportFilePath);
                return File(fileBytes, "application/vnd.ms-excel", Request.QueryString["GenerationID"].ToString());
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            return null;
        }
    }
}
