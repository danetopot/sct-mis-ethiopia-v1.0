﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    SCT-MIS Dashboard
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <ul id="myDashboard">

    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
		<!-- load the dashboard css -->
		<link href="../Scripts/sDashboards/sDashboard.css" rel="stylesheet">

		<!-- load gitter css -->
		<link href="../Scripts/sDashboards/css/gitter/css/jquery.gritter.css" rel="stylesheet"/>
		<link href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css" rel="stylesheet">
		<!-- load jquery library -->
		<script src="../Scripts/sDashboards/libs/jquery/jquery-1.8.2.js" type="text/javascript"> </script>
		<!-- load jquery ui library -->
		<script src="../Scripts/sDashboards/libs/jquery/jquery-ui.js" type="text/javascript"> </script>

		
		<!-- load touch punch library to enable dragging on touch based devices -->
		<script src="../Scripts/sDashboards/libs/touchpunch/jquery.ui.touch-punch.js" type="text/javascript"> </script>
		<!-- load gitter notification library -->
		<script src="../Scripts/sDashboards/libs/gitter/jquery.gritter.js" type="text/javascript"> </script>

		<!-- load datatables library -->
		<script src="../Scripts/sDashboards/libs/datatables/jquery.dataTables.js"> </script>

		<!-- load flotr2 charting library -->
		<!--[if IE]>
		<script language="javascript" type="text/javascript" src="../Scripts/sDashboards/libs/flotr2/flotr2.ie.min.js"></script>
		<![endif]-->
		<script src="../Scripts/sDashboards/libs/flotr2/flotr2.js" type="text/javascript"> </script>
 
		<!-- load dashboard library -->
		<script src="../Scripts/sDashboards/jquery-sDashboard.js" type="text/javascript"> </script>

		<!-- sample data external script file -->
		<script src="../Scripts/sDashboards/libs/exampleData.js" type="text/javascript"> </script>

		<script type="text/javascript">
		    $(function () {
		        //**********************************************//
		        //dashboard json data
		        //this is the data format that the dashboard framework expects
		        //**********************************************//

		        var dashboardJSON = [{
		            widgetTitle: "Analysis of household profile by age",
		            widgetId: "id001",
		            widgetType: "chart",
		            widgetContent: {
		                data: myExampleData.barChartData,
		                options: myExampleData.barChartOptions
		            }

		        },{
		            widgetTitle: "Analysis of household profile by vulnerability",
		            widgetId: "id002",
		            widgetType: "chart",
		            widgetContent: {
		                data: myExampleData.bubbleChartData,
		                options: myExampleData.bubbleChartOptions
		            }

		        }, {
		            widgetTitle: "Analysis of clients compliance status by reporting period",
		            widgetId: "id003",
		            widgetType: "chart",
		            getDataBySelection: true,
		            widgetContent: {
		                data: myExampleData.lineChartData,
		                options: myExampleData.lineChartOptions
		            }

		        },{
		            widgetTitle: "Number Of cases of non complying clients solved",
		            widgetId: "id004",
		        widgetType: "chart",
		        widgetContent: {
		            data: myExampleData.barChartData4,
		            options: myExampleData.barChartOptions4
		        }

		    }];

		    //basic initialization example
		    $("#myDashboard").sDashboard({
		        dashboardData: dashboardJSON
		    });

		    });

		    var previousPoint = null;
		    $("#myDashboard").bind("plothover", function (event, pos, item) {
		        if (item) {
		            if (previousPoint != item.dataIndex) {
		                previousPoint = item.dataIndex;

		                $("#tooltip").remove();
		                var x = item.datapoint[0],
                            y = item.datapoint[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		                showTooltip(item.pageX, item.pageY, item.series.label + ": " + y);

		            }
		        }
		        else {
		            $("#tooltip").remove();
		            previousPoint = null;
		        }
		    });


		    function showTooltip(x, y, contents) {
		        $('<div id="tooltip">' + contents + '</div>').css({
		            position: 'absolute',
		            display: 'none',
		            top: y + 5,
		            left: x + 5,
		            border: '1px solid #fdd',
		            padding: '2px',
		            'background-color': '#fee',
		            opacity: 0.80
		        }).appendTo("body").fadeIn(200);
		    }

		</script>
</asp:Content>
