ALTER PROC [dbo].[getForm4BAdminList]
(
	@jtStartIndex	Int = 0,
	@jtPageSize		Int	= 10
)
AS
BEGIN
	-- 05-June-2018, get only for current FY by default
	declare @CurrentFiscalYear int
	select @CurrentFiscalYear = dbo.f_getFiscalYearFromDate (getdate())

	SELECT COUNT(1) RecCount 
	FROM ComplianceSTDSPLWHeader
	INNER JOIN AdminStructureView ON ComplianceSTDSPLWHeader.KebeleID = AdminStructureView.KebeleID
	WHERE ( FiscalYear = @CurrentFiscalYear )

	SELECT 
		ROW_NUMBER() OVER(ORDER BY ComplianceID) AS ColumnID,
		CONVERT(VARCHAR(100), ComplianceID) AS ComplianceID,
		RegionName,
		WoredaName, 
		KebeleName,
		dbo.f_GetServiceName(ServiceID) ServiceName,
		dbo.f_getComplianceBCount(ComplianceID) HouseholdCount,
		ReportFileName, 
		dbo.f_GetUserNameByUserID(CapturedBy) GeneratedBy,
		CONVERT(VARCHAR(11), CapturedOn, 106) GeneratedOn,
		CapturedOn CreatedOn
	FROM ComplianceSTDSPLWHeader
	INNER JOIN AdminStructureView ON ComplianceSTDSPLWHeader.KebeleID = AdminStructureView.KebeleID
	WHERE ( FiscalYear = @CurrentFiscalYear )
	ORDER BY CreatedOn DESC
	OFFSET @jtStartIndex ROWS
	FETCH NEXT @jtPageSize ROWS ONLY
END


