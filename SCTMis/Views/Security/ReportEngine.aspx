﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ReportEngineModel>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Reporting Engine
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("KiparaNgoto", "Security", FormMethod.Post, new { id = "formReportEngine" }))
   { %>
    <%--The First Row--%>
    <div class="col-2">
        <label>
            Region
            <%: Html.ValidationMessageFor(model => model.RegionID) %>
            <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1" })%>
        </label>
    </div>
    <div class="col-2">
        <label>
            Woreda
            <%: Html.ValidationMessageFor(model => model.WoredaID) %>
            <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
        </label>
    </div>
    <div class="col-2">
        <label>
            Report Name
            <%: Html.ValidationMessageFor(model => model.ReportName) %>
            <%: Html.DropDownListFor(model => model.ReportName,
                    new SelectList(Model.rptTypes, "ReportID", "ReportName"), new { tabindex = "3" })%>
        </label>
    </div>
    <div class="col-2">
        <label>
            Reporting Period
            <%: Html.ValidationMessageFor(model => model.ReportingPeriod) %>
            <%: Html.DropDownListFor(model => model.ReportingPeriod,
                    new SelectList(Model.reportgperiod, "PeriodID", "PeriodName"), new { tabindex = "4" })%>
        </label>
    </div>
    <br />
    <div class="form-actions">
        <%: Html.HiddenFor(model => model.LocationType) %>
        <button id="cmdProduce" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i><span>Produce</span></button>
    </div>
    <br />
    <%: Html.ValidationSummary(true) %>
    <% } %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
    $(document).ready(function() {
        $("#RegionID,#WoredaID,#ReportName,#ReportingPeriod").prop("disabled", false);
        $("#RegionID,#WoredaID,#ReportName,#ReportingPeriod").val('');
        $("#cmdProduce").prop("disabled", false);
        $('#loading').hide();
        $('#RegionID').focus();

        var locationType = $('#LocationType').val();

        switch (locationType) 
        {
            case '1':
                $('#formReportEngine').attr('action', '/security/printpdf');
                $('#RegionID').attr('required', 'required');
                $('#WoredaID').attr('required', 'required');

                $('#cmdProduce span').text('Download Report');

                break;

            case '2':
                $('#formReportEngine').attr('action', '/security/printform6');
                $('#formReportEngine').attr('target', '/security/_blank');

                $('#WoredaID').prop('disabled', true);

                $('#RegionID').attr('required', 'required');
                $('#cmdProduce span').text('View Report');
                break;

            case '3':
                $('#formReportEngine').attr('action', '/security/printform6');
                $('#formReportEngine').attr('target', '/security/_blank');

                $('#RegionID').prop('disabled', true);
                $('#WoredaID').prop('disabled', true);

                $('#cmdProduce span').text('View Report');

                break;
        }

        $('#ReportName').attr('required', 'required');

        $('#formReportEngine').submit(function(ev) {
            ev.preventDefault()
            $('#loading').hide();
            
            var locationType = $('#LocationType').val();

            var isValid = true;
            if (locationType === '1') {
                isValid = false;

                var woredaID = $('#WoredaID').val();
                if ((woredaID === null) | (woredaID === undefined) | (woredaID === '0')) {
                    isValid = false;
                    $('#WoredaID').focus();
                } else {
                    isValid = true;
                };
            } else if (locationType === '2') {
                isValid = false;

                var regionId = $('#RegionID').val();
                if ((regionId === null) | (regionId === undefined) | (regionId === '0')) {
                    isValid = false;
                    $('#RegionID').focus();
                } else {
                    isValid = true;
                };
            }

            if (isValid === true) {
                this.submit();
            }
        });
    });
    </script>
</asp:Content>