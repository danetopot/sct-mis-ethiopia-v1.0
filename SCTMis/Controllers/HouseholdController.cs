﻿using SCTMis.Models;
using SCTMis.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Xml.Linq;

namespace SCTMis.Controllers
{
    public class PageParams
    {
        public int CurrentPage { get; set; }
    }

    [SessionConfig.SessionExpireFilter]
    public class HouseholdController : Controller
    {
        GeneralServices dbService = new GeneralServices();
        HouseholdServices dbHousehold = new HouseholdServices();
        AccountServices dbAcService = new AccountServices();
        string ip = System.Web.HttpContext.Current.Request.UserHostAddress;

        static int _currentPage = 0;

        [HttpPost]
        public void UpdateCurrentPage(PageParams pageParam)
        {
            _currentPage = pageParam.CurrentPage;
        }

        //
        // GET: /Household/
        [Authorize]
        [OutputCache(Duration = 10, VaryByParam = "none")]
        public ActionResult Form1A()
        {
            ViewBag.Subtitle = "Form 1A Listing";
            ViewBag.CurrentPage = _currentPage;

            dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Form1A List View", ip, "",
            "Form1A List Access  at " + DateTime.Now.ToString());
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form1AHHList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = GeneralServices.strQueryString("SearchKeyword", string.Empty);                             

                Tuple<List<int>, List<Form1AGrid>> resultset
                    = GeneralServices.NpocoConnection().FetchMultiple<int, Form1AGrid>(";Exec GetForm1AHouseholdHeadList @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword", 
                    new {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });              

                DataTable dt = GeneralServices.ToDataTable<Form1AGrid>(resultset.Item2);
                            
                ViewBag.CurrentPage = _currentPage;

                var canApproveForm = dbAcService.GetTaskAccessRights(6);

                string sb;
                if (canApproveForm)
                {
                    sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 3);
                }
                else
                {
                    sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 7);
                }
                
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        //CaptureForm1A
        [Authorize]
        public ActionResult CaptureForm1A()
        {
            ViewBag.Subtitle = "Capture Form 1A";

            CaptureForm1A householdModel = new Models.CaptureForm1A();

            householdModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            householdModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            householdModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
            householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
            householdModel.genders = dbService.GetGenders();
            householdModel.yesnos = dbService.GetYesNo();
            householdModel.nutrStatus = dbService.GetNutritionalStatus();
            householdModel.worker = dbService.GetSocialWorkers();
            householdModel.grades = dbService.GetSchoolGrade();
            
            return View(householdModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult CaptureForm1A(CaptureForm1A newmodel)
        {
            ViewBag.Subtitle = "Capture Form 1A";

            try
            {
                //check PSNP Number uniqueness
                var psnpexists = GeneralServices.PnspNumberExists(newmodel.HouseHoldIDNumber, AppConstants.Form1A, null, false);

                //check CBHI Number uniqueness
                var cbhiexists = GeneralServices.CBHINumberExists(newmodel.CBHINumber, AppConstants.Form1A, null, false);

                if (psnpexists)
                {
                    ModelState.AddModelError("", string.Format("There exists a Household with the PSNP Number {0}", newmodel.HouseHoldIDNumber));

                    newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                    newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                    newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                    newmodel.genders = dbService.GetGenders();
                    newmodel.yesnos = dbService.GetYesNo();
                    newmodel.nutrStatus = dbService.GetNutritionalStatus();
                    newmodel.worker = dbService.GetSocialWorkers();
                    newmodel.grades = dbService.GetSchoolGrade();

                    return View("CaptureForm1A", newmodel);
                }

                if (cbhiexists)
                {
                    ModelState.AddModelError("", string.Format("There exists a Household with the CBHI Number {0}", newmodel.CBHINumber));

                    newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                    newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                    newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                    newmodel.genders = dbService.GetGenders();
                    newmodel.yesnos = dbService.GetYesNo();
                    newmodel.nutrStatus = dbService.GetNutritionalStatus();
                    newmodel.worker = dbService.GetSocialWorkers();
                    newmodel.grades = dbService.GetSchoolGrade();

                    return View("CaptureForm1A", newmodel);
                }

                if (ModelState.IsValid)
                {
                    var result = dbHousehold.InsertForm1A(newmodel);
                    ViewBag.Subtitle = "Form 1A Listing";
                    dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Capture Form1A", ip, "",
                    "Form1A Details " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Captured Successfuly  at " + DateTime.Now.ToString());

                    return RedirectToAction("Form1A");

                }

                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                ModelState.AddModelError("", message);

                newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                newmodel.genders = dbService.GetGenders();
                newmodel.yesnos = dbService.GetYesNo();
                newmodel.nutrStatus = dbService.GetNutritionalStatus();
                newmodel.worker = dbService.GetSocialWorkers();
                newmodel.grades = dbService.GetSchoolGrade();

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Capture Form1A Error", ip, "",
                    "Form1A Details with Message " + message + " for " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Encountered an error at " + DateTime.Now.ToString());

                return View("CaptureForm1A", newmodel);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                newmodel.genders = dbService.GetGenders();
                newmodel.yesnos = dbService.GetYesNo();
                newmodel.nutrStatus = dbService.GetNutritionalStatus();
                newmodel.worker = dbService.GetSocialWorkers();
                newmodel.grades = dbService.GetSchoolGrade();

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Capture Form1A Error", ip, "",
                "Form1A Details with Message " + ex.Message + " for " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Encountered an error at " + DateTime.Now.ToString());

                return View("CaptureForm1A", newmodel);
            }
        }

        //Form1ASubList
        [HttpPost]
        [Authorize]
        [ValidateInput(false)]
        public JsonResult Form1ASubList(CaptureForm1A model, int jtStartIndex = 0, int jtPageSize = 0)
        {
            try
            {
                var form1AList = dbService.GetForm1AMemberListing(model.MemberXml);// con.Fetch<Form1A>(";Exec getForm1AAdminList @jtStartIndex, @jtPageSize", new { jtStartIndex, jtPageSize });

                var recordcount = 0;

                return Json(new { Result = "OK", Records = form1AList, TotalRecordCount = recordcount });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        public ActionResult ModifyForm1A(CaptureForm1A householdModel)
        {
            ViewBag.Subtitle = "Modify Form 1A";

            householdModel = dbHousehold.FetchForm1ADetailsByID(householdModel.ProfileDSHeaderID);
            householdModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            householdModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            householdModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
            householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
            householdModel.genders = dbService.GetGenders();
            householdModel.yesnos = dbService.GetYesNo();
            householdModel.nutrStatus = dbService.GetNutritionalStatus();
            householdModel.worker = dbService.GetSocialWorkers();
            householdModel.grades = dbService.GetSchoolGrade();

            // householdModel.AllowEdit = true;
            return View(householdModel);
        }

        //CaptureForm1A
        [Authorize]
        public ActionResult ApproveForm1A(CaptureForm1A householdModel)
        {
            ViewBag.Subtitle = "Modify/Approve Form 1A";

            householdModel = dbHousehold.FetchForm1ADetailsByID(householdModel.ProfileDSHeaderID);
            householdModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            householdModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            householdModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
            householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
            householdModel.genders = dbService.GetGenders();
            householdModel.yesnos = dbService.GetYesNo();
            householdModel.nutrStatus = dbService.GetNutritionalStatus();
            householdModel.worker = dbService.GetSocialWorkers();
            householdModel.grades = dbService.GetSchoolGrade();

            return View(householdModel);
        }

        [Authorize]
        [HttpPost]
        public JsonResult UpdateApprovedForm1A(CaptureForm1A householdModel)
        {
            ViewBag.Subtitle = "Modify/Approve Form 1A";
            string errMessage = string.Empty;
            string filename = string.Empty;
            try
            {
                //PdfForm1AReport
                HouseholdServices monit = new HouseholdServices();
                if (householdModel.CreatedBy.ToUpper() == Membership.GetUser().UserName.ToString().ToUpper())
                {
                    errMessage = "You cannot approve a record you created!";
                    throw new ArgumentException (errMessage);
                }
                
                var RptGenerated = monit.PdfForm1AReport(householdModel.ProfileDSHeaderID,
                    Membership.GetUser().UserName.ToString(), out errMessage, out filename);
                if (RptGenerated == true)
                {
                    List<RecordCount> isSuccess
                        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec ApproveForm1ADetails @ProfileDSHeaderID,@ReportFilePath,@CreatedBy",
                            new
                            {
                                ProfileDSHeaderID = householdModel.ProfileDSHeaderID,
                                ReportFilePath = filename,
                                CreatedBy = Membership.GetUser().UserName.ToString()
                            });

                    ViewBag.Subtitle = "Form 1A Listing";
                    dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Approve Form1A", ip, "",
                    "Form1A Approval " + householdModel.HouseHoldIDNumber + " Successful at " + DateTime.Now.ToString());
                    return Json(isSuccess, JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("Form1A");
                }
                else
                {
                    ViewBag.Subtitle = "Form 1A Listing";
                    householdModel = dbHousehold.FetchForm1ADetailsByID(householdModel.ProfileDSHeaderID);

                    householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
                    householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
                    householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
                    householdModel.genders = dbService.GetGenders();
                    householdModel.yesnos = dbService.GetYesNo();
                    householdModel.nutrStatus = dbService.GetNutritionalStatus();
                    householdModel.worker = dbService.GetSocialWorkers();
                    householdModel.grades = dbService.GetSchoolGrade();

                    ModelState.AddModelError("", errMessage);

                    dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Approve Form1A Error", ip, "",
                    "Form1A Details with Message " + errMessage + " for " + householdModel.HouseHoldIDNumber + " Encountered an error at " + DateTime.Now.ToString());
                    return Json(errMessage, JsonRequestBehavior.AllowGet);
                    //return View("ApproveForm1A", householdModel);
                }

            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                ViewBag.Subtitle = "Form 1A Listing";
                householdModel = dbHousehold.FetchForm1ADetailsByID(householdModel.ProfileDSHeaderID);

                householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
                householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
                householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
                householdModel.genders = dbService.GetGenders();
                householdModel.yesnos = dbService.GetYesNo();
                householdModel.nutrStatus = dbService.GetNutritionalStatus();
                householdModel.worker = dbService.GetSocialWorkers();
                householdModel.grades = dbService.GetSchoolGrade();

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Approve Form1A Error", ip, "",
                "Form1A Details with Message " + ex.Message + " for " + householdModel.HouseHoldIDNumber + " Encountered an error at " + DateTime.Now.ToString());
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
                //return View("ApproveForm1A", householdModel);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult UpdateForm1A(CaptureForm1A newmodel)
        {
            ViewBag.Subtitle = "Update Form 1A";

            try
            {
                //check PSNP Number uniqueness
                var exists = GeneralServices.PnspNumberExists(newmodel.HouseHoldIDNumber, AppConstants.Form1A, newmodel.ProfileDSHeaderID, true);

                //check CBHI Number uniqueness
                var cbhiexists = GeneralServices.CBHINumberExists(newmodel.CBHINumber, AppConstants.Form1A, newmodel.ProfileDSHeaderID, true);

                if (exists)
                {
                    newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                    newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                    newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                    newmodel.genders = dbService.GetGenders();
                    newmodel.yesnos = dbService.GetYesNo();
                    newmodel.nutrStatus = dbService.GetNutritionalStatus();
                    newmodel.worker = dbService.GetSocialWorkers();
                    newmodel.grades = dbService.GetSchoolGrade();

                    ModelState.AddModelError("", string.Format("There exists a Household with the PSNP Number {0}", newmodel.HouseHoldIDNumber));

                    return View("ModifyForm1A", newmodel);
                }

                if (cbhiexists)
                {
                    ModelState.AddModelError("", string.Format("There exists a Household with the CBHI Number {0}", newmodel.CBHINumber));

                    newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                    newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                    newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                    newmodel.genders = dbService.GetGenders();
                    newmodel.yesnos = dbService.GetYesNo();
                    newmodel.nutrStatus = dbService.GetNutritionalStatus();
                    newmodel.worker = dbService.GetSocialWorkers();
                    newmodel.grades = dbService.GetSchoolGrade();

                    return View("CaptureForm1A", newmodel);
                }

                if (ModelState.IsValid)
                {
                    var result = dbHousehold.UpdateForm1A(newmodel);
                    ViewBag.Subtitle = "Form 1A Listing";
                    dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Update Form1A", ip, "",
                    "Form1A Details " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Update Successfuly  at " + DateTime.Now.ToString());

                    return RedirectToAction("Form1A");
                }

                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                ModelState.AddModelError("", message);

                newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                newmodel.genders = dbService.GetGenders();
                newmodel.yesnos = dbService.GetYesNo();
                newmodel.nutrStatus = dbService.GetNutritionalStatus();
                newmodel.worker = dbService.GetSocialWorkers();
                newmodel.grades = dbService.GetSchoolGrade();

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Update Form1A Error", ip, "",
                    "Form1A Details with Message " + message + " for " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Encountered an error at " + DateTime.Now.ToString());

                return View("ApproveForm1A", newmodel);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                newmodel.genders = dbService.GetGenders();
                newmodel.yesnos = dbService.GetYesNo();
                newmodel.nutrStatus = dbService.GetNutritionalStatus();
                newmodel.worker = dbService.GetSocialWorkers();
                newmodel.grades = dbService.GetSchoolGrade();

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Update Form1A Error", ip, "",
                "Form1A Details with Message " + ex.Message + " for " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Encountered an error at " + DateTime.Now.ToString());

                return View("ApproveForm1A", newmodel);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult ModifiedForm1A(CaptureForm1A newmodel)
        {
            ViewBag.Subtitle = "Update Form 1A";

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbHousehold.UpdateForm1A(newmodel);
                    ViewBag.Subtitle = "Form 1A Listing";
                    dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Update Form1A", ip, "",
                    "Form1A Details " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Update Successfuly  at " + DateTime.Now.ToString());
                    //Household/ApproveForm1A
                    return RedirectToAction("ApproveForm1A", "Household", new { ProfileDSHeaderID = newmodel.ProfileDSHeaderID });
                }

                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                ModelState.AddModelError("", message);

                newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                newmodel.genders = dbService.GetGenders();
                newmodel.yesnos = dbService.GetYesNo();
                newmodel.nutrStatus = dbService.GetNutritionalStatus();
                newmodel.worker = dbService.GetSocialWorkers();
                newmodel.grades = dbService.GetSchoolGrade();

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Update Form1A Error", ip, "",
                    "Form1A Details with Message " + message + " for " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Encountered an error at " + DateTime.Now.ToString());

                return View("ApproveForm1A", newmodel);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                newmodel.genders = dbService.GetGenders();
                newmodel.yesnos = dbService.GetYesNo();
                newmodel.nutrStatus = dbService.GetNutritionalStatus();
                newmodel.worker = dbService.GetSocialWorkers();
                newmodel.grades = dbService.GetSchoolGrade();

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Update Form1A Error", ip, "",
                "Form1A Details with Message " + ex.Message + " for " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Encountered an error at " + DateTime.Now.ToString());

                return View("ApproveForm1A", newmodel);
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult FetchJsonForm1ADetailsByID(CaptureForm1A oldModel)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");
            
            try
            {
                var jtProfileDSHeaderID = oldModel.ProfileDSHeaderID;

                Tuple<List<int>, List<Form1AMembersGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form1AMembersGrid>(";Exec FetchGridForm1ADetailsByID @ProfileDSHeaderID", new { ProfileDSHeaderID = jtProfileDSHeaderID });

                //DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form1AMembersGrid>(resultset.Item2);

                //string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], 0, 0, 1);
                //var xml = XDocument.Parse(sb);

                return Json(resultset.Item2, JsonRequestBehavior.AllowGet);

                //return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item2.Count });
                //return Json(WorDetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                //return XDocument.Parse("xml");
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchGridForm1ADetailsByID()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                var jtProfileDSHeaderID = Request.QueryString["ProfileDSHeaderID"].ToString();

                Tuple<List<int>, List<Form1AMembersGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form1AMembersGrid>(";Exec FetchGridForm1ADetailsByID @ProfileDSHeaderID", new { ProfileDSHeaderID = jtProfileDSHeaderID });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form1AMembersGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], 0, 0, 5);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                return XDocument.Parse("<xml>error</xml>");
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult FetchJsonForm1BDetailsByID(CaptureForm1B oldModel)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                var headerId = oldModel.ProfileTDSPLWID;

                Tuple<List<int>, List<Form1BMembersGrid>> resultset = GeneralServices.NpocoConnection().FetchMultiple<int, Form1BMembersGrid>(";Exec FetchGridForm1BDetailsByID @ProfileTDSPLWID", new { ProfileTDSPLWID = headerId });
                
                return Json(resultset.Item2, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchGridForm1BDetailsByID()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                var headerId = Request.QueryString["ProfileTDSPLWID"].ToString();

                Tuple<List<int>, List<Form1BMembersGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form1BMembersGrid>(";Exec FetchGridForm1BDetailsByID @ProfileTDSPLWID", new { ProfileTDSPLWID = headerId });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form1BMembersGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], 0, 0, 5);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                return XDocument.Parse("<xml>error</xml>");
            }
        }
        
        //FORM 1B
        [Authorize]
        public ActionResult Form1B()
        {
            ViewBag.Subtitle = "Form 1B Listing";
            ViewBag.CurrentPage = _currentPage;

            dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Form1B List View", ip, "",
            "Form1B List Access  at " + DateTime.Now.ToString());

            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form1BPLWList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = GeneralServices.strQueryString("SearchKeyword", string.Empty);

                Tuple <List<int>, List<Form1BGrid>> resultset
                    = GeneralServices.NpocoConnection().FetchMultiple<int, Form1BGrid>(";Exec GetForm1BPLWList @jtStartIndex, @jtPageSize,@isSearch,@SearchTypeID,@SearchKeyword", 
                    new {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });

                DataTable dt = GeneralServices.ToDataTable<Form1BGrid>(resultset.Item2);

                ViewBag.CurrentPage = _currentPage;

                var canApproveForm = dbAcService.GetTaskAccessRights(9);

                string sb;
                if (canApproveForm)
                {
                    sb = GeneralServices.GetDhtmlxXML(dt, int.Parse(resultset.Item1[0].ToString()), jtStartIndex, 0, 3);
                }
                else
                {
                    sb = GeneralServices.GetDhtmlxXML(dt, int.Parse(resultset.Item1[0].ToString()), jtStartIndex, 0, 7);
                }
                
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                return XDocument.Parse("xml");
            }
        }
        //CaptureForm1B
        [Authorize]
        public ActionResult CaptureForm1B()
        {
            ViewBag.Subtitle = "Capture Form 1B";

            CaptureForm1B householdModel = new Models.CaptureForm1B();

            householdModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            householdModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            householdModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
            householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
            householdModel.genders = dbService.GetGenders();
            householdModel.nutrStatus = dbService.GetNutritionalStatus();
            householdModel.worker = dbService.GetSocialWorkers();
            householdModel.plws = dbService.GetPLW();
            householdModel.yesnos = dbService.GetYesNo();


            return View(householdModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult CaptureForm1B(CaptureForm1B newmodel)
        {
            ViewBag.Subtitle = "Capture Form 1B";

            try
            {
                //check PSNP Number uniqueness
                var psnpexists = GeneralServices.PnspNumberExists(newmodel.HouseHoldIDNumber, AppConstants.Form1A, null, false);

                //check CBHI Number uniqueness
                var cbhiexists = GeneralServices.CBHINumberExists(newmodel.CBHINumber, AppConstants.Form1A, null, false);

                if (psnpexists)
                {
                    ModelState.AddModelError("", string.Format("There exists a Household with the PSNP Number {0}", newmodel.HouseHoldIDNumber));


                    newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                    newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                    newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                    newmodel.RegionID = int.Parse(Session["RegionID"].ToString());
                    newmodel.WoredaID = int.Parse(Session["WoredaID"].ToString());
                    newmodel.genders = dbService.GetGenders();
                    newmodel.nutrStatus = dbService.GetNutritionalStatus();
                    newmodel.worker = dbService.GetSocialWorkers();
                    newmodel.plws = dbService.GetPLW();
                    newmodel.yesnos = dbService.GetYesNo();
                    /*
                    newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                    newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                    newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                    newmodel.genders = dbService.GetGenders();
                    newmodel.nutrStatus = dbService.GetNutritionalStatus();
                    newmodel.worker = dbService.GetSocialWorkers();
                    newmodel.plws = dbService.GetPLW();
                    newmodel.yesnos = dbService.GetYesNo();
                    */

                    return View("CaptureForm1B", newmodel);
                }

                if (cbhiexists)
                {
                    ModelState.AddModelError("", string.Format("There exists a Household with the CBHI Number {0}", newmodel.CBHINumber));

                    newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                    newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                    newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                    newmodel.RegionID = int.Parse(Session["RegionID"].ToString());
                    newmodel.WoredaID = int.Parse(Session["WoredaID"].ToString());
                    newmodel.genders = dbService.GetGenders();
                    newmodel.nutrStatus = dbService.GetNutritionalStatus();
                    newmodel.worker = dbService.GetSocialWorkers();
                    newmodel.plws = dbService.GetPLW();
                    newmodel.yesnos = dbService.GetYesNo();
                    /*
                    newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                    newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                    newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                    newmodel.genders = dbService.GetGenders();
                    newmodel.nutrStatus = dbService.GetNutritionalStatus();
                    newmodel.worker = dbService.GetSocialWorkers();
                    newmodel.plws = dbService.GetPLW();
                    newmodel.yesnos = dbService.GetYesNo();
                    */

                    return View("CaptureForm1B", newmodel);
                }

                if (newmodel.PLW == "L" && string.IsNullOrEmpty(newmodel.MemberXml))
                {
                    newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                    newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                    newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                    newmodel.genders = dbService.GetGenders();
                    newmodel.nutrStatus = dbService.GetNutritionalStatus();
                    newmodel.worker = dbService.GetSocialWorkers();
                    newmodel.plws = dbService.GetPLW();
                    newmodel.yesnos = dbService.GetYesNo();

                    ModelState.AddModelError("", "Atleast one Member is required!");

                    return View("CaptureForm1B", newmodel);
                }

                if (ModelState.IsValid)
                {
                    var result = dbHousehold.InsertForm1B(newmodel);
                    ViewBag.Subtitle = "Form 1B Listing";
                    dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Capture Form1B", ip, "",
                    "Form1B Details " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Captured Successfuly  at " + DateTime.Now.ToString());

                    return RedirectToAction("Form1B");
                }
                
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                ModelState.AddModelError("", message);

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.RegionID = int.Parse(Session["RegionID"].ToString());
                newmodel.WoredaID = int.Parse(Session["WoredaID"].ToString());
                newmodel.genders = dbService.GetGenders();
                newmodel.nutrStatus = dbService.GetNutritionalStatus();
                newmodel.worker = dbService.GetSocialWorkers();
                newmodel.plws = dbService.GetPLW();
                newmodel.yesnos = dbService.GetYesNo();

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Capture Form1B Error", ip, "",
                    "Form1B Details with Message " + message +" for " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Encountered an error at " + DateTime.Now.ToString());

                return View("CaptureForm1B", newmodel);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                newmodel.genders = dbService.GetGenders();
                newmodel.nutrStatus = dbService.GetNutritionalStatus();
                newmodel.worker = dbService.GetSocialWorkers();
                newmodel.plws = dbService.GetPLW();

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Capture Form1B Error", ip, "",
                "Form1B Details with Message " + ex.Message + " for " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Encountered an error at " + DateTime.Now.ToString());

                return View("CaptureForm1B", newmodel);
            }
        }
        //Approve Form 1B
        [Authorize]
        public ActionResult ModifyForm1B(ModifyForm1B householdModel)
        {
            ViewBag.Subtitle = "Modify Form 1B";

            householdModel = dbHousehold.FetchForm1BDetailsByID(householdModel.ProfileTDSPLWID);
            /* Commented on 07JUNE2019 
            householdModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            householdModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            householdModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            */

            householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
            householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
            householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);

            /*
            householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
            householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
            */
            householdModel.RegionID = householdModel.RegionID;
            householdModel.WoredaID = householdModel.WoredaID;
            householdModel.genders = dbService.GetGenders();
            householdModel.nutrStatus = dbService.GetNutritionalStatus();
            householdModel.worker = dbService.GetSocialWorkers();
            householdModel.plws = dbService.GetPLW();
            householdModel.yesnos = dbService.GetYesNo();
            return View(householdModel);
        }

        [Authorize]
        public ActionResult ApproveForm1B(ModifyForm1B householdModel)
        {
            ViewBag.Subtitle = "Approve Form 1B";

            /* Commented On 07JUNE2019
            householdModel = dbHousehold.FetchForm1BDetailsByID(householdModel.ProfileTDSPLWID);
            householdModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            householdModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            householdModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
            householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
            householdModel.genders = dbService.GetGenders();
            householdModel.nutrStatus = dbService.GetNutritionalStatus();
            householdModel.worker = dbService.GetSocialWorkers();
            householdModel.plws = dbService.GetPLW();
            */

            householdModel = dbHousehold.FetchForm1BDetailsByID(householdModel.ProfileTDSPLWID);
            householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
            householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
            householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
            householdModel.RegionID = householdModel.RegionID;
            householdModel.WoredaID = householdModel.WoredaID;
            householdModel.genders = dbService.GetGenders();
            householdModel.nutrStatus = dbService.GetNutritionalStatus();
            householdModel.worker = dbService.GetSocialWorkers();
            householdModel.plws = dbService.GetPLW();
            householdModel.yesnos = dbService.GetYesNo();

            return View(householdModel);
        }

        [Authorize]
        [HttpPost]
        public JsonResult UpdateApprovedForm1B(ModifyForm1B householdModel)
        {
            ViewBag.Subtitle = "Approve Form 1B";
            string errMessage = string.Empty;
            string filename = string.Empty;
            try
            {
                if (householdModel.CreatedBy.ToUpper() == Membership.GetUser().UserName.ToString().ToUpper())
                {
                    errMessage = "You cannot approve a record you created!";
                    throw new ArgumentException(errMessage);
                }
                
                HouseholdServices monit = new HouseholdServices();
                var RptGenerated = monit.PdfForm1BReport(householdModel.ProfileTDSPLWID,
                    Membership.GetUser().UserName.ToString(), out errMessage, out filename);
                if (RptGenerated == true)
                {
                    List<RecordCount> isSuccess
                        = GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec ApproveForm1BDetails @ProfileTDSPLWID,@ReportFilePath,@CreatedBy",
                            new
                            {
                                ProfileTDSPLWID = householdModel.ProfileTDSPLWID,
                                ReportFilePath = filename,
                                CreatedBy = Membership.GetUser().UserName.ToString()
                            });
                    ViewBag.Subtitle = "Form 1B Listing";
                    dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Approve Form1B", ip, "",
                    "Form1B Approval " + householdModel.HouseHoldIDNumber + " Successful at " + DateTime.Now.ToString());
                    //return RedirectToAction("Form1B");
                    return Json(isSuccess, JsonRequestBehavior.AllowGet);
                }

                householdModel = dbHousehold.FetchForm1BDetailsByID(householdModel.ProfileTDSPLWID);

                householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
                householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
                householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
                householdModel.genders = dbService.GetGenders();
                householdModel.nutrStatus = dbService.GetNutritionalStatus();
                householdModel.worker = dbService.GetSocialWorkers();
                householdModel.plws = dbService.GetPLW();

                ModelState.AddModelError("", errMessage);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Approve Form1B Error", ip, "",
                    "Form1B Details with Message " + errMessage + " for " + householdModel.HouseHoldIDNumber + " Encountered an error at " + DateTime.Now.ToString());

                //return View("ApproveForm1B", householdModel);
                return Json(errMessage, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                householdModel = dbHousehold.FetchForm1BDetailsByID(householdModel.ProfileTDSPLWID);

                householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
                householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
                householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
                householdModel.genders = dbService.GetGenders();
                householdModel.nutrStatus = dbService.GetNutritionalStatus();
                householdModel.worker = dbService.GetSocialWorkers();
                householdModel.plws = dbService.GetPLW();

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Approve Form1B Error", ip, "",
                "Form1B Details with Message " + ex.Message + " for " + householdModel.HouseHoldIDNumber + " Encountered an error at " + DateTime.Now.ToString());

                //return View("ApproveForm1B", householdModel);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult UpdateForm1B(ModifyForm1B householdModel)
        {
            ViewBag.Subtitle = "Modify Form 1B";
            try
            {
                //check PSNP Number uniqueness
                var psnpexists = GeneralServices.PnspNumberExists(householdModel.HouseHoldIDNumber, AppConstants.Form1B, householdModel.ProfileTDSPLWID, true);

                //check CBHI Number uniqueness
                var cbhiexists = GeneralServices.CBHINumberExists(householdModel.CBHINumber, AppConstants.Form1B, householdModel.ProfileTDSPLWID, true);

                if (psnpexists)
                {
                    householdModel = dbHousehold.FetchForm1BDetailsByID(householdModel.ProfileTDSPLWID);

                    householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
                    householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
                    householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
                    householdModel.genders = dbService.GetGenders();
                    householdModel.nutrStatus = dbService.GetNutritionalStatus();
                    householdModel.worker = dbService.GetSocialWorkers();
                    householdModel.plws = dbService.GetPLW();
                    householdModel.yesnos = dbService.GetYesNo();

                    ModelState.AddModelError("", string.Format("There exists a Household with the PSNP Number {0}", householdModel.HouseHoldIDNumber));

                    return View("ModifyForm1B", householdModel);
                }

                if (cbhiexists)
                {
                    householdModel = dbHousehold.FetchForm1BDetailsByID(householdModel.ProfileTDSPLWID);

                    householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
                    householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
                    householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
                    householdModel.genders = dbService.GetGenders();
                    householdModel.nutrStatus = dbService.GetNutritionalStatus();
                    householdModel.worker = dbService.GetSocialWorkers();
                    householdModel.plws = dbService.GetPLW();
                    householdModel.yesnos = dbService.GetYesNo();

                    ModelState.AddModelError("", string.Format("There exists a Household with the CBHI Number {0}", householdModel.CBHINumber));

                    return View("ModifyForm1B", householdModel);
                }

                string strXml = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();

                if (!String.IsNullOrEmpty(householdModel.MemberXml))
                {
                    CaptureForm1B[] memberList = objJavascript.Deserialize<CaptureForm1B[]>(householdModel.MemberXml);

                    foreach (var array in memberList)
                    {
                        strXml = strXml + "<row>";
                        strXml = strXml + "<BabyDateOfBirth>" + array.BabyDateOfBirth + "</BabyDateOfBirth>";
                        strXml = strXml + "<BabyName>" + array.BabyName + "</BabyName>";
                        strXml = strXml + "<BabySex>" + array.BabySex + "</BabySex>";
                        strXml = strXml + "<NutritionalStatusInfant>" + array.NutritionalStatusInfant + "</NutritionalStatusInfant>";
                        //strXml = strXml + "<ChildProtectionRisk>" + array.ChildProtectionRisk + "</ChildProtectionRisk>";
                        strXml = strXml + "</row>";
                        Console.WriteLine(strXml);
                    }

                    strXml = "<members>" + strXml + "</members>";
                }

                List<RecordCount> isSuccess
                    = GeneralServices.NpocoConnection().Fetch<RecordCount>
                    (";Exec ModifyForm1B @ProfileTDSPLWID,@KebeleID,@Gote,@Gare,@CollectionDate,@SocialWorker,@CCCCBSPCMember,@PLW,@NameOfPLW,@HouseHoldIDNumber,@MedicalRecordNumber,@PLWAge,@StartDateTDS,@EndDateTDS,@NutritionalStatusPLW,@Remarks,@ChildProtectionRisk,@CBHIMembership,@CBHINumber,@MemberXML",
                        new
                        {
                            ProfileTDSPLWID = householdModel.ProfileTDSPLWID,
                            KebeleID = householdModel.KebeleID,
                            Gote = householdModel.Gote,	
                            Gare = householdModel.Gare,
                            CollectionDate = householdModel.CollectionDate,
                            SocialWorker = householdModel.SocialWorker,
                            CCCCBSPCMember = householdModel.CCCCBSPCMember,
                            PLW = householdModel.PLW,
                            NameOfPLW = householdModel.NameOfPLW,
                            HouseHoldIDNumber = householdModel.HouseHoldIDNumber,
                            MedicalRecordNumber = householdModel.MedicalRecordNumber,
                            PLWAge = householdModel.PLWAge,
                            StartDateTDS = householdModel.StartDateTDS,
                            EndDateTDS = householdModel.EndDateTDS,
                            NutritionalStatusPLW = householdModel.NutritionalStatusPLW,
                            Remarks = householdModel.Remarks,
                            ChildProtectionRisk =householdModel.ChildProtectionRisk,
                            CBHIMembership = householdModel.CBHIMembership,
                            CBHINumber = householdModel.CBHINumber,
                            MemberXML = strXml
                        });

                ViewBag.Subtitle = "Form 1B Listing";
                //dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Modify Form1B", ip, "",
                //"Form1B Approval " + householdModel.HouseHoldIDNumber + " Successful at " + DateTime.Now.ToString());
                return RedirectToAction("Form1B");
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                householdModel = dbHousehold.FetchForm1BDetailsByID(householdModel.ProfileTDSPLWID);

                householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
                householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
                householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
                householdModel.genders = dbService.GetGenders();
                householdModel.nutrStatus = dbService.GetNutritionalStatus();
                householdModel.worker = dbService.GetSocialWorkers();
                householdModel.plws = dbService.GetPLW();
                householdModel.yesnos = dbService.GetYesNo();

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Modify Form1B Error", ip, "",
                "Form1B Details with Message " + ex.Message + " for " + householdModel.HouseHoldIDNumber + " Encountered an error at " + DateTime.Now.ToString());

                return View("ModifyForm1B", householdModel);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult UpdateModifiedForm1B(ModifyForm1B householdModel)
        {
            ViewBag.Subtitle = "Modify Form 1B";
            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>
                    (";Exec ModifyForm1B @ProfileTDSPLWID,@Gote,@Gare,@CollectionDate,@SocialWorker,@CCCCBSPCMember,@PLW,@NameOfPLW,@HouseHoldIDNumber,@MedicalRecordNumber,@PLWAge,@StartDateTDS,@BabyDateOfBirth,@BabyName,@BabySex,@EndDateTDS,@NutritionalStatusPLW,@NutritionalStatusInfant,@CreatedBy,@Remarks",
                        new
                        {
                            ProfileTDSPLWID = householdModel.ProfileTDSPLWID,
                            Gote = householdModel.Gote,
                            Gare = householdModel.Gare,
                            CollectionDate = householdModel.CollectionDate,
                            SocialWorker = householdModel.SocialWorker,
                            CCCCBSPCMember = householdModel.CCCCBSPCMember,
                            PLW = householdModel.PLW,
                            NameOfPLW = householdModel.NameOfPLW,
                            HouseHoldIDNumber = householdModel.HouseHoldIDNumber,
                            MedicalRecordNumber = householdModel.MedicalRecordNumber,
                            PLWAge = householdModel.PLWAge,
                            StartDateTDS = householdModel.StartDateTDS,
                            BabyDateOfBirth = householdModel.BabyDateOfBirth,
                            BabyName = householdModel.BabyName,
                            BabySex = householdModel.BabySex,
                            EndDateTDS = householdModel.EndDateTDS,
                            NutritionalStatusPLW = householdModel.NutritionalStatusPLW,
                            NutritionalStatusInfant = householdModel.NutritionalStatusInfant,
                            CreatedBy = User.Identity.Name,//householdModel.CreatedBy,
                            Remarks = householdModel.Remarks
                        });

                ViewBag.Subtitle = "Form 1B Listing";
                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Modify Form1B", ip, "",
                "Form1B Approval " + householdModel.HouseHoldIDNumber + " Successful at " + DateTime.Now.ToString());
                return RedirectToAction("ApproveForm1B", "Household", new { ProfileTDSPLWID = householdModel.ProfileTDSPLWID });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                householdModel = dbHousehold.FetchForm1BDetailsByID(householdModel.ProfileTDSPLWID);

                householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
                householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
                householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
                householdModel.genders = dbService.GetGenders();
                householdModel.nutrStatus = dbService.GetNutritionalStatus();
                householdModel.worker = dbService.GetSocialWorkers();
                householdModel.plws = dbService.GetPLW();

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Modify Form1B Error", ip, "",
                "Form1B Details with Message " + ex.Message + " for " + householdModel.HouseHoldIDNumber + " Encountered an error at " + DateTime.Now.ToString());

                return View("ModifyForm1B", householdModel);
            }
        }

        /////////
        //FORM 1C
        [Authorize]
        public ActionResult Form1C()
        {
            ViewBag.Subtitle = "Form 1C Listing";
            ViewBag.CurrentPage = _currentPage;

            dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Form1C List View", ip, "",
            "Form1C List Access  at " + DateTime.Now.ToString());
            return View();
        }
        [HttpGet]
        [Authorize]
        public XDocument Form1CTDSCMCList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = GeneralServices.strQueryString("SearchKeyword", string.Empty);

                Tuple<List<int>, List<Form1CGrid>> resultset
                    = GeneralServices.NpocoConnection().FetchMultiple<int, Form1CGrid>(";Exec GetForm1CTDSCMCList @jtStartIndex, @jtPageSize,@isSearch,@SearchTypeID,@SearchKeyword", 
                    new {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });

                DataTable dt = GeneralServices.ToDataTable<Form1CGrid>(resultset.Item2);

                ViewBag.CurrentPage = _currentPage;

                var canApproveForm = dbAcService.GetTaskAccessRights(12);

                string sb;
                if (canApproveForm)
                {
                    sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 3);
                }
                else
                {
                    sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 7);
                }
                
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                return XDocument.Parse("xml");
            }
        }
        //CaptureForm1B
        [Authorize]
        public ActionResult CaptureForm1C()
        {
            ViewBag.Subtitle = "Capture Form 1C";

            CaptureForm1C householdModel = new Models.CaptureForm1C();
            householdModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            householdModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            householdModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
            householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
            householdModel.genders = dbService.GetGenders();
            householdModel.worker = dbService.GetSocialWorkers();
            householdModel.nutrStatus = dbService.GetNutritionalStatus();
            householdModel.yesnos = dbService.GetYesNo();

            return View(householdModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult CaptureForm1C(CaptureForm1C newmodel)
        {
            ViewBag.Subtitle = "Capture Form 1C";

            try
            {
                newmodel.KebeleID = newmodel.Kebele;

                
                CaptureForm1C[] memberList = new JavaScriptSerializer().Deserialize<CaptureForm1C[]>(newmodel.MemberXml);

                foreach (var array in memberList)
                {
                    var psnpNumber = array.HouseHoldIDNumber;
                    //check PSNP Number uniqueness
                    var psnpexists = GeneralServices.PnspNumberExists(psnpNumber, AppConstants.Form1C, null, false);

                    if (psnpexists)
                    {
                        ModelState.AddModelError("", string.Format("There exists a Household with the PSNP Number {0}", psnpNumber));

                        newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                        newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                        newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                        newmodel.genders = dbService.GetGenders();
                        newmodel.nutrStatus = dbService.GetNutritionalStatus();
                        newmodel.worker = dbService.GetSocialWorkers();
                        newmodel.yesnos = dbService.GetYesNo();

                        return View("CaptureForm1C", newmodel);
                    }

                    
                }

                //check CBHI Number uniqueness
                var cbhiNumber = newmodel.CBHINumber;
                var cbhiexists = GeneralServices.CBHINumberExists(cbhiNumber, AppConstants.Form1C, null, false);
                if (cbhiexists)
                {
                    ModelState.AddModelError("", string.Format("There exists a Household with the CBHI Number {0}", cbhiNumber));

                    newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                    newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                    newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                    newmodel.genders = dbService.GetGenders();
                    newmodel.nutrStatus = dbService.GetNutritionalStatus();
                    newmodel.worker = dbService.GetSocialWorkers();
                    newmodel.yesnos = dbService.GetYesNo();

                    return View("CaptureForm1C", newmodel);
                }

                if (ModelState.IsValid)
                {
                    var result = dbHousehold.InsertForm1C(newmodel);
                    ViewBag.Subtitle = "Form 1C Listing";
                    dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Capture Form1C", ip, "",
                    "Form1C Details " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Captured Successfuly  at " + DateTime.Now.ToString());

                    return RedirectToAction("Form1C");

                }
                
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                ModelState.AddModelError("", message);

                newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);

                newmodel.genders = dbService.GetGenders();
                newmodel.nutrStatus = dbService.GetNutritionalStatus();
                newmodel.worker = dbService.GetSocialWorkers();
                newmodel.yesnos = dbService.GetYesNo();

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Capture Form1C Error", ip, "",
                    "Form1C Details with Message " + message + " for " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Encountered an error at " + DateTime.Now.ToString());

                return View("CaptureForm1C", newmodel);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                newmodel.genders = dbService.GetGenders();
                newmodel.nutrStatus = dbService.GetNutritionalStatus();
                newmodel.worker = dbService.GetSocialWorkers();
                newmodel.yesnos = dbService.GetYesNo();

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Capture Form1C Error", ip, "",
                "Form1C Details with Message " + ex.Message + " for " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Encountered an error at " + DateTime.Now.ToString());

                return View("CaptureForm1C", newmodel);
            }
        }


        //Approve Form 1C
        [Authorize]
        public ActionResult ModifyForm1C(ModifyForm1C householdModel)
        {
            ViewBag.Subtitle = "Modify Form 1C";

            /*  Commented On 07JUNE2019
            householdModel = dbHousehold.FetchForm1CDetailsByID(householdModel.ProfileTDSCMCID);
            //CaptureForm1A householdModel = new Models.CaptureForm1A();
            householdModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            householdModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            householdModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
            householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
            householdModel.genders = dbService.GetGenders();
            householdModel.nutrStatus = dbService.GetNutritionalStatus();
            householdModel.worker = dbService.GetSocialWorkers();
            householdModel.yesnos = dbService.GetYesNo();
            */


            householdModel = dbHousehold.FetchForm1CDetailsByID(householdModel.ProfileTDSCMCID);
            householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
            householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
            householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
            householdModel.RegionID = householdModel.RegionID;
            householdModel.WoredaID = householdModel.WoredaID;
            householdModel.genders = dbService.GetGenders();
            householdModel.nutrStatus = dbService.GetNutritionalStatus();
            householdModel.worker = dbService.GetSocialWorkers();
            householdModel.yesnos = dbService.GetYesNo();

            return View(householdModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult UpdateForm1C(ModifyForm1C householdModel)
        {
            ViewBag.Subtitle = "Modify Form 1C";
            try
            {
                //check PSNP Number uniqueness
                var psnpexists = GeneralServices.PnspNumberExists(householdModel.HouseHoldIDNumber, AppConstants.Form1C, householdModel.ProfileTDSCMCID, true);

                //check PSNP Number uniqueness
                var cbhiexists = GeneralServices.CBHINumberExists(householdModel.CBHINumber, AppConstants.Form1C, householdModel.ProfileTDSCMCID, true);

                if (psnpexists)
                {
                    householdModel = dbHousehold.FetchForm1CDetailsByID(householdModel.ProfileTDSCMCID);

                    householdModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                    householdModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                    householdModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                    householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
                    householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
                    householdModel.genders = dbService.GetGenders();
                    householdModel.nutrStatus = dbService.GetNutritionalStatus();
                    householdModel.worker = dbService.GetSocialWorkers();
                    householdModel.yesnos = dbService.GetYesNo();

                    ModelState.AddModelError("", string.Format("There exists a Household with the PSNP Number {0}", householdModel.HouseHoldIDNumber));

                    return View("ModifyForm1C", householdModel);
                }

                if (cbhiexists)
                {
                    householdModel = dbHousehold.FetchForm1CDetailsByID(householdModel.ProfileTDSCMCID);

                    householdModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                    householdModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                    householdModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                    householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
                    householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
                    householdModel.genders = dbService.GetGenders();
                    householdModel.nutrStatus = dbService.GetNutritionalStatus();
                    householdModel.worker = dbService.GetSocialWorkers();
                    householdModel.yesnos = dbService.GetYesNo();

                    ModelState.AddModelError("", string.Format("There exists a Household with the CBHI Number {0}", householdModel.CBHINumber));

                    return View("ModifyForm1C", householdModel);
                }

                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>
                    (";Exec ModifyForm1C @ProfileTDSCMCID,@KebeleID,@Gote,@Gare,@CollectionDate,@SocialWorker,@CCCCBSPCMember,@NameOfCareTaker,@CaretakerID,@HouseHoldIDNumber,@ChildID,@MalnourishedChildName,@MalnourishedChildSex,@ChildDateOfBirth,@DateTypeCertificate,@MalnourishmentDegree,@StartDateTDS,@NextCNStatusDate,@EndDateTDS,@CreatedBy,@Remarks,@CBHIMembership,@CBHINumber,@ChildProtectionRisk",
                        new
                        {
                            ProfileTDSCMCID = householdModel.ProfileTDSCMCID,
                            KebeleID = householdModel.KebeleID,
                            Gote = householdModel.Gote,
                            Gare = householdModel.Gare,
                            CollectionDate = householdModel.CollectionDate,
                            SocialWorker = householdModel.SocialWorker,
                            CCCCBSPCMember = householdModel.CCCCBSPCMember,
                            NameOfCareTaker = householdModel.NameOfCareTaker,
                            CaretakerID = householdModel.CaretakerID,
                            HouseHoldIDNumber = householdModel.HouseHoldIDNumber,
                            ChildID = householdModel.ChildID,
                            MalnourishedChildName = householdModel.MalnourishedChildName,
                            MalnourishedChildSex = householdModel.MalnourishedChildSex,
                            ChildDateOfBirth = householdModel.ChildDateOfBirth,
                            DateTypeCertificate = householdModel.DateTypeCertificate,
                            MalnourishmentDegree = householdModel.MalnourishmentDegree,
                            StartDateTDS = householdModel.StartDateTDS,
                            NextCNStatusDate = householdModel.NextCNStatusDate,
                            EndDateTDS = householdModel.EndDateTDS,
                            CreatedBy = User.Identity.Name.ToString(),
                            Remarks     = householdModel.Remarks,
                            CBHIMembership = householdModel.CBHIMembership,
                            CBHINumber = householdModel.CBHINumber,
                            ChildProtectionRisk= householdModel.ChildProtectionRisk,
                        });

                ViewBag.Subtitle = "Form 1C Listing";
                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Modify Form1C", ip, "",
                "Form1C Modify " + householdModel.HouseHoldIDNumber + " Successful at " + DateTime.Now.ToString());
                if (householdModel.isUpdate == "YES")
                {
                    return RedirectToAction("Form1C");
                }
                else
                {
                    return RedirectToAction("ApproveForm1C", "Household", new { ProfileTDSCMCID=householdModel.ProfileTDSCMCID });
                }
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                householdModel = dbHousehold.FetchForm1CDetailsByID(householdModel.ProfileTDSCMCID);

                householdModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                householdModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                householdModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
                householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
                householdModel.genders = dbService.GetGenders();
                householdModel.nutrStatus = dbService.GetNutritionalStatus();
                householdModel.worker = dbService.GetSocialWorkers();
                householdModel.yesnos = dbService.GetYesNo();

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Modify Form1B Error", ip, "",
                "Form1B Details with Message " + ex.Message + " for " + householdModel.HouseHoldIDNumber + " Encountered an error at " + DateTime.Now.ToString());

                return View("ModifyForm1C", householdModel);
            }
        }

        [Authorize]
        public ActionResult ApproveForm1C(ModifyForm1C householdModel)
        {
            ViewBag.Subtitle = "Approve Form 1C";

            /*
            householdModel = dbHousehold.FetchForm1CDetailsByID(householdModel.ProfileTDSCMCID);
            //CaptureForm1A householdModel = new Models.CaptureForm1A();
            householdModel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            householdModel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            householdModel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            householdModel.RegionID = int.Parse(Session["RegionID"].ToString());
            householdModel.WoredaID = int.Parse(Session["WoredaID"].ToString());
            householdModel.genders = dbService.GetGenders();
            householdModel.nutrStatus = dbService.GetNutritionalStatus();
            householdModel.worker = dbService.GetSocialWorkers();
            householdModel.yesnos = dbService.GetYesNo();
            */

            householdModel = dbHousehold.FetchForm1CDetailsByID(householdModel.ProfileTDSCMCID);
            householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
            householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
            householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
            householdModel.RegionID = householdModel.RegionID;
            householdModel.WoredaID = householdModel.WoredaID;
            householdModel.genders = dbService.GetGenders();
            householdModel.nutrStatus = dbService.GetNutritionalStatus();
            householdModel.worker = dbService.GetSocialWorkers();
            householdModel.yesnos = dbService.GetYesNo();
            return View(householdModel);
        }

        //[Authorize]
        //[HttpPost]
        //public ActionResult UpdateApprovedForm1B(ModifyForm1B householdModel)
        //{
        //    ViewBag.Subtitle = "Approve Form 1B";
        //    string errMessage = string.Empty;
        //    string filename = string.Empty;
        //    try
        //    {
        //        HouseholdServices monit = new HouseholdServices();
        //        var RptGenerated = monit.PdfForm1BReport(householdModel.ProfileTDSPLWID,
        //            Membership.GetUser().UserName.ToString(), out errMessage, out filename);
        //        if (RptGenerated == true)
        //        {
        //            List<RecordCount> isSuccess
        //                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec ApproveForm1BDetails @ProfileTDSPLWID,@ReportFilePath,@CreatedBy",
        //                    new
        //                    {
        //                        ProfileTDSPLWID = householdModel.ProfileTDSPLWID,
        //                        ReportFilePath = filename,
        //                        CreatedBy = Membership.GetUser().UserName.ToString()
        //                    });
        //            ViewBag.Subtitle = "Form 1B Listing";
        //            dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Approve Form1B", ip, "",
        //            "Form1B Approval " + householdModel.HouseHoldIDNumber + " Successful at " + DateTime.Now.ToString());
        //            return RedirectToAction("Form1B");
        //        }
        //        else
        //        {
        //            householdModel = dbHousehold.FetchForm1BDetailsByID(householdModel.ProfileTDSPLWID);

        //            householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
        //            householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
        //            householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
        //            householdModel.genders = dbService.GetGenders();
        //            householdModel.nutrStatus = dbService.GetNutritionalStatus();
        //            householdModel.worker = dbService.GetSocialWorkers();

        //            ModelState.AddModelError("", errMessage);

        //            dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Approve Form1B Error", ip, "",
        //            "Form1B Details with Message " + errMessage + " for " + householdModel.HouseHoldIDNumber + " Encountered an error at " + DateTime.Now.ToString());

        //            return View("ApproveForm1B", householdModel);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        householdModel = dbHousehold.FetchForm1BDetailsByID(householdModel.ProfileTDSPLWID);

        //        householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
        //        householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
        //        householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);
        //        householdModel.genders = dbService.GetGenders();
        //        householdModel.nutrStatus = dbService.GetNutritionalStatus();
        //        householdModel.worker = dbService.GetSocialWorkers();

        //        ModelState.AddModelError("", ex.Message);

        //        dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Approve Form1B Error", ip, "",
        //        "Form1B Details with Message " + ex.Message + " for " + householdModel.HouseHoldIDNumber + " Encountered an error at " + DateTime.Now.ToString());

        //        return View("ApproveForm1B", householdModel);
        //    }
        //}

        [Authorize]
        [HttpPost]
        public JsonResult UpdateApprovedForm1C(ModifyForm1C householdModel)
        {
            ViewBag.Subtitle = "Approve Form 1C";
            string errMessage = string.Empty;
            string filename = string.Empty;
            try
            {
                if (householdModel.CreatedBy.ToUpper() == Membership.GetUser().UserName.ToString().ToUpper())
                {
                    errMessage = "You cannot approve a record you created!";
                    throw new ArgumentException(errMessage);
                }
                
                HouseholdServices monit = new HouseholdServices();
                var RptGenerated = monit.PdfForm1CReport(householdModel.ProfileTDSCMCID,
                    Membership.GetUser().UserName.ToString(), out errMessage, out filename);
                if (RptGenerated == true)
                {

                    List<RecordCount> isSuccess
                        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec ApproveForm1CDetails @ProfileTDSCMCID,@ReportFilePath,@CreatedBy",
                            new
                            {
                                ProfileTDSCMCID = householdModel.ProfileTDSCMCID,
                                ReportFilePath = filename,
                                CreatedBy = Membership.GetUser().UserName.ToString()
                            });
                    ViewBag.Subtitle = "Form 1C Listing";

                    dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Approve Form1C", ip, "",
                    "Form1C Approval " + householdModel.HouseHoldIDNumber + " Successful at " + DateTime.Now.ToString());

                    //return RedirectToAction("Form1C");
                    return Json(isSuccess, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    householdModel = dbHousehold.FetchForm1CDetailsByID(householdModel.ProfileTDSCMCID);

                    householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
                    householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
                    householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);

                    householdModel.genders = dbService.GetGenders();
                    householdModel.nutrStatus = dbService.GetNutritionalStatus();
                    householdModel.worker = dbService.GetSocialWorkers();

                    ModelState.AddModelError("", errMessage);

                    dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Approve Form1C Error", ip, "",
                    "Form1A Details with Message " + errMessage + " for " + householdModel.HouseHoldIDNumber + " Encountered an error at " + DateTime.Now.ToString());

                    //return View("ApproveForm1C", householdModel);
                    return Json(errMessage, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                householdModel = dbHousehold.FetchForm1CDetailsByID(householdModel.ProfileTDSCMCID);

                householdModel.regions = dbService.GetRegionByIDs(householdModel.RegionID);
                householdModel.woredas = dbService.GetWoredaByIDs(householdModel.RegionID, householdModel.WoredaID);
                householdModel.kebeles = dbService.GetKebeles(householdModel.WoredaID);

                householdModel.genders = dbService.GetGenders();
                householdModel.nutrStatus = dbService.GetNutritionalStatus();
                householdModel.worker = dbService.GetSocialWorkers();

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Houshold", "Approve Form1C Error", ip, "",
                "Form1A Details with Message " + ex.Message + " for " + householdModel.HouseHoldIDNumber + " Encountered an error at " + DateTime.Now.ToString());

                //return View("ApproveForm1C", householdModel);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }


        //MISC

        //[HttpPost]
        //[Authorize]
        //public ActionResult PrintForm1CPdf(Form1CGrid model)
        //{
        //    string errMessage = string.Empty;
        //    HouseholdServices monit = new HouseholdServices();

        //    dbAcService.InsertAuditTrail(User.Identity.Name, "Downloads", "Generate Form 1C Report", ip, "",
        //    "Previewed a downloadable file " + model.ProfileTDSCMCID + " at " + DateTime.Now.ToString());

        //    var RptNameDetails = monit.PdfForm1CReport(model, Membership.GetUser().UserName.ToString(), out errMessage);
        //    return Json(RptNameDetails, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        [Authorize]
        public JsonResult SelectWoreda(Woreda model)
        {
            var WorDetails = dbService.GetWoredas(model.RegionID);
            return Json(WorDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult SelectKebele(Kebele model)
        {
            var KebDetails = dbService.GetKebeles(model.WoredaID);
            return Json(KebDetails, JsonRequestBehavior.AllowGet);
        }
        //Dashboard Info
        [HttpPost]
        [Authorize]
        public JsonResult SelectDashWoreda(Woreda model)
        {
            var WorDetails = dbService.GetDashWoredas(model.RegionID);
            return Json(WorDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult SelectDashKebele(Kebele model)
        {
            var KebDetails = dbService.GetDashKebeles(model.WoredaID);
            return Json(KebDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult SelectServiceProvider(ServiceProviderModel model)
        {
            var SPDetails = dbService.GetServiceProvidersUpdated(model);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult SelectReportingPeriods(ReportingPeriod model)
        {
            var periods = dbService.GetReportingPeriods(model.FiscalYear);
            return Json(periods, JsonRequestBehavior.AllowGet);
        }
    }
}
