﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCTMis.Models
{
    public class AdminStructure
    {
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string CreatedBy { get; set; }        
    }

    public class ServiceAdminStructure
    {
        public string Region { get; set; }
        public string Woreda { get; set; }
        public string Kebele { get; set; }
        public string ServiceProviderName { get; set; }
        public string TypeOfService { get; set; }
        public string ReportingPeriodName { get; set; }
        public string CreatedBy { get; set; }         
    }

    public class ReportAdminStructure
    {
        public string Region { get; set; }
        public string Woreda { get; set; }
        public string Kebele { get; set; }
        public string ReportingPeriod { get; set; }
    }

    public class Form2AGrid
    {
        public int ColumnID { get; set; }
        public string CoRespDSHeaderID { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string HouseholdCount { get; set; }
        public string GeneratedBy { get; set; }
        public string GeneratedOn { get; set; }
        public string ReportFileName { get; set; }        
    }

    public class Form4Model
    {
        public int ColumnID { get; set; }
        public string CoRespDSHeaderID { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string ServiceName { get; set; }
        public string HouseholdCount { get; set; }
        public string ReportFileName { get; set; }
        public string GeneratedBy { get; set; }
        public string GeneratedOn { get; set; }
    }

    //ServiceName
    public class Form4GridCompliance
    {
        public int ColumnID { get; set; }
        public string ComplianceID { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string ReportFileName { get; set; }
    }

    //public class Form4AGrid
    //{
    //    public int ColumnID { get; set; }
    //    public Int32 ProfileDSDetailID { get; set; }
    //    public string KebeleName { get; set; }
    //    public string ServiceName { get; set; }
    //    public string HouseHoldMemberName { get; set; }
    //    public string HouseHoldIDNumber { get; set; }
    //    public int Age { get; set; }
    //    public string Sex { get; set; }

    //}

    public class ProduceForm2A
    {
        public int ColumnID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
    }

    public class Form2AMatrix
    {
        public string ProfileDSDetailID { get; set; }
		public string ProfileDSHeaderID { get; set; }
		public string HouseHoldIDNumber { get; set; }
        public string NameOfHouseHoldMember { get; set; }
        public string IndividualID { get; set; }
		public string Sex { get; set; }
        public string Age { get; set; }
		public string MemberCondition { get; set; }
		public string PreAnteNatalCare { get; set; }
		public string Immunization { get; set; }
		public string SupplementaryFeeding { get; set; }
		public string MonthlyGMPSessions { get; set; }
		public string EnrolmentAttendance { get; set; }
        public string BCCSessions { get; set; }
        public string UnderTSF { get; set; }
        public string BirthRegistration { get; set; }
        public string Gote { get; set; }

        public string WoredaName { get; set; }
        public string KebeleName { get; set; }

        public string ChildProtectionRisk { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }	
    }

    public class ProduceForm2B
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
    }

    public class ProduceForm2B2
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
    }


    public class Form2BMatrix
    {
		public string ProfileTDSPLWID { get; set; }
		public string HouseHoldIDNumber { get; set; }
        public string MedicalRecordNumber { get; set; }
		public string NameOfPLW { get; set; }
		public string PreNatalHealthPost { get; set; }
		public string PreNatalHealthCentre { get; set; }
		public string PostNatalHealthPost { get; set; }
		public string Immunization { get; set; }
		public string SupplementaryFeeding { get; set; }
		public string MonthlyGMPSessions { get; set; }
        public string BCCSessions { get; set; }
        public string BirthRegistration { get; set; }
        public string Gote { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }

    }

    public class Form2B2Matrix
    {
        public string ProfileTDSPLWID { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string MedicalRecordNumber { get; set; }
        public string NameOfPLW { get; set; }
        public string PreNatalHealthPost { get; set; }
        public string PreNatalHealthCentre { get; set; }
        public string PostNatalHealthPost { get; set; }
        public string Immunization { get; set; }
        public string SupplementaryFeeding { get; set; }
        public string MonthlyGMPSessions { get; set; }
        public string BCCSessions { get; set; }
        public string BirthRegistration { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string Gote { get; set; }       
    }

    public class ProduceForm2C
    {
        public int ColumnID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
    }

    public class Form2CMatrix
    {
        public string ProfileTDSCMCID { get; set; }
		public string HouseHoldIDNumber { get; set; }
		public string NameOfCareTaker { get; set; }
        public string ChildID { get; set; }
        public string MalnourishedChildName { get; set; }
		public string HealthPostCheckUp { get; set; }
		public string SupplementaryFeeding { get; set; }
		public string MonthlyGMPSessions { get; set; }
        public string BCCSessions { get; set; }
        public string Immunization { get; set; }
        public string BirthRegistration { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string Gote { get; set; }  

    }
}
