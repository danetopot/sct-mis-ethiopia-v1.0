﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TitleContent" runat="server">
    Replicate Data
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">    
<h4>Replicate Data</h4>
<% using (Html.BeginForm("FileUpload", "Administration", FormMethod.Post, new { id = "MyForm", enctype = "multipart/form-data" }))
   { %>
        <%--third Row--%>
            <div class="col-6">                              
            <label> 
                <input type="file" id="fileToUpload" name="file" onchange="checkfile()"/>
                <span class="field-validation-error" id="spanfile"></span>
            </label>
            </div>
            <br /><br />

            <div class="form-actions">
	            <button id="cmdGenerate" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Replicate</button>
                
            </div>
            <br />
            <%: Html.ValidationSummary(true) %><br />
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
<script type="text/jscript">
    jQuery.uaMatch = function (ua) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
            /(webkit)[ \/]([\w.]+)/.exec(ua) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
            /(msie) ([\w.]+)/.exec(ua) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
            [];

        return {
            browser: match[1] || "",
            version: match[2] || "0"
        };
    };

    matched = jQuery.uaMatch(navigator.userAgent);
    browser = {};

    if (matched.browser) {
        browser[matched.browser] = true;
        browser.version = matched.version;
    }

    // Chrome is Webkit, but Webkit is also Safari.
    if (browser.chrome) {
        browser.webkit = true;
    } else if (browser.webkit) {
        browser.safari = true;
    }

    jQuery.browser = browser;

    $("#cmdGenerate").prop("disabled", true);

    $("#btnSubmit").live("click", function () {

        if ($('#fileToUpload').val() == "") {
            $("#spanfile").html("Please upload file");
            return false;
        }
        else {
            return checkfile();
        }
    });


    //get file size
    function GetFileSize(fileid) {
        try {
            var fileSize = 0;
            //for IE
            if ($.browser.msie) {
                //before making an object of ActiveXObject, 
                //please make sure ActiveX is enabled in your IE browser
                var objFSO = new ActiveXObject("Scripting.FileSystemObject"); var filePath = $("#" + fileid)[0].value;
                var objFile = objFSO.getFile(filePath);
                var fileSize = objFile.size; //size in kb
                fileSize = fileSize / 1048576; //size in mb 
            }
                //for FF, Safari, Opeara and Others
            else {
                fileSize = $("#" + fileid)[0].files[0].size //size in kb
                fileSize = fileSize / 1048576; //size in mb 
            }

            // alert("Uploaded File Size is" + fileSize + "MB");
            return fileSize;
        }
        catch (e) {
            alert("Error is :" + e);
        }
    }

    //get file path from client system
    function getNameFromPath(strFilepath) {

        var objRE = new RegExp(/([^\/\\]+)$/);
        var strName = objRE.exec(strFilepath);

        if (strName == null) {
            return null;
        }
        else {
            return strName[0];
        }

    }
    function checkfile() {
        var file = getNameFromPath($("#fileToUpload").val());
        if (file != null) {
            var extension = file.substr((file.lastIndexOf('.') + 1));
            //  alert(extension);
            switch (extension) {
                case 'zip':
                    flag = true;
                    break;
                default:
                    flag = false;
            }
        }
        if (flag == false) {
            $("#spanfile").text("You can upload only .zip extension file");
            return false;

        }
        else {
            var size = GetFileSize('fileToUpload');
            if (size > 100) {
                $("#spanfile").text("You can upload file up to 100 MB");
                return false;
            }
            else {
                $("#spanfile").text("");
                $("#cmdGenerate").prop("disabled", false);
            }
        }
    }
</script>
</asp:Content>