﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ProduceForm4C>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Produce compliance form 4C
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<h5>Add New Girl Form</h5>   --%>     
<% using (Html.BeginForm("ProduceForm4C", "Compliance", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-4">                
            <label>
                Region <%: Html.ValidationMessageFor(model => model.RegionID) %>
                <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
            </label>
            </div>
            <div class="col-4">                
            <label>
                Woreda  <%: Html.ValidationMessageFor(model => model.WoredaID) %>
                <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
            </label>
            </div>
    <br />
            <div class="col-4">                              
            <label>
                Kebele <%: Html.ValidationMessageFor(model => model.KebeleID) %>  
                <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
            </label>
            </div>
    
            <div class="col-4">                
            <label>
                Reporting Period <%: Html.ValidationMessageFor(model => model.ReportingPeriodID) %>
                <%: Html.DropDownListFor(model => model.ReportingPeriodID,
                    new SelectList(Model.reportgperiod, "PeriodID", "PeriodName"), new { tabindex = "4" })%>
            </label>
            </div>

          <br />
            <div class="form-actions">
	            <button id="cmdProduce" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Produce Compliance Form 4C</button>
                <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
            </div>
            <br /><br />
            <%: Html.ValidationSummary(true) %>    
            <%: Html.HiddenFor(model => model.FormID, new { Value = "3" })%>       
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#KebeleID").prop("disabled", false);
            $("#cmdBack,#cmdProduce").prop("disabled", false);
            $("#RegionID,#WoredaID").prop("disabled", true);
            $(":input").blur();
            $('#KebeleID').focus();
            $('#loading').hide();

            $("#cmdBack").click(function () {
                window.location.href = "../Compliance/Form4C";
            });

        });
    </script>
</asp:Content>
