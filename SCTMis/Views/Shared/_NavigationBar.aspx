﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<div class="navbar botmain">
    <div class="topnaver">
        <div class="topnavL">
            <ul>
                <% if (Request.IsAuthenticated) { %>
                    <% using (Html.BeginForm("LogOff", "Account", FormMethod.Post, new { id = "logoutForm" })) { %>
                        <%: Html.AntiForgeryToken() %>                        
                        <li id="mnDashboard"><%: Html.ActionLink("Dashboard", "Index", "Home") %></li>
                        <li id="mnPasswordChange"><%: Html.ActionLink("Change Password", "ModifyPassword", "Security") %></li>
                        <%--<li id="mnPasswordChange"><a href="#">Change Password</a></li>--%>
                        <li id="mnLogout"><a href="javascript:document.getElementById('logoutForm').submit()">Log off</a></li>    
                    <% } %> 
                <% } %>
            </ul>            
        </div>  
        
        <div class="topnavR">            
            <% if (Request.IsAuthenticated) { %>
                Welcome <%: Html.ActionLink(User.Identity.Name, null, null, routeValues: null, htmlAttributes: new { @class = "username", title = "Manage" }) %>
                You are logged in to <strong><a href="#"><%: Session["LocationName"] %></a></strong>
            <% } %>
        </div>                 
    </div>
</div>

<div class="navbar main">
    <div class="topnav" id="IDtopnav">
        <div class="topnavLarge" id="IDtopnavLeft">
            <span> DIRECT SUPPORT PROGRAMMES</span>
            <div class="topnavMini">
                <span>Management Information System</span><span style="text-transform: initial; font-size: 8pt;">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Version 3.9</span>
            </div>
        </div>
    </div>
</div>

<div class="navbar botmain">
    <div class="topnaver">
        <ul class="topnavBottom">
            <li id="Li4"><%: ViewBag.Subtitle %></li>
            <li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</li>
            <li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</li>            
            <li id="liCurrentFY" style="color:firebrick; font-size:large"><%:  Session["CurrentFiscalYear"] %></li>
        </ul>
    </div>
</div>
