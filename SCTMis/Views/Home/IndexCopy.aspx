﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Dashboard.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.DashboardModel>" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    SCT-MIS Dashboard
</asp:Content>

<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("Dashboards", "Household", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-7">                
            <label>
                Region <%: Html.ValidationMessageFor(model => model.RegionID) %>
                <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Woreda  <%: Html.ValidationMessageFor(model => model.WoredaID) %>
                <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Kebele <%: Html.ValidationMessageFor(model => model.KebeleID) %>  
                <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Type of service accessed (chart 3) <%: Html.ValidationMessageFor(model => model.ReportingIndicatorId) %>  
                <%: Html.DropDownListFor(model => model.ReportingIndicatorId,
                    new SelectList(Model.intgrtedservis, "ServiceID", "ServiceName"), new { tabindex = "4" })%>
            </label>
            </div>
        <%--Sixth Row--%>
            <button id="cmdFilterDashboard" type="button" class="btn btn-icon btn-primary glyphicons search" tabindex="4"><i></i>Filter</button>
                <%: Html.HiddenFor(model => model.ColumnID)%>
                <%: Html.HiddenFor(model => model.KebeleID)%>
            <hr />
            <%: Html.ValidationSummary(true) %>          
    <% } %>
    <div id="columns">
        <div class="col1">
            <div id="headerDiv1">
                <p>1. Analysis of household profile by age</p>
            </div>
            <div id="contentDiv1">
                <div id="chartDiv" style="width:420px;height:210px;margin:1px;border:1px solid #A4BED4"></div>
            </div>
            <div id="footerDiv1">
                <p>3. Analysis of client compliance status by reporting period</p>
                <div id="lineChart" style="width:450px;height:210px;margin:1px;border:1px solid #A4BED4"></div>
            </div>
        </div>
        <div class="col2">
            <div id="hDiv2">
                <p>2. Analysis of household profile by vulnerability</p>
            </div>
            <div id="cDiv2">
                <div id="chartDiv2" style="width:420px;height:210px;margin:1px;border:1px solid #A4BED4"></div>
            </div>
            <div id="fDiv2">
                <p>4. Analysis of social worker visits</p>
                <div id="fourthChart" style="width:420px;height:210px;margin:1px;border:1px solid #A4BED4"></div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <style>
        #columns{
            display: table;
            width: 500px;
        }
        .col1{
            display: table-cell;
            width: 55%;
            padding: 1em;
            position: relative;
            left: auto;
        }
        .col2{
            width: 35%;
            display: table-cell;
            padding: 1em;
            position: relative;
        }
    </style>
	<script type="text/javascript">
	    var myBarCHartAge;
	    var myBarCHartVulnerability;
	    var myLineChart;
	    var myFourthLineChart;
	    $(document).ready(function () {
	        loadDashboardData();

	        $("#cmdFilterDashboard").click(function () {
	            location.reload(false);
	            //location.reload(true);

	            //loadDashboardData();
	        });
	    });

	    function loadDashboardData() {
	        myBarCHartAge = new dhtmlXChart({
	            view: "bar",
	            container: "chartDiv",
	            value: "#Number#",
	            radius: 0,
	            alpha: 0.5,
	            border: true,
	            width: 70,
	            label: "#Number#",
	            tooltip: "<span style='color:#293CD6'>#Age#</span>",
	            xAxis: {
	                title: 'Household profile',
	                template: "#Age#"
	            },
	            yAxis: {
	                title: "Number",
	                template: function (obj) {
	                    return (obj % 20 ? "" : obj)
	                }
	            }
	        });

	        myBarCHartVulnerability = new dhtmlXChart({
	            view: "bar",
	            container: "chartDiv2",
	            value: "#Number#",
	            radius: 0,
	            alpha: 0.5,
	            border: true,
	            width: 70,
	            label: "#Number#",
	            tooltip: "<span style='color:#293CD6'>#Vulnerability#</span>",
	            xAxis: {
	                title: 'Household vulnerability',
	                template: "#Vulnerability#"
	            },
	            yAxis: {
	                title: "Number",
	                template: function (obj) {
	                    return (obj % 20 ? "" : obj)
	                }
	            }
	        });

	        myLineChart = new dhtmlXChart({
	            view: "spline",
	            container: "lineChart",
	            value: "#Complete#",
	            label: "#Complete#",
	            item: {
	                borderColor: "#447900",
	                color: "#69ba00"
	            },
	            line: {
	                color: "#69ba00",
	                width: 2
	            },
	            tooltip: {
	                template: "#Complete#"
	            },
	            offset: 0,
	            xAxis: {
	                template: "#Months#",
	                title: "Months"
	            },
	            yAxis: {
	                title: "Count"
	            },
	            padding: {
	                left: 50,
	                bottom: 50
	            },
	            origin: 0,
	            legend: {
	                values: [{ text: "Total", color: "#447900" }, { text: "Complied", color: "#0a796a" }, { text: "Not complied", color: "#b7286c" }],
	                align: "left",
	                valign: "bottom",
	                layout: "x",
	                width: 80,
	                marker: {
	                    type: "item"
	                }
	            }
	        });

	        myLineChart.addSeries({
	            value: "#Complied#",
	            label: "#Complied#",
	            item: {
	                borderColor: "#0a796a",
	                color: "#4aa397",
	                type: "s",
	                radius: 4
	            },
	            line: {
	                color: "#4aa397",
	                width: 2
	            },
	            tooltip: {
	                template: "#Complied#"
	            }
	        });
	        myLineChart.addSeries({
	            value: "#UnComplied#",
	            label: "#UnComplied#",
	            item: {
	                borderColor: "#b7286c",
	                color: "#de619c",
	                type: "t",
	                radius: 4
	            },
	            line: {
	                color: "#de619c",
	                width: 2
	            },
	            tooltip: {
	                template: "#UnComplied#"
	            }
	        });

	        //myFourthLineChart
	        myFourthLineChart = new dhtmlXChart({
	            view: "spline",
	            container: "fourthChart",
	            value: "#Visits#",
	            label: "#Visits#",
	            item: {
	                borderColor: "#447900",
	                color: "#69ba00"
	            },
	            line: {
	                color: "#69ba00",
	                width: 2
	            },
	            tooltip: {
	                template: "#Visits#"
	            },
	            offset: 0,
	            xAxis: {
	                template: "#Months#",
	                title: "Months"
	            },
	            yAxis: {
	                title: "Count"
	            },
	            padding: {
	                left: 50,
	                bottom: 50
	            },
	            origin: 0,
	            legend: {
	                values: [{ text: "SW Visits", color: "#447900" }, { text: "Cases Managed", color: "#0a796a" }, { text: "Complied Clients", color: "#b7286c" }],
	                align: "left",
	                valign: "bottom",
	                layout: "x",
	                width: 80,
	                marker: {
	                    type: "item"
	                }
	            }
	        });

	        myFourthLineChart.addSeries({
	            value: "#Cases#",
	            label: "#Cases#",
	            item: {
	                borderColor: "#0a796a",
	                color: "#4aa397",
	                type: "s",
	                radius: 4
	            },
	            line: {
	                color: "#4aa397",
	                width: 2
	            },
	            tooltip: {
	                template: "#Cases#"
	            }
	        });
	        myFourthLineChart.addSeries({
	            value: "#Complied#",
	            label: "#Complied#",
	            item: {
	                borderColor: "#b7286c",
	                color: "#de619c",
	                type: "t",
	                radius: 4
	            },
	            line: {
	                color: "#de619c",
	                width: 2
	            },
	            tooltip: {
	                template: "#Complied#"
	            }
	        });

	        myBarCHartAge.load("/Dashboard_Old/FetchDashboardHouseholdByAge?RegionCodeID=" + $("#RegionCodeID").val() + "&WoredaCodeID=" + $("#WoredaCodeID").val() + "&KebeleCodeID=" + $("#KebeleCodeID").val());
	        myBarCHartVulnerability.load("/Dashboard_Old/FetchDashboardHouseholdByVulnerability?RegionCodeID=" + $("#RegionCodeID").val() + "&WoredaCodeID=" + $("#WoredaCodeID").val() + "&KebeleCodeID=" + $("#KebeleCodeID").val());
	        myLineChart.load("/Dashboard_Old/FetchDashboardPeriodLineChart?RegionCodeID=" + $("#RegionCodeID").val() + "&WoredaCodeID=" + $("#WoredaCodeID").val() + "&KebeleCodeID=" + $("#KebeleCodeID").val() + "&ServiceID=" + $("#ServiceID").val());
	        myFourthLineChart.load("/Dashboard_Old/FetchDashboardFourthLineChart?RegionCodeID=" + $("#RegionCodeID").val() + "&WoredaCodeID=" + $("#WoredaCodeID").val() + "&KebeleCodeID=" + $("#KebeleCodeID").val());
	        //fourthChart

	        $("#RegionCodeID").change(function () {
	            regionCodeChange();
	        });

	        $("#WoredaCodeID").change(function () {
	            woredaCodeChange();
	        });

	        function regionCodeChange() {
	            $('#WoredaCodeID,#KebeleCodeID').empty();
	            $.ajax({
	                type: "POST",
	                url: "../Household/SelectDashWoreda",
	                data: "{  'RegionID' : '" + $("#RegionCodeID").val() + "'}",
	                dataType: "json",
	                contentType: "application/json; charset=utf-8",
	                error: function (xx, yy) {
	                    //alert(yy);
	                },
	                success: function (data) {
	                    for (var i = 0; i < data.length; i++) {
	                        var d = data[i];
	                        $('#WoredaCodeID').append($("<option></option>").attr("value", d.WoredaID).text(d.WoredaName));
	                    }
	                    woredaCodeChange();
	                }
	            });
	        }

	        function woredaCodeChange() {
	            $('#KebeleCodeID').empty();
	            $.ajax({
	                type: "POST",
	                url: "../Household/SelectDashKebele",
	                data: "{  'WoredaID' : '" + $("#WoredaCodeID").val() + "'}",
	                dataType: "json",
	                contentType: "application/json; charset=utf-8",
	                error: function (xx, yy) {
	                    alert(yy);
	                },
	                success: function (data) {
	                    for (var i = 0; i < data.length; i++) {
	                        var d = data[i];
	                        $('#KebeleCodeID').append($("<option></option>").attr("value", d.KebeleID).text(d.KebeleName));
	                    }
	                }
	            });
	        }
	    }
	</script>
</asp:Content>
