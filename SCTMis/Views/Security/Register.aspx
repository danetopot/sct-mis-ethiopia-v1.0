﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.RegisterUserModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Add New User
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h3>Add New User</h3>        
<% using (Html.BeginForm(null, null, FormMethod.Post, new { id = "MyForm" }))
   { %>        
        <%--The First Row--%>
            <div class="col-4">                              
            <label>
                Login Name 
                <%: Html.TextBoxFor(model => model.UserName, new { tabindex = "1",@KeyPress="fnTextChanged" })%>
            </label>
            </div>

            <div class="col-4">                
            <label>
                First Name
                <%: Html.TextBoxFor(model => model.FirstName, new { tabindex = "2" })%>
            </label>
            </div>
        <%--Second Row--%>
            <div class="col-4">                
            <label>
                Last Name
                <%: Html.TextBoxFor(model => model.LastName, new { tabindex = "3" })%>
            </label>
            </div>
    <br />
            <div class="col-4">                
            <label>
                Role Name
                <%: Html.DropDownListFor(model => model.RoleID,
                    new SelectList(Model.roles, "RoleID", "RoleName"), new { tabindex = "4" })%>
            </label>
            </div>
        <%--Third Row--%>
            <div class="col-4">                  
            <label>
                Password
                <%: Html.PasswordFor(model => model.Password, new { tabindex = "5" })%>
            </label>
            </div>
        
            <div class="col-4">                
            <label>
                Confirm Password
                <%: Html.PasswordFor(model => model.ConfirmPassword, new { tabindex = "6" })%>
            </label>
            </div>
        <%--Fourth Row--%>
            <div class="col-2">                
            <label>
                Email
                <%: Html.TextBoxFor(model => model.Email, new { tabindex = "7",@maxlength="59"  })%>
            </label>
            </div>
            <div class="col-4">                
            <label>
                Mobile
                <%: Html.TextBoxFor(model => model.Mobile, new { tabindex = "8",@maxlength="13" })%>
            </label>
            </div>
    <br />
            <%: Html.ValidationSummary(true) %><br />

    <div class="form-actions">
	    <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save User</button>
	    <button id="cmdCancel" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
    </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <%--<%: Scripts.Render("~/bundles/jqueryval") %>--%>
    <script src="../Scripts/assets/js/app/Users.js"></script>
</asp:Content>
