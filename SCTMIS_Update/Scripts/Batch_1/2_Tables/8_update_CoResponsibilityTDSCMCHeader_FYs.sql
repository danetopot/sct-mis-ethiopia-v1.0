alter table CoResponsibilityTDSCMCHeader add FiscalYear int
go

alter table CoResponsibilityTDSCMCHeader with check add constraint FK_CoResponsibilityTDSCMCHeader_FiscalYear foreign key(FiscalYear)
references FiscalYears (FiscalYear)
go

update CoResponsibilityTDSCMCHeader set FiscalYear = dbo.f_getFiscalYearFromDate(GeneratedOn)
from CoResponsibilityTDSCMC
inner join CoResponsibilityTDSCMCHeader on CoResponsibilityTDSCMCHeader.CoRespTDSCMCWHeaderID = CoResponsibilityTDSCMC.CoRespTDSCMCWHeaderID

alter table CoResponsibilityTDSCMCHeader alter column FiscalYear int not null
go