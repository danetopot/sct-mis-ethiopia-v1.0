﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Security;

namespace SCTMis.Services
{
    public class CoResponsibilityServices
    {

        //ProduceForm2A
        //ProduceForm2A1(ProduceForm2A newModel, out string strErrMessage)
        public bool ProduceForm2hagshaA(ProduceForm2A newModel, out string strErrMessage)
        {
            strErrMessage = string.Empty;
            try
            {

                Tuple<List<AdminStructure>, List<Form2AMatrix>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<AdminStructure, Form2AMatrix>(";Exec getForm2AMatrix @KebeleID",
                    new
                    {
                        KebeleID = newModel.KebeleID
                    });

                DataTable dtAdminStructure = SCTMis.Services.GeneralServices.ToDataTable<AdminStructure>(resultset.Item1);
                foreach (DataRow dr in dtAdminStructure.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }
                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form2AMatrix>(resultset.Item2);
                if (dtDetails.Rows.Count > 0)
                {
                    string filename = DateTime.Now.ToFileTime().ToString();
                    filename = filename + ".pdf";
                    //filename = dtAdminStructure.Rows[0]["KebeleName"] + filename + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form2AMatrixRptFile.rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                    myReportDocument.Database.Tables[1].SetDataSource(dtAdminStructure);


                    //myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                    //myReportDocument.Database.Tables[1].SetDataSource(dtAdminStructure);

                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                    //OTHER SETTINGS TO GO HERE
                    List<RecordCount> isSuccess = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm2A @KebeleID,@ReportFileName,@CreatedBy",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             ReportFileName = filename,
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                }
                else
                {
                    strErrMessage = "No data to produce report.";
                }
                return true;
            }
            catch (Exception e)
            {
                strErrMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool ProduceForm2A(ProduceForm2A newModel, out string strErrMessage)
        {
            strErrMessage = string.Empty;
            string strRptPath = string.Empty;
            string filename = string.Empty;
            string strReportFilePath = string.Empty;
            try
            {

                List<Form2AMatrix> resultset = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form2AMatrix>(";Exec getForm2AMatrix @KebeleID",
                    new
                    {
                        KebeleID = newModel.KebeleID
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form2AMatrix>(resultset);
                if (dtDetails.Rows.Count > 0)
                {

                    filename = DateTime.Now.ToFileTime().ToString();
                    filename = filename + ".pdf";

                    strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form2AMatrixRptFile.rpt");
                    strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                    myReportDocument.SetParameterValue("WoredaName", dtDetails.Rows[0]["WoredaName"]);
                    myReportDocument.SetParameterValue("KebeleName", dtDetails.Rows[0]["KebeleName"]);
                    myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();


                    //OTHER SETTINGS TO GO HERE
                    List<RecordCount> isSuccess = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm2A @KebeleID,@ReportFileName,@CreatedBy",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             ReportFileName = filename,
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                }
                else
                {
                    strErrMessage = "No data to produce report.";
                }
                return true;
            }
            catch (Exception e)
            {
                SCTMis.Services.GeneralServices.WriteTrxLog(e.InnerException.ToString());
                SCTMis.Services.GeneralServices.WriteTrxLog(strReportFilePath);
                strErrMessage = e.Message;

                GeneralServices.LogError(e);
                return false;
            }
        }
        //ProduceForm2B
        public bool ProduceForm2B(ProduceForm2B newModel, out string strErrMessage)
        {
            strErrMessage = string.Empty;
            try
            {

                Tuple<List<AdminStructure>, List<Form2BMatrix>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<AdminStructure, Form2BMatrix>(";Exec getForm2BMatrix @KebeleID",
                    new
                    {
                        KebeleID = newModel.KebeleID
                    });

                DataTable dtAdminStructure = SCTMis.Services.GeneralServices.ToDataTable<AdminStructure>(resultset.Item1);
                foreach (DataRow dr in dtAdminStructure.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }
                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form2BMatrix>(resultset.Item2);
                if (dtDetails.Rows.Count > 0)
                {
                    string filename = DateTime.Now.ToFileTime().ToString();
                    filename = filename + ".pdf";
                    //filename = dtAdminStructure.Rows[0]["KebeleName"] + filename + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form2BMatrixRptFile.rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                    myReportDocument.Database.Tables[1].SetDataSource(dtAdminStructure);
                    myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                //OTHER SETTINGS TO GO HERE
                    List<RecordCount> isSuccess = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm2B @KebeleID,@ReportFileName,@CreatedBy",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             ReportFileName = filename,
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                }
                else
                {
                    strErrMessage = "No data to produce report.";
                }
                return true;
            }
            catch (Exception e)
            {
                strErrMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        //ProduceForm2B2
        public bool ProduceForm2B2(ProduceForm2B2 newModel, out string strErrMessage)
        {
            strErrMessage = string.Empty;
            try
            {
                Tuple<List<AdminStructure>, List<Form2B2Matrix>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<AdminStructure, Form2B2Matrix>(";Exec getForm2B2Matrix @KebeleID",
                    new
                    {
                        KebeleID = newModel.KebeleID
                    });

                DataTable dtAdminStructure = SCTMis.Services.GeneralServices.ToDataTable<AdminStructure>(resultset.Item1);
                foreach (DataRow dr in dtAdminStructure.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }
                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form2B2Matrix>(resultset.Item2);
                if (dtDetails.Rows.Count > 0)
                {
                    string filename = DateTime.Now.ToFileTime().ToString();
                    filename = filename + ".pdf";
                    //filename = dtAdminStructure.Rows[0]["KebeleName"] + filename + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form2B2MatrixRptFile.rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                    myReportDocument.Database.Tables[1].SetDataSource(dtAdminStructure);

                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                    //OTHER SETTINGS TO GO HERE
                    List<RecordCount> isSuccess = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm2B2 @KebeleID,@ReportFileName,@CreatedBy",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             ReportFileName = filename,
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                }
                else
                {
                    strErrMessage = "No data to produce report.";
                }
                return true;
            }
            catch (Exception e)
            {
                strErrMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //ProduceForm2C
        public bool ProduceForm2C(ProduceForm2C newModel, out string strErrMessage)
        {
            strErrMessage = string.Empty;
            try
            {

                Tuple<List<AdminStructure>, List<Form2CMatrix>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<AdminStructure, Form2CMatrix>(";Exec getForm2CMatrix @KebeleID",
                    new
                    {
                        KebeleID = newModel.KebeleID
                    });

                DataTable dtAdminStructure = SCTMis.Services.GeneralServices.ToDataTable<AdminStructure>(resultset.Item1);
                foreach (DataRow dr in dtAdminStructure.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }
                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form2CMatrix>(resultset.Item2);
                if (dtDetails.Rows.Count > 0)
                {
                    string filename = DateTime.Now.ToFileTime().ToString();
                    filename = filename + ".pdf";
                    //filename = dtAdminStructure.Rows[0]["KebeleName"] + filename + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form2CMatrixRptFile.rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                    myReportDocument.Database.Tables[1].SetDataSource(dtAdminStructure);

                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                    //OTHER SETTINGS TO GO HERE
                    List<RecordCount> isSuccess = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm2C @KebeleID,@ReportFileName,@CreatedBy",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             ReportFileName = filename,
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                }
                else
                {
                    strErrMessage = "No data to produce report.";
                }
                return true;
            }
            catch (Exception e)
            {
                strErrMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //CHECKLISTS
        public bool ProduceForm3A(ProduceForm3A newModel, out string strErrMessage)
        {
            strErrMessage = string.Empty;
            try
            {
                List<Form3AChecklist> form3aChecklist = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form3AChecklist>(";Exec getForm3ACheclist @KebeleID",
                    new
                    {
                        KebeleID = newModel.KebeleID
                    });

                DataTable dsResults = SCTMis.Services.GeneralServices.ToDataTable<Form3AChecklist>(form3aChecklist);
                foreach (DataRow dr in dsResults.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }
                if (form3aChecklist.Count > 0)
                {
                    string filename = DateTime.Now.ToFileTime().ToString();

                    filename = dsResults.Rows[0]["Kebele"] + filename + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form3AChecklistRptFile.rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dsResults);
                    myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                    //OTHER SETTINGS TO GO HERE
                    List<RecordCount> isSuccess = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm3A @KebeleID,@ReportFileName,@CreatedBy",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             ReportFileName = filename,
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                }
                else
                {
                    strErrMessage = "No data to produce report.";
                }
                return true;
            }
            catch (Exception e)
            {
                strErrMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //ProduceForm3B
        public bool ProduceForm3B(ProduceForm3B newModel, out string strErrMessage)
        {
            strErrMessage = string.Empty;
            try
            {
                List<Form3BChecklist> form2bChecklist = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form3BChecklist>(";Exec getForm3BCheclist @KebeleID",
                    new
                    {
                        KebeleID = newModel.KebeleID
                    });

                DataTable dsResults = SCTMis.Services.GeneralServices.ToDataTable<Form3BChecklist>(form2bChecklist);
                foreach (DataRow dr in dsResults.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                if (dsResults.Rows.Count > 0)
                {
                    string filename = DateTime.Now.ToFileTime().ToString();

                    filename = dsResults.Rows[0]["Kebele"] + filename + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form3BChecklistRptFile.rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dsResults);

                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                    //OTHER SETTINGS TO GO HERE

                    List<RecordCount> isSuccess = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm3B @KebeleID,@ReportFileName,@CreatedBy",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             ReportFileName = filename,
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                }
                else
                {
                    strErrMessage = "No data to produce report.";
                }
                return true;
            }
            catch (Exception e)
            {
                strErrMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //ProduceForm3B2
        public bool ProduceForm3B2(ProduceForm3B2 newModel, out string strErrMessage)
        {
            strErrMessage = string.Empty;
            try
            {
                List<Form3BChecklist> form2bChecklist = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form3BChecklist>(";Exec getForm3B2Checlist @KebeleID",
                    new
                    {
                        KebeleID = newModel.KebeleID
                    });

                DataTable dsResults = SCTMis.Services.GeneralServices.ToDataTable<Form3BChecklist>(form2bChecklist);
                foreach (DataRow dr in dsResults.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                if (dsResults.Rows.Count > 0)
                {
                    string filename = DateTime.Now.ToFileTime().ToString();

                    filename = dsResults.Rows[0]["Kebele"] + filename + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form3B2ChecklistRptFile.rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dsResults);

                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                    //OTHER SETTINGS TO GO HERE

                    List<RecordCount> isSuccess = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm3B2 @KebeleID,@ReportFileName,@CreatedBy",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             ReportFileName = filename,
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                }
                else
                {
                    strErrMessage = "No data to produce report.";
                }
                return true;
            }
            catch (Exception e)
            {
                strErrMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //ProduceForm2C
        public bool ProduceForm3C(ProduceForm3C newModel, out string strErrMessage)
        {
            strErrMessage = string.Empty;
            try
            {
                List<Form3CChecklist> form2cChecklist = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form3CChecklist>(";Exec getForm3CCheclist @KebeleID",
                    new
                    {
                        KebeleID = newModel.KebeleID
                    });

                DataTable dsResults = SCTMis.Services.GeneralServices.ToDataTable<Form3CChecklist>(form2cChecklist);
                foreach (DataRow dr in dsResults.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                if (dsResults.Rows.Count > 0)
                {
                    string filename = DateTime.Now.ToFileTime().ToString();

                    filename = dsResults.Rows[0]["Kebele"] + filename + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form3CChecklistRptFile.rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.FileName = strRptPath;
                    myReportDocument.Database.Tables[0].SetDataSource(dsResults);
                    myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());
                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                    //OTHER SETTINGS TO GO HERE

                    List<RecordCount> isSuccess = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm3C @KebeleID,@ReportFileName,@CreatedBy",
                         new
                         {
                             KebeleID = newModel.KebeleID,
                             ReportFileName = filename,
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                }
                else
                {
                    strErrMessage = "No data to produce report.";
                }
                return true;
            }
            catch (Exception e)
            {
                strErrMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
    }
}
