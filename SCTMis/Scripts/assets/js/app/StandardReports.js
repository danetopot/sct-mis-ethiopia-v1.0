﻿var reportToExportName = '';
var reportToExportLevel = '';

$(document).ready(function () 
{
    
    $('#cmdExportPdf').attr('disabled','disabled');
    $('#cmdExportXls').attr('disabled','disabled');

    $("#RegionID,#WoredaID,#KebeleID,#ReportingPeriod,#ReportName,#ReportDisaggregation,#SocialWorker").prop("disabled", false);
    $("#RegionID,#WoredaID,#KebeleID,#ReportingPeriod,#ReportName,#ReportDisaggregation,#SocialWorker").val('');


    $('#StartDateTDSPLW,#EndDateTDSPLW,#StartDateTDSCMC,#EndDateTDSCMC').Zebra_DatePicker({
        //direction: 1,    // boolean true would've made the date picker future only
        //direction: -1,
        format: 'M/Y',
        view: 'years'
    });
});

$("#cmdProduceReport").click(function () {
    $("#cmdProduceReport").prop("disabled", true);
    $("#progress").show();
    ProduceReport();

});

//$(document).on("change", "#ReportName",function () {
$("#ReportName").change(function () {
    var reportName = $("#ReportName").val();
    ManageFilters(reportName);
});


function ProduceReport() {
    var RegionID = $("#RegionID").val();
    var WoredaID = $("#WoredaID").val();
    var KebeleID = $("#KebeleID").val();
    var ReportingPeriod = $("#ReportingPeriod").val();
    var ReportName = $("#ReportName").val();
    var ReportDisaggregation = $("#ReportDisaggregation").val();

    /* RPT1 Filters */
    var SocialWorker = $("#SocialWorker").val();
    var Gender = $("#Gender").val();
    var Pregnant = $("#Pregnant").val();
    var Lactating = $("#Lactating").val();
    var Handicapped = $("#Handicapped").val();
    var ChronicallyIll = $("#ChronicallyIll").val();
    var NutritionalStatus = $("#NutritionalStatus").val();
    var ChildUnderTSForCMAM = $("#ChildUnderTSForCMAM").val();
    var EnrolledInSchool = $("#EnrolledInSchool").val();
    var ChildProtectionRisk = $("#ChildProtectionRisk").val();
    var CBHIMembership = $("#CBHIMembership").val();

    /* RPT2 Filters */
    var StartDateTDSPLW = $("#StartDateTDSPLW").val();
    var EndDateTDSPLW = $("#EndDateTDSPLW").val();
    var NutritionalStatusPLW = $("#NutritionalStatusPLW").val();
    var NutritionalStatusInfant = $("#NutritionalStatusInfant").val();

    /* RPT3 Filters */
    var StartDateTDSCMC = $("#StartDateTDSCMC").val();
    var EndDateTDSCMC = $("#EndDateTDSCMC").val();
    var MalnourishmentDegree = $("#MalnourishmentDegree").val();

    /* RPT4 Filters */
    var ClientType = $("#ClientType").val();
    var ServiceType = $("#ServiceType").val();

    // Validate ReportName & Report Disaggregation
    var html = '<ul>';
    var errors = [];
    var valid = true;
    if (ReportName == '' || ReportName == null) {
        errors.push('<li><b>Please Select Report Name</b></li>');
        valid = false;                  
    }
    if (ReportDisaggregation == '' || ReportDisaggregation == null) {
        errors.push('<li><b>Please Select Reporting Disaggregation</b></li>');
        valid = false;                  
    }

    if (!valid) 
    {
        html += errors.join('') + '</ul>'
        $('#validationerrors').show();
        $('#validationerrors').empty();
        $('#validationerrors').append(html);
        $("#progress").hide();
        $("#cmdProduceReport").prop("disabled", false);
        return;
    }
    else {
        $("#progress").show();
        $('#validationerrors').hide();
    }

    var rptFilters = {
        RegionId: RegionID,
        WoredaId: WoredaID,
        KebeleId: KebeleID,
        ReportingPeriod: ReportingPeriod,
        ReportName: ReportName,
        ReportDisaggregation: ReportDisaggregation,
        SocialWorker: SocialWorker,
        Gender: Gender,
        Pregnant: Pregnant,
        Lactating: Lactating,
        Handicapped: Handicapped,
        ChronicallyIll: ChronicallyIll,
        NutritionalStatus: NutritionalStatus,
        ChildUnderTSForCMAM: ChildUnderTSForCMAM,
        EnrolledInSchool: EnrolledInSchool,
        ChildProtectionRisk: ChildProtectionRisk,
        CBHIMembership: CBHIMembership,

        StartDateTDSPLW: StartDateTDSPLW,
        EndDateTDSPLW: EndDateTDSPLW,
        NutritionalStatusPLW: NutritionalStatusPLW,
        NutritionalStatusInfant: NutritionalStatusInfant,

        StartDateTDSCMC: StartDateTDSCMC,
        EndDateTDSCMC: EndDateTDSCMC,
        MalnourishmentDegree: MalnourishmentDegree,

        ClientType: ClientType,
        ServiceType: ServiceType
    };
    
    $.ajax({
        type: "POST",
        url: "/Security/StandardReports",
        data: JSON.stringify(rptFilters),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var message =  data.Value;

            $('#success').show();
            $('#success').empty();
            $('#success').append('<ul><li><b>'+ message +'</b></li></ul>');
            $("#progress").hide();
            $("#cmdProduceReport").prop("disabled", false);

            $('#cmdExportPdf').removeAttr('disabled');
            $('#cmdExportXls').removeAttr('disabled');

            reportToExportName = $("#ReportName").val();
            reportToExportLevel = $("#ReportDisaggregation").val();

            previewData();
        },
        error: function (xhr,error, errorThrown) {
            $('#error').show();
            $('#error').empty();
            $('#error').append('<ul><li><b>'+ errorThrown  +'</b></li></ul>');
            $("#progress").hide();
            $("#cmdProduceReport").prop("disabled", false);

            $('#cmdExportPdf').attr('disabled','disabled');
            $('#cmdExportXls').attr('disabled','disabled');

        }
    });
}

function ManageFilters(reportName) {
    ResetFilters();
    
    // RPT1 Filters
    if(reportName=='RPT1'){
        $('#divRpt1Services').show();
        $('#divRpt1Gender').show();
        $('#divRpt1Pregnant').show();
        $('#divRpt1Lactating').show();
        $('#divRpt1Disabled').show();
        $('#divRpt1ChronicallyIll').show();
        $('#divRpt1NutritionalStatus').show();
        $('#divRpt1ChildUnderTSForCMAM').show();
        $('#divRpt1EnrolledInSchool').show();
        $('#divRpt1ChildProtectionRisk').show();
        $('#divRpt1CBHIMembership').show();
    }

    // RPT2 Filters
    if(reportName=='RPT2'){
        $('#divRpt2StartDateTDSPLW').show();
        $('#divRpt2EndDateTDSPLW').show();
        $('#divRpt2NutritionalStatusPLW').show();
        $('#divRpt2NutritionalStatusInfant').show();
        $('#divRpt1ChildProtectionRisk').show();
        $('#divRpt1CBHIMembership').show();
    }

    // RPT3 Filters
    if(reportName=='RPT3'){
        $('#divRpt3MalnourishmentDegree').show();
        $('#divRpt3StartDateTDSCMC').show();
        $('#divRpt3EndDateTDSCMC').show();
        $('#divRpt1ChildProtectionRisk').show();
        $('#divRpt1CBHIMembership').show();
    }

    // RPT4 Filters
    if(reportName=='RPT4'){
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT5 Filters
    if(reportName=='RPT5'){
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT6 Filters
    if(reportName=='RPT6'){
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT7 Filters
    if(reportName=='RPT7'){
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT8 Filters
    if(reportName=='RPT8'){
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT9 Filters
    if(reportName=='RPT9'){
    }

    // RPT10 Filters
    if(reportName=='RPT10'){
    }

}


function ResetFilters() {
    $('#divRpt1Services').hide();
    $('#divRpt1Gender').hide();
    $('#divRpt1Pregnant').hide();
    $('#divRpt1Lactating').hide();
    $('#divRpt1Disabled').hide();
    $('#divRpt1ChronicallyIll').hide();
    $('#divRpt1NutritionalStatus').hide();
    $('#divRpt1ChildUnderTSForCMAM').hide();
    $('#divRpt1EnrolledInSchool').hide();
    $('#divRpt1ChildProtectionRisk').hide();
    $('#divRpt1CBHIMembership').hide();

    $('#divRpt2StartDateTDSPLW').hide();
    $('#divRpt2EndDateTDSPLW').hide();
    $('#divRpt2NutritionalStatusPLW').hide();
    $('#divRpt2NutritionalStatusInfant').hide();

    $('#divRpt3MalnourishmentDegree').hide();
    $('#divRpt3StartDateTDSCMC').hide();
    $('#divRpt3EndDateTDSCMC').hide();

    $('#divRpt4TypeOfClient').hide();
    $('#divRpt4TypeOfService').hide();

    $('#ServiceType').val('');
    $('#Gender').val('-');
    $('#Pregnant').val('');
    $('#Lactating').val('');
    $('#Handicapped').val('');
    $('#ChronicallyIll').val('');
    $('#NutritionalStatus').val('');
    $('#ChildUnderTSForCMAM').val('');
    $('#EnrolledInSchool').val('');
    $('#ChildProtectionRisk').val('');
    $('#CBHIMembership').val('');

    $('#StartDateTDSPLW').val('');
    $('#EndDateTDSPLW').val('');
    $('#NutritionalStatusPLW').val('');
    $('#NutritionalStatusInfant').val('');

    $('#MalnourishmentDegree').val('');
    $('#StartDateTDSCMC').val('');
    $('#EndDateTDSCMC').val('');

    $('#EndDateTDSCMC').val('');
    $('#EndDateTDSCMC').val('');

    $('#ClientType').val('-');
    $('#ServiceType').val('');
}

function previewData() {

    var rptName = $("#ReportName").val();
    var rptLevel= $("#ReportDisaggregation").val();
   
    /* RPT 1 */
    if(rptName=='RPT1')
    {
        if(rptLevel=='DT')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID, ProfileDSHeaderID,ProfileDSHeaderID,Fiscal Year,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'Household Name,PSNP Number,Client<span class="HeaderChange">_</span>Name,Individual ID,DOB,Age,Sex,';
            gridHeader = gridHeader + 'Handicapped,Chronically Ill,Nutritional Status,Under TSF or CMAM,Enrolled In School,School Name,Pregnant,';
            gridHeader = gridHeader + 'Lactating,CCCC/BSPC Member,CBHI Membership,Child Protection,Collection Date,Social Worker';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,0,100,100,100,100,0,100,200,100,200,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);

            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if(rptLevel=='SM')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,ProfileDSHeaderID,Fiscal Year,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'Enrolled In CBHI,Child Protection Cases,Non-Malnuorished,Malnourished,Severely Malnourished,Handicapped,';
            gridHeader = gridHeader + 'Chronically Ill,Lactating,Pregnant,Enrolled In School,Under TSF or CMAM';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,100,100,100,100,0,100,100,100,100,100,100,100,100,100,100,100,100");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    /* RPT2 */
    if(rptName=='RPT2')
    {
        if(rptLevel=='DT')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID, ProfileTDSPLWID, FiscalYear,Region,Woreda,Kebele,KebeleID,Gote,Name Of PLW, PSNP Number, Medical Record Number, PLW Age, StartDate TDS, EndDate TDS,';
            gridHeader = gridHeader + 'Baby DOB, Baby Name, Baby Sex, Nutritional Status Of PLW, Nutritional Status Of Infant,CBHI Membership, Child Protection, CollectionDate, Social Worker';
      
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,100,100,100,100,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if(rptLevel=='SM')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID, ProfileTDSPLWID, Fiscal Year,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'Enrolled In CBHI,Child Protection Cases,Non Malnuorished PLW,Malnourished PLW,Severely Malnourished PLW,';
            gridHeader = gridHeader + 'Non-Malnuorished Infants,Malnourished Infants,Severely Malnourished Infants';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,120,120,120,120,0,0,120,120,120,120,120,120,120,120");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    /* RPT3 */
    if(rptName=='RPT3')
    {
        if(rptLevel=='DT')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID, ProfileTDSPLWID, Fiscal Year,Region,Woreda,Kebele,KebeleID,Gote,Caretaker ID,Name Of CareTaker,PSNP Number,Malnourished Child Name,Malnourished Child Sex,';
            gridHeader = gridHeader + 'ChildID,Child DOB,DateType Certificate,Malnourishment Degree,StartDate TDS,EndDate TDS,Next CN Status Date,CBHI Membership,Child Protection,CCCC/BSPC Member,Collection Date,Social Worker';
      
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,110,110,110,110,0,0,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110,110");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if(rptLevel=='SM')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID, ProfileTDSPLWID, Fiscal Year,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'Enrolled In CBHI,Child Protection Cases,Non-Malnuorished Children,Malnourished Children,Severely Malnourished Children';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,140,140,140,140,0,0,140,140,140,140,140,140");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    /* RPT4 */
    if(rptName=='RPT4')
    {
        if(rptLevel=='DT')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,ProfileHeaderID,Fiscal Year,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'PSNP Number,Individual ID,Client<span class="HeaderChange">_</span>Type,Name OfHouseHold Member,Sex,Age,Pre-AnteNatal Care,Immunization,';
            gridHeader = gridHeader + 'Supplementary Feeding,Monthly GMPS essions,School Enrolment,BCC Sessions,Under TSF or CMAM,Birth Registration,HealthPost CheckUp,';
            gridHeader = gridHeader + 'CBHI Membership,Child Protection,Prenatal Visit 1,Prenatal Visit 3,PostNatal Visit,Collection Date,Social Worker';
      
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,100,100,100,100,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if(rptLevel=='SM')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID, ProfileHeaderID, FiscalYear,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'Pre-AnteNatal Care,Immunization,Supplementary Feeding,Monthly GMP Sessions,School Enrolment,BCC Sessions,';
            gridHeader = gridHeader + 'Under TSF or CMAM,Birth Registration,CBHI Membership,Child Protection,PrenatalVisit 1,PrenatalVisit 3,PostNatal Visit';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,120,120,120,120,0,0,120,120,120,120,120,120,120,120,120,120,120,120,120");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    /* RPT5 & RPT6 */
    if(rptName=='RPT5' || rptName=='RPT6')
    {
        if(rptLevel=='DT')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,ProfileDSHeaderID,Fiscal Year,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'Client<span class="HeaderChange">_</span>Name,Client<span class="HeaderChange">_</span>Type,Individual ID,Age,Sex,Service ID,Service Name,';
            gridHeader = gridHeader + 'Complied,Collection Date,Social Worker';
      
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,200,100,100,100");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if(rptLevel=='SM')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,ProfileDSHeaderID,Fiscal Year,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'Pre-AnteNatal Care,Immunization,Under TSF/CMAM,GMP Sessions,School Enrolment,BCC Sessions,Birth Registration,';
            gridHeader = gridHeader + '3PreNatalCare Visits,1PreNatalCare Visit,CheckUp At Health Post,Supplementary Feeding,CBHI Membership,Child Protection';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    /* RPT7 & RPT8 */
    if(rptName=='RPT7' || rptName=='RPT8')
    {
        if(rptLevel=='DT')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,ProfileDSHeaderID,FiscalYear,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'Individual<span class="HeaderChange">_</span>ID,Client<span class="HeaderChange">_</span>Name,Client<span class="HeaderChange">_</span>Type,Sex,Age,'; 
            gridHeader = gridHeader + 'PreAnteNatal Care FollowUps,Immunization FollowUps,Under TSFCMAM FollowUps,GMP Sessions FollowUps,School Enrolment FollowUps,BCC Sessions FollowUps,Birth Registration FollowUps,';
            gridHeader = gridHeader + '3PreNatal Care Visits FollowUps,1PreNatal Care Visit FollowUps,CheckUp At Health Post FollowUps,Supplementary Feeding FollowUps,CBHI Membership FollowUps,Child Protection FollowUps';
      
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if(rptLevel=='SM')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,Fiscal Year,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'PreAnteNatal Care FollowUps,Immunization FollowUps,Under TSFCMAM FollowUps,GMP Sessions FollowUps,School Enrolment FollowUps,BCC Sessions FollowUps,Birth Registration FollowUps,';
            gridHeader = gridHeader + '3PreNatal Care Visits FollowUps,1PreNatal Care Visit FollowUps,CheckUp At Health Post FollowUps,Supplementary Feeding FollowUps,CBHI Membership FollowUps,Child Protection FollowUps';
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }

    /* RPT9 & RPT10 */
    if(rptName=='RPT9' || rptName=='RPT10')
    {
        if(rptLevel=='DT')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,ProfileHeaderID,FiscalYear,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'PSNP Number,Individual<span class="HeaderChange">_</span>ID,Client<span class="HeaderChange">_</span>Type,Client<span class="HeaderChange">_</span>Name,Sex,Age,PreAnteNatal Care,Immunization,';
            gridHeader = gridHeader + 'Supplementary Feeding,Monthly<span class="HeaderChange">_</span>GMP Sessions,School Enrolment,BCC Sessions,Under TSFCMAM,Birth Registration,HealthPost CheckUp,';
            gridHeader = gridHeader + 'CBHI Membership,Child Protection,1PreNatal Visit,3PreNatal Visit,PostNatal Visit,Collection Date,Social Worker';
      
            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }

        if(rptLevel=='SM')
        {
            $('#loading').hide();
            var page_count = 10;
            var gridHeader = '';
            var gridColType = '';
            var mygrid;
            var gridHeader = '';
            var gridColType = '';
            gridHeader = gridHeader + 'UniqueID,ProfileHeaderID,FiscalYear,Region,Woreda,Kebele,KebeleID,Gote,';
            gridHeader = gridHeader + 'PreAnteNatal Care,Immunization,Supplementary Feeding,Monthly GMP Sessions,School Enrolment,BCC Sessions,Under TSFCMAM,';
            gridHeader = gridHeader + 'Birth Registration,Health PostCheckUp,CBHI Membership,Child Protection,1PreNatalVisit,3PreNatalVisits,PostNatal Visit';

            gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';
            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120,120");
            mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            mygrid.setHeader(gridHeader);
            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.enableAutoWidth(true);
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");
            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            var currentPage = $('#currentPage').val();
            mygrid.load("/Security/StandardReportsPreview?RecCount=" + page_count + "&Report=" + rptName + "&Level=" + rptLevel,
            function() {
                mygrid.changePage(currentPage);
            });
            
            mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
                    $('#currentPage').val(ind);
                    $.ajax({
                        type: "POST",
                        url: "../Security/UpdateCurrentPage",
                        data: "{ 'CurrentPage':" + ind + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8"
                    });
                }); 
            dhtmlxError.catchError("ALL", my_error_handler);
        }
    }


    ResetFilters();
    
    $('#ReportName').val('');
    $('#ReportDisaggregation').val('');
    $('#ReportingPeriod').val('');
    $('#RegionID').val('');
    $('#WoredaID').val('');
    $('#KebeleID').val('');
    $('#SocialWorker').val('');
}

function ExportReport(reportFormat) {
    var rptParams = {
        rptName: reportToExportName,
        rptLevel: reportToExportLevel,
        rptFormat: reportFormat
    }
    
    $('#exportProgress').show();
    $.ajax({
        type: "POST",
        url: "/Security/StandardReportsExport",
        data: JSON.stringify(rptParams),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $('#exportProgress').hide();
            window.location.href = "../Security/StandardReportsDownload?path=" + data.Path + "&filename=" + data.FileName + "&format=" + data.Format;
        },
        error: function (xhr,error, errorThrown) {
            console.log(errorThrown);
        }
    });
}