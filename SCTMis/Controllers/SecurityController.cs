﻿using SCTMis.Models;
using SCTMis.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml.Linq;
using System.Data;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

using System.IO;
using CsvHelper;
using System.Web;
using Newtonsoft.Json;

namespace SCTMis.Controllers
{
    [SessionConfig.SessionExpireFilter]
    public class SecurityController : Controller
    {
        //
        // GET: /Security/

        AccountServices dbService = new AccountServices();
        GeneralServices dbGeneral = new GeneralServices();
        ReportServices dbReport = new ReportServices();
        static int _currentPage = 0;
        string ip = System.Web.HttpContext.Current.Request.UserHostAddress;

        // Handling Standard Reports
        [HttpPost]
        public void UpdateCurrentPage(PageParams pageParam)
        {
            _currentPage = pageParam.CurrentPage;
        }

        [HttpGet]
        public ActionResult StandardReports()
        {
            ViewBag.Subtitle = "Standard Reports";
            ViewBag.CurrentPage = _currentPage;

            StandardReportModel models = new Models.StandardReportModel();

            models.regions = dbGeneral.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbGeneral.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbGeneral.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.rptTypes = dbGeneral.GetStandardReportTypes();
            models.rptPeriod = dbGeneral.GetReportingPeriods();
            models.rptDisaggregation = dbGeneral.GetReportDisaggregationTypes();
            models.services = dbGeneral.GetServiceTypes();
            models.clients = dbGeneral.GetClientTypes();
            models.sex = dbGeneral.GetGenders();
            models.worker = dbGeneral.GetSocialWorkers();
            models.yesnos = dbGeneral.GetYesNo();
            models.nutrStatus = dbGeneral.GetNutritionalStatus();

            dbService.InsertAuditTrail(User.Identity.Name, "Standard Reports", "View", ip, "",
            "Accessed Standard Reports Module at " + DateTime.Now.ToString());

            return View(models);
        }

        [Authorize]
        [HttpPost]
        public JsonResult StandardReports(StandardReportModel standardReportModel)
        {
            string Result = null;
            string Message = null;
            ViewBag.Subtitle = "Standard Reports";

            try
            {
                int? regionID = standardReportModel.RegionID;
                int? woredaID = standardReportModel.WoredaID;
                int? kebeleID = standardReportModel.KebeleID;
                string rptType = standardReportModel.ReportName;
                string rptPeriod = standardReportModel.ReportingPeriod;
                string rptDisaggregation = standardReportModel.ReportDisaggregation;
                string socialWorker = standardReportModel.SocialWorker;

                string sex = standardReportModel.Gender;
                string pregnant = standardReportModel.Pregnant;
                string lactating = standardReportModel.Lactating;
                string handicapped = standardReportModel.Handicapped;
                string chronicallyIll = standardReportModel.ChronicallyIll;
                string nutrStatus = standardReportModel.NutritionalStatus;
                string childUnderTSForCMAM = standardReportModel.ChildUnderTSForCMAM;
                string enrolledInSchool = standardReportModel.EnrolledInSchool;
                string childProtectionRisk = standardReportModel.ChildProtectionRisk;
                string cbhiMembership = standardReportModel.CBHIMembership;

                DateTime startDateTDS = standardReportModel.StartDateTDSPLW;
                DateTime endDateTDS = standardReportModel.EndDateTDSPLW;
                string nutrStatusPLW = standardReportModel.NutritionalStatusPLW;
                string nutrStatusInfant = standardReportModel.NutritionalStatusInfant;

                DateTime startDateCMC = standardReportModel.StartDateTDSCMC;
                DateTime endDateCMC = standardReportModel.EndDateTDSCMC;
                string malnutrDegree = standardReportModel.MalnourishmentDegree;

                string clientType = standardReportModel.ClientType;
                string serviceType = standardReportModel.ServiceType;

                string[] rpt1Filters = new string[] {
                    sex, pregnant, lactating, handicapped, chronicallyIll,
                    nutrStatus, childUnderTSForCMAM, enrolledInSchool,
                    childProtectionRisk, cbhiMembership
                };

                string[] rpt2Filters = new string[] {
                    startDateTDS.ToString("MMM/yyyy"), endDateTDS.ToString("MMM/yyyy"), nutrStatusPLW, nutrStatusInfant,
                    childProtectionRisk, cbhiMembership
                };

                string[] rpt3Filters = new string[] {
                    startDateTDS.ToString("MMM/yyyy"), endDateTDS.ToString("MMM/yyyy"), malnutrDegree,
                    childProtectionRisk, cbhiMembership
                };

                string[] rpt4Filters = new string[] {
                    clientType, serviceType
                };

                string[] rpt5Filters = new string[] {
                    clientType, serviceType
                };

                string[] rpt6Filters = new string[] {
                    clientType, serviceType
                };

                string[] rpt7Filters = new string[] {
                    clientType, serviceType
                };

                string[] rpt8Filters = new string[] {
                    clientType, serviceType
                };

                string[] rpt9Filters = new string[] {
                    clientType, serviceType
                };

                string[] rpt10Filters = new string[] {
                    clientType, serviceType
                };


                Dictionary<string, string> myDict = dbReport.GenerateReport(
                    User.Identity.Name, regionID,woredaID,kebeleID,rptType,rptDisaggregation,rptPeriod,socialWorker,
                    rpt1Filters, rpt2Filters, rpt3Filters, rpt4Filters, rpt5Filters, rpt6Filters, rpt7Filters, rpt8Filters, 
                    rpt9Filters, rpt10Filters
                    );

                foreach (KeyValuePair<string, string> kvp in myDict)
                {
                    Result = kvp.Key;
                    Message = kvp.Value;
                }
                return Json(new { Key = Result, Value = Message });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = "Error Generating Report :- " + ex.Message });
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument StandardReportsListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = GeneralServices.strQueryString("SearchKeyword", string.Empty);

                Tuple<List<int>, List<ReportLogModel>> resultset
                    = GeneralServices.NpocoConnection().FetchMultiple<int, ReportLogModel>(";Exec GetReportingLogList  @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });

                ViewBag.CurrentPage = _currentPage;
                DataTable dt = GeneralServices.ToDataTable<ReportLogModel>(resultset.Item2);
                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 8);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        [HttpGet]
        public ActionResult ProduceStandardReports()
        {
            ViewBag.Subtitle = "Standard Reports";

            StandardReportModel models = new Models.StandardReportModel();

            models.regions = dbGeneral.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbGeneral.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbGeneral.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.rptTypes = dbGeneral.GetStandardReportTypes();
            models.rptPeriod = dbGeneral.GetReportingPeriods();
            models.rptDisaggregation = dbGeneral.GetReportDisaggregationTypes();
            models.services = dbGeneral.GetServiceTypes();
            models.clients = dbGeneral.GetClientTypes();
            models.sex = dbGeneral.GetGenders();
            models.worker = dbGeneral.GetSocialWorkers();
            models.yesnos = dbGeneral.GetYesNo();
            models.nutrStatus = dbGeneral.GetNutritionalStatus();

            dbService.InsertAuditTrail(User.Identity.Name, "Standard Reporting", "View", ip, "",
            "Accessed Standard Reports Module at " + DateTime.Now.ToString());

            return View(models);
        }

        [HttpGet]
        [Authorize]
        public XDocument StandardReportsPreview()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");
            ViewBag.CurrentPage = _currentPage;

            try
            {
                int jtStartIndex = GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = GeneralServices.strQueryString("SearchKeyword", string.Empty);
                string report = GeneralServices.strQueryString("Report", string.Empty);
                string level = GeneralServices.strQueryString("Level", string.Empty);
                string sb = string.Empty;
                DataTable dt;

                if (report=="RPT1")
                {

                    Tuple<List<HouseholdProfilePDSDetailReport>, List<int>, List<HouseholdProfilePDSSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfilePDSDetailReport,int, HouseholdProfilePDSSummaryReport, int>(
                        ";Exec GetStandardRpt1  @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                        new
                        {
                            jtStartIndex = jtStartIndex,
                            jtPageSize = jtPageSize,
                            isSearch = jtisSearch,
                            SearchTypeID = jtSearchTypeID,
                            SearchKeyword = jtSearchKeyword
                        });
                    
                    if (level == "DT")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfilePDSDetailReport>(resultset.Item1);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item2[0], jtStartIndex, 0, 9);
                    }
                    if (level == "SM")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfilePDSSummaryReport>(resultset.Item3);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item4[0], jtStartIndex, 0, 9);
                    }
                }

                if (report == "RPT2")
                {

                    Tuple<List<HouseholdProfileTDSPLWDetailReport>, List<int>, List<HouseholdProfileTDSPLWSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSPLWDetailReport, int, HouseholdProfileTDSPLWSummaryReport, int>(
                        ";Exec GetStandardRpt2  @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                        new
                        {
                            jtStartIndex = jtStartIndex,
                            jtPageSize = jtPageSize,
                            isSearch = jtisSearch,
                            SearchTypeID = jtSearchTypeID,
                            SearchKeyword = jtSearchKeyword
                        });

                    if (level == "DT")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileTDSPLWDetailReport>(resultset.Item1);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item2[0], jtStartIndex, 0, 9);
                    }
                    if (level == "SM")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileTDSPLWSummaryReport>(resultset.Item3);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item4[0], jtStartIndex, 0, 9);
                    }
                }

                if (report == "RPT3")
                {

                    Tuple<List<HouseholdProfileTDSCMCDetailReport>, List<int>, List<HouseholdProfileTDSCMCSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSCMCDetailReport, int, HouseholdProfileTDSCMCSummaryReport, int>(
                        ";Exec GetStandardRpt3  @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                        new
                        {
                            jtStartIndex = jtStartIndex,
                            jtPageSize = jtPageSize,
                            isSearch = jtisSearch,
                            SearchTypeID = jtSearchTypeID,
                            SearchKeyword = jtSearchKeyword
                        });

                    if (level == "DT")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileTDSCMCDetailReport>(resultset.Item1);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item2[0], jtStartIndex, 0, 9);
                    }
                    if (level == "SM")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileTDSCMCSummaryReport>(resultset.Item3);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item4[0], jtStartIndex, 0, 9);
                    }
                }

                if (report == "RPT4")
                {

                    Tuple<List<SocialServicesNeedsDetailReport>, List<int>, List<SocialServicesNeedsSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<SocialServicesNeedsDetailReport, int, SocialServicesNeedsSummaryReport, int>(
                        ";Exec GetStandardRpt4  @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                        new
                        {
                            jtStartIndex = jtStartIndex,
                            jtPageSize = jtPageSize,
                            isSearch = jtisSearch,
                            SearchTypeID = jtSearchTypeID,
                            SearchKeyword = jtSearchKeyword
                        });

                    if (level == "DT")
                    {
                        dt = GeneralServices.ToDataTable<SocialServicesNeedsDetailReport>(resultset.Item1);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item2[0], jtStartIndex, 0, 9);
                    }
                    if (level == "SM")
                    {
                        dt = GeneralServices.ToDataTable<SocialServicesNeedsSummaryReport>(resultset.Item3);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item4[0], jtStartIndex, 0, 9);
                    }
                }

                if (report == "RPT5")
                {

                    Tuple<List<ServiceComplianceDetailReport>, List<int>, List<ServiceComplianceSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<ServiceComplianceDetailReport, int, ServiceComplianceSummaryReport, int>(
                        ";Exec GetStandardRpt5  @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                        new
                        {
                            jtStartIndex = jtStartIndex,
                            jtPageSize = jtPageSize,
                            isSearch = jtisSearch,
                            SearchTypeID = jtSearchTypeID,
                            SearchKeyword = jtSearchKeyword
                        });

                    if (level == "DT")
                    {
                        dt = GeneralServices.ToDataTable<ServiceComplianceDetailReport>(resultset.Item1);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item2[0], jtStartIndex, 0, 9);
                    }
                    if (level == "SM")
                    {
                        dt = GeneralServices.ToDataTable<ServiceComplianceSummaryReport>(resultset.Item3);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item4[0], jtStartIndex, 0, 9);
                    }
                }

                if (report == "RPT6")
                {

                    Tuple<List<ServiceComplianceDetailReport>, List<int>, List<ServiceComplianceSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<ServiceComplianceDetailReport, int, ServiceComplianceSummaryReport, int>(
                        ";Exec GetStandardRpt6 @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                        new
                        {
                            jtStartIndex = jtStartIndex,
                            jtPageSize = jtPageSize,
                            isSearch = jtisSearch,
                            SearchTypeID = jtSearchTypeID,
                            SearchKeyword = jtSearchKeyword
                        });

                    if (level == "DT")
                    {
                        dt = GeneralServices.ToDataTable<ServiceComplianceDetailReport>(resultset.Item1);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item2[0], jtStartIndex, 0, 9);
                    }
                    if (level == "SM")
                    {
                        dt = GeneralServices.ToDataTable<ServiceComplianceSummaryReport>(resultset.Item3);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item4[0], jtStartIndex, 0, 9);
                    }
                }

                if (report == "RPT7")
                {

                    Tuple<List<CaseManagementComplianceDetailReport>, List<int>, List<CaseManagementComplianceSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<CaseManagementComplianceDetailReport, int, CaseManagementComplianceSummaryReport, int>(
                        ";Exec GetStandardRpt7 @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                        new
                        {
                            jtStartIndex = jtStartIndex,
                            jtPageSize = jtPageSize,
                            isSearch = jtisSearch,
                            SearchTypeID = jtSearchTypeID,
                            SearchKeyword = jtSearchKeyword
                        });

                    if (level == "DT")
                    {
                        dt = GeneralServices.ToDataTable<CaseManagementComplianceDetailReport>(resultset.Item1);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item2[0], jtStartIndex, 0, 9);
                    }
                    if (level == "SM")
                    {
                        dt = GeneralServices.ToDataTable<CaseManagementComplianceSummaryReport>(resultset.Item3);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item4[0], jtStartIndex, 0, 9);
                    }
                }

                if (report == "RPT8")
                {

                    Tuple<List<CaseManagementComplianceDetailReport>, List<int>, List<CaseManagementComplianceSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<CaseManagementComplianceDetailReport, int, CaseManagementComplianceSummaryReport, int>(
                        ";Exec GetStandardRpt8 @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                        new
                        {
                            jtStartIndex = jtStartIndex,
                            jtPageSize = jtPageSize,
                            isSearch = jtisSearch,
                            SearchTypeID = jtSearchTypeID,
                            SearchKeyword = jtSearchKeyword
                        });

                    if (level == "DT")
                    {
                        dt = GeneralServices.ToDataTable<CaseManagementComplianceDetailReport>(resultset.Item1);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item2[0], jtStartIndex, 0, 9);
                    }
                    if (level == "SM")
                    {
                        dt = GeneralServices.ToDataTable<CaseManagementComplianceSummaryReport>(resultset.Item3);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item4[0], jtStartIndex, 0, 9);
                    }
                }

                if (report == "RPT9")
                {

                    Tuple<List<RetargetingDetailReport>, List<int>, List<RetargetingSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<RetargetingDetailReport, int, RetargetingSummaryReport, int>(
                        ";Exec GetStandardRpt9 @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                        new
                        {
                            jtStartIndex = jtStartIndex,
                            jtPageSize = jtPageSize,
                            isSearch = jtisSearch,
                            SearchTypeID = jtSearchTypeID,
                            SearchKeyword = jtSearchKeyword
                        });

                    if (level == "DT")
                    {
                        dt = GeneralServices.ToDataTable<RetargetingDetailReport>(resultset.Item1);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item2[0], jtStartIndex, 0, 9);
                    }
                    if (level == "SM")
                    {
                        dt = GeneralServices.ToDataTable<RetargetingSummaryReport>(resultset.Item3);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item4[0], jtStartIndex, 0, 9);
                    }
                }

                if (report == "RPT10")
                {

                    Tuple<List<RetargetingDetailReport>, List<int>, List<RetargetingSummaryReport>, List<int>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<RetargetingDetailReport, int, RetargetingSummaryReport, int>(
                        ";Exec GetStandardRpt10 @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                        new
                        {
                            jtStartIndex = jtStartIndex,
                            jtPageSize = jtPageSize,
                            isSearch = jtisSearch,
                            SearchTypeID = jtSearchTypeID,
                            SearchKeyword = jtSearchKeyword
                        });

                    if (level == "DT")
                    {
                        dt = GeneralServices.ToDataTable<RetargetingDetailReport>(resultset.Item1);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item2[0], jtStartIndex, 0, 9);
                    }
                    if (level == "SM")
                    {
                        dt = GeneralServices.ToDataTable<RetargetingSummaryReport>(resultset.Item3);
                        sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item4[0], jtStartIndex, 0, 9);
                    }
                }

                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult StandardReportsExport(string rptName, string rptLevel, string rptFormat)
        {
            try
            {
                string[] report = dbReport.ExportReport(rptName, rptLevel, rptFormat);

                dbService.InsertAuditTrail(User.Identity.Name, "Standard Reporting", "Print", ip, "",
                "Accessed Standard Reports Module at " + DateTime.Now.ToString());

                return Json(new { Path = report[0], FileName = report[1], Format = rptFormat }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            return Json(string.Empty, JsonRequestBehavior.AllowGet);
            
        }

        [Authorize]
        [HttpGet]
        public FileResult StandardReportsDownload()
        {
            try
            {
                string rptPath = GeneralServices.strQueryString("path", string.Empty);
                string rptName = GeneralServices.strQueryString("filename", string.Empty);
                string rptFormat = GeneralServices.strQueryString("format", string.Empty);

                byte[] fileBytes = System.IO.File.ReadAllBytes(rptPath);

                if (rptFormat == "xls") {
                    return File(fileBytes, "application/vnd.ms-excel", rptName);
                }
                if (rptFormat == "pdf"){
                    return File(fileBytes, "application/pdf", rptName );
                }
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
            }

            return null;
        }

        [HttpGet]
        public ActionResult PrintStdRptPdf(string strReportFilePath)
        {
            byte[] filedata = System.IO.File.ReadAllBytes(strReportFilePath);
            string contentType = MimeMapping.GetMimeMapping(strReportFilePath);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "TESTING123.pdf",
                Inline = true,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(filedata, contentType);
        }

        public ActionResult ReportEngine()
        {
            ViewBag.Subtitle = "Report Engine";

            ReportEngineModel models = new Models.ReportEngineModel();

            models.regions = dbGeneral.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbGeneral.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());
            models.rptTypes = dbGeneral.GetReportTypes();
            models.reportgperiod = dbGeneral.GetReportingPeriods();
            models.LocationType = dbGeneral.GetLocationType();

            dbService.InsertAuditTrail(User.Identity.Name, "Reporting", "View", ip, "",
            "Accessed Report Engine Module at " + DateTime.Now.ToString());
            
            return View(models);
        }

        public ActionResult ReportExport()
        {
            ViewBag.Subtitle = "Report Export";

            ReportExportModel models = new Models.ReportExportModel();

            models.regions = dbGeneral.GetDashRegions();
            models.woredas = dbGeneral.GetDashWoredas(int.Parse(Session["RegionID"].ToString()));
            models.kebeles = dbGeneral.GetDashKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.rptTypes = dbGeneral.GetExportReportTypes();
            models.fiscalYears = dbGeneral.GetFiscalYears(null);

            dbService.InsertAuditTrail(User.Identity.Name, "Reporting", "View", ip, "",
            "Accessed Report Export Module at " + DateTime.Now.ToString());

            return View(models);
        }

        //ReportGenerator
        [HttpPost]
        [Authorize]
        public ActionResult PrintPdf(ReportEngineModel model)
        {            
            string errMessage = string.Empty;

            MonitoringServices monit = new MonitoringServices();

            dbService.InsertAuditTrail(User.Identity.Name, "Downloads", "View", ip, "",
            "Previewed a downloadable file " + model.ReportName + " at " + DateTime.Now.ToString());

            var RptNameDetails = monit.PdfReport(model, Membership.GetUser().UserName.ToString(), out errMessage);
            return Json(RptNameDetails, JsonRequestBehavior.AllowGet);
        }

        public FileResult PrintForm6(ReportEngineModel model)
        {
            string rptFileName = string.Empty;
            string rptSP = string.Empty;
            string errMessage = string.Empty;
            string reportType = string.Empty;

            var locationType = new GeneralServices().GetLocationType();

            switch (locationType)
            {
                case (int)LocationTypes.Regional:
                    reportType = "Region";

                    break;

                case (int)LocationTypes.Federal:
                    reportType = "Federal";

                    break;
            }

            try
            {
                switch (model.ReportName)
                {
                    case "6A":
                        rptFileName = string.Format("Form6AReportFile{0}", reportType);
                        rptSP = "getForm6AReport";
                        break;
                    case "6B":
                        rptFileName = string.Format("Form6BReportFile{0}", reportType);
                        rptSP = "getForm6BReport";
                        break;
                    case "6C":
                        rptFileName = string.Format("Form6CReportFile{0}", reportType);
                        rptSP = "getForm6CReport";
                        break;
                }

                Tuple<List<Form6ReportHeader>, List<Form6Report>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<Form6ReportHeader, Form6Report>(";Exec " + rptSP + " @RegionID, @WoredaID, @ReportingPeriod",
                    new
                    {
                        RegionID = model.RegionID > 0 ? model.RegionID : null,
                        WoredaID = model.WoredaID > 0 ? model.WoredaID : null,
                        ReportingPeriod = model.ReportingPeriod
                    });

                DataTable dtAdminStructure = SCTMis.Services.GeneralServices.ToDataTable<Form6ReportHeader>(resultset.Item1);
                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form6Report>(resultset.Item2);

                if (dtDetails.Rows.Count > 0)
                {

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/" + rptFileName + ".rpt");

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    if (model.ReportName == "6A")
                    {
                        myReportDocument.Database.Tables[1].SetDataSource(dtDetails);
                        myReportDocument.Database.Tables[0].SetDataSource(dtAdminStructure);
                    }
                    else
                    {
                        myReportDocument.Database.Tables[0].SetDataSource(dtAdminStructure);
                        myReportDocument.Database.Tables[1].SetDataSource(dtDetails);
                    }

                    myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                    var filename = DateTime.Now.ToFileTime().ToString();
                    filename = filename + ".pdf";
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                    return File(strReportFilePath, "application/pdf");
                }
            }
            catch (Exception ee)
            {
                errMessage = ee.Message;
                GeneralServices.LogError(ee);
            }

            return null;
        }

        [HttpPost]
        [Authorize]
        public FileResult PrintExcel(ReportExportModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string errMessage = string.Empty;
                    string rptFileName = string.Empty;
                    MonitoringServices monit = new MonitoringServices();

                    dbService.InsertAuditTrail(User.Identity.Name, "Export", "View", ip, "",
                    "Previewed a exportable file " + model.ReportName + " at " + DateTime.Now.ToString());

                    var RptNameDetails = monit.ExcelExportReport(model, Membership.GetUser().UserName.ToString(), out errMessage, out rptFileName);

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + rptFileName);

                    byte[] fileBytes = System.IO.File.ReadAllBytes(strRptPath);
                    return File(fileBytes, "application/vnd.ms-excel", rptFileName);
                }

                RedirectToAction("ReportExport");
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                model.regions = dbGeneral.GetDashRegions();
                model.woredas = dbGeneral.GetDashWoredas(int.Parse(Session["RegionID"].ToString()));
                model.kebeles = dbGeneral.GetDashKebeles(int.Parse(Session["WoredaID"].ToString()));
                model.rptTypes = dbGeneral.GetExportReportTypes();
                model.fiscalYears = dbGeneral.GetFiscalYears(null);

                ModelState.AddModelError("", ex.Message);

                RedirectToAction("ReportExport");
            }

            return null;            
        }

        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult AuditTrail()
        {
            ViewBag.Subtitle = "Audit Trail";
            return View();
        }


        [Authorize]
        public ActionResult Roles(RegisterRoleModel model)
        {
            ViewBag.Subtitle = "Roles Maintenance";
            dbService.InsertAuditTrail(User.Identity.Name, "Roles", "View", ip, "",
            "Viewed role: "+ model.RoleName+ " at "+ DateTime.Now.ToString());
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public XDocument GridRoleData()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                Tuple<List<int>, List<RoleList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, RoleList>(";Exec getAllRoles @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<RoleList>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dtDetails, resultset.Item1[0], jtStartIndex, 0, 1);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("xml");
            }
        }
        //getAuditTrailList
        [HttpGet]
        [Authorize]
        public XDocument GridAuditTrailData()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                Tuple<List<int>, List<AuditTrailModel>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, AuditTrailModel>(";Exec getAuditTrailList @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<AuditTrailModel>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dtDetails, resultset.Item1[0], jtStartIndex, 0, 1);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("xml");
            }
        }

        [Authorize]
        public ActionResult AddRoles()
        {
            //if (!dbService.GetTaskAccessRights(45))
            //{
            //    return View("Roles");
            //}

            ViewBag.Subtitle = "Add New Roles";

            dbService.InsertAuditTrail(User.Identity.Name, "Role Profile", "View", ip, "",
            "Accessed screen to add new role at " + DateTime.Now.ToString());

            return View();
        }

        [Authorize]
        public ActionResult RoleProfiles(RegisterRoleModel model)
        {
            //if (!dbService.GetTaskAccessRights(49))
            //{
            //    return View("Roles");
            //}

            ViewBag.Subtitle = "View Role Profiles";
            dbService.InsertAuditTrail(User.Identity.Name, "Role Profile", "View", ip, "",
            "Viewed roleprofile for role "+ model.RoleName + " at " + DateTime.Now.ToString());
            return View(model);
        }
        //getProfile
        [HttpGet]
        [Authorize]
        public XDocument GridProfilesData()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());
                int intRoleID = int.Parse(Request.QueryString["RoleID"].ToString());

                Tuple<List<int>, List<string>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, string>(";Exec getProfile @RoleID,@jtStartIndex, @jtPageSize",
                    new
                    {
                        RoleID = intRoleID,
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                string sb;
                sb = "<?xml version='1.0' encoding='UTF-8'?>";
                sb = sb + resultset.Item2[0].ToString();
                //sb = sb + "</rows>";
                var xml = XDocument.Parse(sb);

                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("xml");
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveProfile(int RoleID, EditRoleProfile[] model)
        {
            //int RoleID, ProfileObj[]RoleProfile
            ViewBag.Subtitle = "View Role Profiles";

            AccountServices dbService = new AccountServices();
            try
            {
                bool isValidCall = dbService.InsertRoleProfile(RoleID, model);
                if (isValidCall == true)
                {
                    dbService.InsertAuditTrail(User.Identity.Name, "Role Profile", "Save", ip, "",
                    "Save profile information for role "+ model.ToString() +" at " + DateTime.Now.ToString());

                    return RedirectToAction("RoleProfiles");// RedirectToAction("Index", "Home");
                }
                else { ModelState.AddModelError("", "Error While Saving Role Profile");
                dbService.InsertAuditTrail(User.Identity.Name, "Role Profile", "Save", ip, "",
                "Error while saving role profile " + RoleID + " with message: Error While Saving Role Profile at " + DateTime.Now.ToString());                
                }
            }
            catch (Exception e)
            {
                dbService.InsertAuditTrail(User.Identity.Name, "Role Profile", "Save", ip, "",
                "Error while saving role profile " + RoleID + " with message: " + e.Message  + " at " + DateTime.Now.ToString());

                ModelState.AddModelError("", e.Message);
            }

            return RedirectToAction("RoleProfiles");
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddRoles(RegisterRoleModel model)
        {
            ViewBag.Subtitle = "Add New Role";
            AccountServices dbService = new AccountServices();
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    bool isValidCall = dbService.InsertRole(model, User.Identity.Name);
                    if (isValidCall == true)
                    {
                        dbService.InsertAuditTrail(User.Identity.Name, "Roles", "Add", ip, "",
                        "Save role " + model.RoleName + " at " + DateTime.Now.ToString());

                        return RedirectToAction("Roles");// RedirectToAction("Index", "Home");
                    }
                    else { ModelState.AddModelError("", "Error While Creating Role"); }
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [Authorize]
        public ActionResult Users()
        {
            ViewBag.Subtitle = "User Maintenance";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument GridUsersData()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                Tuple<List<int>, List<UserList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, UserList>(";Exec getAllUsers @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<UserList>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dtDetails, resultset.Item1[0], jtStartIndex, 0, 1);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("xml");
            }
        }

        [Authorize]
        public ActionResult Register()
        {
            AccountServices dbService = new AccountServices();

            //if (!dbService.GetTaskAccessRights(49))
            //{
            //    return View("Users");
            //}

            ViewBag.Subtitle = "Add New User";
            RegisterUserModel model = new RegisterUserModel();
            model.roles = dbService.GetRoles();
            return View(model);
        }

        [Authorize]
        public ActionResult EditUserRegister(UpdateRegisterUserModel model)
        {
            AccountServices dbService = new AccountServices();

            //if (!dbService.GetTaskAccessRights(50))
            //{
            //    return View("Users");
            //}

            ViewBag.Subtitle = "Edit User";
            model = dbService.GetUserByID(model.UserID);
            model.roles = dbService.GetRoles();
            return View(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult UpdateUserRegister(UpdateRegisterUserModel model)
        {
            ViewBag.Subtitle = "Edit User";
            MembershipCreateStatus status;
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    CustomMembershipProvider xx = new CustomMembershipProvider();
                    xx.UpdateUser(model.UserName, model.FirstName, model.LastName, model.Email, model.Mobile, model.RoleID, User.Identity.Name, out status);

                    if (status == MembershipCreateStatus.Success)
                    {
                        dbService.InsertAuditTrail(User.Identity.Name, "Users", "Modification", ip, "",
                        "Updated the user " + model.UserName + " at " + DateTime.Now.ToString());

                        return RedirectToAction("Users");// RedirectToAction("Index", "Home");
                    }
                    else {
                        dbService.InsertAuditTrail(User.Identity.Name, "Users", "Modification", ip, "",
                        "Updated failed for user " + model.UserName + ". Error message: " + ErrorCodeToString(status) + " at " + DateTime.Now.ToString());

                        ModelState.AddModelError("", ErrorCodeToString(status)); 
                    }
                }
                catch (MembershipCreateUserException e)
                {
                    dbService.InsertAuditTrail(User.Identity.Name, "Users", "Modification", ip, "",
                    "Updated failed for user " + model.UserName + ". Error message: " + ErrorCodeToString(e.StatusCode) + " at " + DateTime.Now.ToString());

                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            // If we got this far, something failed, redisplay form

            return RedirectToAction("EditUserRegister", model);
        }

        [Authorize]
        public ActionResult EditPassword(PasswordUserModel model)
        {
            AccountServices dbService = new AccountServices();

            //if (!dbService.GetTaskAccessRights(51))
            //{
            //    return View("Users");
            //}

            ViewBag.Subtitle = "Edit Password";
            model = dbService.GetBasicUserByID(model.UserID);
            return View(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult UpdatePassword(PasswordUserModel model)
        {
            ViewBag.Subtitle = "Edit Password";
            MembershipCreateStatus status;
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    CustomMembershipProvider xx = new CustomMembershipProvider();
                    xx.UpdatePassword(model.UserID, model.UserName, model.Password, User.Identity.Name, out status);

                    if (status == MembershipCreateStatus.Success)
                    {
                        dbService.InsertAuditTrail(User.Identity.Name, "Users", "Password Change", ip, "",
                        "Password update for user " + model.UserName + " at " + DateTime.Now.ToString());

                        return RedirectToAction("Users");// RedirectToAction("Index", "Home");
                    }
                    else { ModelState.AddModelError("", ErrorCodeToString(status)); }
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }
            var message = string.Join(" | ", ModelState.Values
                .SelectMany(v => v.Errors)
                .Select(e => e.ErrorMessage));
            ModelState.AddModelError("", message);
            // If we got this far, something failed, redisplay form
            dbService.InsertAuditTrail(User.Identity.Name, "Users", "Modification", ip, "",
            "Updated failed for user " + model.UserName + ". Error message: " + message + " at " + DateTime.Now.ToString());

            return View(model);
        }
        //
        // USER PASSWORD MODIFICATION
        [Authorize]
        public ActionResult ModifyPassword()
        {
            ViewBag.Subtitle = "Modify Password";
            AccountServices dbService = new AccountServices();
            ModifyPasswordModel model = new ModifyPasswordModel();
            model.UserID = int.Parse(Membership.GetUser().ProviderUserKey.ToString());
            model.UserName = User.Identity.Name;
            return View(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult ModifyPassword(ModifyPasswordModel model)
        {
            ViewBag.Subtitle = "Modify Password";
            MembershipCreateStatus status;
            var message = string.Empty;

            var isPasswordCorrect = Membership.ValidateUser(model.UserName, model.OldPassword);
            if (isPasswordCorrect)
            {
                if (ModelState.IsValid)
                {
                    // Attempt to register the user
                    try
                    {
                        CustomMembershipProvider xx = new CustomMembershipProvider();
                        xx.UpdatePassword(model.UserID, model.UserName, model.Password, User.Identity.Name, out status);

                        if (status == MembershipCreateStatus.Success)
                        {
                            dbService.InsertAuditTrail(User.Identity.Name, "Users", "Password Change", ip, "",
                            "Password update for user " + model.UserName + " at " + DateTime.Now.ToString());

                            //return View("Users");// Account/Login
                            FormsAuthentication.SignOut();
                            return RedirectToAction("Login", "Account");
                        }
                        else { ModelState.AddModelError("", ErrorCodeToString(status)); }
                    }
                    catch (MembershipCreateUserException e)
                    {
                        ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                    }
                }
                message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
            }
            else
            {
                message = "Invalid current password.";
            }

            ModelState.AddModelError("", message);
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Register(RegisterUserModel model)
        {
            ViewBag.Subtitle = "Add New User";
            AccountServices dbService = new AccountServices();

            MembershipCreateStatus status;
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    CustomMembershipProvider xx = new CustomMembershipProvider();
                    xx.CreateNewUser(model.UserName, model.Password, model.FirstName, model.LastName, model.Email, model.Mobile, model.RoleID, User.Identity.Name, out status);
                    //Membership.CreateNewUser(model.UserName, model.Password);
                    //Membership.Login(model.UserName, model.Password);
                    if (status == MembershipCreateStatus.Success)
                    {
                        dbService.InsertAuditTrail(User.Identity.Name, "Users", "Creation", ip, "",
                        "Created new user " + model.UserName + " at " + DateTime.Now.ToString());

                        return RedirectToAction("Users");// RedirectToAction("Index", "Home");
                    }
                    else { ModelState.AddModelError("", ErrorCodeToString(status)); }
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }
            else
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);

                    dbService.InsertAuditTrail(User.Identity.Name, "Security", "USer Saving Error", ip, "",
                    "Save User with Message " + message + "  Encountered an error at " + DateTime.Now.ToString());

                    //model.UserName, 
                    //model.Password, 
                    //model.FirstName, 
                    //model.LastName, 
                    //model.Email, 
                    //model.Mobile, 
                    //model.RoleID

                    model.roles = dbService.GetRoles();
                    return View("Register", model);
                }

            model.roles = dbService.GetRoles();

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
    }
}
