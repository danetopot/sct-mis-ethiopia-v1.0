ALTER Proc [dbo].[InsertForm2B2]
(
	@KebeleID				Int,
	@ReportFileName			varchar(50),
	@CreatedBy				varchar(30)
)
AS
BEGIN
SET NOCOUNT ON

	DECLARE @CoRespTDSPLWHeaderID	uniqueidentifier

	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD

		SELECT @CoRespTDSPLWHeaderID = NEWID()

		INSERT INTO CoResponsibilityTDSPLWHeader(CoRespTDSPLWHeaderID,PLW,ReportFileName,FiscalYear)
		VALUES(@CoRespTDSPLWHeaderID,'L',@ReportFileName,dbo.f_getFiscalYearFromDate(GETDATE()))			

		INSERT INTO CoResponsibilityTDSPLW(CoRespTDSPLWID,CoRespTDSPLWHeaderID,ProfileTDSPLWID,PLWName,TargetClientID,GeneratedBy,GeneratedOn)
		SELECT NEWID(),@CoRespTDSPLWHeaderID,ProfileTDSPLWID,NameOfPLW,dbo.f_GetForm2BTargetClientID(PLWAge,''),
				dbo.f_getUserIDByUserName(@CreatedBy),GETDATE()
		FROM ProfileTDSPLW
		WHERE KebeleID = @KebeleID 
		AND ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(Form2Produced,0) = 0
		AND ISNULL(PLW,'') = 'L'		
		
		UPDATE ProfileTDSPLW
		SET Form2Produced = 1
		FROM ProfileTDSPLW
		WHERE KebeleID = @KebeleID 
		AND ISNULL(Form2Produced,0) = 0
		AND ISNULL(ApprovedBy,'') <> ''
		AND ISNULL(PLW,'') = 'L'

		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
		  --PRINT 'Error detected, all changes reversed'
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END
