﻿using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace SCTMis.Services
{
    public class ReportServices
    {
        AccountServices dbService = new AccountServices();


        public DataTable GenerateReport(
            string username,
            int? regionID, int? woredaID, int? kebeleID,
            string rptType, string rptDisaggregation,string rptPeriod, string socialWorker,
            string [] rpt1Filters, string[] rpt2Filters, string[] rpt3Filters, string[] rpt4Filters, 
            string[] rpt5Filters, string[] rpt6Filters, string[] rpt7Filters, string[] rpt8Filters, 
            string[] rpt9Filters, string[] rpt10Filters)
        {
            string rptMsg = String.Empty;
            string errorMsg = String.Empty;
            string rptName = String.Empty;
            DataTable dtDetails, dtSummary, dtDefault=null;
            string strReportFilePath = string.Empty;

            try
            {
                /** DT = detailReport , SM=SummaryReport */


                /** HouseholdProfilePDS Reports **/
                if (rptType == "RPT1")
                {

                    Tuple<List<HouseholdProfilePDSDetailReport>, List<HouseholdProfilePDSSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfilePDSDetailReport, HouseholdProfilePDSSummaryReport>(
                         ";Exec HouseholdProfileReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker, " +
                         "@Gender, @Pregnant, @Lactating, @Handicapped, @ChronicallyIll, @NutritionalStatus, @ChildUnderTSForCMAM," +
                         "@EnrolledInSchool, @ChildProtectionRisk, @CBHIMembership",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         Gender = rpt1Filters[0],
                         Pregnant = rpt1Filters[1],
                         Lactating = rpt1Filters[2],
                         Handicapped = rpt1Filters[3],
                         ChronicallyIll = rpt1Filters[4],
                         NutritionalStatus = rpt1Filters[5],
                         ChildUnderTSForCMAM = rpt1Filters[6],
                         EnrolledInSchool = rpt1Filters[7],
                         ChildProtectionRisk = rpt1Filters[8],
                         CBHIMembership = rpt1Filters[9]
                     });

                    switch (rptDisaggregation)
                    {

                        case "DT":
                            dtDetails = SCTMis.Services.GeneralServices.ToDataTable<HouseholdProfilePDSDetailReport>(resultset.Item1);

                            /*
                            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfilePDSDetailsRptFile.rpt");
                            ReportDocument myReportDocument;
                            myReportDocument = new ReportDocument();
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            var filename = "HouseholdProfileDetailsReport_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".xls";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            //myReportDocument.ExportToDisk(ExportFormatType.Excel, strReportFilePath);
                            //myReportDocument.Close();

                           
                            // Declare variables and get the export options.
                            ExportOptions exportOpts = new ExportOptions();
                            ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                            DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                            exportOpts = myReportDocument.ExportOptions;

                            // Set the excel format options.
                            excelFormatOpts.ExcelUseConstantColumnWidth = true;
                            excelFormatOpts.ExcelTabHasColumnHeadings = true;
                            excelFormatOpts.ShowGridLines = true;
                            exportOpts.ExportFormatType = ExportFormatType.ExcelRecord;
                            exportOpts.FormatOptions = excelFormatOpts;

                            // Set the disk file options and export.
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;

                            myReportDocument.Export();


                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Household Profile Details Report", rptPeriod, filename, username, DateTime.Now);
                            */
                            return dtDetails;

                        case "SM":

                            dtSummary = SCTMis.Services.GeneralServices.ToDataTable<HouseholdProfilePDSSummaryReport>(resultset.Item2);

                            myReportDocument = new ReportDocument();
                            strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfilePDSSummaryRptFile.rpt");
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtSummary);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            filename = "HouseholdProfileDetailsReport_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Household Profile Summary Report", rptPeriod, filename, username, DateTime.Now);

                            return dtSummary;
                    }
                }

                /** HouseholdProfileTDSPLW Reports **/
                if (rptType == "RPT2")
                {
                    string startDateTDS = rpt2Filters[0];
                    string endDateTDS = rpt2Filters[1];
                    if (startDateTDS == "Jan/0001") { startDateTDS = null; }
                    if (endDateTDS == "Jan/0001") { endDateTDS = null; }

                    Tuple<List<HouseholdProfileTDSPLWDetailReport>, List<HouseholdProfileTDSPLWSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSPLWDetailReport, HouseholdProfileTDSPLWSummaryReport>(";Exec HouseholdProfileReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker," +
                        "null, null, null, null, null, null, null,null, @ChildProtectionRisk, @CBHIMembership,@StartDateTDS, @EndDateTDS, @NutritionalStatusPLW, @NutritionalStatusInfant",
                     new
                     {

                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ChildProtectionRisk = rpt2Filters[4],
                         CBHIMembership = rpt2Filters[5],
                         StartDateTDS = startDateTDS,
                         EndDateTDS = endDateTDS,
                         NutritionalStatusPLW = rpt2Filters[2],
                         NutritionalStatusInfant = rpt2Filters[3]
                     });

                    switch (rptDisaggregation)
                    {

                        case "DT":
                            dtDetails = SCTMis.Services.GeneralServices.ToDataTable<HouseholdProfileTDSPLWDetailReport>(resultset.Item1);

                            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfileTDSPLWDetailsRptFile.rpt");
                            ReportDocument myReportDocument;
                            myReportDocument = new ReportDocument();
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            var filename = "HouseholdProfileDetailsReport_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Household Profile Details Report(PLW)", rptPeriod, filename, username, DateTime.Now);

                            return dtDetails;

                        case "SM":
                            dtSummary = SCTMis.Services.GeneralServices.ToDataTable<HouseholdProfileTDSPLWSummaryReport>(resultset.Item2);

                            myReportDocument = new ReportDocument();
                            strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfileTDSPLWSummaryRptFile.rpt");
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtSummary);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            filename = "HouseholdProfileDetailsReport_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Household Profile Summary Report(PLW)", rptPeriod, filename, username, DateTime.Now);

                            return dtSummary;
                    }
                }

                /** HouseholdProfileTDSCMC Reports **/
                if (rptType == "RPT3")
                {
                    string startDateTDS = rpt2Filters[0];
                    string endDateTDS = rpt2Filters[1];
                    if (startDateTDS == "Jan/0001") { startDateTDS = null; }
                    if (endDateTDS == "Jan/0001") { endDateTDS = null; }

                    Tuple<List<HouseholdProfileTDSCMCDetailReport>, List<HouseholdProfileTDSCMCSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSCMCDetailReport, HouseholdProfileTDSCMCSummaryReport>(";Exec HouseholdProfileReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker," +
                        "null, null, null, null, null, null, null,null, @ChildProtectionRisk, @CBHIMembership,@StartDateTDS, @EndDateTDS, null, null, @MalnutritionDegree",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ChildProtectionRisk = rpt3Filters[3],
                         CBHIMembership = rpt3Filters[4],
                         StartDateTDS = startDateTDS,
                         EndDateTDS = endDateTDS,
                         MalnutritionDegree = rpt3Filters[2],
                     });

                    switch (rptDisaggregation)
                    {

                        case "DT":
                            dtDetails = SCTMis.Services.GeneralServices.ToDataTable<HouseholdProfileTDSCMCDetailReport>(resultset.Item1);

                            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfileTDSCMCDetailsRptFile.rpt");
                            ReportDocument myReportDocument;
                            myReportDocument = new ReportDocument();
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            var filename = "HouseholdProfileDetailsReport_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Household Profile Details Report(CMC)", rptPeriod, filename, username, DateTime.Now);

                            return dtDetails;

                        case "SM":
                            dtSummary = SCTMis.Services.GeneralServices.ToDataTable<HouseholdProfileTDSCMCSummaryReport>(resultset.Item2);

                            myReportDocument = new ReportDocument();
                            strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfileTDSCMCSummaryRptFile.rpt");
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtSummary);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            filename = "HouseholdProfileDetailsReport_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Household Profile Summary Report(CMC)", rptPeriod, filename, username, DateTime.Now);

                            return dtSummary;
                    }
                }
                /** Social Services Needs Report **/
                if (rptType == "RPT4")
                {
                    Tuple<List<SocialServicesNeedsDetailReport>, List<SocialServicesNeedsSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<SocialServicesNeedsDetailReport, SocialServicesNeedsSummaryReport>(";Exec SocialServicesNeedsReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ClientType = rpt4Filters[0],
                         ServiceType = rpt4Filters[1]
                     });

                    switch (rptDisaggregation)
                    {

                        case "DT":
                            dtDetails = SCTMis.Services.GeneralServices.ToDataTable<SocialServicesNeedsDetailReport>(resultset.Item1);

                            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/SocialServicesNeedsDetailsRptFile.rpt");
                            ReportDocument myReportDocument;
                            myReportDocument = new ReportDocument();
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            var filename = "SocialServicesNeedsDetailsRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Social Services Needs Details Report", rptPeriod, filename, username, DateTime.Now);

                            return dtDetails;

                        case "SM":
                            dtSummary = SCTMis.Services.GeneralServices.ToDataTable<SocialServicesNeedsSummaryReport>(resultset.Item2);

                            myReportDocument = new ReportDocument();
                            strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/SocialServicesNeedsSummaryRptFile.rpt");
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtSummary);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            filename = "SocialServicesNeedsSummaryRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Social Services Needs Summary Report", rptPeriod, filename, username, DateTime.Now);

                            return dtSummary;
                    }
                }
                /** Service Compliance Report **/
                if (rptType == "RPT5")
                {
                    Tuple<List<ServiceComplianceDetailReport>, List<ServiceComplianceSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<ServiceComplianceDetailReport, ServiceComplianceSummaryReport>(";Exec ServiceComplianceReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ClientType = rpt5Filters[0],
                         ServiceType = rpt5Filters[1]
                     });

                    switch (rptDisaggregation)
                    {

                        case "DT":
                            dtDetails = SCTMis.Services.GeneralServices.ToDataTable<ServiceComplianceDetailReport>(resultset.Item1);

                            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/ServiceComplianceDetailsRptFile.rpt");
                            ReportDocument myReportDocument;
                            myReportDocument = new ReportDocument();
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            var filename = "ServiceComplianceDetailsRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Service Compliance Details Report", rptPeriod, filename, username, DateTime.Now);

                            return dtDetails;

                        case "SM":
                            dtSummary = SCTMis.Services.GeneralServices.ToDataTable<ServiceComplianceSummaryReport>(resultset.Item2);

                            myReportDocument = new ReportDocument();
                            strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/ServiceComplianceSummaryRptFile.rpt");
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtSummary);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            filename = "ServiceComplianceDetailsRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Service Compliance Summary Report", rptPeriod, filename, username, DateTime.Now);

                            return dtSummary;
                    }
                }
                /** Service NonCompliance Report **/
                if (rptType == "RPT6")
                {
                    Tuple<List<ServiceComplianceDetailReport>, List<ServiceComplianceSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<ServiceComplianceDetailReport, ServiceComplianceSummaryReport>(";Exec ServiceNonComplianceReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ClientType = rpt6Filters[0],
                         ServiceType = rpt6Filters[1]
                     });

                    switch (rptDisaggregation)
                    {

                        case "DT":
                            dtDetails = SCTMis.Services.GeneralServices.ToDataTable<ServiceComplianceDetailReport>(resultset.Item1);

                            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/ServiceNonComplianceDetailsRptFile.rpt");
                            ReportDocument myReportDocument;
                            myReportDocument = new ReportDocument();
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            var filename = "ServiceNonComplianceDetailsRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Service NonCompliance Details Report", rptPeriod, filename, username, DateTime.Now);

                            return dtDetails;

                        case "SM":
                            dtSummary = SCTMis.Services.GeneralServices.ToDataTable<ServiceComplianceSummaryReport>(resultset.Item2);

                            myReportDocument = new ReportDocument();
                            strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/ServiceNonComplianceSummaryRptFile.rpt");
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtSummary);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            filename = "ServiceNonComplianceDetailsRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Service NonCompliance Summary Report", rptPeriod, filename, username, DateTime.Now);

                            return dtSummary;
                    }
                }
                /** Case Management Compliance Report **/
                if (rptType == "RPT7")
                {

                     Tuple<List<CaseManagementComplianceDetailReport>, List<CaseManagementComplianceSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<CaseManagementComplianceDetailReport, CaseManagementComplianceSummaryReport>(";Exec CaseManagementComplianceReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ClientType = rpt7Filters[0],
                         ServiceType = rpt7Filters[1]
                     });

                    switch (rptDisaggregation)
                    {
                        case "DT":
                            dtDetails = SCTMis.Services.GeneralServices.ToDataTable<CaseManagementComplianceDetailReport>(resultset.Item1);

                            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/CaseManagementComplianceDetailRptFile.rpt");
                            ReportDocument myReportDocument;
                            myReportDocument = new ReportDocument();
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            var filename = "CaseManagementComplianceRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Case Management Compliance Detail Report", rptPeriod, filename, username, DateTime.Now);

                            return dtDetails;

                        case "SM":
                            dtSummary = SCTMis.Services.GeneralServices.ToDataTable<CaseManagementComplianceSummaryReport>(resultset.Item2);

                            myReportDocument = new ReportDocument();
                            strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/CaseManagementComplianceSummaryRptFile.rpt");
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtSummary);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            filename = "CaseManagementComplianceRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Case Management Compliance Summary Report", rptPeriod, filename, username, DateTime.Now);

                            return dtSummary;
                    }
                }
                /** Case Management Non Compliance Report **/
                if (rptType == "RPT8")
                {
                    Tuple<List<CaseManagementComplianceDetailReport>, List<CaseManagementComplianceSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<CaseManagementComplianceDetailReport, CaseManagementComplianceSummaryReport>(";Exec CaseManagementNonComplianceReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ClientType = rpt8Filters[0],
                         ServiceType = rpt8Filters[1]
                     });

                    switch (rptDisaggregation)
                    {
                        case "DT":
                            dtDetails = SCTMis.Services.GeneralServices.ToDataTable<CaseManagementComplianceDetailReport>(resultset.Item1);

                            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/CaseManagementComplianceDetailRptFile.rpt");
                            ReportDocument myReportDocument;
                            myReportDocument = new ReportDocument();
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            var filename = "CaseManagementComplianceRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Case Management Compliance Report", rptPeriod, filename, username, DateTime.Now);

                            return dtDetails;

                        case "SM":
                            dtSummary = SCTMis.Services.GeneralServices.ToDataTable<CaseManagementComplianceSummaryReport>(resultset.Item2);

                            myReportDocument = new ReportDocument();
                            strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/CaseManagementComplianceSummaryRptFile.rpt");
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtSummary);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            filename = "CaseManagementComplianceRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Case Management Compliance Summary Report", rptPeriod, filename, username, DateTime.Now);

                            return dtSummary;
                    }
                }

                /** Retargeting New Report **/
                if (rptType == "RPT9")
                {
                    Tuple<List<RetargetingDetailReport>, List<RetargetingSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<RetargetingDetailReport, RetargetingSummaryReport>(";Exec RetargetingNewReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker
                         /*
                         ClientType = rpt9Filters[0],
                         ServiceType = rpt9Filters[1]
                         */
                     });

                    switch (rptDisaggregation)
                    {

                        case "DT":
                            dtDetails = SCTMis.Services.GeneralServices.ToDataTable<RetargetingDetailReport>(resultset.Item1);

                            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/RetargetingNewDetailsRptFile.rpt");
                            ReportDocument myReportDocument;
                            myReportDocument = new ReportDocument();
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            var filename = "RetargetingDetailsRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Retargeting Details Report (New)", rptPeriod, filename, username, DateTime.Now);

                            return dtDetails;
                        case "SM":
                            dtSummary = SCTMis.Services.GeneralServices.ToDataTable<RetargetingSummaryReport>(resultset.Item2);

                            myReportDocument = new ReportDocument();
                            strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/RetargetingNewSummaryRptFile.rpt");
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtSummary);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            filename = "RetargetingSummaryRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Retargeting Summary Report (New)", rptPeriod, filename, username, DateTime.Now);

                            return dtSummary;
                    }
                }

                /** Retargeting Exit Report **/
                if (rptType == "RPT10")
                {
                    Tuple<List<RetargetingDetailReport>, List<RetargetingSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<RetargetingDetailReport, RetargetingSummaryReport>(";Exec RetargetingExitReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker
                         /*
                         ClientType = rpt10Filters[0],
                         ServiceType = rpt10Filters[1]
                         */
                     });

                    switch (rptDisaggregation)
                    {

                        case "DT":
                            dtDetails = SCTMis.Services.GeneralServices.ToDataTable<RetargetingDetailReport>(resultset.Item1);

                            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/RetargetingExitDetailsRptFile.rpt");
                            ReportDocument myReportDocument;
                            myReportDocument = new ReportDocument();
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            var filename = "RetargetingDetailsRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Retargeting Details Report (Exit)", rptPeriod, filename, username, DateTime.Now);

                            return dtDetails;
                        case "SM":
                            dtSummary = SCTMis.Services.GeneralServices.ToDataTable<RetargetingSummaryReport>(resultset.Item2);

                            myReportDocument = new ReportDocument();
                            strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/RetargetingExitSummaryRptFile.rpt");
                            myReportDocument.Load(strRptPath);
                            myReportDocument.Database.Tables[0].SetDataSource(dtSummary);
                            myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                            filename = "RetargetingSummaryRptFile_" + DateTime.Now.ToFileTime().ToString();
                            filename = filename + ".pdf";
                            strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                            myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                            myReportDocument.Close();

                            if (String.IsNullOrEmpty(rptPeriod))
                            {
                                rptPeriod = "ALL RECORDS";
                            }
                            dbService.InsertReportLog("Retargeting Summary Report (Exit)", rptPeriod, filename, username, DateTime.Now);

                            return dtSummary;
                    }
                }

                return dtDefault;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return dtDefault;
            }
        }
    }
}
