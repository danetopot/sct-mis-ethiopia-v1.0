﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.StandardReportModel>" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
            Standard Reports
    </asp:Content>
    
    <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   
        <% using (Html.BeginForm("StandardReports", "Security", FormMethod.Post, new { id = "formReportTemplate" })) { %>
      
            <div class="col-4">
                    <label>
                        Report Name
                        <%: Html.ValidationMessageFor(model => model.ReportName) %>
                        <%: Html.DropDownListFor(model => model.ReportName,
                                new SelectList(Model.rptTypes, "ReportID", "ReportName"), new { tabindex = "1" })%>
                    </label>
                </div>
        
                <div class="col-4">
                    <label>
                        Reporting Disaggregation
                        <%: Html.ValidationMessageFor(model => model.ReportDisaggregation) %>
                        <%: Html.DropDownListFor(model => model.ReportDisaggregation,
                                new SelectList(Model.rptDisaggregation, "DisaggregationID", "DisaggregationName"), new { tabindex = "2" })%>
                    </label>
                </div>
                <div class="col-4">
                    <label>
                        Reporting Period
                        <%: Html.ValidationMessageFor(model => model.ReportingPeriod) %>
                        <%: Html.DropDownListFor(model => model.ReportingPeriod,
                                new SelectList(Model.rptPeriod, "PeriodID", "PeriodName"), new { tabindex = "3" })%>
                    </label>
                </div>
                <br>
        
                <div class="col-4">                
                <label>
                    Region <%: Html.ValidationMessageFor(model => model.RegionID) %>
                    <%: Html.DropDownListFor(model => model.RegionID,
                        new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "4" })%>
                </label>
                </div>
                <div class="col-4">                
                <label>
                    Woreda  <%: Html.ValidationMessageFor(model => model.WoredaID) %>
                    <%: Html.DropDownListFor(model => model.WoredaID,
                        new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "5" })%>
                </label>
                </div>
                <div class="col-4">                
                    <label>
                        Kebele  <%: Html.ValidationMessageFor(model => model.KebeleID) %>
                        <%: Html.DropDownListFor(model => model.KebeleID,
                            new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "6" })%>
                    </label>
                </div>
                <br>
        
                <div class="col-4">                
                <label>
                    Social Worker <%: Html.ValidationMessageFor(model => model.SocialWorker) %>
                        <%: Html.DropDownListFor(model => model.SocialWorker,
                            new SelectList(Model.worker, "ID", "Name"), new { tabindex = "7" })%>
                </label>
                </div>
                <br>
        
                <div id="divRpt1Gender" class="col-4" style="display: none;">                
                <label>
                    Gender <%: Html.ValidationMessageFor(model => model.Gender) %>
                    <%: Html.DropDownListFor(model => model.Gender,
                        new SelectList(Model.sex, "GenderID", "GenderName"), new { tabindex = "8" })%>
                </label>
                </div>
                <div id="divRpt1Pregnant" class="col-4" style="display: none;">                
                <label>
                    Pregnant
                        <%: Html.DropDownListFor(model => model.Pregnant,
                            new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "9" })%>
                </label>
                </div>
                <br />
        
                <div id="divRpt1Lactating" class="col-4" style="display: none;">
                    <label>
                        Lactating
                            <%: Html.DropDownListFor(model => model.Lactating,
                                new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "10" })%>
                    </label>
                </div>
                <div id="divRpt1Disabled" class="col-4" style="display: none;">
                    <label>
                        Disabled
                            <%: Html.DropDownListFor(model => model.Handicapped,
                                new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "11" })%>
                    </label>
                </div>
                <br>
        
                <div id="divRpt1ChronicallyIll" class="col-4" style="display: none;">
                    <label>
                        Chronically Ill
                            <%: Html.DropDownListFor(model => model.ChronicallyIll,
                                new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "12" })%>
                    </label>
                </div>
                <div id="divRpt1NutritionalStatus" class="col-4" style="display: none;">
                    <label>
                        Nutritional status (0 – 5 and PLW)
                            <%: Html.DropDownListFor(model => model.NutritionalStatus,
                                new SelectList(Model.nutrStatus, "ID", "Name"), new { tabindex = "13" })%>
                    </label>
                </div>
                <br>
        
                <div id="divRpt1ChildUnderTSForCMAM" class="col-4" style="display: none;">
                    <label>
                        Under TSF or CMAM  
                            <%: Html.DropDownListFor(model => model.ChildUnderTSForCMAM,
                                new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "14" })%>
                    </label>
                </div>
                <div id="divRpt1EnrolledInSchool" class="col-4" style="display: none;">
                    <label>
                        Enrolled in School
                            <%: Html.DropDownListFor(model => model.EnrolledInSchool,
                                new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "15" })%>
                    </label>
                </div>
                <br>
        
                <div id="divRpt1ChildProtectionRisk" class="col-4" style="display: none;">
                    <label>
                        Child Protection Risk  
                            <%: Html.DropDownListFor(model => model.ChildProtectionRisk,
                                new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "16" })%>
                    </label>
                </div>
                <div id="divRpt1CBHIMembership" class="col-4" style="display: none;">
                    <label>
                        CBHI Membership
                            <%: Html.DropDownListFor(model => model.CBHIMembership,
                                new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "17" })%>
                    </label>
                </div>
                <br>
                <div id="divRpt2StartDateTDSPLW" class="col-4" style="display: none;">         
                    <label>
                        Start Date TDS
                        <%: Html.TextBoxFor(model => model.StartDateTDSPLW, new { tabindex = "18",@Style="width:95%;"})%>
                    </label>
                </div>
                <div id="divRpt2EndDateTDSPLW" class="col-4" style="display: none;">         
                    <label>
                        End Date TDS
                        <%: Html.TextBoxFor(model => model.EndDateTDSPLW, new { tabindex = "19",@Style="width:95%;"})%>
                    </label>
                </div>
                <br>
        
                <div id="divRpt2NutritionalStatusPLW" class="col-4" style="display: none;">
                    <label>
                        Nutritional Status Of PLW
                            <%: Html.DropDownListFor(model => model.NutritionalStatusPLW,
                                new SelectList(Model.nutrStatus, "ID", "Name"), new { tabindex = "20" })%>
                    </label>
                </div>
                <div id="divRpt2NutritionalStatusInfant" class="col-4" style="display: none;">
                    <label>
                        Nutritional Status Of Infant
                            <%: Html.DropDownListFor(model => model.NutritionalStatusInfant,
                                new SelectList(Model.nutrStatus, "ID", "Name"), new { tabindex = "21" })%>
                    </label>
                </div>
                <br>
        
                <div id="divRpt3StartDateTDSCMC" class="col-4" style="display: none;">         
                    <label>
                        Start Date TDS
                        <%: Html.TextBoxFor(model => model.StartDateTDSCMC, new { tabindex = "22",@Style="width:95%;"})%>
                    </label>
                </div>
                <div id="divRpt3EndDateTDSCMC" class="col-4" style="display: none;">         
                    <label>
                        End Date TDS
                        <%: Html.TextBoxFor(model => model.EndDateTDSCMC, new { tabindex = "23",@Style="width:95%;"})%>
                    </label>
                </div>
                <br>
                <div id="divRpt3MalnourishmentDegree" class="col-4" style="display: none;">
                    <label>
                        Degree Of Malnourishment 
                        <%: Html.DropDownListFor(model => model.MalnourishmentDegree,
                            new SelectList(Model.nutrStatus, "ID", "Name"), new { tabindex = "24" })%>
                    </label>
                </div>
                <br>
        
        
                <div id="divRpt4TypeOfClient" class="col-4" style="display: none;">
                    <label>
                        Type Of Client 
                        <%: Html.DropDownListFor(model => model.ClientType,
                            new SelectList(Model.clients, "ClientTypeID", "ClientTypeName"), new { tabindex = "24" })%>
                    </label>
                </div>
                <div id="divRpt4TypeOfService" class="col-4" style="display: none;">
                    <label>
                        Type Of Service 
                        <%: Html.DropDownListFor(model => model.ServiceType,
                            new SelectList(Model.services, "ServiceID", "ServiceName"), new { tabindex = "24" })%>
                    </label>
                </div>
                
                 <!--  Start Area :  Errors -->
                 <div id="error" style="color: red;"></div>
                 <div id="success" style="color: green;"></div>
                 <div id="validationerrors" style="color: red;"></div>
                 <!--  End Area :  Errors -->

                <hr>
                <br>
                <div>
                    <button id="cmdProduceReport" type="button" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>
                        Produce Report
                    </button>

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
                   
                    <button id="cmdExportPdf" type="button" class="btn btn-primary btn-icon glyphicons download" onclick="ExportReport('pdf')"><i></i>
                        Export To PDF
                    </button>
                    <button id="cmdExportXls" type="button" class="btn btn-primary btn-icon glyphicons download" onclick="ExportReport('xls')"><i></i>
                        Export To Excel
                    </button>
                </div>

                
                <br>
           <hr>
           <div id="rptViewSection">
                <div id="gridbox" style="position:relative; max-width: 1200px; min-height: 310px ;overflow-x: scroll;">
                
                </div>
                <span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>
                <%: Html.Hidden("currentPage", (int)ViewBag.CurrentPage) %> 
            </div>
            
       
        <% } %>
    
        <div id="progress" class="modal" style="display: none">
            <div class="center">
                <p>Generating report, please wait . . . <img src="../Images/loader.gif"></p>
            </div>
        </div>

        <div id="exportProgress" class="modal" style="display: none">
            <div class="center">
                <p>Exporting report, please wait . . . <img src="../Images/loader.gif"></p>
            </div>
        </div>
    
    </asp:Content>
    
    <asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
        
        <script src="../Scripts/assets/js/moment.min.js"></script>
        <script type="text/javascript" src="../Scripts/assets/js/jquery.mask.min.js"></script>
        <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
        <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>
        <script type="text/javascript" src="../Scripts/assets/js/app/StandardReports.js"></script>
    
    </asp:Content>
    
    