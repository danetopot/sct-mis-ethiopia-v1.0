﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SCTMis.Models
{
    public class PsnpCheck
    {
        public string HouseHoldIDNumber { get; set; }
        public string FormId { get; set; }
    }

    public class CbhiCheck
    {
        public string CBHINumber { get; set; }
        public string FormId { get; set; }
    }

    public class Form1AMembersGrid
    {
        public int ColumnID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string DateOfBirth { get; set; }
        //public string Age { get => Services.GeneralServices.GetAge(DateOfBirth); }
        public string Age { get { return Services.GeneralServices.GetAge(DateOfBirth); } }
        public string Sex { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string NutritionalStatusName { get; set; }
        public string childUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string Grade { get; set; }
        public string SchoolName { get; set; }
        public string ChildProtectionRisk { get; set; }
    }

    public class Form1A
    {
        public int ColumnID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string SocialWorker { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string CollectionDate { get; set; }
        /*
        public string ChildProtectionRisk { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        */
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public int RecordCount { get; set; }
        public int TotalCount { get; set; }
    }

    public class Form1AGrid
    {
        public int ColumnID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public int RecordCount { get; set; }
        public string Kebele { get; set; }
        public string WoredaName { get; set; }
        public string Gote { get; set; }
        /*
        public string ChildProtectionRisk { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        */
        public string CreatedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string ReportFilePath { get; set; }
        public string Remarks { get; set; }
        public string StatusID { get; set; }
        //public string FiscalYear { get; set; }
    }

    public class CaptureForm1A
    {
        public int ColumnID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public int Kebele { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
		public string NameOfHouseHoldHead { get; set; }
		public string HouseHoldIDNumber { get; set; }
		public string SocialWorker { get; set; }
		public string CCCCBSPCMember { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public string CollectionDate { get; set; }
		public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string Remarks { get; set; }

        [Required(ErrorMessage = "Atleast one Member is required!")]
        public string MemberXml { get; set; }

        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        //public string MedicalRecordNumber { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string PWL { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string childUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string Grade { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string SchoolName { get; set; }
        public string RandomKey { get; set; }
        public Boolean AllowEdit { get; set; }       
        public string ProfileDSDetailID { get; set; }
        public string ApprovedBy { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<Gender> genders { get; set; }
        public IEnumerable<YesNo> yesnos { get; set; }
        public IEnumerable<NutritionalStatus> nutrStatus { get; set; }
        public IEnumerable<SocialWorker> worker { get; set; }
        public IEnumerable<SchoolGrade> grades { get; set; }   
    }

    public class Form1AMembers
    {
        public int ColumnID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public string MedicalRecordNumber { get; set; }
        public string DateOfBirth { get; set; }
        public decimal Age { get; set; }
        public string Sex { get; set; }
        public string SexName { get; set; }
        public string PWL { get; set; }
        public string PWLName { get; set; }
        public string Handicapped { get; set; }
        public string HandicappedName { get; set; }
        public string ChronicallyIll { get; set; }
        public string ChronicallyIllName { get; set; }
        public string NutritionalStatus { get; set; }
        public string NutritionalStatusName { get; set; }
        public string EnrolledInSchool { get; set; }
        public string EnrolledInSchoolName { get; set; }
        public string SchoolName { get; set; }
    }

    public class Form1BGrid
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfPLW { get; set; }
        public string Kebele { get; set; }
        public string WoredaName { get; set; }
        public string Gote { get; set; }
        public string CreatedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string ReportFilePath { get; set; }
        public string StatusID { get; set; }
    }

    public class Form1CGrid
    {
        public int ColumnID { get; set; }
        public string MalnourishmentDegree { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string Kebele { get; set; }
        public string WoredaName { get; set; }
        public string Gote { get; set; }
        public string CreatedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string ReportFilePath { get; set; }
        public string StatusID { get; set; }        
    }

    public class CaptureForm1B
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public int Kebele { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }

        [Required(ErrorMessage = "Pregnant or Lactating is Required")]
        public string PLW { get; set; }

        [Required(ErrorMessage = "PLW Name is Required")]
        public string NameOfPLW { get; set; }

        [Required(ErrorMessage = "Household ID Number is required")]
        public string HouseHoldIDNumber { get; set; }

        [Required(ErrorMessage = "Please select Social Worker")]
        public string SocialWorker { get; set; }

        [Required(ErrorMessage = "CCC/CBSPC member is required")]
        public string CCCCBSPCMember { get; set; }

        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }

        [Required(ErrorMessage = "Please select collected Date")]
        public string CollectionDate { get; set; }

        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string Remarks { get; set; }        

        //[Required(ErrorMessage = "Atleast one Member is required!")]
        public string MemberXml { get; set; }

        [Required(ErrorMessage = "Medical Record Number Required")]
        public string MedicalRecordNumber { get; set; }

        [Required(ErrorMessage = "Age of PLW required")]
        public string PLWAge { get; set; }

        [Required(ErrorMessage = "Start of being temporary DS client Required")]
        public string StartDateTDS { get; set; }
        
        [Required(ErrorMessage = "Date when temporary DS status will end Required")]
        public string EndDateTDS { get; set; }

        [Required(ErrorMessage = "Nutritional status of PLW is required")]
        public string NutritionalStatusPLW { get; set; }

        public string BabyDateOfBirth { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        public string NutritionalStatusInfant { get; set; }
        public string ChildProtectionRisk { get; set; }

        public Boolean AllowEdit { get; set; }

        public IEnumerable<YesNo> yesnos { get; set; }
        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<Gender> genders { get; set; }
        public IEnumerable<NutritionalStatus> nutrStatus { get; set; }
        public IEnumerable<SocialWorker> worker { get; set; }
        public IEnumerable<PLW> plws { get; set; }        
    }

    public class ModifyForm1B
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public int Kebele { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string PLW { get; set; }
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string SocialWorker { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public string CollectionDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        [Required(ErrorMessage = "Atleast one Member is required!")]
        public string MemberXml { get; set; }

        public string MedicalRecordNumber { get; set; }
        public string PLWAge { get; set; }
        public string StartDateTDS { get; set; }
        public string BabyDateOfBirth { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        public string EndDateTDS { get; set; }
        public string NutritionalStatusPLW { get; set; }
        public string NutritionalStatusInfant { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string Remarks { get; set; }
        public Boolean AllowEdit { get; set; }

        public IEnumerable<YesNo> yesnos { get; set; }
        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<Gender> genders { get; set; }
        public IEnumerable<NutritionalStatus> nutrStatus { get; set; }
        public IEnumerable<SocialWorker> worker { get; set; }
        public IEnumerable<PLW> plws { get; set; }
    }

    public class Form1BMembersGrid
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWDetailID { get; set; }
        public string BabyDateOfBirth { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        public string NutritionalStatusInfant { get; set; }
        //public string ChildProtectionRisk { get; set; }
    }

    public class CaptureForm1C
    {
        public int ColumnID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int Kebele { get; set; }
        [Required(ErrorMessage = "Select Kebele!")]
        public int KebeleID { get; set; }
        //[Required(ErrorMessage = "Gote is Required!")]
        public string Gote { get; set; }
        //[Required(ErrorMessage = "Gare is Required!")]
        public string Gare { get; set; }
        //[Required(ErrorMessage = "Caretaker Name required!")]
        public string NameOfCareTaker { get; set; }
        //[Required(ErrorMessage = "ID Number required!")]
        public string HouseHoldIDNumber { get; set; }
        public string CaretakerID { get; set; }
        public string MalnourishedChildName { get; set; }
        public string ChildID { get; set; }
        public string MalnourishedChildSex { get; set; }
        public string ChildDateOfBirth { get; set; }
        public decimal Age { get; set; }
        public string DateTypeCertificate { get; set; }
        public string MalnourishmentDegree { get; set; }
        public string StartDateTDS { get; set; }
        public string NextCNStatusDate { get; set; }
        public string ContinuityMCDate { get; set; }
        public string EndDateTDS { get; set; }
        [Required(ErrorMessage = "Social worker required!")]
        public string SocialWorker { get; set; }
        [Required(ErrorMessage = "CCC/CBSPCMember required!")]
        public string CCCCBSPCMember { get; set; }
        [Required(ErrorMessage = "Collection Date required!")]
        public string CollectionDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string Remarks { get; set; }
        public Boolean AllowEdit { get; set; }

        public string ChildProtectionRisk { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }

        [Required(ErrorMessage = "Atleast one Member is required!")]
        public string MemberXml { get; set; }

        public IEnumerable<YesNo> yesnos { get; set; }
        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<Gender> genders { get; set; }
        public IEnumerable<NutritionalStatus> nutrStatus { get; set; }
        public IEnumerable<SocialWorker> worker { get; set; }
    }


    public class ModifyForm1C
    {
        public int ColumnID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int Kebele { get; set; }
        [Required(ErrorMessage = "Select Kebele!")]
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string NameOfCareTaker { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string CaretakerID { get; set; }
        public string MalnourishedChildName { get; set; }
        public string ChildID { get; set; }
        public string MalnourishedChildSex { get; set; }
        public string ChildDateOfBirth { get; set; }
        public decimal Age { get; set; }
        public string DateTypeCertificate { get; set; }
        public string MalnourishmentDegree { get; set; }
        public string StartDateTDS { get; set; }
        public string NextCNStatusDate { get; set; }
        public string ContinuityMCDate { get; set; }
        public string EndDateTDS { get; set; }
        public string SocialWorker { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string CollectionDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string Remarks { get; set; }
        public Boolean AllowEdit { get; set; }
        public string isUpdate { get; set; }

        public string ChildProtectionRisk { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }

        public IEnumerable<YesNo> yesnos { get; set; }
        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<Gender> genders { get; set; }
        public IEnumerable<NutritionalStatus> nutrStatus { get; set; }
        public IEnumerable<SocialWorker> worker { get; set; }
    }

    public class ServiceProviders
    {
        public int ServiceProviderID { get; set; }
        public int FormID { get; set; }
        public string ServiceProviderName { get; set; }
    }

    public class Form4ServiceProviders
    {
        public int ProviderServiceID { get; set; }
        //public int ServiceProviderID { get; set; }
        public string ServiceProviderName { get; set; }
    }

    public class IntegratedServices
    {
        public int ServiceID { get; set; }
        public string ServiceName { get; set; }
    }

    public class ExportForm1AModel
    {
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string Gote { get; set; }
        public string HouseHoldName { get; set; }
        public string HouseholdIDNumber { get; set; }
        public string ClientName { get; set; }
        public string IndividualID { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string EnrolledInSchool { get; set; }
        public string SchoolName { get; set; }
        public string Pregnant { get; set; }
        public string lactating { get; set; }
        public string CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedOn { get; set; }
        public string Remarks { get; set; }
    }

    public class ExportForm1BModel
    {
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string Gote { get; set; }
        public string HouseHoldName { get; set; }
        public string HouseholdIDNumber { get; set; }
        public string ClientName { get; set; }
        public string IndividualID { get; set; }
        public string PLWAge { get; set; }
        public string StartDateTDS { get; set; }
        public string EndDateTDS { get; set; }
        public string BabyDateOfBirth { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        public string NutritionalStatusPLW { get; set; }
        public string NutritionalStatusInfant { get; set; }
        public string CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedOn { get; set; }
        public string Remarks { get; set; }
    }

    public class ExportForm1CModel
    {
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string Gote { get; set; }
        public string CareTakerName { get; set; }
        public string HouseholdIDNumber { get; set; }
        public string ChildName { get; set; }
        public string ChildID { get; set; }
        public string ChildBirthDate { get; set; }
        public string DateTypeCertificate { get; set; }
        public string MalnourishmentDegree { get; set; }
        public string StartDateTDS { get; set; }
        public string NextCNStatusDate { get; set; }
        public string EndDateTDS { get; set; }
        public string CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedOn { get; set; }
        public string Remarks { get; set; }
    }

    public class ReportingPeriod
    {
        public int ColumnID { get; set; }
        public string PeriodID { get; set; }      
        
        [Required(ErrorMessage = "Period Name required")]
        public string PeriodName { get; set; }
        [Required(ErrorMessage = "Start Date required")]
        public string StartDate { get; set; }
        [Required(ErrorMessage = "End date required")]
        public string EndDate { get; set; }
        public string FiscalYearName { get; set; }
        public string CreatedBy { get; set; }
        public int FiscalYear { get; set; }
    }

    public class HouseholdVisit
    {
        public int ColumnID { get; set; }
        public string HouseholdVisitID { get; set; }
        [Required(ErrorMessage = "Household Visit Name required")]
        public string HouseholdVisitName { get; set; }
        public string CreatedBy { get; set; }
    }

    public class Region
    {
        public int RegionID { get; set; }
        public string RegionName { get; set; }
    }

    public class Woreda
    {
        public int WoredaID { get; set; }
        public int RegionID { get; set; }
        public string WoredaName { get; set; }
    }

    public class Kebele
    {
        public int KebeleID { get; set; }
        public int WoredaID { get; set; }
        public string KebeleName { get; set; }
    }

    public class ReportingIndicator
    {
        public int ReportingIndicatorId { get; set; }
        public string ReportingIndicatorName { get; set; }
    }
    

    public class FinancialYear
    {
        public int ColumnID { get; set; }
        public int FiscalYear { get; set; }
        public string FiscalYearName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string OpenedBy { get; set; }
        public string OpenedOn { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public string ClosedBy { get; set; }
        public string ClosedOn { get; set; }
    }

    public class FinancialYearGrid
    {
        public int ColumnID { get; set; }
        public int FiscalYear { get; set; }
        public string FiscalYearName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StatusName { get; set; }
    }

    public class ServiceProviderModel
    {
        public string FormCode { get; set; }
        public int KebeleID { get; set; }
        public int ReportingPeriodID { get; set; }
    }

    public class Gender
    {
        public string GenderID { get; set; }
        public string GenderName { get; set; }
    }

    public class YesNo
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class PLW
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class NutritionalStatus
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class SocialWorker
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
    public class SchoolGrade
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
    
    public class Form1AReportHeader
    {
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string HouseHoldID { get; set; }
        public string HouseholdHead { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public string CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CCCMember { get; set; }
    }

    public class Form1AReport
    {
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public string  MedicalRecordNumber { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string childUnderTSForCMAM { get; set; }        
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string EnrolledInSchool { get; set; }
        public string SchoolName { get; set; }
        public string Grade { get; set; }
        public string CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CCCCBSPCMember { get; set; }

        /* Added the new variables CP & CBHI */
        public string ChildProtectionRisk { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
    }

    public class Form1BReport
    {
        public string HouseHoldID { get; set; }
        public string HouseholdHead { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string Gote { get; set; }
        public string MedicalRecordNumber { get; set; }
        public int PLWAge { get; set; }
        public string StartDateTDS { get; set; }
        public string DateOfBirth { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        public string EndDateTDS { get; set; }
        public string NutritionalStatusPLW { get; set; }
        public string NutritionalStatusInfant { get; set; }
        public string CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string CreatedBy { get; set; }
        public string Remarks { get; set; }

        /* Added the new variables CP & CBHI */
        public string ChildProtectionRisk { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
    }

    public class Form1CReport
    {
        public string CaretakerID { get; set; }
        public string NameOfCareTaker { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string ChildID { get; set; }
        public string MalnourishedChildName { get; set; }
        public string MalnourishedChildSex { get; set; }
        public string ChildDateOfBirth { get; set; }
        public string DateTypeCertificate { get; set; }
        public string MalnourishmentDegree { get; set; }
        public string StartDateTDS { get; set; }
        public string NextCNStatusDate { get; set; }
        public string EndDateTDS { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string Gote { get; set; }
        public string CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string CreatedBy { get; set; }
        public string Remarks { get; set; }

        /* Added the new variables CP & CBHI */
        public string ChildProtectionRisk { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
    }

    public class Form7Grid
    {
        public int ColumnID { get; set; }
        public string RetargetingHeaderID { get; set; }
        public string HouseholdIDNumber { get; set; }
        public string NameOfHouseholdHead { get; set; }
        public int Members { get; set; }
        public string Kebele { get; set; }
        public string Gote { get; set; }
        public string FiscalYearName { get; set; }
        public string CreatedBy { get; set; }
        public string StatusID { get; set; }
    }

    public class Form7SummaryGrid
    {
        public int ColumnID { get; set; }
        public int KebeleID { get; set; }
        public string FiscalYearName { get; set; }
        public string RegionName { get; set; }        
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int Households { get; set; }
    }

    public class GenerateForm7
    {        
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Region must be selected")]
        public int RegionID { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Woreda must be selected")]
        public int WoredaID { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Kebele must be selected")]
        public int KebeleID { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Fiscal Year must be selected")]
        public int FiscalYear { get; set; }
        
        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<FinancialYear> fiscalYears { get; set; }
    }

    public class Form7ReportHeader
    {
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string FiscalYear { get; set; }
        public string RetargetingHeaderID { get; set; }
        public string HouseHoldID { get; set; }
        public string HouseholdHead { get; set; }        
        public string CreatedBy { get; set; }
    }

    public class Form7Report
    {
        public string RetargetingHeaderID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string ClientCategory { get; set; }
        public string Status { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string ChildUnderTSForCMAM { get; set; }        
        public string EnrolledInSchool { get; set; }
        public string SchoolName { get; set; }
        public string Grade { get; set; }
    }
        
    public class Form7Header
    {
        public int ColumnID { get; set; }
        public string RetargetingHeaderID { get; set; }
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public int WoredaID { get; set; }
        public string WoredaName { get; set; }
        public int KebeleID { get; set; }
        public string KebeleName { get; set; }
        public string Gote { get; set; }
        public string Gare { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public int FiscalYear { get; set; }        
        public string DateUpdated { get; set; }        
        public string SocialWorker { get; set; }        
        public string CCCCBSPCMember { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public bool ApprovalStatus { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovedOn { get; set; }
        public bool AllowEdit { get; set; }
        public bool Approving { get; set; }

        [Required(ErrorMessage = "Atleast one Member is required!")]
        public string MemberXml { get; set; }
        public string RetargetingDetailID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string ClientCategory { get; set; }
        public string ClientCategoryName { get; set; }
        public string Status { get; set; }
        public string StatusName { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string ChildUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string Grade { get; set; }
        public string SchoolName { get; set; }        
        public IEnumerable<YesNo> yesnos { get; set; }
        public IEnumerable<NutritionalStatus> nutrStatus { get; set; }
        public IEnumerable<SocialWorker> worker { get; set; }
        public IEnumerable<ClientStatus> clientStatus { get; set; }
        public IEnumerable<SchoolGrade> grades { get; set; }
    }

    public class Form7MembersGrid
    {
        public int ColumnID { get; set; }
        public string RetargetingDetailID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string IndividualID { get; set; }        
        public string DateOfBirth { get; set; }        
        public string Age { get { return Services.GeneralServices.GetAge(DateOfBirth); } }
        public string Sex { get; set; }
        public string NutritionalStatusName { get; set; }
        public string ClientCategoryName { get; set; }
        public string StatusName { get; set; }
        public string Status { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string ChildUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string SchoolName { get; set; }
    }
    
    public class ClientStatus
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}