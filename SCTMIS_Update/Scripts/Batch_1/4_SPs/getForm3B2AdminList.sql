ALTER PROC [dbo].[getForm3B2AdminList]
(
	@jtStartIndex	Int = 0,
	@jtPageSize		Int	= 10
)
AS
BEGIN
	-- 05-June-2018, get only for current FY by default
	declare @CurrentFiscalYear int
	select @CurrentFiscalYear = dbo.f_getFiscalYearFromDate (getdate())

	SELECT 
		ROW_NUMBER() OVER(ORDER BY KebeleID) AS ColumnID,
		KebeleID,
		RegionName,
		WoredaName, 
		KebeleName,
		HouseholdCount,
		ReportFileName,
		dbo.f_GetUserNameByUserID(GeneratedBy) GeneratedBy,
		CONVERT(VARCHAR(11), GeneratedOn, 106) GeneratedOn,
		GeneratedOn CreatedOn
	FROM ViewForm3BTDSPLWHeader WHERE PLW = 'L' AND FiscalYear = @CurrentFiscalYear
	ORDER BY CreatedOn Desc
	OFFSET @jtStartIndex ROWS
	FETCH NEXT @jtPageSize ROWS ONLY
END

