﻿$(document).ready(function () 
{
    $("#RegionID,#WoredaID,#KebeleID,#ReportingPeriod,#ReportName,#ReportDisaggregation,#SocialWorker").prop("disabled", false);
    $("#RegionID,#WoredaID,#KebeleID,#ReportingPeriod,#ReportName,#ReportDisaggregation,#SocialWorker").val('');


    $('#StartDateTDSPLW,#EndDateTDSPLW,#StartDateTDSCMC,#EndDateTDSCMC').Zebra_DatePicker({
        //direction: 1,    // boolean true would've made the date picker future only
        //direction: -1,
        format: 'M/Y',
        view: 'years'
    });
});

$("#cmdProduceReport").click(function () {
    $("#cmdProduceReport").prop("disabled", true);
    $("#progress").show();
    ProduceReport();

});

//$(document).on("change", "#ReportName",function () {
$("#ReportName").change(function () {
    var reportName = $("#ReportName").val();
    ManageFilters(reportName);
});


function ProduceReport() {
    var RegionID = $("#RegionID").val();
    var WoredaID = $("#WoredaID").val();
    var KebeleID = $("#KebeleID").val();
    var ReportingPeriod = $("#ReportingPeriod").val();
    var ReportName = $("#ReportName").val();
    var ReportDisaggregation = $("#ReportDisaggregation").val();

    /* RPT1 Filters */
    var SocialWorker = $("#SocialWorker").val();
    var Gender = $("#Gender").val();
    var Pregnant = $("#Pregnant").val();
    var Lactating = $("#Lactating").val();
    var Handicapped = $("#Handicapped").val();
    var ChronicallyIll = $("#ChronicallyIll").val();
    var NutritionalStatus = $("#NutritionalStatus").val();
    var ChildUnderTSForCMAM = $("#ChildUnderTSForCMAM").val();
    var EnrolledInSchool = $("#EnrolledInSchool").val();
    var ChildProtectionRisk = $("#ChildProtectionRisk").val();
    var CBHIMembership = $("#CBHIMembership").val();

    /* RPT2 Filters */
    var StartDateTDSPLW = $("#StartDateTDSPLW").val();
    var EndDateTDSPLW = $("#EndDateTDSPLW").val();
    var NutritionalStatusPLW = $("#NutritionalStatusPLW").val();
    var NutritionalStatusInfant = $("#NutritionalStatusInfant").val();

    /* RPT3 Filters */
    var StartDateTDSCMC = $("#StartDateTDSCMC").val();
    var EndDateTDSCMC = $("#EndDateTDSCMC").val();
    var MalnourishmentDegree = $("#MalnourishmentDegree").val();

    /* RPT4 Filters */
    var ClientType = $("#ClientType").val();
    var ServiceType = $("#ServiceType").val();

    // Validate ReportName & Report Disaggregation
    var html = '<ul>';
    var errors = [];
    var valid = true;
    if (ReportName == '' || ReportName == null) {
        errors.push('<li><b>Please Select Report Name</b></li>');
        valid = false;                  
    }
    if (ReportDisaggregation == '' || ReportDisaggregation == null) {
        errors.push('<li><b>Please Select Reporting Disaggregation</b></li>');
        valid = false;                  
    }

    if (!valid) 
    {
        html += errors.join('') + '</ul>'
        $('#validationerrors').show();
        $('#validationerrors').empty();
        $('#validationerrors').append(html);
        $("#progress").hide();
        $("#cmdProduceReport").prop("disabled", false);
        return;
    }
    else {
        $("#progress").show();
        $('#validationerrors').hide();
    }

    var rptFilters = {
        RegionId: RegionID,
        WoredaId: WoredaID,
        KebeleId: KebeleID,
        ReportingPeriod: ReportingPeriod,
        ReportName: ReportName,
        ReportDisaggregation: ReportDisaggregation,
        SocialWorker: SocialWorker,
        Gender: Gender,
        Pregnant: Pregnant,
        Lactating: Lactating,
        Handicapped: Handicapped,
        ChronicallyIll: ChronicallyIll,
        NutritionalStatus: NutritionalStatus,
        ChildUnderTSForCMAM: ChildUnderTSForCMAM,
        EnrolledInSchool: EnrolledInSchool,
        ChildProtectionRisk: ChildProtectionRisk,
        CBHIMembership: CBHIMembership,

        StartDateTDSPLW: StartDateTDSPLW,
        EndDateTDSPLW: EndDateTDSPLW,
        NutritionalStatusPLW: NutritionalStatusPLW,
        NutritionalStatusInfant: NutritionalStatusInfant,

        StartDateTDSCMC: StartDateTDSCMC,
        EndDateTDSCMC: EndDateTDSCMC,
        MalnourishmentDegree: MalnourishmentDegree,

        ClientType: ClientType,
        ServiceType: ServiceType
    };



    $.ajax({
        type: "POST",
        url: "/Security/StandardReports",
        data: JSON.stringify(rptFilters),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            window.location.href = '/Security/StandardReports';
            $('#success').show();
            $('#success').empty();
            $('#success').append('<ul><li><b>'+ data.Message +'</b></li></ul>');
            $("#progress").hide();

            $("#cmdProduceReport").prop("disabled", false);
        },
        error: function (xhr,error, errorThrown) {
            window.location.href = '/Security/ProduceStandardReports';
            $('#error').show();
            $('#error').empty();
            $('#error').append('<ul><li><b>'+ errorThrown +'</b></li></ul>');
            $("#progress").hide();
            $("#cmdProduceReport").prop("disabled", false);

        }
    });
}

function ManageFilters(reportName) {
    ResetFilters();
    
    // RPT1 Filters
    if(reportName=='RPT1'){
        $('#divRpt1Services').show();
        $('#divRpt1Gender').show();
        $('#divRpt1Pregnant').show();
        $('#divRpt1Lactating').show();
        $('#divRpt1Disabled').show();
        $('#divRpt1ChronicallyIll').show();
        $('#divRpt1NutritionalStatus').show();
        $('#divRpt1ChildUnderTSForCMAM').show();
        $('#divRpt1EnrolledInSchool').show();
        $('#divRpt1ChildProtectionRisk').show();
        $('#divRpt1CBHIMembership').show();
    }

    // RPT2 Filters
    if(reportName=='RPT2'){
        $('#divRpt2StartDateTDSPLW').show();
        $('#divRpt2EndDateTDSPLW').show();
        $('#divRpt2NutritionalStatusPLW').show();
        $('#divRpt2NutritionalStatusInfant').show();
        $('#divRpt1ChildProtectionRisk').show();
        $('#divRpt1CBHIMembership').show();
    }

    // RPT3 Filters
    if(reportName=='RPT3'){
        $('#divRpt3MalnourishmentDegree').show();
        $('#divRpt3StartDateTDSCMC').show();
        $('#divRpt3EndDateTDSCMC').show();
        $('#divRpt1ChildProtectionRisk').show();
        $('#divRpt1CBHIMembership').show();
    }

    // RPT4 Filters
    if(reportName=='RPT4'){
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT5 Filters
    if(reportName=='RPT5'){
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT6 Filters
    if(reportName=='RPT6'){
        $('#divRpt4TypeOfClient').show();
        $('#divRpt4TypeOfService').show();
    }

    // RPT7 Filters
    if(reportName=='RPT7'){
        $('#divRpt4TypeOfClient').show();
    }

    // RPT8 Filters
    if(reportName=='RPT8'){
        $('#divRpt4TypeOfClient').show();
    }

    // RPT9 Filters
    if(reportName=='RPT9'){
    }

    // RPT10 Filters
    if(reportName=='RPT10'){
    }

}


function ResetFilters() {
    $('#divRpt1Services').hide();
    $('#divRpt1Gender').hide();
    $('#divRpt1Pregnant').hide();
    $('#divRpt1Lactating').hide();
    $('#divRpt1Disabled').hide();
    $('#divRpt1ChronicallyIll').hide();
    $('#divRpt1NutritionalStatus').hide();
    $('#divRpt1ChildUnderTSForCMAM').hide();
    $('#divRpt1EnrolledInSchool').hide();
    $('#divRpt1ChildProtectionRisk').hide();
    $('#divRpt1CBHIMembership').hide();

    $('#divRpt2StartDateTDSPLW').hide();
    $('#divRpt2EndDateTDSPLW').hide();
    $('#divRpt2NutritionalStatusPLW').hide();
    $('#divRpt2NutritionalStatusInfant').hide();

    $('#divRpt3MalnourishmentDegree').hide();
    $('#divRpt3StartDateTDSCMC').hide();
    $('#divRpt3EndDateTDSCMC').hide();

    $('#divRpt4TypeOfClient').hide();
    $('#divRpt4TypeOfService').hide();

    $('#ServiceType').val('');
    $('#Gender').val('-');
    $('#Pregnant').val('');
    $('#Lactating').val('');
    $('#Handicapped').val('');
    $('#ChronicallyIll').val('');
    $('#NutritionalStatus').val('');
    $('#ChildUnderTSForCMAM').val('');
    $('#EnrolledInSchool').val('');
    $('#ChildProtectionRisk').val('');
    $('#CBHIMembership').val('');

    $('#StartDateTDSPLW').val('');
    $('#EndDateTDSPLW').val('');
    $('#NutritionalStatusPLW').val('');
    $('#NutritionalStatusInfant').val('');

    $('#MalnourishmentDegree').val('');
    $('#StartDateTDSCMC').val('');
    $('#EndDateTDSCMC').val('');

    $('#EndDateTDSCMC').val('');
    $('#EndDateTDSCMC').val('');

    $('#ClientType').val('-');
    $('#ServiceType').val('');
}