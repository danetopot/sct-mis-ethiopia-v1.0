﻿var page_count = 10;
var gridHeader = 'ColumnID,User<span class="HeaderChange">_</span>Name,Module<span class="HeaderChange">_</span>Name,PC<span class="HeaderChange">_</span>Name,Ip<span class="HeaderChange">_</span>Address,Transaction<span class="HeaderChange">_</span>Details, Date';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,img';

$(document).ready(function () {

    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,90,105,80,90,350,130,0");
    mygrid.setColAlign("right,right, left,left,left,right,left,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");
    //mygrid.attachEvent("onRowSelect", fnGridRowSelected);

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();

    //mygrid.loadXML("../Qualify/xmlGridListingData?RecCount=" + page_count);
    mygrid.loadXML("/Security/GridAuditTrailData?RecCount=" + page_count);
    dhtmlxError.catchError("ALL", my_error_handler);

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    //$('#SearchTypeID').append($("<option></option>").attr("value", 2).text('First Name'));
    $('#loading').hide();
});

function my_error_handler() {
}

function fnGridRowSelected() {
}

function reloadGrid() {    
    $("#cmdSearch,#SearchTypeID,#SearchKeyword").prop("disabled", true);

    var Search_TypeID = $("#SearchTypeID").val();
    var Search_Keyword = $("#SearchKeyword").val();
    showLoading(true);
    mygrid.clearAndLoad("/Security/GridAuditTrailData?RecCount=" + page_count + "&isSearch=1&SearchTypeID=" + Search_TypeID + "&SearchKeyword=" + Search_Keyword);
    if (window.a_direction)
        mygrid.setSortImgState(true, window.s_col, window.a_direction);

    $("#cmdSearch,#SearchTypeID,#SearchKeyword").prop("disabled", false);
}


