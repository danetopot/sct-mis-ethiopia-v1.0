﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.CaptureForm4>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Capture Form 4B
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<h5>Add New Girl Form</h5>   --%>     
<% using (Html.BeginForm("CaptureForm4B", "ComplianceCapture", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-7">                
            <label>
                Region <%: Html.ValidationMessageFor(model => model.RegionID) %>
                <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Woreda  <%: Html.ValidationMessageFor(model => model.WoredaID) %>
                <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Kebele <%: Html.ValidationMessageFor(model => model.KebeleID) %>  
                <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
            </label>
            </div>
            <div class="col-4">                
            <label>
                Name of Service Provider <%: Html.ValidationMessageFor(model => model.ProviderServiceID) %>
                <%: Html.DropDownListFor(model => model.ProviderServiceID,
                    new SelectList(Model.serviceprovids, "ProviderServiceID", "ServiceProviderName"), new { tabindex = "4", @class = "alphaonly" })%>
            </label>
            </div>
        <br />
            <div class="col-4">                              
            <label>
                Type of service to be accessed <%: Html.ValidationMessageFor(model => model.ServiceID) %>  
                <%: Html.DropDownListFor(model => model.ServiceID,
                    new SelectList(Model.intgrtedservis, "ServiceID", "ServiceName"), new { tabindex = "5" })%>
            </label>
            </div>
            <div class="col-4">                
            <label>
                Reporting Period <%: Html.ValidationMessageFor(model => model.ReportingPeriodID) %>
                <%: Html.DropDownListFor(model => model.ReportingPeriodID,
                    new SelectList(Model.reportgperiod, "PeriodID", "PeriodName"), new { tabindex = "6" })%>
            </label>
            </div>
            <button id="cmdProceed" type="button" class="btn btn-icon btn-primary glyphicons circle_plus" tabindex="20"><i></i>Load Grid</button>
          <br />
            <br />
            <div id="thebox" style="position:relative; width: 800px; height: 340px"></div>
            <%--<div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>--%>
          <br />
            <div class="form-actions">
	            <%--<button id="cmdProduce" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save Captured Form 4B Details</button>--%>
                <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
            </div>
            <br />
            <%: Html.ValidationSummary(true) %>
            <%: Html.HiddenFor(model => model.Kebele)%>    
            <%: Html.HiddenFor(model => model.Service)%>
            <%: Html.HiddenFor(model => model.CapturedXml)%> 
            <%: Html.HiddenFor(model => model.FormID, new { Value = "2" })%>              
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <link href="../Scripts/jtable/themes/lightcolor/gray/jtable.min.css" rel="stylesheet" />
    <script src="../Scripts/jtable/jquery.jtable.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#loading').hide();
            $("#KebeleID,#cmdProceed").prop("disabled", false);
            $("#KebeleID").val('0');
            $("#cmdBack").prop("disabled", false);
            $("#RegionID,#WoredaID,#cmdProduce").prop("disabled", true);
            $(":input").blur();
            $('#KebeleID').focus();

            $('#KebeleID').change(function (e) {
                $('#ProviderServiceID').empty();
                $.ajax({
                    type: "POST",
                    url: "../ComplianceCapture/Select4BServiceProvider",
                    data: "{'KebeleID' : '" + $("#KebeleID").val() + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    error: function (xx, yy) {
                        alert(yy);
                    },
                    success: function (data) {
                        for (var i = 0; i < data.length; i++) {
                            var d = data[i];
                            $('#ProviderServiceID').append($("<option></option>").attr("value", d.ProviderServiceID).text(d.ServiceProviderName));
                        }
                    }
                });
            });

            $('#ProviderServiceID').change(function (e) {
                $('#ServiceID').empty();
                $.ajax({
                    type: "POST",
                    url: "../ComplianceCapture/Select4BServiceNames",
                    data: "{'KebeleID' : '" + $("#KebeleID").val() + "','ServiceProviderID' : '" + $("#ProviderServiceID").val() + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    error: function (xx, yy) {
                        alert(yy);
                    },
                    success: function (data) {
                        for (var i = 0; i < data.length; i++) {
                            var d = data[i];
                            $('#ServiceID').append($("<option></option>").attr("value", d.ServiceID).text(d.ServiceName));
                        }
                    }
                });
            });

            $('#thebox').jtable({
                title: 'Capture Client Compliance Status',
                paging: true,
                actions: {
                    listAction: '/ComplianceCapture/getForm4BTDSPLWList',
                    updateAction: '/ComplianceCapture/CapturedForm4BData'
                },
                fields: {
                    ColumnID: {
                        key: true,
                        create: false,
                        edit: false,
                        list: false
                    },
                    ProfileTDSPLWID: {
                        key: true,
                        create: false,
                        edit: false,
                        list: false
                    },
                    ServiceID: {
                        key: true,
                        create: false,
                        edit: false,
                        list: false
                    },
                    KebeleID: {
                        key: true,
                        create: false,
                        edit: false,
                        list: false
                    },
                    ReportingPeriodID: {
                        key: true,
                        create: false,
                        edit: false,
                        list: false
                    },
                    NameOfPLW: {
                        title: 'HouseHold Member Name',
                        width: '15%',
                        create: false,
                        edit: false
                    },
                    HouseHoldIDNumber: {
                        title: 'HouseHold ID#',
                        width: '10%',
                        create: false,
                        edit: false
                    },
                    PLWAge: {
                        title: 'Age',
                        width: '5%',
                        create: false,
                        edit: false
                    },
                    HasComplied: {
                        title: 'Has Complied?',
                        width: '8%',
                        create: false,
                        edit: true,
                        options: { 'YES': 'YES', 'NO': 'NO' }
                    },
                    Remarks: {
                        title: 'Description',
                        width: '10%',
                        create: false,
                        edit: true
                    },
                }
            });
            $("#cmdProceed").click(function () {
                $('#thebox').jtable('load', { KebeleID: $('#KebeleID').val(), ReportingPeriodID: $('#ReportingPeriodID').val(), ServiceID: $('#ServiceID').val() });
            });

            $("#cmdBack").click(function () {
                window.location.href = "../ComplianceCapture/Form4BList";
            });
        });
    </script>
</asp:Content>
