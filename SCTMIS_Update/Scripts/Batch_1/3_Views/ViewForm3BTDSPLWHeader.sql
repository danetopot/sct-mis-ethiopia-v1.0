ALTER View [dbo].[ViewForm3BTDSPLWHeader]
AS
SELECT  Distinct
		ProfileTDSPLW.KebeleID,
		KebeleName,
		WoredaID,
		WoredaName,
		RegionID,HouseholdCount,
		RegionName,
		CheckListTDSPLW.PLW,
		CheckListTDSPLW.ReportFileName,
		CheckListTDSPLW.GeneratedBy,
		CheckListTDSPLW.GeneratedOn,
		CheckListTDSPLW.FiscalYear
FROM CheckListTDSPLW
INNER JOIN CoResponsibilityTDSPLW ON CheckListTDSPLW.CoRespTDSPLWID = CoResponsibilityTDSPLW.CoRespTDSPLWID
INNER JOIN ProfileTDSPLW ON CoResponsibilityTDSPLW.ProfileTDSPLWID = ProfileTDSPLW.ProfileTDSPLWID
--AND CheckListTDSPLW.PLW = ProfileTDSPLW.PLW
INNER JOIN ViewForm2BTDSPLWHeader ON ViewForm2BTDSPLWHeader.KebeleID = ProfileTDSPLW.KebeleID
AND CheckListTDSPLW.PLW = ViewForm2BTDSPLWHeader.PLW
GO

