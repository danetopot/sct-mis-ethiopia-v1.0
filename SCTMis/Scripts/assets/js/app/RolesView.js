﻿var page_count = 10;
var gridHeader = 'ColumnID,Role<span class="HeaderChange">_</span>ID,Role<span class="HeaderChange">_</span>Name,Profile';
var gridColType = 'ro,ro,ro,img';

$(document).ready(function () {

    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,65,210,90");
    mygrid.setColAlign("right, left, left, left");
    mygrid.setHeader(gridHeader);
    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");
    mygrid.attachEvent("onRowSelect", fnGridRowSelected);

    mygrid.preventIECaching(true);
    //mygrid.selectRowById($("#hdnRoleID").val());
    mygrid.selectRowById(1);

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();
    mygrid.loadXML("/Security/GridRoleData?RecCount=" + page_count);

    dhtmlxError.catchError("ALL", my_error_handler);
    $('#loading').hide();
});

function fnGridRowSelected() {

}
function my_error_handler() {
    //alert('Allah!!!');
}
function someFinction() {
    alert('Hurray!!');
}

function reloadGrid() {
    var Search_TypeID = $("#SearchTypeID").val();
    var Search_Keyword = $("#SearchKeyword").val();
    showLoading(true)
    mygrid.clearAndLoad("/Security/GridRoleData?RecCount=" + page_count + "&isSearch=1&SearchTypeID=" + Search_TypeID + "&SearchKeyword=" + Search_Keyword);
    if (window.a_direction)
        mygrid.setSortImgState(true, window.s_col, window.a_direction);
}

function createWindow(roleID) {
    var strLink = '/Security/RoleProfiles?RoleID=' + roleID;
    window.location.href = strLink;
}