USE [SCT-MIS]
GO
/****** Object:  StoredProcedure [dbo].[GetChildProtectionHHList]    Script Date: 25/07/2019 10:58:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[GetChildProtectionHHList]
(
	@KebeleID		INT,
	@ClientType		VARCHAR(5),
	@jtStartIndex	INT = 0,
	@jtPageSize		INT	= 10
)
AS
BEGIN
	DECLARE 
	@ErrorMsg VARCHAR(256),
	@CurrentFiscalYear INT
	SELECT @CurrentFiscalYear = dbo.f_getCurrentFiscalYear()

	IF @ClientType='PDS'
		SELECT
		T3.RegionName, T3.KebeleName, T3.WoredaName,T2.Gote+'/'+T2.Gare AS GoteGare, T1.ProfileDSDetailID,
		T2.HouseHoldIDNumber, T2.NameOfHouseHoldHead, T1.HouseHoldMemberName, 
		T1.Age HouseHoldMemberAge, CASE WHEN T1.Sex='M' THEN 'Male' ELSE 'FEMALE' END AS HouseHoldMemberSex,
		T2.FiscalYear
		FROM ProfileDSDetail T1 
		INNER JOIN ProfileDSHeader T2 ON T1.ProfileDSHeaderID=T2.ProfileDSHeaderID
		INNER JOIN AdminStructureView T3 ON T2.KebeleID=T3.KebeleID
		WHERE 
		DATEDIFF(MONTH,T1.DateOfBirth,GETDATE()) BETWEEN 72 AND 216
		AND (T2.FiscalYear = @CurrentFiscalYear)
		AND (T2.KebeleID = @KebeleID)
		ORDER BY T2.NameOfHouseHoldHead
		OFFSET @jtStartIndex ROWS
		FETCH NEXT @jtPageSize ROWS ONLY



	ELSE IF @ClientType='PLW'
		SELECT
		T3.RegionName, T3.KebeleName, T3.WoredaName,T2.Gote+'/'+T2.Gare AS GoteGare, T1.ProfileTDSPLWDetailID,
		T2.HouseHoldIDNumber, T2.NameOfPLW NameOfHouseHoldHead, ISNULL(T1.BabyName, 'Mother Pregnant') HouseHoldMemberName,
		CASE WHEN DATEDIFF(YEAR, T1.BabyDateOfBirth, GETDATE()) > 1 THEN CAST(DateDiff(Year, T1.BabyDateOfBirth, GETDATE()) AS VARCHAR) ELSE 0  END HouseHoldMemberAge,
		CASE WHEN T1.BabySex='M' THEN 'Male' ELSE 'FEMALE' END AS HouseHoldMemberSex,
		T2.FiscalYear
		FROM ProfileTDSPLWDetail T1 
		INNER JOIN ProfileTDSPLW T2 ON T2.ProfileTDSPLWID = T1.ProfileTDSPLWDetailID
		INNER JOIN AdminStructureView T3 ON T2.KebeleID=T3.KebeleID
		WHERE (T2.FiscalYear = @CurrentFiscalYear)
		AND (T2.KebeleID = @KebeleID)
		ORDER BY T2.NameOfPLW
		OFFSET @jtStartIndex ROWS
		FETCH NEXT @jtPageSize ROWS ONLY



	ELSE IF @ClientType='CMC'
	    SELECT 
		T3.RegionName, T3.KebeleName, T3.WoredaName,T1.Gote+'/'+T1.Gare AS GoteGare, T1.ProfileTDSCMCID,
		T1.HouseHoldIDNumber, T1.NameOfCareTaker NameOfHouseHoldHead, T1.MalnourishedChildName HouseHoldMemberName,
		CASE WHEN DATEDIFF(YEAR, T1.ChildDateOfBirth, GETDATE()) > 1 THEN CAST(DateDiff(Year, T1.ChildDateOfBirth, GETDATE()) AS VARCHAR) ELSE 0  END HouseHoldMemberAge,
		CASE WHEN T1.MalnourishedChildSex='M' THEN 'Male' ELSE 'FEMALE' END AS HouseHoldMemberSex,
		T1.FiscalYear
		FROM ProfileTDSCMC T1
		INNER JOIN AdminStructureView T3 ON T1.KebeleID=T3.KebeleID
		WHERE (T1.FiscalYear = @CurrentFiscalYear)
		AND (T1.KebeleID = @KebeleID)
		ORDER BY T1.NameOfCareTaker
		OFFSET @jtStartIndex ROWS
		FETCH NEXT @jtPageSize ROWS ONLY
	ELSE
		RAISERROR('An error occured in the backend',16,1)
		RETURN
END
--GO
--EXEC [dbo].[GetChildProtectionHHList] 3, 'PDS'

--1,'PDS',20192020,0,10
