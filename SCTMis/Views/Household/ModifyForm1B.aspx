﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ModifyForm1B>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Modify Form 1B
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<h5>Add New Girl Form</h5>   --%>
    <% using (Html.BeginForm("UpdateForm1B", "Household", FormMethod.Post, new { id = "MyForm" }))
   { %>
    <%--The First Row--%>
    <div class="col-7">
        <label>
            Region <%: Html.ValidationMessageFor(model => model.RegionID) %>
            <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Woreda <%: Html.ValidationMessageFor(model => model.WoredaID) %>
            <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Kebele <%: Html.ValidationMessageFor(model => model.KebeleID) %>
            <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Gote <%: Html.ValidationMessageFor(model => model.Gote) %>
            <%: Html.TextBoxFor(model => model.Gote, new { tabindex = "4" , @maxlength="30" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Gare <%: Html.ValidationMessageFor(model => model.Gare) %>
            <%: Html.TextBoxFor(model => model.Gare, new { tabindex = "5" , @maxlength="30" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            CBHI Membership
            <%: Html.DropDownListFor(model => model.CBHIMembership,
                            new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "6" })%>
        </label>
    </div>
    <br />
    <%--Second Row--%>
    <div class="col-4">
        <label>
            Collection Date
            <%: Html.TextBoxFor(model => model.CollectionDate, new { tabindex = "7"})%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Social Worker
            <%: Html.DropDownListFor(model => model.SocialWorker,
                    new SelectList(Model.worker, "ID", "Name"), new { tabindex = "8" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            CCC/CBSPC member
            <%: Html.TextBoxFor(model => model.CCCCBSPCMember, new { tabindex = "9" , @maxlength="25" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Pregnant/Lactating
            <%: Html.DropDownListFor(model => model.PLW,
                    new SelectList(Model.plws, "ID", "Name"), new { tabindex = "10" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            CBHI Number:
            <%: Html.TextBoxFor(model => model.CBHINumber, new { tabindex = "11" ,@maxlength="20" })%>
        </label>
    </div>
    <div id="errors" style="color: red;"></div>
    <hr />
    <%--Third Row--%>
    <div class="col-4">
        <label>
            Name of PLW incl. name of grandfather
            <%: Html.TextBoxFor(model => model.NameOfPLW, new { tabindex = "12" , @class = "alphaonly", @maxlength="50" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            PSNP HH Number
            <%: Html.TextBoxFor(model => model.HouseHoldIDNumber, new { tabindex = "13", @maxlength="20" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Individual ID in Health Family Folder
            <%: Html.TextBoxFor(model => model.MedicalRecordNumber, new { tabindex = "14" , @maxlength="20" })%>
        </label>
    </div>
    <div class="col-7">
        <label>
            Age of PLW
            <%: Html.TextBoxFor(model => model.PLWAge, new { tabindex = "15", @class = "numbersOnly", @maxlength="3"})%>
        </label>
    </div>
    <br />
    <%--Fourth Row--%>
    <div class="col-4">
        <label>
            Transitioning from PW to temporary
            <%: Html.TextBoxFor(model => model.StartDateTDS, new { tabindex = "16"})%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Expected temporary DS end Date
            <%: Html.TextBoxFor(model => model.EndDateTDS, new { tabindex = "17"})%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Nutritional status of PLW
            <%: Html.DropDownListFor(model => model.NutritionalStatusPLW,
                            new SelectList(Model.nutrStatus, "ID", "Name"), new { tabindex = "18" })%>
        </label>
    </div>

    <%--Fifth Row--%>
    <br />
    <div class="col-2">
        <label>
            Remarks (like client passed away, moved, etc.):
            <%: Html.TextBoxFor(model => model.Remarks, new { tabindex = "19" ,@maxlength="200" })%>
        </label>
    </div>

    <div class="col-4">
        <label>
            Potential Child Protection Risk?
            <%: Html.DropDownListFor(model => model.ChildProtectionRisk,
                            new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "20" })%>
        </label>
    </div>

    <%--Babies--%>
    <br />
    <hr />

    <div class="col-4">
        <label>
            Name of infant
            <%: Html.TextBoxFor(model => model.BabyName, new { tabindex = "21" , @class = "alphaonly",@maxlength="50" })%>
        </label>
    </div>
    <div class="col-4">
        <label>
            Date of birth of baby
            <%: Html.TextBoxFor(model => model.BabyDateOfBirth, new { tabindex = "22"})%>
        </label>
    </div>

    <div class="col-7">
        <label>
            Sex of infant
            <%: Html.DropDownListFor(model => model.BabySex,
                    new SelectList(Model.genders, "GenderID", "GenderName"), new { tabindex = "23" })%>
        </label>
    </div>
    <br />

    <div class="col-4">
        <label>
            Nutritional status of infant
            <%: Html.DropDownListFor(model => model.NutritionalStatusInfant,
                    new SelectList(Model.nutrStatus, "ID", "Name"), new { tabindex = "24" })%>
        </label>
    </div>

    <button id="cmdProceed" type="button" class="btn btn-icon btn-primary glyphicons circle_plus"
        tabindex="22"><i></i>Modify Baby Details</button>

    <button id="cmdNewMember" type="button" class="btn btn-icon btn-primary glyphicons circle_plus"
        tabindex="23"><i></i>Add New Baby</button>
    <br />

    <div id="errors2" style="color: red;"></div>
    <%: Html.ValidationSummary(true) %>
    <br />
    <div id="gridbox" style="position:relative; width: 790px; height: 120px"></div>
    <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>
    <br />

    <hr />
    <div class="form-actions">
        <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"
            tabindex="21"><i></i>Modify Form 1B</button>
        <button id="cmdBack" type="button"
            class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
    </div>
    <%: Html.HiddenFor(model => model.ColumnID)%>
    <%: Html.HiddenFor(model => model.ProfileTDSPLWID)%>
    <%: Html.HiddenFor(model => model.Kebele)%>
    <%: Html.HiddenFor(model => model.MemberXml)%>
    <%: Html.HiddenFor(model => model.AllowEdit)%>
    <%: Html.HiddenFor(model => model.KebeleID,new{@id="KebeleId"})%>
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript" src="../Scripts/assets/js/jquery.mask.min.js"></script>
    <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
    <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>

    <script type="text/javascript">
        var EVENTID = null;
        var arrObj = [];
        var SerialCount = 0;
        var page_count = 4;
        var mygrid;
        var gridHeader = 'ColumnID,ProfileTDSPLWDetailID,Baby DOB,Baby Name,Baby Gender,Baby Nutrition,Edit,Delete';
        var gridColType = 'ro,ro,ro,ro,ro,ro,img,img';

        //var mycalStartTDS, mycalSEndTDS, myBabyDOB, myColDate;
        $(document).ready(function () {

            mygrid = new dhtmlXGridObject('gridbox');
            mygrid.clearAll();
            mygrid.setImagePath("../DHTMLX/codebase/imgs/");
            mygrid.setInitWidths("0,0,150,300,120,100,60,60");
            mygrid.setColAlign("left,left,left,right,left,right,right,right");
            mygrid.setHeader(gridHeader);

            mygrid.setColTypes(gridColType);
            mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
            mygrid.setPagingSkin("bricks");
            mygrid.setSkin("dhx_skyblue");

            mygrid.attachEvent("onXLE", showLoading);
            mygrid.attachEvent("onXLS", function () { showLoading(true) });
            mygrid.init();
            mygrid.loadXML("/Household/FetchGridForm1BDetailsByID?RecCount=" + page_count + "&ProfileTDSPLWID=" + $("#ProfileTDSPLWID").val());
            dhtmlxError.catchError("ALL", my_error_handler);

            $.ajax({
                type: "POST",
                url: "../Household/FetchJsonForm1BDetailsByID",
                data: "{  'RecCount':" + page_count + ",ProfileTDSPLWID : '" + $("#ProfileTDSPLWID").val() + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                error: function (xx, yy) {
                    //alert(yy);
                },
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        var d = data[i];
                        MemberCount = data.length;
                        if (MemberCount != undefined) {

                            for (var i = 0; i < data.length; i++) {
                                var theDtls = new Object;
                                var d = data[i];
                                SerialCount = SerialCount + 1;
                                theDtls.ColumnID = SerialCount;
                                theDtls.ProfileTDSPLWDetailID = d.ProfileTDSPLWDetailID;
                                theDtls.BabyDateOfBirth = d.BabyDateOfBirth;
                                theDtls.BabyName = d.BabyName;
                                theDtls.BabySex = d.BabySex;
                                theDtls.NutritionalStatusInfant = d.NutritionalStatusInfant;
                                //theDtls.ChildProtectionRisk = d.ChildProtectionRisk;

                                arrObj.push(theDtls);
                            }
                            $("#MemberXml").val(JSON.stringify(arrObj));
                        }
                    }
                }
            });


            $('#loading').hide();
            $('#CollectionDate,#BabyDateOfBirth').Zebra_DatePicker({
                direction: -1,    // boolean true would've made the date picker future only
                format: 'd/M/Y'
            });

            //plwChange();

            $('#EndDateTDS').Zebra_DatePicker({
                //direction: +1,    // boolean true would've made the date picker future only
                format: 'M/Y'
            });

            $('#StartDateTDS').Zebra_DatePicker({
                //direction: 1,    // boolean true would've made the date picker future only
                format: 'M/Y'
            });

            $("#RegionID,#WoredaID,#KebeleID").prop("disabled", true);

            $('#PLW').change(function (e) {
                plwChange();
            });

            $("#cmdModify,#cmdProceed,#cmdNewMember").prop("disabled", true);
            if ($("#AllowEdit").val() == 'True') {
                plwChange();
                if ($('#PLW') === 'L') {
                    $("#cmdNewMember").prop("disabled", false);
                }
            }

            $("#cmdNewMember").click(function () {
                EVENTID = "ADD";
                $("#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant").val("");

                $("#cmdNewMember").prop("disabled", true);
                $("#cmdProceed").prop("disabled", false);

                $("#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant").prop("disabled", false);
                $("#CollectionDate,#StartDateTDS,#EndDateTDS,#KebeleID,#Gote,#Gare,#SocialWorker,#CCCCBSPCMember,#PLW,#NameOfPLW,#HouseHoldIDNumber,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#EndDateTDS,#NutritionalStatusPLW,#Remarks").prop("disabled", false);

                $('#BabyName').focus();
            });

            $("#cmdProceed").click(function () {
                //EVENTID = "ADD";
                var errors = [];
                var html = '<ul>';
                var valid = true;
                $('#errors2').empty();
                if ($('#BabyName').val() == '') {
                    errors.push('<li>Baby Name is Required</li>');
                    valid = false;
                }

                if ($('#BabyDateOfBirth').val() == "" && parseInt($('#Age').val()) <= 0) {
                    errors.push('<li>Baby Date Of Birth is required</li>');
                    valid = false;
                }

                if ($('#BabySex').val() == '') {
                    errors.push('<li>Baby Gender is Required</li>');
                    valid = false;
                }

                if ($('#NutritionalStatusInfant').val() == '') {
                    errors.push('<li>Baby NutritionalStatus is required</li>');
                    valid = false;
                }

                /*
                if ($('#ChildProtectionRisk').val() == null) {
                    errors.push('<li>Please specify if Child Potentially at Risk</li>');
                    valid = false;
                }
                */


                if (!valid) {
                    html += errors.join('') + '</ul>';
                    $('#errors2').show();
                    $('#errors2').append(html);
                    return valid;
                }
                else {
                    $('#errors2').hide();
                }

                var theDtls = new Object;
                if (EVENTID == "EDIT") {
                    for (var i in arrObj) {
                        if (arrObj.hasOwnProperty(i)) {
                            if (arrObj[i].ColumnID == $("#ColumnID").val()) {
                                arrObj[i].ProfileTDSPLWDetailID = $("#ProfileTDSPLWDetailID").val();
                                arrObj[i].BabyDateOfBirth = $("#BabyDateOfBirth").val();
                                arrObj[i].BabyName = $("#BabyName").val();
                                arrObj[i].BabySex = $("#BabySex").val();
                                arrObj[i].NutritionalStatusInfant = $("#NutritionalStatusInfant").val();
                                //arrObj[i].ChildProtectionRisk = $("#ChildProtectionRisk").val();
                            }
                        }
                    }
                }
                else {
                    $('#errors2').hide();

                    SerialCount = SerialCount + 1;
                    theDtls.ColumnID = SerialCount;
                    theDtls.ProfileTDSPLWDetailID = $("#ProfileTDSPLWDetailID").val();
                    theDtls.BabyDateOfBirth = $("#BabyDateOfBirth").val();
                    theDtls.BabyName = $("#BabyName").val();
                    theDtls.BabySex = $("#BabySex").val();
                    theDtls.NutritionalStatusInfant = $("#NutritionalStatusInfant").val();
                    //theDtls.ChildProtectionRisk = $("#ChildProtectionRisk").val();

                    arrObj.push(theDtls);
                }
                var myXml = createXmlstring(arrObj, 1);
                mygrid.clearAll();
                mygrid.parse(myXml);

                $("#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant").val('');
                $("#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant").prop("disabled", true);

                $("#cmdNewMember").prop("disabled", true);
                if ($("#AllowEdit").val() == 'True') {
                    $("#cmdModify").prop("disabled", false);
                    $("#cmdNewMember").prop("disabled", false);
                }

                $("#cmdProceed").prop("disabled", true);
            });

            function plwChange() {
                if ($("#PLW").val() == "P") {
                    $('#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant').attr('disabled', "disabled");
                    $("#BabySex option[value='M']").attr("disabled", "disabled");
                    $("#BabySex option[value='F']").attr("disabled", "disabled");

                    $("#BabySex").attr("readonly", "readonly");
                    $('#BabyDateOfBirth').val('');

                    $("#cmdNewMember").prop("disabled", true);

                    $("#NutritionalStatusInfant option[value='N']").attr("disabled", "disabled");
                    $("#NutritionalStatusInfant option[value='M']").attr("disabled", "disabled");
                    $("#NutritionalStatusInfant option[value='SM']").attr("disabled", "disabled");


                    //$("#ChildProtectionRisk").val("");
                } else {
                    $('#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant').attr('disabled', false);
                    $("#BabySex option[value='F']").attr("disabled", false);
                    $("#BabySex option[value='M']").attr("disabled", false);

                    $("#NutritionalStatusInfant option[value='N']").attr("disabled", false);
                    $("#NutritionalStatusInfant option[value='M']").attr("disabled", false);
                    $("#NutritionalStatusInfant option[value='SM']").attr("disabled", false);

                    $("#cmdNewMember").prop("disabled", false);

                    //$("#ChildProtectionRisk").prop("disabled", false);
                }
            };


            if ($("#AllowEdit").val() == 'True') {
                $("#cmdSave").prop("disabled", false);
                $("#CollectionDate,#StartDateTDS,#EndDateTDS,#BabyDateOfBirth,#KebeleID,#Gote,#Gare,#SocialWorker,#CCCCBSPCMember,#PLW,#NameOfPLW,#HouseHoldIDNumber,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#BabyDateOfBirth,#BabyName,#BabySex,#EndDateTDS,#NutritionalStatusPLW,#NutritionalStatusInfant,#Remarks").prop("disabled", false);
                $("#cmdSave").val('Modify Details');
            } else {
                $("#cmdSave").prop("disabled", true);
                $("#cmdSave").val('Modify Already Done');

                $("#CollectionDate,#StartDateTDS,#EndDateTDS,#BabyDateOfBirth,#KebeleID,#Gote,#Gare,#SocialWorker,#CCCCBSPCMember,#PLW,#NameOfPLW,#HouseHoldIDNumber,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#BabyDateOfBirth,#BabyName,#BabySex,#EndDateTDS,#NutritionalStatusPLW,#NutritionalStatusInfant,#Remarks").prop("disabled", true);
            }



            $("#cmdBack").click(function () {
                window.location.href = "/Household/Form1B";
            });
            $('#PLW').focus();

            $("#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant").prop("disabled", true);

            $('#CBHIMembership').change();
        });

        function UpdateMembers(_ColumnID) {
            createWindow(_ColumnID);
        }

        function createXmlstring(arrObject, inMemory) {
            var xml;
            var gridID;
            var reclength = 0;
            xml = '';
            for (var i in arrObj) {
                if (arrObj.hasOwnProperty(i)) {
                    reclength + reclength + 1;
                    xml = xml + '<row id="' + i + '">';
                    for (var j in arrObj[i]) {
                        if (arrObj[i].hasOwnProperty(j)) {
                            if (j == "ColumnID") {
                                xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                                gridID = arrObj[i][j];
                            }
                        }
                        if (j == "ProfileTDSPLWDetailID" || j == "BabyDateOfBirth" || j == "BabyName" || j == "BabySex"
                            || j == "NutritionalStatusInfant") {
                            xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                        }
                    }
                    xml = xml + '<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit^javascript:UpdateMembers(' + gridID + ');^_self</cell>';
                    xml = xml + '<cell>../DHTMLX/codebase/imgs/but_cut.gif^Delete record^javascript:DeleteRecord(' + gridID + ');^_self</cell>';
                    xml = xml + '</row>';
                }
            }
            xml = '<rows total_count="' + reclength + '">' + xml + '</rows>';
            $("#MemberXml").val(JSON.stringify(arrObject));

            return xml;
        }

        function createWindow(_ColumnID) {
            var selectedRow = mygrid.getSelectedId();
            _ColumnID = mygrid.cells(selectedRow, 0).getValue();
            $("#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant").val('');
            for (var i in arrObj) {
                if (arrObj.hasOwnProperty(i)) {
                    if (arrObj[i].ColumnID == _ColumnID) {
                        $("#ColumnID").val(arrObj[i].ColumnID);
                        $("#BabyDateOfBirth").val(arrObj[i].BabyDateOfBirth);
                        $("#BabyName").val(arrObj[i].BabyName);
                        $("#BabySex").val(arrObj[i].BabySex);
                        $("#NutritionalStatusInfant").val(arrObj[i].NutritionalStatusInfant);
                        //$("#ChildProtectionRisk").val(arrObj[i].ChildProtectionRisk);
                    }
                }
            }

            $("#cmdModify,#cmdProceed,#cmdNewMember").prop("disabled", false);
            $("#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant").prop("disabled", false);


            $("#cmdModify,#cmdProceed,#cmdNewMember").prop("disabled", true);
            if ($("#AllowEdit").val() === 'True') {
                $("#cmdProceed,#cmdNewMember").prop("disabled", false);
                $("#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant").prop("disabled", false);
                $('#BabyName').focus();
            }


            EVENTID = "EDIT";
        }

        function DeleteRecord(_ColumnID) {
            if ($("#AllowEdit").val().toUpperCase() != "FALSE") {
                var selectedRow = mygrid.getSelectedId();
                var columnID = mygrid.cells(selectedRow, 0).getValue();
                createWindow(columnID);
                var result = confirm("Do you want to delete The Record?");
                if (result) {
                    for (var i in arrObj) {

                        if (arrObj.hasOwnProperty(i)) {
                            if (arrObj[i].ColumnID == columnID) {
                                var index = arrObj.indexOf(arrObj[i]);
                                if (index > -1) {
                                    arrObj.splice(index, 1);

                                    var myXml = createXmlstring(arrObj, 1);
                                    mygrid.clearAll();
                                    mygrid.parse(myXml);

                                    break;
                                }
                            }
                        }
                    }
                }

                $("#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant").val('');
                $("#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant").prop("disabled", true);
                $("#cmdModify").prop("disabled", false);
                $("#cmdProceed,#cmdNewMember").prop("disabled", true);
            }
        }

        $('#CBHIMembership').change(function () {
            var membership = $('#CBHIMembership').val();
            if (membership == 'YES') {
                $('#CBHINumber').prop("disabled", false);
            } else {
                $('#CBHINumber').prop("disabled", true);
                $('#CBHINumber').val("");
            }
        });
    </script>
</asp:Content>