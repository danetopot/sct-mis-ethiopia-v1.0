﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Form 4C Listing
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.Partial("_CommonSearch") %>
    <div id="gridbox" style="position:relative; width: 910px; height: 250px"></div>
    <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>

    <div class="form-actions">
	    <button id="cmdNew" type="button" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>Produce Form 4C Compliance</button>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">

    <script type="text/javascript" src="../Scripts/assets/js/app/Form4CList.js"></script>

</asp:Content>
