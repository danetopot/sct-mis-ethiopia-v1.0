﻿var page_count = 10;

var gridHeader = 'ColumnID,KebeleID,Financial<span class="HeaderChange">_</span>Year,Region,Woreda, Kebele,Households,Edit,print';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,img,img';
var mygrid;

function createWindow(_kebeleID) {

    var selectedRow = mygrid.getSelectedId();
    _kebeleID = mygrid.cells(selectedRow, 1).getValue();
    post('../ReTargeting/Form7', { kebeleID: _kebeleID });
}

function PreviewPdfFile(_kebeleID) {
    var selectedRow = mygrid.getSelectedId();
    _kebeleID = mygrid.cells(selectedRow, 1).getValue();
    
    get('../ReTargeting/PrintForm7', { kebeleID: _kebeleID });
}

$(document).ready(function () {
    $('#loading').hide();
    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,0,120,120,120,120,90,50,50");
    mygrid.setColAlign("left,left,right,right,right,right,right,right,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();

    //mygrid.loadXML("/Household/Form1AHHList?RecCount=" + page_count);

    // On edit of a record, user want's the grid to show the edited records page
    var currentPage = $('#currentPage').val();
    mygrid.load("/ReTargeting/Form7SummaryList?RecCount=" + page_count, function () {
        mygrid.changePage(currentPage);
    })

    mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
        $('#currentPage').val(ind);

        $.ajax({
            type: "POST",
            url: "../ReTargeting/UpdateCurrentPage",
            data: "{ 'CurrentPage':" + ind + "}",
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        });
    });
    //end here, user edit page demand

    dhtmlxError.catchError("ALL", my_error_handler);

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Household ID'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('Household Head'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 1).text('Kebele'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('Gote'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 5).text('Gare'));

    var canGenerateForm7 = $('#canGenerateForm7').val();
    if (canGenerateForm7 == 'True') {
        $("#cmdNew").show();
    } else {
        $("#cmdNew").hide();
    }

    $("#cmdNew").click(function () {
        window.location.href = "/ReTargeting/GenerateForm7";
    });
});

function reloadGrid() {
    $("#cmdSearch,#SearchTypeID,#SearchKeyword").prop("disabled", true);

    var Search_TypeID = $("#SearchTypeID").val();
    var Search_Keyword = $("#SearchKeyword").val();
    showLoading(true);

    mygrid.clearAndLoad("/ReTargeting/Form7SummaryList?RecCount=" + page_count + "&isSearch=1&SearchTypeID=" + Search_TypeID + "&SearchKeyword=" + Search_Keyword);

    $("#cmdSearch,#SearchTypeID,#SearchKeyword").prop("disabled", false);
}


