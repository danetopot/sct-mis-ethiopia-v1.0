﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCTMis.Models
{


    public class CapturedForm4Models
    {
        public int KebeleID { get; set; }
        public string ReportingPeriodID { get; set; }
        public Int16 ServiceID { get; set; }
        public Int16 jtStartIndex { get; set; }
        public Int16 jtPageSize { get; set; }
    }

    public class Form4AGrid
    {
        public int ColumnID { get; set; }
        public Int32 CoRespDSHeaderID { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string ReportFileName { get; set; }        
    }

    public class ProduceForm4A
    {
        public int ColumnID { get; set; }
        public string ComplianceID { get; set; }
        public int ServiceProviderID { get; set; }
        public int ReportingPeriodID { get; set; }
        public int ServiceID { get; set; }
        //[RequiredIf("SalesID==1", ErrorMessage = "License is required.")]
        public string Grade { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public int FormID { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<ServiceProviders> serviceprovids { get; set; }
        public IEnumerable<IntegratedServices> intgrtedservis { get; set; }
        public IEnumerable<ReportingPeriod> reportgperiod { get; set; }
        public IEnumerable<SchoolGrade> grades { get; set; }         
    }

    public class ServiceIDModel
    {
        public Int32 ServiceID { get; set; }
    }

    public class Form4AReport
    {
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string ReportingPeriodName { get; set; }
        public string ServiceProvider { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string IndividualID { get; set; }
        public string CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CreatedBy { get; set; }  
    }

    public class Form4ACompliance
    {
        public string ProfileDSDetailID { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string IndividualID { get; set; }       
    }

    public class ProduceForm4B
    {
        public int ColumnID { get; set; }
        public string ComplianceID { get; set; }
        public int ServiceProviderID { get; set; }
        public int ReportingPeriodID { get; set; } 
        public int ServiceID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public int FormID { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<ServiceProviders> serviceprovids { get; set; }
        public IEnumerable<IntegratedServices> intgrtedservis { get; set; }
        public IEnumerable<ReportingPeriod> reportgperiod { get; set; }
    }

    public class Form4BReport
    {
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string ReportingPeriodName { get; set; }
        public string ServiceProvider { get; set; }
        public Int32 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public int PLWAge { get; set; }
        public string BabySex { get; set; }
        public string MedicalRecordNumber { get; set; }
        public string PLW { get; set; }
        public string CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CreatedBy { get; set; }
    }
    public class Form4BCompliance
    {
        public string ProfileTDSPLWID { get; set; }
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public int PLWAge { get; set; }
        public string BabySex { get; set; } 
        public string MedicalRecordNumber { get; set; }
        public string PLW { get; set; }
    }

    public class ProduceForm4C
    {
        public int ColumnID { get; set; }
        public string ComplianceID { get; set; }
        public int ServiceProviderID { get; set; }
        public int ReportingPeriodID { get; set; } 
        public int ServiceID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public int FormID { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<ServiceProviders> serviceprovids { get; set; }
        public IEnumerable<IntegratedServices> intgrtedservis { get; set; }
        public IEnumerable<ReportingPeriod> reportgperiod { get; set; }
    }

    public class ProduceForm4D
    {
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public int FormID { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<ReportingPeriod> reportgperiod { get; set; }
    }

    public class Form4CReport
    {
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string ReportingPeriodName { get; set; }
        public string ServiceProvider { get; set; }
        public int ServiceID { get; set; }
        public string CaretakerID { get; set; }
        public string ServiceName { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CreatedBy { get; set; }
    }

    public class Form4CCompliance
    {
        public Int32 ProfileTDSCMCID { get; set; }
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string HouseHoldIDNumber { get; set; }
    }

    public class CapturedForm4MainAGrid
    {
        public int ColumnID { get; set; }
        public Int32 KebeleID { get; set; }
        public string ReportingPeriodID { get; set; }
        public string ReportingPeriod { get; set; }
        public string KebeleName { get; set; }
        public string ServiceProvider { get; set; }
        public Int16 ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string CreatedBy { get; set; }
    }

    public class CapturedForm4AGrid
    {
        public int ColumnID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string KebeleName { get; set; }
        public string ServiceName { get; set; }
        public string HouseHoldMemberName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public int Age { get; set; }
        public string Sex { get; set; }
        public string CreatedBy { get; set; }
        public string Complied { get; set; }
        public string Remarks { get; set; }
        public string GeneratedOn { get; set; }
    }

    public class CapturedForm4BGrid
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string KebeleName { get; set; }
        public string ServiceName { get; set; }
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public int PLWAge { get; set; }
        public string Complied { get; set; }
        public string Remarks { get; set; }
        public string CapturedBy { get; set; }
        public string CapturedOn { get; set; }
    }

    public class CapturedForm4Model
    {
        public int KebeleID { get; set; }
        public int ReportingPeriodID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public Int32 ServiceID { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
    }

    public class CapturedForm4BModel
    {
        public int KebeleID { get; set; }
        public int ReportingPeriodID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string ServiceID { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
    }

    public class CapturedForm4CModel
    {
        public int KebeleID { get; set; }
        public int ReportingPeriodID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string ServiceID { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
    }

    public class CapturedForm4CGrid
    {
        public int ColumnID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string KebeleName { get; set; }
        public string ServiceName { get; set; }
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string Complied { get; set; }
        public string Remarks { get; set; }
        public string CapturedBy { get; set; }
        public string CapturedOn { get; set; }
    }

    public class CaptureForm4
    {
        public int ColumnID { get; set; }
        public string ComplianceID { get; set; }
        public int ServiceProviderID { get; set; }
        public int ProviderServiceID { get; set; }
        public int ReportingPeriodID { get; set; } 
        public Int16 UniqueID { get; set; }
        public int ServiceID { get; set; }
        public int Service { get; set; }
        public int RegionID { get; set; }
        
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public int Kebele { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
		public string HasComplied { get; set; }
        public string Remarks { get; set; }
        public string CapturedXml { get; set; }
        public int FormID { get; set; }
        
        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<Form4ServiceProviders> serviceprovids { get; set; }
        public IEnumerable<IntegratedServices> intgrtedservis { get; set; }
        public IEnumerable<ReportingPeriod> reportgperiod { get; set; }
    }

    public class CapturedForm4XMLDetails
    {
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
        public Int16 UniqueID { get; set; }
    }

    public class Form4ADSMainModel
    {
        public int KebeleID { get; set; }
        public Int32 ReportingPeriodID { get; set; }
        public int jtStartIndex { get; set; }
        public int jtPageSize { get; set; }
    }

    public class getForm4ADSMainList
    {
        public int ColumnID { get; set; }
        public Int32 ReportingPeriodID { get; set; }
        public string KebeleID { get; set; }
        public string KebeleName { get; set; }
        public string ReportingPeriod { get; set; }
        public string ServiceID { get; set; }
        public string ServiceProvider { get; set; }
        public string ServiceName { get; set; }
        public string Records { get; set; }
    }

    public class getForm4ADSList
    {
        public int ColumnID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string ServiceID { get; set; }
        public Int32 ReportingPeriodID { get; set; }
        public string KebeleID { get; set; }
        public string NameOfHouseHoldHead { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public int Age { get; set; }
        public string Sex { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
    }

    public class getForm4BTDSPLWList
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string ServiceID { get; set; }
        public Int32 ReportingPeriodID { get; set; }
        public string KebeleID { get; set; }
        public string NameOfPLW { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public int PLWAge { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
    }

    public class getForm4CTDSCMCList
    {
        public int ColumnID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string ServiceID { get; set; }
        public Int32 ReportingPeriodID { get; set; }
        public string KebeleID { get; set; }
        public string NameOfCareTaker { get; set; }
        public string MalnourishedChildName { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string HasComplied { get; set; }
        public string Remarks { get; set; }
    }
}