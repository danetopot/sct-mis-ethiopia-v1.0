﻿using Newtonsoft.Json;
using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace SCTMis.Controllers
{
    public class Dashboard_OldController : Controller
    {
        [HttpGet]
        [Authorize]
        public XDocument FetchDashboardHouseholdByAge()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");
            int intRegionID = int.Parse(Request.QueryString["RegionCodeID"].ToString());
            int intWoredaID = int.Parse(Request.QueryString["WoredaCodeID"].ToString());
            int intKebeleID = int.Parse(Request.QueryString["KebeleCodeID"].ToString());
            try
            {
                var householdage
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<HouseholdByAge>(";Exec DashboardHouseholdByAge @RegionID,@WoredaID,@KebeleID",
                    new
                    {
                        RegionID = intRegionID,
                        WoredaID = intWoredaID,
                        KebeleID = intKebeleID
                    });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<HouseholdByAge>(householdage);

                string sb = GetXML(dt, "Number", "Age", "", "");
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("<xml>error</xml>");
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchDashboardHouseholdByVulnerability()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");
            //int intRegionID = int.Parse(Request.QueryString["RegionCodeID"].ToString());
            //int intWoredaID = int.Parse(Request.QueryString["WoredaCodeID"].ToString());
            //int intKebeleID = int.Parse(Request.QueryString["KebeleCodeID"].ToString());

            int intRegionID = 2;
            int intWoredaID = 2;
            int intKebeleID = 1;

            try
            {
                var householdVulnerability
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<HouseholdByVulnerability>(";Exec DashboardHouseholdByVulnerability @RegionID,@WoredaID,@KebeleID",
                    new
                    {
                        RegionID = intRegionID,
                        WoredaID = intWoredaID,
                        KebeleID = intKebeleID
                    });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<HouseholdByVulnerability>(householdVulnerability);

                string sb = GetXML(dt, "Number", "Vulnerability","","");
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("<xml>error</xml>");
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchDashboardPeriodLineChart()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");
            int intRegionID = int.Parse(Request.QueryString["RegionCodeID"].ToString());
            int intWoredaID = int.Parse(Request.QueryString["WoredaCodeID"].ToString());
            int intKebeleID = int.Parse(Request.QueryString["KebeleCodeID"].ToString());
            int intServiceID = int.Parse(Request.QueryString["ServiceID"].ToString());

            try
            {
                var householdVulnerability
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<PeriodLineChart>(";Exec DashboardPeriodLineChart @RegionID,@WoredaID,@KebeleID,@ServiceID", 
                    new { RegionID=intRegionID ,
                          WoredaID = intWoredaID,
                          KebeleID = intKebeleID,
                          ServiceID = intServiceID
                    });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<PeriodLineChart>(householdVulnerability);

                string sb = GetXML(dt, "Complete", "Complied", "UnComplied", "Months");
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("<xml>error</xml>");
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchDashboardFourthLineChart()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");
            int intRegionID = int.Parse(Request.QueryString["RegionCodeID"].ToString());
            int intWoredaID = int.Parse(Request.QueryString["WoredaCodeID"].ToString());
            int intKebeleID = int.Parse(Request.QueryString["KebeleCodeID"].ToString());

            try
            {
                var householdVulnerability
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<FourthLineChart>(";Exec DashboardFourthChart @RegionID,@WoredaID,@KebeleID",
                    new
                    {
                        RegionID = intRegionID,
                        WoredaID = intWoredaID,
                        KebeleID = intKebeleID
                    });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<FourthLineChart>(householdVulnerability);

                string sb = GetXML(dt, "Visits", "Cases", "Complied", "Months");
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("<xml>error</xml>");
            }
        }
        //[HttpGet]
        //[Authorize]
        //public JsonResult FetchDashboardPeriodLineChart()
        //{
        //    Response.Clear();
        //    Response.AddHeader("Content-Type", "text/json");

        //    try
        //    {
        //        var householdVulnerability
        //            = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<PeriodLineChart>(";Exec DashboardPeriodLineChart");

        //        DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<PeriodLineChart>(householdVulnerability);

        //        string sb = DataTableToJsonWithJsonNet(dt);
        //        return Json(sb, JsonRequestBehavior.AllowGet);
        //        //return Json(new { Result = "OK", Records = sb, TotalRecordCount = 0 });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}

        public string DataTableToJsonWithJsonNet(DataTable table)
        {
            string jsonString = string.Empty;
            jsonString = JsonConvert.SerializeObject(table);
            return jsonString;
        }

        public static string GetXML(DataTable dt, string col1, string col2, string col3, string col4, params KeyValuePair<string, object>[] pairs)
        {
            StringBuilder sb = new StringBuilder();
            string HeadStr = string.Empty;
            string TempStr = HeadStr;
            string itemVal;

            int rowID;
            sb.Append("<?xml version='1.0' encoding='UTF-8'?>");

            sb.Append("<data>");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                rowID = i;
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<item id='" + dt.Rows[i][j].ToString() + "'>");
                    }

                    if (dt.Columns[j].ToString() == col1 || dt.Columns[j].ToString() == col2 || dt.Columns[j].ToString() == col3 || dt.Columns[j].ToString() == col4)
                    {
                        sb.Append("<" + dt.Columns[j].ToString() + ">");
                        itemVal = dt.Rows[i][j].ToString().Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
                        if (itemVal == "False") { itemVal = "0"; }

                        sb.Append(itemVal);

                        sb.Append("</" + dt.Columns[j].ToString() + ">");
                    }
                }
                sb.Append("</item>");

            }
            sb.Append("</data>");

            string encodedXml = SCTMis.Services.GeneralServices.PrintXML(sb.ToString());

            XmlDocument contentxml = new XmlDocument();
            contentxml.LoadXml(@encodedXml);

            return encodedXml;
        }

    }
}
