﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.WoredaModifyModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Add New Woreda
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   
<% using (Html.BeginForm("UpdateWoreda", "Administration", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
        <div class="col-2">                
        <label>
            Region Name <%: Html.ValidationMessageFor(model => model.RegionID) %>
            <%: Html.DropDownListFor(model => model.RegionID,
                new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
        </label>
        </div>
    <br />
        <div class="col-2">                
        <label>
            Woreda Code <%: Html.ValidationMessageFor(model => model.WoredaCode) %>
            <%: Html.TextBoxFor(model => model.WoredaCode, new { tabindex = "2", @class = "alphaonly", @maxlength="3" })%>
        </label>
        </div>

        <div class="col-2">                
        <label>
            Woreda Name <%: Html.ValidationMessageFor(model => model.WoredaName) %>
            <%: Html.TextBoxFor(model => model.WoredaName, new { tabindex = "3", @class = "alphaonly", @maxlength="50" })%>
        </label>
        </div>
         <%: Html.HiddenFor(model => model.WoredaID)%>
        <%: Html.ValidationSummary(true) %><br />

    <div class="form-actions">
	    <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok" tabindex = "4"><i></i>Update Woreda</button>
	    <button id="cmdCancel" type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Back</button>
    </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script>
        $(document).ready(function () {
            $("#RegionID").prop("disabled", true);
            $("#WoredaCode").attr("readonly", true);

            $("#cmdCancel").click(function () {
                window.location.href = "/Administration/Woredas";
            });
            $('#loading').hide();
            $("#RegionID").focus()
        });
    </script>
</asp:Content>
