CREATE TABLE FiscalYears (
	FiscalYear		int NOT NULL,
	FiscalYearName	varchar(9) NULL,
	OpenedBy		bigint NOT NULL,
	OpenedOn		smalldatetime NOT NULL,
	[Status]		int NOT NULL,
	ClosedBy		bigint NULL,
	ClosedOn		smalldatetime NULL,
CONSTRAINT [PK_FiscalYears] PRIMARY KEY (FiscalYear ASC))
GO

ALTER TABLE FiscalYears WITH NOCHECK ADD CONSTRAINT FK_FiscalYears_OpenedBy FOREIGN KEY(OpenedBy)
REFERENCES Users (UserID)
GO

ALTER TABLE FiscalYears CHECK CONSTRAINT FK_FiscalYears_OpenedBy
GO

ALTER TABLE FiscalYears  WITH NOCHECK ADD CONSTRAINT FK_FiscalYears_ClosedBy FOREIGN KEY(ClosedBy)
REFERENCES Users (UserID)
GO

ALTER TABLE FiscalYears ADD CONSTRAINT UK_FiscalYearName UNIQUE (FiscalYearName)
GO