﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SCTMis.Models;
using SCTMis.Services;
using System.Xml.Linq;
using System.Data;

namespace SCTMis.Controllers
{
    [SessionConfig.SessionExpireFilter]
    public class ChecklistController : Controller
    {
        GeneralServices dbService = new GeneralServices();
        AccountServices dbAcService = new AccountServices();
        CoResponsibilityServices dbSCoResp = new CoResponsibilityServices();
        string ip = System.Web.HttpContext.Current.Request.UserHostAddress;
        //
        // GET: /Checklist/
        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]        
        public ActionResult Form3A()
        {
            ViewBag.Subtitle = "Form 3A Listing";

            dbAcService.InsertAuditTrail(User.Identity.Name, "Coresponsibility", "View Form3A", ip, "",
            "Form3A Accesses  at " + DateTime.Now.ToString());

            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form3AListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");

                List<Form2AGrid> form2AList = con.Fetch<Form2AGrid>(";Exec getForm3AAdminList @jtStartIndex, @jtPageSize", new { jtStartIndex, jtPageSize });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form2AGrid>(form2AList);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, form2AList.Count, jtStartIndex, 0, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("xml");
            }
        }

        //ProduceForm2A
        [Authorize]
        public ActionResult ProduceForm3A()
        {
            ViewBag.Subtitle = "Produce Form 3A";

            ProduceForm3A models = new Models.ProduceForm3A();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            //models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("3A", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm3A(ProduceForm3A newmodel)
        {
            ViewBag.Subtitle = "Produce Form 3A";
            string strErrMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbSCoResp.ProduceForm3A(newmodel, out strErrMessage);

                    if (string.IsNullOrEmpty(strErrMessage))
                    {
                        ViewBag.Subtitle = "Form 3A Listing";
                        return RedirectToAction("Form3A");
                    }
                }

                if (string.IsNullOrEmpty(strErrMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", strErrMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("3A", int.Parse(Session["WoredaID"].ToString()));

                return View("ProduceForm3A", newmodel);
                
            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("3A", int.Parse(Session["WoredaID"].ToString()));

                ModelState.AddModelError("", ex.Message);

                return View("ProduceForm3A", newmodel);
            }
        }

        //FORM 3B
        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form3B()
        {
            ViewBag.Subtitle = "Form 3B Listing";
            dbAcService.InsertAuditTrail(User.Identity.Name, "Coresponsibility", "View Form3B", ip, "",
            "Form3B Accesses  at " + DateTime.Now.ToString());
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form3BListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");

                List<Form2AGrid> form2BList = con.Fetch<Form2AGrid>(";Exec getForm3BAdminList @jtStartIndex, @jtPageSize", new { jtStartIndex, jtPageSize });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form2AGrid>(form2BList);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, form2BList.Count, jtStartIndex, 1, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("xml");
            }
        }
        //ProduceForm3B
        [Authorize]
        public ActionResult ProduceForm3B()
        {
            ViewBag.Subtitle = "Produce Form 3B";

            ProduceForm3B models = new Models.ProduceForm3B();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            //models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("3B1", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm3B(ProduceForm3B newmodel)
        {
            ViewBag.Subtitle = "Produce Form 3B";
            string strErrMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbSCoResp.ProduceForm3B(newmodel, out strErrMessage);

                    if (string.IsNullOrEmpty(strErrMessage))
                    {
                        ViewBag.Subtitle = "Form 3B Listing";
                        return RedirectToAction("Form3B");
                    }
                }

                if (string.IsNullOrEmpty(strErrMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", strErrMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("3B1", int.Parse(Session["WoredaID"].ToString()));

                return View("ProduceForm3B", newmodel);
                
            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("3B1", int.Parse(Session["WoredaID"].ToString()));

                ModelState.AddModelError("", ex.Message);

                return View("ProduceForm3B", newmodel);
            }
        }

        //3b2
        //FORM 3B
        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form3B2()
        {
            ViewBag.Subtitle = "Form 3B2 Listing";
            dbAcService.InsertAuditTrail(User.Identity.Name, "Coresponsibility", "View Form3B2", ip, "",
            "Form3B2 Accesses  at " + DateTime.Now.ToString());
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form3B2ListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");

                List<Form2AGrid> form2BList = con.Fetch<Form2AGrid>(";Exec getForm3B2AdminList @jtStartIndex, @jtPageSize", new { jtStartIndex, jtPageSize });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form2AGrid>(form2BList);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, form2BList.Count, jtStartIndex, 1, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("xml");
            }
        }
        
        //ProduceForm3B2
        [Authorize]
        public ActionResult ProduceForm3B2()
        {
            ViewBag.Subtitle = "Produce Form 3B2";

            ProduceForm3B2 models = new Models.ProduceForm3B2();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            //models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("3B2", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm3B2(ProduceForm3B2 newmodel)
        {
            ViewBag.Subtitle = "Produce Form 3B2";
            string strErrMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbSCoResp.ProduceForm3B2(newmodel, out strErrMessage);

                    if (string.IsNullOrEmpty(strErrMessage))
                    {
                        ViewBag.Subtitle = "Form 3B2 Listing";
                        return RedirectToAction("Form3B2");
                    }
                }

                if (string.IsNullOrEmpty(strErrMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", strErrMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("3B2", int.Parse(Session["WoredaID"].ToString()));

                return View("ProduceForm3B2", newmodel);

            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("3B2", int.Parse(Session["WoredaID"].ToString()));

                ModelState.AddModelError("", ex.Message);

                return View("ProduceForm3B2", newmodel);
            }
        }


        //FORM 3C

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form3C()
        {
            ViewBag.Subtitle = "Form 3C Listing";
            dbAcService.InsertAuditTrail(User.Identity.Name, "Coresponsibility", "View Form3C", ip, "",
            "Form3C Accesses  at " + DateTime.Now.ToString());

            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form3CListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");

                List<Form2AGrid> form2CList = con.Fetch<Form2AGrid>(";Exec getForm3CAdminList @jtStartIndex, @jtPageSize", new { jtStartIndex, jtPageSize });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form2AGrid>(form2CList);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, form2CList.Count, jtStartIndex, 1, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("xml");
            }
        }
        //ProduceForm2C
        [Authorize]
        public ActionResult ProduceForm3C()
        {
            ViewBag.Subtitle = "Produce Form 3C";

            ProduceForm3C models = new Models.ProduceForm3C();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            //models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("3C", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm3C(ProduceForm3C newmodel)
        {
            ViewBag.Subtitle = "Produce Form 3C";
            string strErrMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbSCoResp.ProduceForm3C(newmodel, out strErrMessage);

                    if (string.IsNullOrEmpty(strErrMessage))
                    {
                        ViewBag.Subtitle = "Form 3C Listing";
                        return RedirectToAction("Form3C");
                    }

                }

                if (string.IsNullOrEmpty(strErrMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", strErrMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("3C", int.Parse(Session["WoredaID"].ToString()));

                return View("ProduceForm3C", newmodel);
                
            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("3C", int.Parse(Session["WoredaID"].ToString()));

                ModelState.AddModelError("", ex.Message);

                return View("ProduceForm3C", newmodel);
            }
        }

    }
}
