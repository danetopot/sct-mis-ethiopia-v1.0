﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.CaptureForm5>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Capture Form 5A
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("CaptureForm5A", "MonitoringCapture", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-7">                
            <label>
                Region <%: Html.ValidationMessageFor(model => model.RegionID) %>
                <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Woreda  <%: Html.ValidationMessageFor(model => model.WoredaID) %>
                <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Kebele <%: Html.ValidationMessageFor(model => model.KebeleID) %>  
                <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
            </label>
            </div>
            <div class="col-4">                
            <label>
                Reporting Period <%: Html.ValidationMessageFor(model => model.ReportingPeriod) %>
                <%: Html.DropDownListFor(model => model.ReportingPeriod,
                    new SelectList(Model.reportgperiod, "PeriodID", "PeriodName"), new { tabindex = "4"})%>
            </label>
            </div>
          
            <button id="cmdProceed" type="button" class="btn btn-icon btn-primary glyphicons circle_plus" tabindex="20"><i></i>Load Grid</button>
            <br /><br />
            <div id="thebox" style="position:relative; width: 800px; height: 340px"></div>
            <br />
            <%--<button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>--%>
            <%--<div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>--%>
          <br />
            <%: Html.ValidationSummary(true) %>
            <%: Html.HiddenFor(model => model.Kebele)%>
            <%: Html.HiddenFor(model => model.rptPeriod)%>  
            <%: Html.HiddenFor(model => model.CapturedXml)%>      
    
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
        <link href="../Scripts/jtable/themes/lightcolor/gray/jtable.min.css" rel="stylesheet" />
        <script src="../Scripts/jtable/jquery.jtable.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#KebeleID,#ReportingPeriod,#cmdProceed").prop("disabled", false);
            $("#SocialWorker").attr("readonly", false);
            $("#KebeleID,#ReportingPeriod").val('0');
            $("#cmdBack").prop("disabled", false);
            $("#RegionID,#WoredaID,#cmdProduce").prop("disabled", true);
            $(":input").blur();
            $('#KebeleID').focus();
            
            $("#cmdBack").click(function () {
                window.location.href = "/MonitoringCapture/Form5AList";
            });

            $('#thebox').jtable({
                title: 'Case Management',
                paging: true,
                actions: {
                    listAction: '/MonitoringCapture/getForm5A2DSList',
                },
                fields: {
                    ColumnID: {
                        key: true,
                        create: false,
                        edit: false,
                        list: false
                    },
                    ProfileDSHeaderID: {
                        key: true,
                        create: false,
                        edit: false,
                        list: false
                    },
                    Members: {
                        title: '',
                        width: '5%',
                        sorting: false,
                        edit: false,
                        create: false,
                        display: function (memberData) {
                            //Create an image that will be used to open child table
                            var $img = $('<img src="../DHTMLX/codebase/imgs/plus2.gif" title="view Household members" />');
                            //Open child table when user clicks the image
                            $img.click(function () {
                                $('#thebox').jtable('openChildTable',
                                        $img.closest('tr'),
                                        {
                                            title: memberData.record.ReportingPeriod + ' - Non Complying Clients',
                                            paging: true,
                                            actions: {
                                                listAction: '/MonitoringCapture/FetchGridForm5AMISDetailsByID?ProfileDSHeaderID=' + memberData.record.ProfileDSHeaderID,
                                            },
                                            fields: {
                                                ProfileDSDetailID: {
                                                    key: true,
                                                    create: false,
                                                    edit: false,
                                                    list: false
                                                },
                                                MISummary: {
                                                    title: 'Summary for MIS',
                                                    width: '5%',
                                                    sorting: false,
                                                    edit: false,
                                                    create: false,
                                                    display: function (MISData) {
                                                        //Create an image that will be used to open child table
                                                        var $img = $('<img src="../DHTMLX/codebase/imgs/combo_select_dhx_black.gif" title="view Household members" />');
                                                        //Open child table when user clicks the image
                                                        $img.click(function () {
                                                            $('#thebox').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: MISData.record.HouseHoldMemberName + ' - Tick Summary For MIS',
                                                                        paging: true,
                                                                        pageSize: 5,
                                                                        pageSizes: [5,10],
                                                                        actions: {
                                                                            listAction: '/MonitoringCapture/FetchGridForm5AMISSummary?FormName=5A2',
                                                                            updateAction: '/MonitoringCapture/SaveForm5A1MIS?KebeleID=' + $('#KebeleID').val() + '&ReportingPeriodID=' + $('#ReportingPeriod').val() + '&ProfileDSDetailID=' + MISData.record.ProfileDSDetailID,
                                                                        },
                                                                        fields: {
                                                                            ID: {
                                                                                key: true,
                                                                                create: false,
                                                                                edit: false,
                                                                                list: false
                                                                            },
                                                                            Reason: {
                                                                                title: 'non-compliance Reason',
                                                                                width: '20%',
                                                                                create: false,
                                                                                edit: false
                                                                            },
                                                                            ActionResponse: {
                                                                                title: 'Response',
                                                                                width: '5%',
                                                                                create: false,
                                                                                edit: true,
                                                                                options: { 'NO': 'NO', 'YES': 'YES' }
                                                                            },
                                                                            Action: {
                                                                                title: 'Mitigating action suggested',
                                                                                width: '40%',
                                                                                create: false,
                                                                                edit: true,
                                                                                options: '/MonitoringCapture/GridFetchForm5AMISSummary?FormName=5A2'
                                                                            },
                                                                            CompletedDate: {
                                                                                title: 'Completed Date',
                                                                                width: '15%',
                                                                                type: 'date',
                                                                                displayFormat: 'mm/dd/yy',
                                                                                create: false,
                                                                                edit: true,
                                                                                list: false
                                                                            },
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                        });
                                                        //Return image to show on the person row
                                                        return $img;
                                                    }
                                                },
                                                HouseHoldMemberName: {
                                                    title: 'HouseHold Member Name',
                                                    width: '15%',
                                                    create: false,
                                                    edit: false
                                                },
                                                Age: {
                                                    title: 'Age',
                                                    width: '5%',
                                                    create: false,
                                                    edit: false
                                                },
                                                Sex: {
                                                    title: 'Sex',
                                                    width: '5%',
                                                    create: false,
                                                    edit: false
                                                },
                                                Remarks: {
                                                    title: 'Reason(s) Given',
                                                    width: '10%',
                                                    create: false,
                                                    edit: true
                                                }
                                            }
                                        }, function (data) { //opened handler
                                            data.childTable.jtable('load');
                                        });
                            });
                            //Return image to show on the person row
                            return $img;
                        }
                    },
                    ReportingPeriodID: {
                        title: '',
                        type: 'hidden',
                        create: false,
                        edit: false,
                        list: false,
                    },
                    ReportingPeriod: {
                        title: 'Reporting Period',
                        width: '10%',
                        create: false,
                        edit: false,
                    },
                    HouseHoldIDNumber: {
                        title: 'HouseHold ID Number',
                        width: '10%',
                        create: false,
                        edit: false,
                    },
                    NameOfHouseHoldHead: {
                        title: 'Name Of HouseHold Head',
                        width: '20%',
                        create: false,
                        edit: false,
                    }
                }
            });

            $("#cmdBack").click(function () {
                window.location.href = "/MonitoringCapture/Form5A1List";
            });

            $("#cmdProceed").click(function () {
                $('#thebox').jtable('load', { KebeleID: $('#KebeleID').val(), ReportingPeriodID: $("#ReportingPeriod").val() });
            });

        });
        //sadjsajkhdsjkahdjkshadsjk

    </script>
</asp:Content>

