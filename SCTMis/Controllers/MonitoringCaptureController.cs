﻿using SCTMis.Models;
using SCTMis.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml.Linq;

namespace SCTMis.Controllers
{
    [SessionConfig.SessionExpireFilter]
    public class MonitoringCaptureController : Controller
    {
        GeneralServices dbService = new GeneralServices();
        MonitoringServices dbCompliance = new MonitoringServices();

        //Form5A1 Starts
        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form5A1List()
        {
            ViewBag.Subtitle = "Form 5A1 Non Compliant Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument CapturedForm5A1List()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                Tuple<List<int>, List<CapturedForm5AGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm5AGrid>(";Exec getCapturedForm5A1List @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<CapturedForm5AGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dtDetails, resultset.Item1[0], jtStartIndex, 0, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        //CaptureForm5A
        [Authorize]
        public ActionResult CaptureForm5A1()
        {
            ViewBag.Subtitle = "Captured Form 5A1 Listing";

            CaptureForm5 models = new Models.CaptureForm5();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }


        [HttpPost]
        [Authorize]
        public JsonResult getForm5A1DSList(Form5A1DSModel model)
        {
            try
            {
                Tuple<List<int>, List<getForm5ADSList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm5ADSList>(";Exec getForm5A1DSList @KebeleID, @ReportingPeriod",
                    new
                    {
                        KebeleID = model.KebeleID,
                        ReportingPeriod = model.ReportingPeriodID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        [Authorize]
        public JsonResult getForm5A2DSList(Form5A1DSModel model)
        {
            try
            {
                Tuple<List<int>, List<getForm5ADSList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm5ADSList>(";Exec getForm5A2DSList @KebeleID,@ReportingPeriod",
                    new
                    {
                        KebeleID = model.KebeleID,
                        ReportingPeriod = model.ReportingPeriodID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult FetchGridForm5A1MISDetailsByID(CaptureForm5MIS models)
        {
            try
            {

                Tuple<List<int>, List<Form5A1MISGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form5A1MISGrid>(";Exec FetchGridForm1A1DetailsByID @ProfileDSHeaderID",
                    new { ProfileDSHeaderID = models.ProfileDSHeaderID });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult FetchGridForm5AMISDetailsByID()
        {
            try
            {
                var jtProfileDSHeaderID = Request.QueryString["ProfileDSHeaderID"].ToString();

                Tuple<List<int>, List<Form5AMISGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form5AMISGrid>(";Exec FetchGridForm5ADetailsByID @ProfileDSHeaderID", new { ProfileDSHeaderID = jtProfileDSHeaderID });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult FetchGridForm5AMISSummary(MISSummaryModel model)
        {
            try
            {
                //string jtFormName = Request.QueryString["FormName"].ToString();

                Tuple<List<int>, List<Form5AMISSummary>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form5AMISSummary>(";Exec GetSummaryForMIS @jtStartIndex,@jtPageSize,@FormName",
                    new {
                        jtStartIndex=model.jtStartIndex,
                        jtPageSize=model.jtPageSize,
                        FormName = model.FormName 
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //[HttpPost]
        [Authorize]
        public JsonResult GridFetchForm5AMISSummary(MISSummaryModel model)
        {
            try
            {
                //string jtFormName = Request.QueryString["FormName"].ToString();

                Tuple<List<int>, List<OptionsSummary>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, OptionsSummary>(";Exec SummaryForMISFetch @jtStartIndex,@jtPageSize,@FormName",
                    new
                    {
                        jtStartIndex = model.jtStartIndex,
                        jtPageSize = model.jtPageSize,
                        FormName = model.FormName
                    });

                return Json(new { Result = "OK", Options = resultset.Item2 });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        ////CaptureForm5AMIS
        [HttpPost]
        [Authorize]
        public JsonResult CaptureForm5A1MIS(CaptureForm5MIS models)
        {
            try
            {
                Tuple<List<int>, List<CaptureForm5MIS>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CaptureForm5MIS>(";Exec FetchForm5AMISDetailsByID @ProfileDSHeaderID",
                     new
                     {
                         ProfileDSHeaderID = models.ProfileDSHeaderID
                     });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }

        }

        [Authorize]
        [HttpPost]
        public JsonResult SaveForm5A1MIS(CaptureForm5MISModel newModel)
        {
            string errMessage = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    List<RecordCount> isSuccess
                        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm5A1 @ProfileDSDetailID, @KebeleID, @ReportingPeriodID, @CompletedDate,@ActionResponse, @SocialWorker, @CreatedBy",
                         new
                         {
                             ProfileDSDetailID = newModel.ProfileDSDetailID,
                             KebeleID = newModel.KebeleID,
                             ReportingPeriodID = newModel.ReportingPeriodID,
                             CompletedDate = newModel.CompletedDate,
                             ActionResponse = newModel.ID,
                             SocialWorker = Membership.GetUser().UserName.ToString(),
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });

                    if (string.IsNullOrEmpty(errMessage))
                    {
                        return Json(new { Result = "OK"});
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));

                    return Json(new { Result = "ERROR", Message = message });
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                return Json(new { Result = "ERROR", Message = errMessage });

            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult SaveForm5A2MIS(CaptureForm5MISModel newModel)
        {
            string errMessage = string.Empty;
            //newmodel.CompletedDate = newmodel.CompletedDateID;
            try
            {
                if (ModelState.IsValid)
                {
                    List<RecordCount> isSuccess
                        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm5A2 @ProfileDSDetailID, @KebeleID, @ReportingPeriodID, @CompletedDate,@ActionResponse, @SocialWorker, @CreatedBy",
                         new
                         {
                             ProfileDSDetailID = newModel.ProfileDSDetailID,
                             KebeleID = newModel.KebeleID,
                             ReportingPeriodID = newModel.ReportingPeriodID,
                             CompletedDate = newModel.CompletedDate,
                             ActionResponse = newModel.ID,
                             SocialWorker = Membership.GetUser().UserName.ToString(),
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        return Json(new { Result = "OK" });
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));

                    return Json(new { Result = "ERROR", Message = message });
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                return Json(new { Result = "ERROR", Message = errMessage });

            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult SaveForm5BMIS(CaptureForm5BMISModel newModel)
        {
            string errMessage = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    List<RecordCount> isSuccess
                        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm5B @ProfileTDSPLWID, @KebeleID, @ReportingPeriodID, @CompletedDate,@ActionResponse, @SocialWorker, @CreatedBy",
                         new
                         {
                             ProfileTDSPLWID = newModel.ProfileTDSPLWID,
                             KebeleID = newModel.KebeleID,
                             ReportingPeriodID = newModel.ReportingPeriodID,
                             CompletedDate = newModel.CompletedDate,
                             ActionResponse = newModel.ID,
                             SocialWorker = Membership.GetUser().UserName.ToString(),
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        return Json(new { Result = "OK" });
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));

                    return Json(new { Result = "ERROR", Message = message });
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                return Json(new { Result = "ERROR", Message = errMessage });

            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult SaveForm5CMIS(CaptureForm5CMISModel newModel)
        {
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    List<RecordCount> isSuccess
                        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm5C @ProfileTDSCMCID, @KebeleID, @ReportingPeriodID, @CompletedDate,@ActionResponse, @SocialWorker, @CreatedBy",
                         new
                         {
                             ProfileTDSCMCID = newModel.ProfileTDSCMCID,
                             KebeleID = newModel.KebeleID,
                             ReportingPeriodID = newModel.ReportingPeriodID,
                             CompletedDate = newModel.CompletedDate,
                             ActionResponse = newModel.ID,
                             SocialWorker = Membership.GetUser().UserName.ToString(),
                             CreatedBy = Membership.GetUser().UserName.ToString()
                         });
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        return Json(new { Result = "OK" });
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));

                    return Json(new { Result = "ERROR", Message = message });
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                return Json(new { Result = "ERROR", Message = errMessage });

            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //Form5A1 Ends
        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form5AList()
        {
            ViewBag.Subtitle = "Form 5A2 Non Compliant Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument CapturedForm5AList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                Tuple<List<int>, List<CapturedForm5AGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm5AGrid>(";Exec getCapturedForm5AList @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<CapturedForm5AGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dtDetails, resultset.Item1[0], jtStartIndex, 0, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        //CaptureForm5A -- B4 -> 21-March-2018
        [Authorize]
        public ActionResult CaptureForm5A()
        {
            ViewBag.Subtitle = "Captured Form 5A2 Listing";

            CaptureForm5 models = new Models.CaptureForm5();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }

        
        //CaptureForm5AMIS
        [Authorize]
        public ActionResult CaptureForm5AMIS(CaptureForm5MIS models)
        {
            ViewBag.Subtitle = "Capture Form 5A2 MIS Details";

            models = dbCompliance.FetchForm5AMISDetailsByID(models.ProfileDSHeaderID);
            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());
            models.worker = dbService.GetSocialWorkers();

            return View(models);
        }
        //CaptureForm5AMIS ENDS

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form5BList()
        {
            ViewBag.Subtitle = "Form 5B Non Compliant Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument CapturedForm5BList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                Tuple<List<int>, List<CapturedForm5BGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm5BGrid>(";Exec getCapturedForm5BList @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<CapturedForm5BGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dtDetails, resultset.Item1[0], jtStartIndex, 0, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        //CaptureForm5B
        [Authorize]
        public ActionResult CaptureForm5B()
        {
            ViewBag.Subtitle = "Capture Form 5B";

            CaptureForm5 models = new Models.CaptureForm5();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm5BTDSPLWList(Form5A1DSModel model)
        {
            try
            {
                Tuple<List<int>, List<getForm5BTDSPLWList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm5BTDSPLWList>(";Exec getForm5BTDSPLWList @KebeleID, @ReportingPeriod",
                    new
                    {
                        KebeleID = model.KebeleID,
                        ReportingPeriod = model.ReportingPeriodID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        public ActionResult CaptureForm5BMIS(CaptureForm5BMIS models)
        {
            ViewBag.Subtitle = "Capture Form 5B MIS Details";

            //FetchForm5AMISDetailsByID
            models = dbCompliance.FetchForm5BMISDetailsByID(models.ProfileTDSPLWID);
            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());
            models.worker = dbService.GetSocialWorkers();

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public JsonResult FetchGridForm5BMISDetailsByID()
        {
            try
            {
                int jtProfileDSHeaderID = int.Parse(Request.QueryString["ProfileTDSPLWID"].ToString());

                Tuple<List<int>, List<Form5BMISGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form5BMISGrid>(";Exec FetchGridForm5BDetailsByID @ProfileTDSPLWID", new { ProfileTDSPLWID = jtProfileDSHeaderID });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        //FORM B Ends

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form5CList()
        {
            ViewBag.Subtitle = "Form 5C Non Compliant Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument CapturedForm5CList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                Tuple<List<int>, List<CapturedForm5CGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm5CGrid>(";Exec getCapturedForm5CList @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<CapturedForm5CGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dtDetails, resultset.Item1[0], jtStartIndex, 0, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        //CaptureForm5C
        [Authorize]
        public ActionResult CaptureForm5C()
        {
            ViewBag.Subtitle = "Capture Form 5C";

            CaptureForm5 models = new Models.CaptureForm5();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm5CTDSCMCList(Form5A1DSModel model)
        {
            try
            {
                Tuple<List<int>, List<getForm5CTDSCMCList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm5CTDSCMCList>(";Exec getForm5CTDSCMCList @KebeleID, @ReportingPeriod",
                    new
                    {
                        KebeleID = model.KebeleID,
                        ReportingPeriod = model.ReportingPeriodID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        public ActionResult CaptureForm5CMIS(CaptureForm5CMIS models)
        {
            ViewBag.Subtitle = "Capture Form 5C MIS Details";

            //FetchForm5AMISDetailsByID
            models = dbCompliance.FetchForm5CMISDetailsByID(models.ProfileTDSCMCID);
            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());
            models.worker = dbService.GetSocialWorkers();

            return View(models);
        }
        [HttpGet]
        [Authorize]
        public XDocument FetchGridForm5CMISDetailsByID()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtProfileDSHeaderID = int.Parse(Request.QueryString["ProfileDSHeaderID"].ToString());

                Tuple<List<int>, List<Form5A1MISGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form5A1MISGrid>(";Exec FetchGridForm1A1DetailsByID @ProfileDSHeaderID", new { ProfileDSHeaderID = jtProfileDSHeaderID });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form5A1MISGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], 0, 0, 1);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("<xml>error</xml>");
            }
        }
    }
}
