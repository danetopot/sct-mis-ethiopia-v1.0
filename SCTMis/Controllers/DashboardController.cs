﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using System.Xml.Linq;
using SCTMis.Models;
using SCTMis.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Xml;

namespace SCTMis.Controllers
{
    public class DashboardController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchDashboardComplianceTrendData(string filterString)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                var dashboardSummaryList = GetDashboardsData(filterString);

                List<DashboardComplianceTrend> dashboardComplianceTrends = new List<DashboardComplianceTrend>();


                foreach (var dashboardSummary in dashboardSummaryList)
                {
                    DashboardComplianceTrend dashboardComplianceTrend = new DashboardComplianceTrend
                    {
                        PeriodMonth = dashboardSummary.PeriodMonth,
                        CompliantPercentage = dashboardSummary.CompliantPercentage
                    };

                    dashboardComplianceTrends.Add(dashboardComplianceTrend);
                }

                DataTable dt = GeneralServices.ToDataTable<DashboardComplianceTrend>(dashboardComplianceTrends);

                string sb = GetXML(dt, "PeriodMonth", "CompliantPercentage", "", "");
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                return XDocument.Parse("<xml>error</xml>");
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchDashboardComplianceBarData(string filterString)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");
            
            try
            {
                var dashboardSummaryList = GetDashboardsData(filterString);

                List<DashboardComplianceBar> dashboardComplianceBars = new List<DashboardComplianceBar>();

                
                foreach (var dashboardSummary in dashboardSummaryList)
                {
                    DashboardComplianceBar dashboardComplianceBar = new DashboardComplianceBar
                    {
                        PeriodMonth = dashboardSummary.PeriodMonth,
                        Compliant = dashboardSummary.Compliant,
                        NonCompliant = dashboardSummary.NonCompliant
                    };

                    dashboardComplianceBars.Add(dashboardComplianceBar);
                }

                DataTable dt = GeneralServices.ToDataTable<DashboardComplianceBar>(dashboardComplianceBars);

                string sb = GetXML(dt, "PeriodMonth", "Compliant", "NonCompliant", "");
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("<xml>error</xml>");
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument DashboardSummariesList(string filterString)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");            

            try
            {
                var dashboardSummaryList = GetDashboardsData(filterString);

                List<DashboardSummaryListGrid> dashboardSummaryListGrid = new List<DashboardSummaryListGrid>();
                foreach (var dashboardSummary in dashboardSummaryList)
                {
                    DashboardSummaryListGrid dashboardSummaryGrid = new DashboardSummaryListGrid
                    {
                        PeriodMonthOne = dashboardSummary.PeriodMonth,
                        Compliant = dashboardSummary.Compliant,
                        TotalClients = dashboardSummary.TotalClients,
                        PeriodMonthTwo = dashboardSummary.PeriodMonth,
                        CompliantPercentage = dashboardSummary.CompliantPercentage
                    };

                    dashboardSummaryListGrid.Add(dashboardSummaryGrid);
                }
                                
                DataTable dt = GeneralServices.ToDataTable<DashboardSummaryListGrid>(dashboardSummaryListGrid);
                string sb = GeneralServices.GetDhtmlxXML(dt, dashboardSummaryListGrid.Count, 0, 0, 0);

                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        private List<DashboardSummaryList> GetDashboardsData(string filterString)
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            DashboardModel dashboardModel = jsonSerializer.Deserialize<DashboardModel>(filterString);           

            List <DashboardSummaryList> dashboardSummaryList = GeneralServices.NpocoConnection().Fetch<DashboardSummaryList>(";Exec GetDashboardSummaries @RegionID,@WoredaID,@KebeleID,@ReportingIndicatorId,@FiscalYear,@PeriodFrom,@PeriodTo,@Sex",
                    new
                    {
                        RegionID = dashboardModel.RegionID > 0 ? (int?)dashboardModel.RegionID : null,
                        WoredaID = dashboardModel.WoredaID > 0 ? (int?)dashboardModel.WoredaID : null,
                        KebeleID = dashboardModel.KebeleID > 0 ? (int?)dashboardModel.KebeleID : null,
                        ReportingIndicatorId = dashboardModel.ReportingIndicatorId > 0 ? (int?)dashboardModel.ReportingIndicatorId : null,
                        FiscalYear = dashboardModel.FiscalYear,
                        PeriodFrom = dashboardModel.PeriodFrom > 0 ? (int?)dashboardModel.PeriodFrom : null,
                        PeriodTo = dashboardModel.PeriodTo > 0 ? (int?)dashboardModel.PeriodTo : null,
                        Sex = dashboardModel.Sex == string.Empty ? null : dashboardModel.Sex
                    });

            return dashboardSummaryList;
        }

        private static string GetXML(DataTable dt, string col1, string col2, string col3, string col4, params KeyValuePair<string, object>[] pairs)
        {
            StringBuilder sb = new StringBuilder();
            string HeadStr = string.Empty;
            string TempStr = HeadStr;
            string itemVal;

            int rowID;
            sb.Append("<?xml version='1.0' encoding='UTF-8'?>");

            sb.Append("<data>");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                rowID = i;
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<item id='" + dt.Rows[i][j].ToString() + "'>");
                    }

                    if (dt.Columns[j].ToString() == col1 || dt.Columns[j].ToString() == col2 || dt.Columns[j].ToString() == col3 || dt.Columns[j].ToString() == col4)
                    {
                        sb.Append("<" + dt.Columns[j].ToString() + ">");
                        itemVal = dt.Rows[i][j].ToString().Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
                        if (itemVal == "False") { itemVal = "0"; }

                        sb.Append(itemVal);

                        sb.Append("</" + dt.Columns[j].ToString() + ">");
                    }
                }
                sb.Append("</item>");

            }
            sb.Append("</data>");

            string encodedXml = SCTMis.Services.GeneralServices.PrintXML(sb.ToString());

            XmlDocument contentxml = new XmlDocument();
            contentxml.LoadXml(@encodedXml);

            return encodedXml;
        }
    }
}
