﻿var page_count = 10;
var gridHeader = 'ColumnID,Report<span class="HeaderChange">_</span>Name,Report<span class="HeaderChange">_</span>Period,ReportFileName,Generated<span class="HeaderChange">_</span>By,Generated<span class="HeaderChange">_</span>On,IsReplicated,ReplicatedBy,ReplicatedOn, Download';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,img';
var mygrid;

$(document).ready(function () {

    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,400,145,0,150,150,0,0,0,87");
    mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();
    
    var currentPage = $('#currentPage').val();
    mygrid.load("/Security/StandardReportsListAdmin?RecCount=" + page_count,
        function() {
            mygrid.changePage(currentPage);
        });
        
    mygrid.attachEvent("onPageChanged", function (ind, fInd, lInd) {
            $('#currentPage').val(ind);
    
            $.ajax({
                type: "POST",
                url: "../Security/UpdateCurrentPage",
                data: "{ 'CurrentPage':" + ind + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8"
            });
        }); 

    //mygrid.loadXML("/Security/StandardReportsListAdmin?RecCount=" + page_count);
    dhtmlxError.catchError("ALL", my_error_handler);

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Region'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('Woreda'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('Kebele'));

    $("#cmdProduceReport").click(function () {
        window.location.href = "../Security/ProduceStandardReports";
    });
    $('#loading').hide();
});

function PreviewXlsFile(_ID) {
    var selectedRow = mygrid.getSelectedId();
    _ID = mygrid.cells(selectedRow, 3).getValue();
    console.log(_ID);
    get('/Home/downloadxls', { GenerationID: _ID });
    //get('/Home/downloadpdf', { GenerationID: _ID });
}

/*
function PreviewPdfFile(_ID) {
    var selectedRow = mygrid.getSelectedId();
    _ID = mygrid.cells(selectedRow, 3).getValue();
    console.log(_ID);
    get('/Home/downloadpdf', { GenerationID: _ID });
}
*/
