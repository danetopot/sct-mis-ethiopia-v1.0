﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.CaptureForm1B>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Capture Form 1B
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<h5>Add New Girl Form</h5>   --%>     
<% using (Html.BeginForm("CaptureForm1B", "Household", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-7">                
            <label>
                Region <%: Html.ValidationMessageFor(model => model.RegionID) %>
                <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Woreda  <%: Html.ValidationMessageFor(model => model.WoredaID) %>
                <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Kebele <%: Html.ValidationMessageFor(model => model.KebeleID) %>  
                <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
            </label>
            </div>

            <div class="col-7">                  
            <label>
                Gote <%: Html.ValidationMessageFor(model => model.Gote) %>
                <%: Html.TextBoxFor(model => model.Gote, new { tabindex = "4" , @maxlength="30" })%>
            </label>
            </div>
        <%--Second Row--%>
            <div class="col-7">                
            <label>
                Gare <%: Html.ValidationMessageFor(model => model.Gare) %>
                <%: Html.TextBoxFor(model => model.Gare, new { tabindex = "5" , @maxlength="30" })%>
            </label>
            </div>
    <br />
            <div class="col-4">                
            <label>
                Collection Date
                <%: Html.TextBoxFor(model => model.CollectionDate, new { tabindex = "6"})%>
            </label>
            </div>
            <div class="col-7">                              
            <label>
                Social Worker
                <%: Html.DropDownListFor(model => model.SocialWorker,
                    new SelectList(Model.worker, "ID", "Name"), new { tabindex = "7" })%>
            </label>
            </div>
            <div class="col-7">                  
            <label>
                CCC member
                <%: Html.TextBoxFor(model => model.CCCCBSPCMember, new { tabindex = "8" , @maxlength="20" })%>
            </label>
            </div>
            <div id="errors" style="color: red;"></div>  

        <%--Third Row--%>
            <div class="col-4">                
            <label>
                Pregnant/Lactating
                <%: Html.DropDownListFor(model => model.PLW,
                    new SelectList(Model.plws, "ID", "Name"), new { tabindex = "9" })%>
            </label>
            </div>
    <hr />
            <div class="col-4">                
            <label>
                Name of PLW incl. name of grandfather
                <%: Html.TextBoxFor(model => model.NameOfPLW, new { tabindex = "9" , @class = "alphaonly", @maxlength="50" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                PSNP Number
                <%: Html.TextBoxFor(model => model.HouseHoldIDNumber, new { tabindex = "10", @maxlength="25" })%>
            </label>
            </div>

            <div class="col-4">                
            <label>
                Individual ID in Health Family Folder
                <%: Html.TextBoxFor(model => model.MedicalRecordNumber, new { tabindex = "11" , @maxlength="25" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Age of PLW
                <%: Html.TextBoxFor(model => model.PLWAge, new { tabindex = "12", @class = "numbersOnly", @maxlength="3"})%>
            </label>
            </div>
    <br />
            <div class="col-4">                              
            <label>
                Transitioning from PW to temporary
                <%: Html.TextBoxFor(model => model.StartDateTDS, new { tabindex = "13"})%>
            </label>
            </div>
            <div class="col-4">                  
            <label>
                Date of birth of baby 
                <%: Html.TextBoxFor(model => model.BabyDateOfBirth, new { tabindex = "14"})%>
            </label>
            </div>
        <%--Fourth Row--%>
            <div class="col-7">                
            <label>
                Name of infant
                <%: Html.TextBoxFor(model => model.BabyName, new { tabindex = "15" , @class = "alphaonly",@maxlength="50" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Sex of infant
                <%: Html.DropDownListFor(model => model.BabySex,
                    new SelectList(Model.genders, "GenderID", "GenderName"), new { tabindex = "16" })%>
            </label>
            </div>
    <br />
            <div class="col-4">                              
            <label>
                Expected temporary DS end Date
                <%: Html.TextBoxFor(model => model.EndDateTDS, new { tabindex = "17"})%>
            </label>
            </div>
            <div class="col-4">                  
            <label>
                Nutritional status of PLW 
                <%: Html.DropDownListFor(model => model.NutritionalStatusPLW,
                    new SelectList(Model.nutrStatus, "ID", "Name"), new { tabindex = "18" })%>
            </label>
            </div>
        <%--Fifth Row--%>
            <div class="col-4">                
            <label>
                Nutritional status of infant
                <%: Html.DropDownListFor(model => model.NutritionalStatusInfant,
                    new SelectList(Model.nutrStatus, "ID", "Name"), new { tabindex = "19" })%>
            </label>
            </div>
            <div class="col-2">                  
            <label>
                Remarks (like client passed away, moved, etc.):
                <%: Html.TextBoxFor(model => model.Remarks, new { tabindex = "20" ,@maxlength="200" })%>
            </label>
            </div>
        <%--Sixth Row--%>
            <button id="cmdProceed" type="button" class="btn btn-icon btn-primary glyphicons circle_plus" tabindex="21"><i></i>Proceed</button>
            <br />
            <div id="errors2" style="color: red;"></div>  
            <br />
            <div id="gridbox" style="position:relative; width: 730px; height: 120px"></div>
            <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>
          <br />
            <div class="form-actions">
	            <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save Household Profile</button>
                <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
            </div>
                <%: Html.HiddenFor(model => model.ColumnID)%>
                <%: Html.HiddenFor(model => model.Kebele)%>
                <%: Html.HiddenFor(model => model.MemberXml)%>
            <br />
            <%: Html.ValidationSummary(true) %>          
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript" src="../Scripts/assets/js/jquery.mask.min.js"></script>
    <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
    <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>
    <script type="text/javascript" src="../Scripts/assets/js/app/CaptureForm1BList.js"></script>
</asp:Content>

