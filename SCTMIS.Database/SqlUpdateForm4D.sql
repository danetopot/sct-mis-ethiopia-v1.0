USE [SCT-MIS]
GO
/****** Object:  StoredProcedure [dbo].[InsertCapturedForm4D]    Script Date: 25/07/2019 15:41:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[UpdateCapturedForm4D]
(
	@ChildProtectionHeaderID				UNIQUEIDENTIFIER,
	@ChildProtectionId						VARCHAR(200),		
	@CaseManagementReferral					VARCHAR(200),
	@CaseStatusId							VARCHAR(200),
	@ClientTypeID							VARCHAR(200),
	@CollectionDate							VARCHAR(200),
	@SocialWorker							VARCHAR(200),
	@Remarks								VARCHAR(MAX),
	@UpdatedBy								VARCHAR(50),
	@UpdatedOn								VARCHAR(50),
	@FiscalYear								VARCHAR(20),
	@ChildDetailID							VARCHAR(200),	
	@RiskXML								VARCHAR(MAX),
	@ServiceXML								VARCHAR(MAX),
	@ProviderXML							VARCHAR(MAX)
)
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @RiskRecords				XML,
			@ServiceRecords				XML,
			@ProviderRecords			XML

	BEGIN TRY
		BEGIN TRANSACTION UPDATERECORD
			SET @RiskRecords = CONVERT(XML,@RiskXML)
			SET @ServiceRecords = CONVERT(XML,@ServiceXML)
			SET @ProviderRecords = CONVERT(XML,@ProviderXML)

			--INSERT INTO dbo.ChildProtectionHeader(ChildProtectionHeaderID,ChildProtectionId,CaseManagementReferral,CaseStatusId,ClientTypeID,
			--CollectionDate,SocialWorker,Remarks,CreatedBy,CreatedOn,FiscalYear,ChildDetailID)
			--VALUES (@ChildProtectionHeaderID,@ChildProtectionId,@CaseManagementReferral,@CaseStatusId,@ClientTypeID,@CollectionDate
			--   ,@SocialWorker,@Remarks,@CreatedBy,@CreatedOn,@FiscalYear,@ChildDetailID)
			UPDATE ChildProtectionHeader
			SET 
			ChildProtectionId=@ChildProtectionId,
			CaseManagementReferral=@CaseManagementReferral,
			CaseStatusId=@CaseStatusId,
			ClientTypeID=@ClientTypeID,
			CollectionDate=@CollectionDate,
			SocialWorker=@SocialWorker,
			Remarks=@Remarks,
			UpdatedBy=@UpdatedBy,
			UpdatedOn=@UpdatedOn
			WHERE ChildProtectionHeaderID=@ChildProtectionHeaderID


			/* ChildProtectionRiskDetail */
			CREATE TABLE #riskXML
			(
				RiskTypeId				INT,
				ChildProtectionHeaderID	VARCHAR(200)
			)
			INSERT INTO #riskXML(RiskTypeId,ChildProtectionHeaderID)
			SELECT
				m.c.value('(RiskTypeId)[1]', 'INT'),			
				m.c.value('(ChildProtectionHeaderID)[1]', 'VARCHAR(200)')
			FROM @RiskRecords.nodes('/members/row') AS m(c)
			
			DELETE ChildProtectionRiskDetail WHERE ChildProtectionHeaderID=@ChildProtectionHeaderID
			INSERT INTO ChildProtectionRiskDetail(ChildProtectionRiskDetailId,RiskTypeId,ChildProtectionHeaderID)
			SELECT NEWID(),RiskTypeId, ChildProtectionHeaderID FROM #riskXML

			/* ChildProtectionServiceDetail */
			CREATE TABLE #serviceXML
			(
				ServiceTypeId				INT,
				ChildProtectionHeaderID		VARCHAR(200)
			)
			INSERT INTO #serviceXML(ServiceTypeId,ChildProtectionHeaderID)
			SELECT
				m.c.value('(ServiceTypeId)[1]', 'INT'),	
				m.c.value('(ChildProtectionHeaderID)[1]', 'VARCHAR(200)')
			FROM @ServiceRecords.nodes('/members/row') AS m(c)
			
			DELETE ChildProtectionServiceDetail WHERE ChildProtectionHeaderID=@ChildProtectionHeaderID
			INSERT INTO ChildProtectionServiceDetail(ChildProtectionServiceDetailId,ServiceTypeId,ChildProtectionHeaderID)
			SELECT NEWID(),ServiceTypeId, ChildProtectionHeaderID FROM #serviceXML
			
	
			/* ChildProtectionProviderDetail */
			CREATE TABLE #providerXML
			(
				ServiceProviderId			INT,
				ChildProtectionHeaderID		VARCHAR(200)
			)
			INSERT INTO #providerXML(ServiceProviderId,ChildProtectionHeaderID)
			SELECT	
				m.c.value('(ServiceProviderId)[1]', 'INT'),
				m.c.value('(ChildProtectionHeaderID)[1]', 'VARCHAR(200)')
			FROM @ProviderRecords.nodes('/members/row') AS m(c)

			DELETE ChildProtectionProviderDetail WHERE ChildProtectionHeaderID=@ChildProtectionHeaderID
			INSERT INTO ChildProtectionProviderDetail(ChildProtectionProviderDetailId,ServiceProviderId,ChildProtectionHeaderID)
			SELECT NEWID(),ServiceProviderId, ChildProtectionHeaderID FROM #providerXML

		COMMIT TRANSACTION UPDATERECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION UPDATERECORD
	   END 

	   DECLARE @ERROR_MESSAGE	VARCHAR(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

	SET NOCOUNT OFF
END
