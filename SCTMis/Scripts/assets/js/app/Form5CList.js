﻿var page_count = 10;
var gridHeader = 'ColumnID,NonComplianceID,Reporting<span class="HeaderChange">_</span>Period,Household<span class="HeaderChange">_</span>Visit,CompletedDate,Kebele,Clients, ReportFileName,Generator,Generate<span class="HeaderChange">_</span>On, Preview';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img';
var mygrid;

$(document).ready(function () {

    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,0,195,0,0,150,120,0,90,100,70");
    mygrid.setColAlign("left,left,left,left,right,right,right,right,right,right,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();

    mygrid.loadXML("/Monitoring/Form5CListAdmin?RecCount=" + page_count);
    dhtmlxError.catchError("ALL", my_error_handler);

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 1).text('Reporting Period'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Household Visit'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('Region'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('Woreda'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 5).text('Kebele'));

    $("#cmdNew").click(function () {
        window.location.href = "/Monitoring/ProduceForm5C";
    });
    $('#loading').hide();
});

function PreviewPdfFile(_ID) {
    var selectedRow = mygrid.getSelectedId();
    _ID = mygrid.cells(selectedRow, 7).getValue();
    console.log(_ID);
    get('/Home/downloadpdf', { GenerationID: _ID });
}
