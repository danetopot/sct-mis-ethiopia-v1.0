﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Roles
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="gridbox" style="position:relative; width: 367px; height: 320px"></div>
    <div id="gridfooter">
        <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>
    </div>

    <div class="form-actions">
	    <button id="cmdNew" onclick="return window.location = '../Security/AddRoles';" type="button" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>Add Role</button>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script src="../Scripts/assets/js/app/RolesView.js"></script>
</asp:Content>
