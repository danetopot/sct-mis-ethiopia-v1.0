﻿using SCTMis.Models;
using SCTMis.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace SCTMis.Controllers
{
    [SessionConfig.SessionExpireFilter]
    public class ComplianceController : Controller
    {
        GeneralServices dbService = new GeneralServices();
        ComplianceServices dbCompliance = new ComplianceServices();
        ServiceProviderModel SPmodel = new ServiceProviderModel();

        //
        // GET: /Compliance/
        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form4A()
        {
            ViewBag.Subtitle = "Form 4A Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form4AListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");

                Tuple<List<int>, List<Form4Model>> resultset = GeneralServices.NpocoConnection().FetchMultiple<int, Form4Model>(";Exec getForm4AAdminList @jtStartIndex, @jtPageSize",
                new
                {
                    jtStartIndex = jtStartIndex,
                    jtPageSize = jtPageSize
                });

                DataTable dt = GeneralServices.ToDataTable<Form4Model>(resultset.Item2);

                string sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        //ProduceForm2A
        [Authorize]
        public ActionResult ProduceForm4A()
        {
            ViewBag.Subtitle = "Produce Form 4A";

            ProduceForm4A models = new Models.ProduceForm4A();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            //models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("4A", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            SPmodel.FormCode = "A";
            SPmodel.KebeleID = models.KebeleID;
            SPmodel.ReportingPeriodID = models.ReportingPeriodID;

            models.serviceprovids = dbService.GetServiceProvidersUpdated(SPmodel);
            models.intgrtedservis = dbService.GetIntegratedServices(models.ServiceProviderID, 1);
            models.grades = dbService.GetSchoolGrade();

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm4A(ProduceForm4A newmodel)
        {
            ViewBag.Subtitle = "Produce Form 4A";
            string errMessage = string.Empty;
            
            try
            {
                if (ModelState.IsValid)
                {
                    var result2 = dbCompliance.ProduceForm4ASchool(newmodel, out errMessage);

                    var result = dbCompliance.ProduceForm4A(newmodel, out errMessage);

                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Form 4A Listing";

                        return RedirectToAction("Form4A");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("4A", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                SPmodel.FormCode = "A";
                SPmodel.KebeleID = newmodel.KebeleID;
                SPmodel.ReportingPeriodID = newmodel.ReportingPeriodID;

                newmodel.serviceprovids = dbService.GetServiceProvidersUpdated(SPmodel);
                newmodel.intgrtedservis = dbService.GetIntegratedServices(newmodel.ServiceProviderID, 1);
                newmodel.grades = dbService.GetSchoolGrade();

                return View("ProduceForm4A", newmodel);
                
            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("4A", int.Parse(Session["WoredaID"].ToString()));

                SPmodel.FormCode = "A";
                SPmodel.KebeleID = newmodel.KebeleID;
                SPmodel.ReportingPeriodID = newmodel.ReportingPeriodID;

                newmodel.serviceprovids = dbService.GetServiceProvidersUpdated(SPmodel);
                newmodel.intgrtedservis = dbService.GetIntegratedServices(newmodel.ServiceProviderID, 1);
                newmodel.reportgperiod = dbService.GetReportingPeriods();
                newmodel.grades = dbService.GetSchoolGrade();

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);

                return View("ProduceForm4A", newmodel);
            }
        }

        //FORM 4B

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form4B()
        {
            ViewBag.Subtitle = "Form 4B Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form4BListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");

                Tuple<List<int>, List<Form4Model>> resultset
                = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form4Model>(";Exec getForm4BAdminList @jtStartIndex, @jtPageSize",
                new
                {
                    jtStartIndex = jtStartIndex,
                    jtPageSize = jtPageSize
                });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form4Model>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 2);


                //List<Form4Model> form2BList = con.Fetch<Form4Model>(";Exec getForm4BAdminList @jtStartIndex, @jtPageSize", new { jtStartIndex, jtPageSize });

                //DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form4Model>(form2BList);

                //string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, form2BList.Count, jtStartIndex, 1, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }
        //ProduceForm4B
        [Authorize]
        public ActionResult ProduceForm4B()
        {
            ViewBag.Subtitle = "Produce Form 4B";

            ProduceForm4B models = new Models.ProduceForm4B();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("4B", int.Parse(Session["WoredaID"].ToString()));

            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());
            //models.serviceprovids = dbService.GetServiceProviders();
            models.intgrtedservis = dbService.GetIntegratedServices(models.ServiceProviderID, 2);

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            SPmodel.FormCode = "B";
            SPmodel.KebeleID = models.KebeleID;
            SPmodel.ReportingPeriodID = models.ReportingPeriodID;

            models.serviceprovids = dbService.GetServiceProvidersUpdated(SPmodel);

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm4B(ProduceForm4B newmodel)
        {
            ViewBag.Subtitle = "Produce Form 4B";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.ProduceForm4B(newmodel, out errMessage);

                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Form 4B Listing";

                        return RedirectToAction("Form4B");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("4B", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                SPmodel.FormCode = "B";
                SPmodel.KebeleID = newmodel.KebeleID;
                SPmodel.ReportingPeriodID = newmodel.ReportingPeriodID;

                newmodel.serviceprovids = dbService.GetServiceProvidersUpdated(SPmodel);

                newmodel.intgrtedservis = dbService.GetIntegratedServices(newmodel.ServiceProviderID, 2);

                return View("ProduceForm4B", newmodel);
                
            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("4B", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                SPmodel.FormCode = "B";
                SPmodel.KebeleID = newmodel.KebeleID;
                SPmodel.ReportingPeriodID = newmodel.ReportingPeriodID;

                newmodel.serviceprovids = dbService.GetServiceProvidersUpdated(SPmodel);
                newmodel.intgrtedservis = dbService.GetIntegratedServices(newmodel.ServiceProviderID, 2);

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("ProduceForm4B", newmodel);
            }
        }

        //FORM 4C

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form4C()
        {
            ViewBag.Subtitle = "Form 4C Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form4CListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");

                Tuple<List<int>, List<Form4Model>> resultset
                = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form4Model>(";Exec getForm4CAdminList @jtStartIndex, @jtPageSize",
                new
                {
                    jtStartIndex = jtStartIndex,
                    jtPageSize = jtPageSize
                });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form4Model>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 2);


                //List<Form4Model> form2CList = con.Fetch<Form4Model>(";Exec getForm4CAdminList @jtStartIndex, @jtPageSize", new { jtStartIndex, jtPageSize });

                //DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form4Model>(form2CList);

                //string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, form2CList.Count, jtStartIndex, 1, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        //ProduceForm2C
        [Authorize]
        public ActionResult ProduceForm4C()
        {
            ViewBag.Subtitle = "Produce Form 4C";

            ProduceForm4C models = new Models.ProduceForm4C();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            //models.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("4C", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            SPmodel.FormCode = "C";
            SPmodel.KebeleID = models.KebeleID;
            SPmodel.ReportingPeriodID = models.ReportingPeriodID;

            models.serviceprovids = dbService.GetServiceProvidersUpdated(SPmodel);

            models.intgrtedservis = dbService.GetIntegratedServices(models.ServiceProviderID, 3);

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm4C(ProduceForm4C newmodel)
        {
            ViewBag.Subtitle = "Produce Form 4C";
            string errMessage = string.Empty;
            
            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.ProduceForm4C(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Form 4C Listing";
                        return RedirectToAction("Form4C");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("4C", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                SPmodel.FormCode = "C";
                SPmodel.KebeleID = newmodel.KebeleID;
                SPmodel.ReportingPeriodID = newmodel.ReportingPeriodID;

                newmodel.serviceprovids = dbService.GetServiceProvidersUpdated(SPmodel);

                newmodel.intgrtedservis = dbService.GetIntegratedServices(newmodel.ServiceProviderID, 3);

                    return View("ProduceForm4C", newmodel);
                
            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("4C", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                SPmodel.FormCode = "C";
                SPmodel.KebeleID = newmodel.KebeleID;
                SPmodel.ReportingPeriodID = newmodel.ReportingPeriodID;

                newmodel.serviceprovids = dbService.GetServiceProvidersUpdated(SPmodel);

                newmodel.intgrtedservis = dbService.GetIntegratedServices(newmodel.ServiceProviderID, 3);

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("ProduceForm4C", newmodel);
            }
        }

        // Form 4D
        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form4D()
        {
            ViewBag.Subtitle = "Form 4D Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form4DListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");

                Tuple<List<int>, List<Form4Model>> resultset
                = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form4Model>(";Exec getForm4DAdminList @jtStartIndex, @jtPageSize",
                new
                {
                    jtStartIndex = jtStartIndex,
                    jtPageSize = jtPageSize
                });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form4Model>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 2);


                //List<Form4Model> form2BList = con.Fetch<Form4Model>(";Exec getForm4BAdminList @jtStartIndex, @jtPageSize", new { jtStartIndex, jtPageSize });

                //DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form4Model>(form2BList);

                //string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, form2BList.Count, jtStartIndex, 1, 2);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }



        [Authorize]
        public ActionResult ProduceForm4D()
        {
            ViewBag.Subtitle = "Produce Form 4D";

            ProduceForm4D models = new Models.ProduceForm4D();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("4D", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm4D(ProduceForm4C newmodel)
        {
            ViewBag.Subtitle = "Produce Form 4D";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.ProduceForm4C(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Form 4D Listing";
                        return RedirectToAction("Form4D");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("4C", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                SPmodel.FormCode = "C";
                SPmodel.KebeleID = newmodel.KebeleID;
                SPmodel.ReportingPeriodID = newmodel.ReportingPeriodID;

                newmodel.serviceprovids = dbService.GetServiceProvidersUpdated(SPmodel);

                newmodel.intgrtedservis = dbService.GetIntegratedServices(newmodel.ServiceProviderID, 3);

                return View("ProduceForm4D", newmodel);

            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                //newmodel.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("4C", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                SPmodel.FormCode = "C";
                SPmodel.KebeleID = newmodel.KebeleID;
                SPmodel.ReportingPeriodID = newmodel.ReportingPeriodID;

                newmodel.serviceprovids = dbService.GetServiceProvidersUpdated(SPmodel);

                newmodel.intgrtedservis = dbService.GetIntegratedServices(newmodel.ServiceProviderID, 3);

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("ProduceForm4D", newmodel);
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult SelectService(ServiceProviders model)
        {
            var ServiceDetails = dbService.GetIntegratedServices(model.ServiceProviderID, model.FormID);
            return Json(ServiceDetails, JsonRequestBehavior.AllowGet);
        }
    }
}
