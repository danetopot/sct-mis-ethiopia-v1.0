﻿function showLoading(fl) {
    var span = document.getElementById("recfound")
    if (!span) return;

    span.style.color = "#580808";
    if (fl === true) {
        span.innerHTML = "loading...";
        $('#loading').show();
    }
    else {
        $('#loading').hide();
        span.innerHTML = "";
    }
}

$("#MyForm").submit(function (e) {
    $("#loading").show();
});

function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

function post(path, parameters) {
    var form = $('<form></form>');

    form.attr("method", "post");
    form.attr("action", path);

    $.each(parameters, function (key, value) {
        var field = $('<input></input>');

        field.attr("type", "hidden");
        field.attr("name", key);
        field.attr("value", value);

        form.append(field);
    });

    // The form needs to be a part of the document in
    // order for us to be able to submit it.
    $(document.body).append(form);
    form.submit();
}

function get(path, parameters) {
    var form = $('<form></form>');

    form.attr("method", "get");
    form.attr("action", path);
    form.attr("target", "_blank");

    $.each(parameters, function (key, value) {
        var field = $('<input></input>');

        field.attr("type", "hidden");
        field.attr("name", key);
        field.attr("value", value);

        form.append(field);
    });

    // The form needs to be a part of the document in
    // order for us to be able to submit it.
    $(document.body).append(form);
    form.submit();
}

$("#SearchTypeID").click(function () {
    $("#SearchKeyword").val('');
});


$('.numbersOnly').keypress(function () {
    if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    }
});
$('.alphaonly').keypress(function (e) {
    if (e.ctrlKey || e.altKey) {//e.shiftKey || 
        e.preventDefault();
    } else {
        //var key = e.keyCode;
        var key = (e.which) ? e.which :
                         ((e.charCode) ? e.charCode :
                           ((e.keyCode) ? e.keyCode : 0));
        //console.log(key);
        if (!((key == 8) || (key == 9) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90) || (key >= 97 && key <= 122))) {
            e.preventDefault();
        }
    }
});

function limitTextLength(field, maxChar) {
    var ref = $(field),
        val = ref.val();
    if (val.length >= maxChar) {
        ref.val(function () {
            //console.log(val.substr(0, maxChar))
            return val.substr(0, maxChar);
        });
    }
}

function regionChange() {
    $('#WoredaID,#KebeleID').empty();
    $.ajax({
        type: "POST",
        url: "../Household/SelectWoreda",
        data: "{  'RegionID' : '" + $("#RegionID").val() + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        error: function (xx, yy) {
            alert(yy);
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                $('#WoredaID').append($("<option></option>").attr("value", d.WoredaID).text(d.WoredaName));
            }
            woredaChange();
        }
    });
}

function woredaChange() {
    $('#KebeleID').empty();
    $.ajax({
        type: "POST",
        url: "../Household/SelectKebele",
        data: "{  'WoredaID' : '" + $("#WoredaID").val() + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        error: function (xx, yy) {
            alert(yy);
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                $('#KebeleID').append($("<option></option>").attr("value", d.KebeleID).text(d.KebeleName));
            }
        }
    });
}

function ServiceProviderChange() {
    $('#ServiceID').empty();
    $.ajax({
        type: "POST",
        url: "../Compliance/SelectService",
        data: "{  'ServiceProviderID' : '" + $("#ServiceProviderID").val() + "','FormID' : '" + $("#FormID").val() + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        error: function (xx, yy) {
            alert(yy);
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                $('#ServiceID').append($("<option></option>").attr("value", d.ServiceID).text(d.ServiceName));
            }
        }
    });
}

$("#RegionID").change(function () {
    regionChange();
});

$("#WoredaID").change(function () {
    woredaChange();
});

$("#ServiceProviderID").change(function () {
    ServiceProviderChange();
});

function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 10; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function my_error_handler() {
}

//function getAge(dateString) {
//    var today = new Date();
//    var birthDate = new Date(dateString);
//    var age = today.getFullYear() - birthDate.getFullYear();
//    var m = today.getMonth() - birthDate.getMonth();
//    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
//        age--;
//    }
//    return age;
//}

function getAge(dateString) {
    var startMonths = getMonthsFromDate(moment(dateString));
    var endMonths = getMonthsFromDate(moment());

    var months = endMonths - startMonths;
    
    var ageInYears = Math.floor(months / 12);

    if (ageInYears >= 1) {
        return ageInYears;
    }
    
    var ageInMonths = months % 12;

    var age;

    if (ageInMonths > 0) {
        if (ageInMonths > 9) {
            age = (Number(ageInYears) + Number(ageInMonths / 100)).toFixed(2);
        } else {
            age = Number(ageInYears) + Number((ageInMonths / 10).toFixed(1));
        }
    }
    else {
        age = Number(ageInYears);
    }

    return age;
}

function getMonthsFromDate(moment_date) {
    var months = Number(moment_date.format("MM"));
    var years = Number(moment_date.format("YYYY"));

    return months + (years * 12);
}

function isValid(str) {
    if (str === "" || str === "0" || str === 0) {
        return false;
    } else {
        return true;
    }
}
function reloadGrid()
{

}

function LoadGridWithSearchxmlData(myXml) {
    mygrid.clearAll();
    mygrid.parse(myXml);
    showLoading(false);
}


function check_product(param) {
    return $.ajax({
        type: 'POST',
        data: param,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: '../Dashboard/HouseProfileByAge'
    });
}

//$('#btnSubmit').bind('click', function () {
//    var txtVal = $('#txtDate').val();
//    if (isDate(txtVal))
//        alert('Valid Date');
//    else
//        alert('Invalid Date');
//});
function isDate(txtDate)
{
    var currVal = txtDate;
    if(currVal == '')
        return false;
    
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    
    if (dtArray == null) 
        return false;
    
    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay= dtArray[3];
    dtYear = dtArray[5];        
    
    if (dtMonth < 1 || dtMonth > 12) 
        return false;
    else if (dtDay < 1 || dtDay> 31) 
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
        return false;
    else if (dtMonth == 2) 
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
            return false;
    }
    return true;
}

function fnActivateCalender(dateCntr) {
    cal1 = new dhtmlXCalendarObject(dateCntr);
    cal1.setSkin("dhx_skyblue");
    cal1.setDateFormat("%d/%M/%Y");
}
function jsDateDiff(date1, date2, interval) {
    var second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
    date1 = new Date(date1);
    date2 = new Date(date2);
    var timediff = date2 - date1;
    if (isNaN(timediff)) return NaN;
    switch (interval) {
        case "years": return date2.getFullYear() - date1.getFullYear();
        case "months": return (
            (date2.getFullYear() * 12 + date2.getMonth())
            -
            (date1.getFullYear() * 12 + date1.getMonth())
        );
        case "weeks": return Math.floor(timediff / week);
        case "days": return Math.floor(timediff / day);
        case "hours": return Math.floor(timediff / hour);
        case "minutes": return Math.floor(timediff / minute);
        case "seconds": return Math.floor(timediff / second);
        default: return undefined;
    }
}