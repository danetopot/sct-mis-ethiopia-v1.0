﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ReportExportModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Reporting ReportExport
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   
<% using (Html.BeginForm("PrintExcel", "Security", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-2">                
            <label>
                Region <%: Html.ValidationMessageFor(model => model.RegionCodeID) %>
                <%: Html.DropDownListFor(model => model.RegionCodeID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
            </label>
            </div>
            <div class="col-2">                
            <label>
                Woreda  <%: Html.ValidationMessageFor(model => model.WoredaCodeID) %>
                <%: Html.DropDownListFor(model => model.WoredaCodeID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
            </label>
            </div>
            <div class="col-2">                              
            <label>
                Kebele <%: Html.ValidationMessageFor(model => model.KebeleCodeID) %>  
                <%: Html.DropDownListFor(model => model.KebeleCodeID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
            </label>
            </div>
            <div class="col-2">                  
            <label>
                Fiscal Year<%: Html.ValidationMessageFor(model => model.FiscalYear) %>
                <%: Html.DropDownListFor(model => model.FiscalYear,
                    new SelectList(Model.fiscalYears, "FiscalYear", "FiscalYearName"), new { tabindex = "4" })%>
            </label>
            </div>
            <div class="col-2">                              
            <label>
                Report Name <%: Html.ValidationMessageFor(model => model.ReportName) %>  
                <%: Html.DropDownListFor(model => model.ReportName,
                    new SelectList(Model.rptTypes, "ReportID", "ReportName"), new { tabindex = "5" })%>
            </label>
            </div>

            <br />
            <br />
            <div id="validationErrors" style="color: red;"></div>  
            
            <br />
            <br />
            <div class="form-actions">
	            <button id="cmdProduce" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Export Report</button>
            </div>
            <br />
            <%: Html.ValidationSummary(true) %>            
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#RegionCodeID,#WoredaCodeID,#ReportName,#KebeleCodeID").prop("disabled", false);
            $("#RegionCodeID,#WoredaCodeID,#ReportName,#KebeleCodeID").val('');
            $("#cmdProduce").prop("disabled", false);
            $('#loading').hide();
            $('#RegionCodeID').focus();

            $("#RegionCodeID").change(function () {
                regionCodeChange();
            });

            $("#WoredaCodeID").change(function () {
                woredaCodeChange();
            });

            $("#MyForm").submit(function (event) {
                $('#loading').hide();

                var html = '<ul>';
                var errors = [];
                var valid = true;

                if ($('#ReportName').val() == '' || $('#ReportName').val() == null) {
                    errors.push('<li>Please select a Report Name</li>');
                    valid = false;                  
                }

                if (!valid) {
                    html += errors.join('') + '</ul>'
                    $('#validationErrors').show();
                    $('#validationErrors').append(html);
                    
                    event.stopImmediatePropagation();
                    event.preventDefault();
                }
                else {
                    $('#validationErrors').hide();
                }

            });

            function regionCodeChange() {
                $('#WoredaCodeID,#KebeleCodeID').empty();
                $.ajax({
                    type: "POST",
                    url: "../Household/SelectDashWoreda",
                    data: "{  'RegionID' : '" + $("#RegionCodeID").val() + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        for (var i = 0; i < data.length; i++) {
                            var d = data[i];
                            $('#WoredaCodeID').append($("<option></option>").attr("value", d.WoredaID).text(d.WoredaName));
                        }
                        woredaCodeChange();
                    }
                });
            }

            function woredaCodeChange() {
                $('#KebeleCodeID').empty();
                $.ajax({
                    type: "POST",
                    url: "../Household/SelectDashKebele",
                    data: "{  'WoredaID' : '" + $("#WoredaCodeID").val() + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        for (var i = 0; i < data.length; i++) {
                            var d = data[i];
                            $('#KebeleCodeID').append($("<option></option>").attr("value", d.KebeleID).text(d.KebeleName));
                        }
                    }
                });
            }
        });
    </script>
</asp:Content>