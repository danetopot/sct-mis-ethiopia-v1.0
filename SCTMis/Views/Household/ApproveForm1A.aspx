﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.CaptureForm1A>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Modify Form 1A
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<h5>Add New Girl Form</h5>   --%>   
    
<% using (Html.BeginForm("ModifiedForm1A", "Household", FormMethod.Post, new { id = "MyForm" } ))
    { %>
        <%--The First Row--%>
            <div class="col-7">                
            <label>
                Region *
                <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Woreda *
                <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Kebele *  
                <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
            </label>
            </div>
            <div class="col-7">                  
            <label>
                Gote 
                <%: Html.TextBoxFor(model => model.Gote, new { tabindex = "4" , @maxlength="30" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Gare 
                <%: Html.TextBoxFor(model => model.Gare, new { tabindex = "5" , @maxlength="30" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Household ID *
                <%: Html.TextBoxFor(model => model.HouseHoldIDNumber, new { tabindex = "6", @maxlength="20" })%>
            </label>
            </div>   

            <div class="col-7">
                <label>
                    CBHI Membership
                        <%: Html.DropDownListFor(model => model.CBHIMembership,
                            new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "6" })%>
                </label>
            </div>
        <%--Second Row--%>
            <div class="col-4">                
            <label>
                Name of household head *
                <%: Html.TextBoxFor(model => model.NameOfHouseHoldHead, new { tabindex = "7" , @class = "alphaonly", @maxlength="50" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Collection Date
                <%: Html.TextBoxFor(model => model.CollectionDate, new { tabindex = "8",@Style="width:95%;"})%>
            </label>
            </div>
            <div class="col-7">                              
            <label>
                Social Worker
                <%: Html.DropDownListFor(model => model.SocialWorker,
                    new SelectList(Model.worker, "ID", "Name"), new { tabindex = "9" })%>
            </label>
            </div>

            <div class="col-4">                  
            <label>
                Remarks:
                <%: Html.TextBoxFor(model => model.Remarks, new { tabindex = "10" ,@maxlength="200" })%>
            </label>
            </div>

            <div class="col-7">                  
            <label>
                CCC member
                <%: Html.TextBoxFor(model => model.CCCCBSPCMember, new { tabindex = "11" , @maxlength="25" })%>
            </label>
            </div>

            <div class="col-7">
                <label>
                    CBHI Number:
                        <%: Html.TextBoxFor(model => model.CBHINumber, new { tabindex = "10" ,@maxlength="20" })%>
                </label>
            </div>
    <hr />
        <%--Third Row--%>
            <div class="col-3">                
            <label>
                Names of HH member including grandfathers *
                <%: Html.TextBoxFor(model => model.HouseHoldMemberName, new { tabindex = "12" , @class = "alphaonly", @maxlength="50" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Individual ID in Family Folder
                <%: Html.TextBoxFor(model => model.IndividualID, new { tabindex = "12",@maxlength="15"})%>
            </label>
            </div>
            <div class="col-7">                  
            <label>
                Date Of Birth *
                <%: Html.TextBoxFor(model => model.DateOfBirth, new { tabindex = "13",@Style="width:95%;"})%>
            </label>
            </div>
        <%--Fourth Row--%>
            <div class="col-7">                
            <label>
                Age *
                <%: Html.TextBoxFor(model => model.Age, new { tabindex = "14" , @class = "numbersOnly",@maxlength="3" })%>
            </label>
            </div>
    <br />
            <div class="col-7">                
            <label>
                Sex *
                <%: Html.DropDownListFor(model => model.Sex,
                    new SelectList(Model.genders, "GenderID", "GenderName"), new { tabindex = "15" })%>
            </label>
            </div>
            <div class="col-7">                              
            <label>
                Pregnant *
                <%: Html.DropDownListFor(model => model.Pregnant,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "16" })%>
            </label>
            </div>
            <div class="col-7">                              
            <label>
                Lactating *
                <%: Html.DropDownListFor(model => model.Lactating,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "17" })%>
            </label>
            </div>
            <div class="col-7">                  
            <label>
                Handicapped *
                <%: Html.DropDownListFor(model => model.Handicapped,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "18" })%>
            </label>
            </div>
        <%--Fifth Row--%>
            <div class="col-7">                
            <label>
                Chronically ill *
                <%: Html.DropDownListFor(model => model.ChronicallyIll,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "19" })%>
            </label>
            </div>
            <div class="col-4">                
            <label>
                Nutritional status 0 – 5 and PLW *
                <%: Html.DropDownListFor(model => model.NutritionalStatus,
                    new SelectList(Model.nutrStatus, "ID", "Name"), new { tabindex = "20" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Is child under TSF or CMAM  
                <%: Html.DropDownListFor(model => model.childUnderTSForCMAM,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "21" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Enrolled in School? 
                <%: Html.DropDownListFor(model => model.EnrolledInSchool,
                    new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "22" })%>
            </label>
            </div>
            <div class="col-7">                  
            <label>
                Grade
                <%: Html.DropDownListFor(model => model.Grade,
                    new SelectList(Model.grades, "ID", "Name"), new { tabindex = "23" })%>
            </label>
            </div>
            <div class="col-4">                  
            <label>
                School Name
                <%: Html.TextBoxFor(model => model.SchoolName, new { tabindex = "24" , @maxlength="40" })%>
            </label>
            </div>
            <div class="col-4">
                <label>
                    Potential Child Protection Risk? 
                        <%: Html.DropDownListFor(model => model.ChildProtectionRisk,
                            new SelectList(Model.yesnos, "ID", "Name"), new { tabindex = "28" })%>
                </label>
            </div>
        <%--Sixth Row--%>
            <%--<button id="cmdProceed" type="button" class="btn btn-icon btn-primary glyphicons circle_plus" tabindex="26"><i></i>Modify Household Member</button>--%>
            <%--<button id="cmdNewMember" type="button" class="btn btn-icon btn-primary glyphicons circle_plus" tabindex="27"><i></i>Add New HH Member</button>--%>
            <br />
            <div id="errors2" style="color: red;"></div>  
            <br />
            <div id="gridbox" style="position:relative; width: 870px; height: 140px"></div>
            <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>
          <br />
            <div class="form-actions">
                <button id="cmdApprove" type="button" name="submitButton" class="btn btn-icon btn-primary glyphicons circle_ok" tabindex="25"><i></i>Approve Details</button>
	            <button id="cmdModify" type="submit" name="submitButton" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save Modified Details</button>
                <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
            </div>
                <%: Html.HiddenFor(model => model.ColumnID)%>
                <%: Html.HiddenFor(model => model.ProfileDSHeaderID)%>
                <%: Html.HiddenFor(model => model.Kebele)%>
                <%: Html.HiddenFor(model => model.CreatedBy)%>
                <%: Html.HiddenFor(model => model.MemberXml)%>
                <%: Html.HiddenFor(model => model.AllowEdit, new { id = "AllowEdit" })%>
                <%: Html.HiddenFor(model => model.ApprovedBy, new { id = "ApprovedBy" })%>
            <br />
            <%: Html.ValidationSummary(true) %>          
    <% } %>

    <div id="progress" class="modal" style="display: none">
        <div class="center">
            <p>Approving record, please wait . . . <img src="../Images/loader.gif"></p>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <link href="../DHTMLX/Calendar/css/default.css" rel="stylesheet" />
    <script src="../Scripts/assets/js/moment.min.js"></script>
    <script type="text/javascript" src="../DHTMLX/Calendar/javascript/zebra_datepicker.js"></script>
    <script type="text/javascript" src="../Scripts/assets/js/app/ApproveForm1A.js"></script>
</asp:Content>

