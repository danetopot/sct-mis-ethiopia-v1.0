USE [SCT-MIS]
GO
/****** Object:  StoredProcedure [dbo].[InsertCapturedForm4D]    Script Date: 25/07/2019 15:41:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[GetCapturedForm4DAdminList]
(
	@KebeleID								INT,
	@ClientType								VARCHAR(5),
	@jtStartIndex							INT = 0,
	@jtPageSize								INT	= 10
)
AS
BEGIN

	SET NOCOUNT ON

	BEGIN TRY

		CREATE TABLE #CapturedForm4D
		(
			UniqueID						VARCHAR(200),
			RegionName						VARCHAR(200),
			KebeleName						VARCHAR(200),
			WoredaName						VARCHAR(200),
			GoteGare						VARCHAR(200),
			ProfileDetailID					VARCHAR(200),
			HouseHoldIDNumber				VARCHAR(200),
			NameOfHouseHoldHead				VARCHAR(200),
			HouseHoldMemberName				VARCHAR(200),
			HouseHoldMemberAge				VARCHAR(200),
			HouseHoldMemberSex				VARCHAR(200),
			FiscalYear						VARCHAR(200),
			ChildProtectionHeaderID			VARCHAR(200),
			ClientType						VARCHAR(200)
		)

		INSERT INTO #CapturedForm4D(UniqueID, RegionName,KebeleName,WoredaName,GoteGare,ProfileDetailID,HouseHoldIDNumber,NameOfHouseHoldHead,
		HouseHoldMemberName,HouseHoldMemberAge,HouseHoldMemberSex,FiscalYear,ChildProtectionHeaderID,ClientType)
		SELECT 
		NEWID(),T3.RegionName, T3.KebeleName, T3.WoredaName,T2.Gote+'/'+T2.Gare AS GoteGare, T1.ProfileDSDetailID,
		T2.HouseHoldIDNumber, T2.NameOfHouseHoldHead, T1.HouseHoldMemberName, 
		T1.Age HouseHoldMemberAge, CASE WHEN T1.Sex='M' THEN 'Male' ELSE 'FEMALE' END AS HouseHoldMemberSex,
		T2.FiscalYear, T4.ChildProtectionHeaderID, 'PDS' ClientType
		FROM ProfileDSDetail T1 
		INNER JOIN ProfileDSHeader T2 ON T1.ProfileDSHeaderID=T2.ProfileDSHeaderID
		INNER JOIN AdminStructureView T3 ON T2.KebeleID=T3.KebeleID
		INNER JOIN [dbo].[ChildProtectionHeader] T4 ON T1.ProfileDSDetailID=T4.ChildProtectionId
		WHERE 
		DATEDIFF(MONTH,T1.DateOfBirth,GETDATE()) BETWEEN 72 AND 216
		AND (T2.KebeleID = @KebeleID)
		ORDER BY T2.NameOfHouseHoldHead

		INSERT INTO #CapturedForm4D(UniqueID,RegionName,KebeleName,WoredaName,GoteGare,ProfileDetailID,HouseHoldIDNumber,NameOfHouseHoldHead,
		HouseHoldMemberName,HouseHoldMemberAge,HouseHoldMemberSex,FiscalYear,ChildProtectionHeaderID,ClientType)
		SELECT
		NEWID(),T3.RegionName, T3.KebeleName, T3.WoredaName,T2.Gote+'/'+T2.Gare AS GoteGare, T1.ProfileTDSPLWDetailID,
		T2.HouseHoldIDNumber, T2.NameOfPLW NameOfHouseHoldHead, ISNULL(T1.BabyName, 'Mother Pregnant') HouseHoldMemberName,
		CASE WHEN DATEDIFF(YEAR, T1.BabyDateOfBirth, GETDATE()) > 1 THEN CAST(DateDiff(Year, T1.BabyDateOfBirth, GETDATE()) AS VARCHAR) ELSE 0  END HouseHoldMemberAge,
		CASE WHEN T1.BabySex='M' THEN 'Male' ELSE 'FEMALE' END AS HouseHoldMemberSex,
		T2.FiscalYear, T4.ChildProtectionHeaderID, 'PLW' ClientType
		FROM ProfileTDSPLWDetail T1 
		INNER JOIN ProfileTDSPLW T2 ON T2.ProfileTDSPLWID = T1.ProfileTDSPLWDetailID
		INNER JOIN AdminStructureView T3 ON T2.KebeleID=T3.KebeleID
		INNER JOIN [dbo].[ChildProtectionHeader] T4 ON T1.ProfileTDSPLWDetailID=T4.ChildProtectionId
		WHERE (T2.KebeleID = @KebeleID)
		ORDER BY T2.NameOfPLW

		INSERT INTO #CapturedForm4D(UniqueID,RegionName,KebeleName,WoredaName,GoteGare,ProfileDetailID,HouseHoldIDNumber,NameOfHouseHoldHead,
		HouseHoldMemberName,HouseHoldMemberAge,HouseHoldMemberSex,FiscalYear,ChildProtectionHeaderID,ClientType)
		SELECT 
		NEWID(),T3.RegionName, T3.KebeleName, T3.WoredaName,T1.Gote+'/'+T1.Gare AS GoteGare, T1.ProfileTDSCMCID,
		T1.HouseHoldIDNumber, T1.NameOfCareTaker NameOfHouseHoldHead, T1.MalnourishedChildName HouseHoldMemberName,
		CASE WHEN DATEDIFF(YEAR, T1.ChildDateOfBirth, GETDATE()) > 1 THEN CAST(DateDiff(Year, T1.ChildDateOfBirth, GETDATE()) AS VARCHAR) ELSE 0  END HouseHoldMemberAge,
		CASE WHEN T1.MalnourishedChildSex='M' THEN 'Male' ELSE 'FEMALE' END AS HouseHoldMemberSex,
		T1.FiscalYear, T4.ChildProtectionHeaderID, 'CMC' ClientType
		FROM ProfileTDSCMC T1
		INNER JOIN AdminStructureView T3 ON T1.KebeleID=T3.KebeleID
		INNER JOIN [dbo].[ChildProtectionHeader] T4 ON T1.ProfileTDSCMCID=T4.ChildProtectionId
		WHERE (T1.KebeleID = @KebeleID)
		ORDER BY T1.NameOfCareTaker
			

		
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION UPDATERECORD
	   END 

	   DECLARE @ERROR_MESSAGE	VARCHAR(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

	SET NOCOUNT OFF
END
