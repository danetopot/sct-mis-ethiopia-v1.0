﻿using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Xml.Linq;
using System.Web.Mvc;
using System.Web;
using ClosedXML.Excel;
using System.IO;

namespace SCTMis.Services
{
    public class ReportServices
    {
        AccountServices dbService = new AccountServices();

        public string SerializeDataTable(DataTable dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        public Dictionary<string, string> GenerateReport(
            string username,
            int? regionID, int? woredaID, int? kebeleID,
            string rptType, string rptDisaggregation,string rptPeriod, string socialWorker,
            string [] rpt1Filters, string[] rpt2Filters, string[] rpt3Filters, string[] rpt4Filters, 
            string[] rpt5Filters, string[] rpt6Filters, string[] rpt7Filters, string[] rpt8Filters, 
            string[] rpt9Filters, string[] rpt10Filters)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            string Key = "Result";
            string Msg = "Report Successfully Generated";
            string strReportFilePath = string.Empty;

            try
            {
                if (rptType == "RPT1")
                {
                    Tuple<List<HouseholdProfilePDSDetailReport>, List<HouseholdProfilePDSSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfilePDSDetailReport, HouseholdProfilePDSSummaryReport>(
                         ";Exec HouseholdProfileReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker, " +
                         "@Gender, @Pregnant, @Lactating, @Handicapped, @ChronicallyIll, @NutritionalStatus, @ChildUnderTSForCMAM," +
                         "@EnrolledInSchool, @ChildProtectionRisk, @CBHIMembership",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         Gender = rpt1Filters[0],
                         Pregnant = rpt1Filters[1],
                         Lactating = rpt1Filters[2],
                         Handicapped = rpt1Filters[3],
                         ChronicallyIll = rpt1Filters[4],
                         NutritionalStatus = rpt1Filters[5],
                         ChildUnderTSForCMAM = rpt1Filters[6],
                         EnrolledInSchool = rpt1Filters[7],
                         ChildProtectionRisk = rpt1Filters[8],
                         CBHIMembership = rpt1Filters[9]
                     });

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT2")
                {
                    string startDateTDS = rpt2Filters[0];
                    string endDateTDS = rpt2Filters[1];
                    if (startDateTDS == "Jan/0001") { startDateTDS = null; }
                    if (endDateTDS == "Jan/0001") { endDateTDS = null; }

                    Tuple<List<HouseholdProfileTDSPLWDetailReport>, List<HouseholdProfileTDSPLWSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSPLWDetailReport, HouseholdProfileTDSPLWSummaryReport>(";Exec HouseholdProfileReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker," +
                        "null, null, null, null, null, null, null,null, @ChildProtectionRisk, @CBHIMembership,@StartDateTDS, @EndDateTDS, @NutritionalStatusPLW, @NutritionalStatusInfant",
                     new
                     {

                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ChildProtectionRisk = rpt2Filters[4],
                         CBHIMembership = rpt2Filters[5],
                         StartDateTDS = startDateTDS,
                         EndDateTDS = endDateTDS,
                         NutritionalStatusPLW = rpt2Filters[2],
                         NutritionalStatusInfant = rpt2Filters[3]
                     });

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT3")
                {
                    string startDateTDS = rpt2Filters[0];
                    string endDateTDS = rpt2Filters[1];
                    if (startDateTDS == "Jan/0001") { startDateTDS = null; }
                    if (endDateTDS == "Jan/0001") { endDateTDS = null; }

                    Tuple<List<HouseholdProfileTDSCMCDetailReport>, List<HouseholdProfileTDSCMCSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSCMCDetailReport, HouseholdProfileTDSCMCSummaryReport>(";Exec HouseholdProfileReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker," +
                        "null, null, null, null, null, null, null,null, @ChildProtectionRisk, @CBHIMembership,@StartDateTDS, @EndDateTDS, null, null, @MalnutritionDegree",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ChildProtectionRisk = rpt3Filters[3],
                         CBHIMembership = rpt3Filters[4],
                         StartDateTDS = startDateTDS,
                         EndDateTDS = endDateTDS,
                         MalnutritionDegree = rpt3Filters[2],
                     });

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT4")
                {
                    Tuple<List<SocialServicesNeedsDetailReport>, List<SocialServicesNeedsSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<SocialServicesNeedsDetailReport, SocialServicesNeedsSummaryReport>(";Exec SocialServicesNeedsReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ClientType = rpt4Filters[0],
                         ServiceType = rpt4Filters[1]
                     });

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT5")
                {
                    Tuple<List<ServiceComplianceDetailReport>, List<ServiceComplianceSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<ServiceComplianceDetailReport, ServiceComplianceSummaryReport>(";Exec ServiceComplianceReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ClientType = rpt5Filters[0],
                         ServiceType = rpt5Filters[1]
                     });

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT6")
                {
                    Tuple<List<ServiceComplianceDetailReport>, List<ServiceComplianceSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<ServiceComplianceDetailReport, ServiceComplianceSummaryReport>(";Exec ServiceNonComplianceReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ClientType = rpt6Filters[0],
                         ServiceType = rpt6Filters[1]
                     });

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT7")
                {
                    Tuple<List<CaseManagementComplianceDetailReport>, List<CaseManagementComplianceSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<CaseManagementComplianceDetailReport, CaseManagementComplianceSummaryReport>(";Exec CaseManagementComplianceReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ClientType = rpt7Filters[0],
                         ServiceType = rpt7Filters[1]
                     });

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT8")
                {
                    Tuple<List<CaseManagementComplianceDetailReport>, List<CaseManagementComplianceSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<CaseManagementComplianceDetailReport, CaseManagementComplianceSummaryReport>(";Exec CaseManagementNonComplianceReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker, @ClientType, @ServiceType",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker,
                         ClientType = rpt8Filters[0],
                         ServiceType = rpt8Filters[1]
                     });

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT9")
                {
                    Tuple<List<RetargetingDetailReport>, List<RetargetingSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<RetargetingDetailReport, RetargetingSummaryReport>(";Exec RetargetingNewReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker
                     });

                    dict.Add(Key, Msg);
                    return dict;
                }

                if (rptType == "RPT10")
                {
                    Tuple<List<RetargetingDetailReport>, List<RetargetingSummaryReport>> resultset
                     = GeneralServices.NpocoConnection().FetchMultiple<RetargetingDetailReport, RetargetingSummaryReport>(";Exec RetargetingExitReport @RegionID, @WoredaID, @KebeleID, @ReportType, @ReportLevel, @FiscalYear, @SocialWorker",
                     new
                     {
                         RegionID = regionID,
                         WoredaID = woredaID,
                         KebeleID = kebeleID,
                         ReportType = rptType,
                         ReportLevel = rptDisaggregation,
                         FiscalYear = rptPeriod,
                         SocialWorker = socialWorker
                     });


                    dict.Add(Key, Msg);
                    return dict;
                }

                return dict;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                dict.Add(Key, "Error Generating Report :- " + e.ToString());
                return dict;
            }
        }

        public string[] ExportReport(string reportName, string reportLevel, string reportFormat)
        {
            DataTable dt;
            string strReportFilePath = string.Empty;
            string filename = string.Empty;

            try
            {
                if (reportName == "RPT1")
                {

                    Tuple<List<HouseholdProfilePDSDetailReport>, List<HouseholdProfilePDSSummaryReport>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfilePDSDetailReport, HouseholdProfilePDSSummaryReport>(
                        ";Exec ProduceStandardRpt1",
                        new
                        { });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfilePDSDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfilePDSDetailsRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "HouseholdProfile_PDS_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }
                            
                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfilePDSSummaryReport>(resultset.Item2);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfilePDSSummaryRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "HouseholdProfile_PDS_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT2")
                {

                    Tuple<List<HouseholdProfileTDSPLWDetailReport>, List<HouseholdProfileTDSPLWSummaryReport>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSPLWDetailReport, HouseholdProfileTDSPLWSummaryReport>(
                        ";Exec ProduceStandardRpt2",
                        new
                        { });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileTDSPLWDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfileTDSPLWDetailsRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "HouseholdProfile_PLW_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileTDSPLWSummaryReport>(resultset.Item2);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfileTDSPLWSummaryRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "HouseholdProfile_PLW_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT3")
                {

                    Tuple<List<HouseholdProfileTDSCMCDetailReport>, List<HouseholdProfileTDSCMCSummaryReport>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<HouseholdProfileTDSCMCDetailReport, HouseholdProfileTDSCMCSummaryReport>(
                        ";Exec ProduceStandardRpt3",
                        new
                        { });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileTDSCMCDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfileTDSCMCDetailsRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "HouseholdProfile_CMC_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<HouseholdProfileTDSCMCSummaryReport>(resultset.Item2);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/HouseholdProfileTDSCMCSummaryRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "HouseholdProfile_CMC_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT4")
                {

                    Tuple<List<SocialServicesNeedsDetailReport>, List<SocialServicesNeedsSummaryReport>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<SocialServicesNeedsDetailReport, SocialServicesNeedsSummaryReport>(
                        ";Exec ProduceStandardRpt4",
                        new
                        { });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<SocialServicesNeedsDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/SocialServicesNeedsDetailsRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "SocialServicesNeeds_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<SocialServicesNeedsSummaryReport>(resultset.Item2);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/SocialServicesNeedsSummaryRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "SocialServicesNeeds_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT5")
                {

                    Tuple<List<ServiceComplianceDetailReport>, List<ServiceComplianceSummaryReport>> resultset
                        = GeneralServices.NpocoConnection().FetchMultiple<ServiceComplianceDetailReport, ServiceComplianceSummaryReport>(
                        ";Exec ProduceStandardRpt5",
                        new
                        { });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<ServiceComplianceDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/ServiceComplianceDetailsRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "ServiceCompliance_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<ServiceComplianceSummaryReport>(resultset.Item2);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/ServiceComplianceSummaryRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "ServiceCompliance_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT6")
                {

                    Tuple<List<ServiceComplianceDetailReport>, List<ServiceComplianceSummaryReport>> resultset
                       = GeneralServices.NpocoConnection().FetchMultiple<ServiceComplianceDetailReport, ServiceComplianceSummaryReport>(
                       ";Exec ProduceStandardRpt6",
                       new
                       { });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<ServiceComplianceDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/ServiceNonComplianceDetailsRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "ServiceNonCompliance_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<ServiceComplianceSummaryReport>(resultset.Item2);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/ServiceNonComplianceSummaryRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "ServiceNonCompliance_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT7")
                {

                    Tuple<List<CaseManagementComplianceDetailReport>, List<CaseManagementComplianceSummaryReport>> resultset
                       = GeneralServices.NpocoConnection().FetchMultiple<CaseManagementComplianceDetailReport, CaseManagementComplianceSummaryReport>(
                       ";Exec ProduceStandardRpt7",
                       new
                       { });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<CaseManagementComplianceDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/CaseManagementComplianceDetailRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "CaseManagementCompliance_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<CaseManagementComplianceSummaryReport>(resultset.Item2);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/CaseManagementComplianceSummaryRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "CaseManagementCompliance_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT8")
                {

                    Tuple<List<CaseManagementComplianceDetailReport>, List<CaseManagementComplianceSummaryReport>> resultset
                       = GeneralServices.NpocoConnection().FetchMultiple<CaseManagementComplianceDetailReport, CaseManagementComplianceSummaryReport>(
                       ";Exec ProduceStandardRpt8",
                       new
                       { });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<CaseManagementComplianceDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/CaseManagementNonComplianceDetailRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "CaseManagementNonCompliance_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<CaseManagementComplianceSummaryReport>(resultset.Item2);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/CaseManagementNonComplianceSummaryRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "CaseManagementNonCompliance_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT9")
                {

                    Tuple<List<RetargetingDetailReport>, List<RetargetingSummaryReport>> resultset
                       = GeneralServices.NpocoConnection().FetchMultiple<RetargetingDetailReport, RetargetingSummaryReport>(
                       ";Exec ProduceStandardRpt9",
                       new
                       { });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<RetargetingDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/RetargetingNewDetailsRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "RetargetingNew_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<RetargetingSummaryReport>(resultset.Item2);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/RetargetingNewSummaryRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "RetargetingNew_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                if (reportName == "RPT10")
                {

                    Tuple<List<RetargetingDetailReport>, List<RetargetingSummaryReport>> resultset
                       = GeneralServices.NpocoConnection().FetchMultiple<RetargetingDetailReport, RetargetingSummaryReport>(
                       ";Exec ProduceStandardRpt10",
                       new
                       { });

                    if (reportLevel == "DT")
                    {
                        dt = GeneralServices.ToDataTable<RetargetingDetailReport>(resultset.Item1);

                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/RetargetingExitDetailsRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "RetargetingExit_Details_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item1;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Detail Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                    if (reportLevel == "SM")
                    {
                        dt = GeneralServices.ToDataTable<RetargetingSummaryReport>(resultset.Item2);
                        string strRptPath = HttpContext.Current.Server.MapPath("~/rptFolder/RetargetingExitSummaryRptFile.rpt");
                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.Database.Tables[0].SetDataSource(dt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        filename = "RetargetingExit_Summary_Report_" + DateTime.Now.ToFileTime().ToString() + "." + reportFormat;
                        strReportFilePath = HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                        ExportOptions exportOpts = new ExportOptions();
                        ExcelFormatOptions excelFormatOpts = new ExcelFormatOptions();
                        DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();
                        exportOpts = myReportDocument.ExportOptions;

                        if (reportFormat == "xls")
                        {
                            var list = resultset.Item2;
                            using (var wb = new XLWorkbook())
                            {
                                dt.TableName = "Summary Report";
                                wb.Worksheets.Add(dt);
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    File.WriteAllBytes(strReportFilePath, stream.ToArray());
                                }
                                wb.Dispose();
                            }

                        }
                        if (reportFormat == "pdf")
                        {
                            exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
                            exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
                            diskOpts.DiskFileName = strReportFilePath;
                            exportOpts.DestinationOptions = diskOpts;
                            myReportDocument.Export();
                        }
                    }
                }

                string[] path = new string[] { strReportFilePath, filename };
                return path;

            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return null;
            }
        }
    }

    
}
