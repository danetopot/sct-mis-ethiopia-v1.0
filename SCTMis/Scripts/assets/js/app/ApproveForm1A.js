﻿var EVENTID = null;
var arrObj = [];
var SerialCount = 0;
var page_count = 5;
var mygrid;

/*
var gridHeader = 'ColumnID,ProfileDSDetailID,Member<span class="HeaderChange">_</span>Name,Individual<span class="HeaderChange">_</span>ID';
gridHeader = gridHeader + ',Pregnant,Lactating,DOB,Age,Gender,Handicapped,Chronically<span class="HeaderChange">_</span>Ill';
gridHeader = gridHeader + ',NSN,Nutritional<span class="HeaderChange">_</span>Status,childUnderTSForCMAM';
gridHeader = gridHeader + ',School<span class="HeaderChange">_</span>Enrolled,Grade,School<span class="HeaderChange">_</span>Name,R,Edit,Delete';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img,img';
*/
var gridHeader = 'ColumnID,ProfileDSDetailID,Member<span class="HeaderChange">_</span>Name,Individual<span class="HeaderChange">_</span>ID';
gridHeader = gridHeader + ',Pregnant,Lactating,DOB,Age,Gender,Handicapped,Chronically<span class="HeaderChange">_</span>Ill';
gridHeader = gridHeader + ',NSN,Nutritional<span class="HeaderChange">_</span>Status,childUnderTSForCMAM';
gridHeader = gridHeader + ',School<span class="HeaderChange">_</span>Enrolled,Grade,School<span class="HeaderChange">_</span>Name,R';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro';

//var myDOB, myColDate;
$(document).ready(function () {
    $("#progress").hide();

    $('#CollectionDate').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        format: 'd/M/Y'
    });

    $('#DateOfBirth').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        format: 'd/M/Y',
        //view: 'years',
        onChange: function (view, elements) {
            DobChange();
        }
    });

    $('#cmdModify').hide();
    
    $("#RegionID,#WoredaID,#KebeleID,#cmdApprove").prop("disabled", true);
    $("#Gote,#Gare,#HouseHoldIDNumber,#NameOfHouseHoldHead,#CollectionDate,#SocialWorker,#Remarks,#CCCCBSPCMember").prop("disabled", true);
    $("#DateOfBirth,#HouseHoldMemberName,#Pregnant,#Lactating,#Age,#Sex,#IndividualID,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#CBHIMembership,#CBHINumber,#ChildProtectionRisk").prop("disabled", true);
    $("#HouseHoldMemberName,#Pregnant,#Lactating,#IndividualID,#childUnderTSForCMAM,#DateOfBirth,#Age,#SchoolName,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade").val("");
    $("#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool").val("-");

    if ($("#AllowEdit").val() == 'True') {
        if ($("#ApprovedBy").val() == '') {
            $("#cmdNewMember,#cmdApprove").prop("disabled", false);
        } else {
            $("#cmdNewMember,#cmdApprove").prop("disabled", true);
            $("#cmdApprove").val('Approval not possible');
        }
    } else {
        $('#Gote,#NameOfHouseHoldHead,#HouseHoldIDNumber,#Gare,#HouseHoldIDNumber,#CollectionDate,#SocialWorker,#CCCCBSPCMember,#Remarks').prop("disabled", true);
    }

    $("#cmdModify,#cmdProceed").prop("disabled", true);

    $('#loading').hide();
    //ProfileDSHeaderID
    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,0,200,105,0,0,0,50,60,0,0,0,150,0,140,0,160,0");
    mygrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();
    mygrid.loadXML("/Household/FetchGridForm1ADetailsByID?RecCount=" + page_count + "&ProfileDSHeaderID=" + $("#ProfileDSHeaderID").val());
    dhtmlxError.catchError("ALL", my_error_handler);

    $.ajax({
        type: "POST",
        url: "../Household/FetchJsonForm1ADetailsByID",
        data: "{  'RecCount':" + page_count + ",ProfileDSHeaderID : '" + $("#ProfileDSHeaderID").val() + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        error: function (xx, yy) {
            //alert(yy);
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                MemberCount = data.length;
                if (MemberCount != undefined) {

                    for (var i = 0; i < data.length; i++) {
                        var theDtls = new Object;
                        var d = data[i];
                        SerialCount = SerialCount + 1;
                        theDtls.ColumnID = SerialCount;
                        theDtls.ProfileDSDetailID = d.ProfileDSDetailID;
                        theDtls.HouseHoldMemberName = d.HouseHoldMemberName;
                        theDtls.IndividualID = d.IndividualID;
                        theDtls.Pregnant = d.Pregnant;
                        theDtls.Lactating = d.Lactating;
                        theDtls.DateOfBirth = d.DateOfBirth;
                        theDtls.Age = d.Age;
                        theDtls.Sex = d.Sex;
                        theDtls.Handicapped = d.Handicapped;
                        theDtls.ChronicallyIll = d.ChronicallyIll;
                        theDtls.NutritionalStatus = d.NutritionalStatus;
                        theDtls.NutritionalStatusName = d.NutritionalStatusName;
                        theDtls.childUnderTSForCMAM = d.childUnderTSForCMAM;
                        theDtls.EnrolledInSchool = d.EnrolledInSchool;
                        theDtls.Grade = d.Grade;
                        theDtls.SchoolName = d.SchoolName;
                        theDtls.ChildProtectionRisk = d.ChildProtectionRisk;
                        //theDtls.Remarks = d.Remarks;
                        
                        arrObj.push(theDtls);
                    }
                    $("#MemberXml").val(JSON.stringify(arrObj));
                }                
            }
        }
    });

    $("#cmdBack").click(function () {
        window.location.href = "/Household/Form1A";
    });

    $('#KebeleID,#NameOfHouseHoldHead,#HouseHoldIDNumber,#CollectionDate,#SocialWorker,#CCCCBSPCMember').bind('focusout', function (event) {
        if (!isValid($(this).val())) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $(this).off("blur");
            $(this).focus();
        }
    });

    $('#HouseHoldMemberName,#DateOfBirth,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus').bind('focusout', function (event) {
        if (!isValid($(this).val())) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $(this).off("blur");
            $(this).focus();
        }
    });

    $("#DateOfBirth").focusout(function () {
        DobChange();
    });

    $('#Sex').change(function (e) {
        var TheAge = getAge($('#DateOfBirth').val());
        if ($('#Sex').val() == 'M') {
            $('#Pregnant,#Lactating').val('-');
            $('#Pregnant,#Lactating').prop("disabled", true);
            if (parseInt(TheAge) >= 6) {
                $("#NutritionalStatus option[value='N']").attr("disabled", "disabled");
                $("#NutritionalStatus option[value='M']").attr("disabled", "disabled");
                $("#NutritionalStatus option[value='SM']").attr("disabled", "disabled");
                $('#NutritionalStatus').val('-');
            }

        } else {
            $('#Pregnant,#Lactating').prop("disabled", false);
        }

        if (parseInt(TheAge) < 12) {
            $('#Pregnant,#Lactating').val('-');
            $('#Pregnant,#Lactating').prop("disabled", true);
        }
    });

    //#EnrolledInSchool
    $('#EnrolledInSchool').change(function (e) {
        var TheAge = getAge($('#DateOfBirth').val());

        if ($('#EnrolledInSchool').val() == 'NO') {
            if (parseInt(TheAge) >= 5 && parseInt(TheAge) < 18) {
                $('#Grade').prop("disabled", false);
                DisableEnableGrade("E");
            } else {
                $('#Grade').prop("disabled", true);
                DisableEnableGrade("D");
            }
        } else {
            $('#Grade').prop("disabled", false);
            DisableEnableGrade("E");
        }
    });

    $("#cmdNewMember").click(function () {
        EVENTID = "ADD";
        $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Pregnant,#Lactating,#Grade").val("");
       
        $("#Handicapped,#ChronicallyIll,#NutritionalStatus,#childUnderTSForCMAM,#EnrolledInSchool,#SchoolName").val("-");
        $("#cmdNewMember,#cmdApprove").prop("disabled", true);
        $("#cmdProceed").prop("disabled", false);
        $("#HouseHoldMemberName,#IndividualID,#Pregnant,#Lactating,#childUnderTSForCMAM,#DateOfBirth,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName").prop("disabled", false);

        $('#HouseHoldMemberName').focus();
    });

    $("#cmdProceed").click(function () {
        //EVENTID = "ADD";
        var errors = [];
        var html = '<ul>';
        valid = true;
        $('#errors2').empty();
        if ($('#HouseHoldMemberName').val() == '') {
            errors.push('<li>House Hold Member Name Required</li>');
            valid = false;
        }

        if ($('#DateOfBirth').val() == "" && parseInt($('#Age').val()) <= 0) {
            errors.push('<li>Date Of Birth or age required</li>');
            valid = false;
        }

        if ($('#Sex').val() == '') {
            errors.push('<li>Gender is Required</li>');
            valid = false;
        }

        if ($('#Handicapped').val() == '') {
            errors.push('<li>Please specify if Handicapped</li>');
            valid = false;
        }

        if ($('#ChronicallyIll').val() == '') {
            errors.push('<li>Please specify if Chronically Ill</li>');
            valid = false;
        }

        if ($('#NutritionalStatus').val() == '') {
            errors.push('<li>Please specify Nutritional Status</li>');
            valid = false;
        }

        if ($('#EnrolledInSchool').val() == '') {
            errors.push('<li>Please specify if Enrolled In School</li>');
            valid = false;
        }

        if ($('#EnrolledInSchool').val() == 'YES' && $('#Grade').val() == '') {
            errors.push('<li>Please specify Grade</li>');
            valid = false;
        }

        if ($('#SchoolName').val() == "" && $('#EnrolledInSchool').val() == 'YES') {
            errors.push('<li>School Name is Required</li>');
            valid = false;
        }

        if (!valid) {
            html += errors.join('') + '</ul>'
            $('#errors2').show();
            $('#errors2').append(html);
            return valid;
        }
        else {
            $('#errors2').hide();
        }

        var theDtls = new Object;
        if (EVENTID == "EDIT") {

            for (var i in arrObj) {
                if (arrObj.hasOwnProperty(i)) {
                    if (arrObj[i].ColumnID == $("#ColumnID").val()) {
                        arrObj[i].ProfileDSDetailID = $("#ProfileDSDetailID").val();
                        arrObj[i].HouseHoldMemberName = $("#HouseHoldMemberName").val();
                        arrObj[i].IndividualID = $("#IndividualID").val();
                        arrObj[i].Pregnant = $("#Pregnant").val();
                        arrObj[i].Lactating = $("#Lactating").val();
                        arrObj[i].DateOfBirth = $("#DateOfBirth").val();
                        arrObj[i].Age = $("#Age").val();
                        arrObj[i].Sex = $("#Sex").val();
                        arrObj[i].SexName = $("#Sex").find('option:selected').text();
                        arrObj[i].Handicapped = $("#Handicapped").val();
                        arrObj[i].HandicappedName = $("#Handicapped").find('option:selected').text();
                        arrObj[i].ChronicallyIll = $("#ChronicallyIll").val();
                        arrObj[i].ChronicallyIllName = $("#ChronicallyIll").find('option:selected').text();
                        arrObj[i].NutritionalStatus = $("#NutritionalStatus").val();
                        arrObj[i].NutritionalStatusName = $("#NutritionalStatus").find('option:selected').text();
                        arrObj[i].childUnderTSForCMAM = $("#childUnderTSForCMAM").val();
                        arrObj[i].EnrolledInSchool = $("#EnrolledInSchool").val();                        
                        arrObj[i].EnrolledInSchoolName = $("#EnrolledInSchool").find('option:selected').text();
                        arrObj[i].Grade = $("#Grade").val();
                        arrObj[i].SchoolName = $("#SchoolName").val();
                        arrObj[i].ChildProtectionRisk = $("#ChildProtectionRisk").val();
                    }
                }
            }
        }
        else {
            $('#errors2').hide();

            SerialCount = SerialCount + 1;
            theDtls.ColumnID = SerialCount;
            theDtls.ProfileDSDetailID = $("#ProfileDSDetailID").val();
            theDtls.HouseHoldMemberName = $("#HouseHoldMemberName").val();
            theDtls.IndividualID = $("#HouseHoldIDNumber").val();
            theDtls.Pregnant = $("#Pregnant").val();
            theDtls.Lactating = $("#Lactating").val();
            theDtls.DateOfBirth = $("#DateOfBirth").val();
            theDtls.Age = $("#Age").val();
            theDtls.Sex = $("#Sex").val();
            theDtls.SexName = $("#Sex").find('option:selected').text();
            theDtls.Handicapped = $("#Handicapped").val();
            theDtls.HandicappedName = $("#Handicapped").find('option:selected').text();
            theDtls.ChronicallyIll = $("#ChronicallyIll").val();
            theDtls.ChronicallyIllName = $("#ChronicallyIll").find('option:selected').text();
            theDtls.NutritionalStatus = $("#NutritionalStatus").val();
            theDtls.NutritionalStatusName = $("#NutritionalStatus").find('option:selected').text();
            theDtls.childUnderTSForCMAM = $("#childUnderTSForCMAM").val();
            theDtls.EnrolledInSchool = $("#EnrolledInSchool").val();
            theDtls.EnrolledInSchoolName = $("#EnrolledInSchool").find('option:selected').text();
            theDtls.Grade = $("#Grade").val();
            theDtls.SchoolName = $("#SchoolName").val();
            //theDtls.Remarks = $("#Remarks").val();            

            arrObj.push(theDtls);
        }
        var myXml = createXmlstring(arrObj, 1);
        mygrid.clearAll();
        mygrid.parse(myXml);

        $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#ChildProtectionRisk").val('');
        $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#ChildProtectionRisk").prop("disabled", true);
        $("#cmdModify,#cmdNewMember").prop("disabled", false);
        $("#cmdProceed").prop("disabled", true);

    });
});

function createWindow(_ColumnID) {
    var selectedRow = mygrid.getSelectedId();
    _ColumnID = mygrid.cells(selectedRow, 0).getValue();
    $("#HouseHoldMemberName,#IndividualID,#Pregnant,#Lactating,#childUnderTSForCMAM,#DateOfBirth,#Age,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#ChildProtectionRisk").val('')
    $("#Pregnant,#Lactating,#childUnderTSForCMAM,#NutritionalStatus,#Handicapped,#ChronicallyIll,#EnrolledInSchool,#ChildProtectionRisk").val('-')
    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            if (arrObj[i].ColumnID == _ColumnID) {
                $("#ColumnID").val(arrObj[i].ColumnID);
                $("#HouseHoldMemberName").val(arrObj[i].HouseHoldMemberName);
                $("#IndividualID").val(arrObj[i].IndividualID);
                $("#DateOfBirth").val(arrObj[i].DateOfBirth);
                $("#Age").val(arrObj[i].Age);
                $("#Sex").val(arrObj[i].Sex);
                $("#Pregnant").val(arrObj[i].Pregnant);
                $("#Lactating").val(arrObj[i].Lactating);
                $("#Handicapped").val(arrObj[i].Handicapped);
                $("#ChronicallyIll").val(arrObj[i].ChronicallyIll);
                $("#NutritionalStatus").val(arrObj[i].NutritionalStatus);
                $("#childUnderTSForCMAM").val((arrObj[i].childUnderTSForCMAM == "N/A" ? "-" : arrObj[i].childUnderTSForCMAM));
                $("#EnrolledInSchool").val((arrObj[i].EnrolledInSchool=="N/A"?"-":arrObj[i].EnrolledInSchool));
                $("#Grade").val(arrObj[i].Grade);
                $("#SchoolName").val(arrObj[i].SchoolName);
                $("#ChildProtectionRisk").val(arrObj[i].ChildProtectionRisk);
                $('#Sex').change();
                $('#DateOfBirth').change();
                $('#EnrolledInSchool').change();
            }
        }
    }

    $("#cmdModify,#cmdProceed,#cmdNewMember,#cmdApprove").prop("disabled", true);
    //console.log($("#AllowEdit").val());
    if ($("#AllowEdit").val() == 'True') {
        $("#cmdProceed,#cmdNewMember,#cmdApprove").prop("disabled", false);
        $("#HouseHoldMemberName,#IndividualID,#Pregnant,#Lactating,#childUnderTSForCMAM,#DateOfBirth,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#ChildProtectionRisk").prop("disabled", false);
        $('#HouseHoldMemberName').focus();
    }
    
    EVENTID = "EDIT";
}

function UpdateMembers(_ColumnID) {
    createWindow(_ColumnID);
}

function createXmlstring(arrObject, inMemory) {
    var xml;
    var gridID;
    var reclength = 0;
    xml = '';
    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            reclength + reclength + 1;
            xml = xml + '<row id="' + i + '">';
            for (var j in arrObj[i]) {
                if (arrObj[i].hasOwnProperty(j)) {
                    if (j == "ColumnID") {
                        xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                        gridID = arrObj[i][j];
                    }
                }
                if (j == "ProfileDSDetailID" || j == "HouseHoldMemberName" || j == "IndividualID" || j == "Pregnant"
                    || j == "Lactating" || j == "DateOfBirth" || j == "Age" || j == "Sex"
                    || j == "Handicapped" || j == "ChronicallyIll" || j == "NutritionalStatusName" || j == "NutritionalStatus"
                    || j == "childUnderTSForCMAM" || j == "EnrolledInSchool" || j == "Grade" || j == "SchoolName") {
                    xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                }
            }
            xml = xml + '<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit^javascript:UpdateMembers(' + gridID + ');^_self</cell>';
            xml = xml + '<cell>../DHTMLX/codebase/imgs/but_cut.gif^Delete record^javascript:DeleteRecord(' + gridID + ');^_self</cell>';
            xml = xml + '</row>';
        }
    }
    xml = '<rows total_count="' + reclength + '">' + xml + '</rows>';
    $("#MemberXml").val(JSON.stringify(arrObject));

    console.log(xml);
    return xml;
}

function DobChange() {
    $("#NutritionalStatus option[value='N']").attr("disabled", "disabled");
    $("#NutritionalStatus option[value='M']").attr("disabled", "disabled");
    $("#NutritionalStatus option[value='SM']").attr("disabled", "disabled");
    $("#ChronicallyIll option[value='-']").attr("disabled", "disabled");
    $("#Handicapped option[value='-']").attr("disabled", "disabled");

    if ($('#DateOfBirth').val() == "") {
        $('#DateOfBirth').focus();
    } else {
        var TheAge = getAge($('#DateOfBirth').val());

        $("#Age").val(TheAge);

        if (parseInt(TheAge) > 5 && parseInt(TheAge) < 18) {
            $('#EnrolledInSchool,#Grade,#SchoolName').attr('readonly', false);

            $("#childUnderTSForCMAM").val("-");
            $('#childUnderTSForCMAM').attr('readonly', true);
            $("#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
            $("#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");

            $("#EnrolledInSchool,#Grade option[value='NO']").attr("disabled", false);
            $("#EnrolledInSchool,#Grade option[value='YES']").attr("disabled", false);
        }
        //else {
        //    $('#EnrolledInSchool,#Grade,#SchoolName,#childUnderTSForCMAM,#NutritionalStatus').attr('readonly', true);
        //    $('#EnrolledInSchool,#Grade,#childUnderTSForCMAM,#NutritionalStatus').val('-');

        //    $("#EnrolledInSchool,#Grade,#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
        //    $("#EnrolledInSchool,#Grade,#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");

        //    //if (parseInt(TheAge) <= 5 || ($("#Sex").val()=="F" && parseInt(TheAge) >= 12 && ($("#Pregnant").val() == "YES" || $("#Lactating").val() == "YES"))) {
        //    //    //Not sure if this part should be enabled.
        //    //    $("#NutritionalStatus option[value='N']").attr("disabled", false);
        //    //    $("#NutritionalStatus option[value='M']").attr("disabled", false);
        //    //    $("#NutritionalStatus option[value='SM']").attr("disabled", false);
        //    //}
        //}
        //THIS IS FOR PREGNANT WOMEN ABOVE 12 YEARS BUT NOT OLDER THAN 50 YEARS
        if (parseInt(TheAge) >= 12 && parseInt(TheAge) <= 50) {
            if ($("#Sex").val() == "F" && ($("#Pregnant").val() == "YES" || $("#Lactating").val() == "YES")) {

                $('#EnrolledInSchool,#Grade,#SchoolName,#childUnderTSForCMAM,#NutritionalStatus').attr('readonly', true);
                $('#EnrolledInSchool,#Grade,#childUnderTSForCMAM,#NutritionalStatus').val('-');

                $("#EnrolledInSchool,#Grade,#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
                $("#EnrolledInSchool,#Grade,#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");

                $("#NutritionalStatus option[value='N']").attr("disabled", false);
                $("#NutritionalStatus option[value='M']").attr("disabled", false);
                $("#NutritionalStatus option[value='SM']").attr("disabled", false);
            }
        }
        //FOR INFANTS BELOW 5 YEARS
        if (parseInt(TheAge) <= 5) {
            $('#EnrolledInSchool,#Grade,#SchoolName,#childUnderTSForCMAM,#NutritionalStatus').attr('readonly', true);
            $('#EnrolledInSchool,#Grade,#childUnderTSForCMAM,#NutritionalStatus').val('-');

            $("#EnrolledInSchool,#Grade,#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
            $("#EnrolledInSchool,#Grade,#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");

            $("#NutritionalStatus option[value='N']").attr("disabled", false);
            $("#NutritionalStatus option[value='M']").attr("disabled", false);
            $("#NutritionalStatus option[value='SM']").attr("disabled", false);
        }

        $('#Sex').change();
    }
    //$('#Sex').focus();
}

$('#NutritionalStatus').change(function (e) {
    var TheAge = getAge($('#DateOfBirth').val());
    if (parseInt(TheAge) <= 5) {
        if ($("#NutritionalStatus").val() == "M" || $("#NutritionalStatus").val() == "SM") {
            //Not sure if this part should be enabled.
            $('#childUnderTSForCMAM').attr('readonly', false);
            $("#childUnderTSForCMAM option[value='NO']").attr("disabled", false);
            $("#childUnderTSForCMAM option[value='YES']").attr("disabled", false);
        } else {
            $("#childUnderTSForCMAM").val("-");
            $('#childUnderTSForCMAM').attr('readonly', true);
            $("#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
            $("#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");
        }
    }

});

function DeleteRecord(_ColumnID) {
    if ($("#AllowEdit").val().toUpperCase() != "FALSE") {
        var selectedRow = mygrid.getSelectedId();
        _ColumnID = mygrid.cells(selectedRow, 0).getValue();
        createWindow(_ColumnID);
        var result = confirm("Do you want to delete The Record?");
        if (result) {
            for (var i in arrObj) {
                if (arrObj.hasOwnProperty(i)) {
                    if (arrObj[i].ColumnID == $("#ColumnID").val()) {
                        delete arrObj[i];
                        arrObj.length = arrObj.length - 1;
                        var myXml = createXmlstring(arrObj, 1);
                        mygrid.clearAll();
                        mygrid.parse(myXml);
                    }
                }
            }
        }
    }

    $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Grade,#SchoolName").val('');
    $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName").prop("disabled", true);
    $("#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool").val("-");
    $("#cmdModify").prop("disabled", false);
    $("#cmdProceed").prop("disabled", true);
}

function DisableEnableGrade(Status) {
    if (Status == "E") {
        $("#Grade option[value='1']").attr("disabled", false);
        $("#Grade option[value='2']").attr("disabled", false);
        $("#Grade option[value='3']").attr("disabled", false);
        $("#Grade option[value='4']").attr("disabled", false);
        $("#Grade option[value='5']").attr("disabled", false);
        $("#Grade option[value='6']").attr("disabled", false);
        $("#Grade option[value='7']").attr("disabled", false);
        $("#Grade option[value='8']").attr("disabled", false);
        $("#Grade option[value='9']").attr("disabled", false);
        $("#Grade option[value='10']").attr("disabled", false);
        $("#Grade option[value='11']").attr("disabled", false);
        $("#Grade option[value='12']").attr("disabled", false);
        $("#Grade option[value='13']").attr("disabled", false);
        $("#Grade option[value='14']").attr("disabled", false);
    } else {
        $("#Grade option[value='1']").attr("disabled", "disabled");
        $("#Grade option[value='2']").attr("disabled", "disabled");
        $("#Grade option[value='3']").attr("disabled", "disabled");
        $("#Grade option[value='4']").attr("disabled", "disabled");
        $("#Grade option[value='5']").attr("disabled", "disabled");
        $("#Grade option[value='6']").attr("disabled", "disabled");
        $("#Grade option[value='7']").attr("disabled", "disabled");
        $("#Grade option[value='8']").attr("disabled", "disabled");
        $("#Grade option[value='9']").attr("disabled", "disabled");
        $("#Grade option[value='10']").attr("disabled", "disabled");
        $("#Grade option[value='11']").attr("disabled", "disabled");
        $("#Grade option[value='12']").attr("disabled", "disabled");
        $("#Grade option[value='13']").attr("disabled", "disabled");
        $("#Grade option[value='14']").attr("disabled", "disabled");
    }
}

$("#cmdApprove").click(function () {
    $("#cmdApprove").prop("disabled", true);

    $("#progress").show();

    $.ajax({
        type: "POST",
        url: "../Household/UpdateApprovedForm1A",
        data: "{  'ProfileDSHeaderID':'" + $("#ProfileDSHeaderID").val() + "',CreatedBy : '" + $("#CreatedBy").val() + "'}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        error: function (xx, yy) {
            $("#progress").hide();
            $("#cmdApprove").prop("disabled", false);
            //alert(yy);
        },
        success: function (data) {            
            window.location.href = "/Household/Form1A";
            $("#progress").hide();
        }
    });

});