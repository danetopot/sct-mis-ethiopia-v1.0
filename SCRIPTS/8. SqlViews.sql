USE [SCT-MIS]
GO
IF OBJECT_ID('[ViewServices]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewServices]
GO
IF OBJECT_ID('[viewServiceDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[viewServiceDetails]
GO
IF OBJECT_ID('[ViewGeneratedComplianceDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewGeneratedComplianceDetails]
GO
IF OBJECT_ID('[ViewGeneratedNonComplianceDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewGeneratedNonComplianceDetails]
GO
IF OBJECT_ID('[ViewForm5CDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm5CDetails]
GO
IF OBJECT_ID('[ViewForm5A2Details]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm5A2Details]
GO
IF OBJECT_ID('[ViewForm5A1Details]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm5A1Details]
GO
IF OBJECT_ID('[ViewForm1ADetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm1ADetails]
GO
IF OBJECT_ID('[ViewForm3CTDSCMCHeader]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm3CTDSCMCHeader]
GO
IF OBJECT_ID('[ViewForm2CTDSCMCHeader]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm2CTDSCMCHeader]
GO
IF OBJECT_ID('[ViewCoResponsibilityDS]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewCoResponsibilityDS]
GO
IF OBJECT_ID('[ViewGeneratedNonComplianceTDSCMCDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewGeneratedNonComplianceTDSCMCDetails]
GO
IF OBJECT_ID('[ViewNonComplianceTDSCMCDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewNonComplianceTDSCMCDetails]
GO
IF OBJECT_ID('[ViewGeneratedComplianceTDSCMCDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewGeneratedComplianceTDSCMCDetails]
GO
IF OBJECT_ID('[ViewComplianceTDSCMCDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewComplianceTDSCMCDetails]
GO
IF OBJECT_ID('[ViewGeneratedComplianceTDSPLWDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewGeneratedComplianceTDSPLWDetails]
GO
IF OBJECT_ID('[ViewDashboardComplianceDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewDashboardComplianceDetails]
GO
IF OBJECT_ID('[ViewForm5CDetailsNew]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm5CDetailsNew]
GO
IF OBJECT_ID('[ViewForm1CDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm1CDetails]
GO
IF OBJECT_ID('[DefaultKebeleView]','V') IS NOT NULL
	DROP VIEW [dbo].[DefaultKebeleView]
GO
IF OBJECT_ID('[ViewForm3BTDSPLWHeader]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm3BTDSPLWHeader]
GO
IF OBJECT_ID('[ViewForm3ADSHeader]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm3ADSHeader]
GO
IF OBJECT_ID('[ViewForm2BTDSPLWHeader]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm2BTDSPLWHeader]
GO
IF OBJECT_ID('[ViewForm2ADSHeader]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm2ADSHeader]
GO
IF OBJECT_ID('[ViewForm1CTDSCMCAdmin]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm1CTDSCMCAdmin]
GO
IF OBJECT_ID('[ViewForm1BPLWAdmin]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm1BPLWAdmin]
GO
IF OBJECT_ID('[ViewForm1BPLW]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm1BPLW]
GO
IF OBJECT_ID('[ViewForm5BDetailsList]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm5BDetailsList]
GO
IF OBJECT_ID('[ViewForm5BDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm5BDetails]
GO
IF OBJECT_ID('[ViewForm1BDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm1BDetails]
GO
IF OBJECT_ID('[viewComplianceDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[viewComplianceDetails]
GO
IF OBJECT_ID('[ViewForm1AHouseHoldProfile]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewForm1AHouseHoldProfile]
GO
IF OBJECT_ID('[AdminStructureView]','V') IS NOT NULL
	DROP VIEW [dbo].[AdminStructureView]
GO
IF OBJECT_ID('[ViewNonComplianceDSDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewNonComplianceDSDetails]
GO
IF OBJECT_ID('[ViewComplianceDSDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewComplianceDSDetails]
GO
IF OBJECT_ID('[ViewGeneratedNonComplianceTDSPLWDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewGeneratedNonComplianceTDSPLWDetails]
GO
IF OBJECT_ID('[ViewNonComplianceTDSPLWDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewNonComplianceTDSPLWDetails]
GO
IF OBJECT_ID('[ViewComplianceTDSPLWDetails]','V') IS NOT NULL
	DROP VIEW [dbo].[ViewComplianceTDSPLWDetails]
GO
/****** Object:  View [dbo].[ViewComplianceTDSPLWDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewComplianceTDSPLWDetails]
AS
SELECT        dbo.ProfileTDSPLW.KebeleID, dbo.ProfileTDSPLW.NameOfPLW, dbo.ProfileTDSPLW.HouseHoldIDNumber, dbo.ProfileTDSPLW.Gote, dbo.ProfileTDSPLW.Gare, dbo.ComplianceSTDSPLWDetail.ProfileTDSPLWID, 
                         dbo.ProfileTDSPLWDetail.BabyName, dbo.ProfileTDSPLWDetail.BabySex, dbo.ProfileTDSPLW.PLW, dbo.ProfileTDSPLW.MedicalRecordNumber, dbo.ProfileTDSPLW.PLWAge, dbo.ComplianceSTDSPLWHeader.ServiceID, 
                         dbo.ProfileTDSPLWDetail.BabyDateOfBirth, dbo.ProfileTDSPLWDetail.NutritionalStatusInfant, dbo.ComplianceSTDSPLWHeader.ReportingPeriodID, dbo.f_GetUserNameByUserID(dbo.ComplianceSTDSPLWHeader.CapturedBy) 
                         AS CapturedBy, CONVERT(VARCHAR(11), dbo.ComplianceSTDSPLWHeader.CapturedOn, 106) AS CapturedOn, dbo.ProfileTDSPLW.CBHIMembership, dbo.ProfileTDSPLW.CBHINumber, 
                         dbo.ProfileTDSPLW.ChildProtectionRisk
FROM            dbo.ProfileTDSPLW LEFT OUTER JOIN
                         dbo.ProfileTDSPLWDetail ON dbo.ProfileTDSPLW.ProfileTDSPLWID = dbo.ProfileTDSPLWDetail.ProfileTDSPLWID INNER JOIN
                         dbo.ComplianceSTDSPLWHeader ON dbo.ComplianceSTDSPLWHeader.KebeleID = dbo.ProfileTDSPLW.KebeleID INNER JOIN
                         dbo.ComplianceSTDSPLWDetail ON dbo.ComplianceSTDSPLWDetail.ComplianceID = dbo.ComplianceSTDSPLWHeader.ComplianceID AND 
                         dbo.ComplianceSTDSPLWDetail.ProfileTDSPLWID = dbo.ProfileTDSPLW.ProfileTDSPLWID
GO
/****** Object:  View [dbo].[ViewNonComplianceTDSPLWDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*AND ViewComplianceTDSPLWDetails.Remarks = NonComplianceTDSPLWDetail.NonComplianceReason*/
CREATE VIEW [dbo].[ViewNonComplianceTDSPLWDetails]
AS
SELECT        dbo.ProfileTDSPLW.KebeleID, dbo.ProfileTDSPLW.NameOfPLW, dbo.ProfileTDSPLW.HouseHoldIDNumber, dbo.ProfileTDSPLW.Gote, dbo.ProfileTDSPLW.Gare, dbo.NonComplianceTDSPLWDetail.ProfileTDSPLWID, 
                         dbo.ProfileTDSPLWDetail.BabyName, dbo.ProfileTDSPLWDetail.BabySex, dbo.ProfileTDSPLW.MedicalRecordNumber, dbo.ProfileTDSPLW.PLWAge, dbo.NonComplianceTDSPLWDetail.NonComplianceReason, 
                         dbo.NonComplianceTDSPLWDetail.ActionTaken, dbo.NonComplianceTDSPLWHeader.SocialWorker, dbo.NonComplianceTDSPLWHeader.ReportingPeriod, dbo.NonComplianceTDSPLWHeader.CompletedDate, 
                         dbo.ViewComplianceTDSPLWDetails.ServiceID, dbo.ProfileTDSPLW.CollectionDate, dbo.ProfileTDSPLW.SocialWorker AS Expr1, dbo.ProfileTDSPLW.FiscalYear
FROM            dbo.ProfileTDSPLW LEFT OUTER JOIN
                         dbo.ProfileTDSPLWDetail ON dbo.ProfileTDSPLWDetail.ProfileTDSPLWID = dbo.ProfileTDSPLW.ProfileTDSPLWID INNER JOIN
                         dbo.NonComplianceTDSPLWHeader ON dbo.NonComplianceTDSPLWHeader.KebeleID = dbo.ProfileTDSPLW.KebeleID INNER JOIN
                         dbo.NonComplianceTDSPLWDetail ON dbo.NonComplianceTDSPLWDetail.NonComplianceID = dbo.NonComplianceTDSPLWHeader.NonComplianceID AND 
                         dbo.NonComplianceTDSPLWDetail.ProfileTDSPLWID = dbo.ProfileTDSPLW.ProfileTDSPLWID INNER JOIN
                         dbo.ViewComplianceTDSPLWDetails ON dbo.ViewComplianceTDSPLWDetails.KebeleID = dbo.ProfileTDSPLW.KebeleID AND dbo.ViewComplianceTDSPLWDetails.ProfileTDSPLWID = dbo.ProfileTDSPLW.ProfileTDSPLWID
GO
/****** Object:  View [dbo].[ViewGeneratedNonComplianceTDSPLWDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewGeneratedNonComplianceTDSPLWDetails]
AS
SELECT DISTINCT 
                         dbo.GenerateNonCCTDSPLW.KebeleID, dbo.ViewNonComplianceTDSPLWDetails.NameOfPLW, dbo.ViewNonComplianceTDSPLWDetails.HouseHoldIDNumber, dbo.ViewNonComplianceTDSPLWDetails.Gote, 
                         dbo.ViewNonComplianceTDSPLWDetails.Gare, dbo.GenerateNonCCTDSPLW.ProfileTDSPLWID, dbo.ViewNonComplianceTDSPLWDetails.MedicalRecordNumber, dbo.ViewNonComplianceTDSPLWDetails.PLWAge, 
                         dbo.ViewNonComplianceTDSPLWDetails.BabyName, dbo.ViewNonComplianceTDSPLWDetails.BabySex, dbo.ViewNonComplianceTDSPLWDetails.NonComplianceReason, dbo.ViewNonComplianceTDSPLWDetails.ActionTaken, 
                         dbo.ViewNonComplianceTDSPLWDetails.SocialWorker, dbo.GenerateNonCCTDSPLW.ReportingPeriod, dbo.ViewNonComplianceTDSPLWDetails.CompletedDate, dbo.ViewNonComplianceTDSPLWDetails.CollectionDate, 
                         dbo.ViewNonComplianceTDSPLWDetails.ServiceID, dbo.ViewNonComplianceTDSPLWDetails.FiscalYear
FROM            dbo.ViewNonComplianceTDSPLWDetails INNER JOIN
                         dbo.GenerateNonCCTDSPLW ON dbo.ViewNonComplianceTDSPLWDetails.KebeleID = dbo.GenerateNonCCTDSPLW.KebeleID AND 
                         dbo.GenerateNonCCTDSPLW.ReportingPeriod = dbo.ViewNonComplianceTDSPLWDetails.ReportingPeriod AND dbo.GenerateNonCCTDSPLW.ProfileTDSPLWID = dbo.ViewNonComplianceTDSPLWDetails.ProfileTDSPLWID
GO
/****** Object:  View [dbo].[ViewComplianceDSDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[ViewComplianceDSDetails]
AS
Select ProfileDSHeader.KebeleID,ProfileDSHeader.NameOfHouseHoldHead,ProfileDSHeader.HouseHoldIDNumber,
Gote,Gare,ComplianceSDSDetail.ProfileDSDetailID,HouseHoldMemberName,IndividualID,MedicalRecordNumber,
Age,Sex,PWL,ServiceID,Complied,ComplianceSDSDetail.Remarks
FROM ProfileDSHeader
INNER JOIN ProfileDSDetail ON ProfileDSHeader.ProfileDSHeaderID = ProfileDSDetail.ProfileDSHeaderID
INNER JOIN ComplianceSDSHeader ON ComplianceSDSHeader.KebeleID = ProfileDSHeader.KebeleID
INNER JOIN ComplianceSDSDetail ON ComplianceSDSDetail.ProfileDSDetailID = ProfileDSDetail.ProfileDSDetailID
AND ComplianceSDSHeader.ComplianceID = ComplianceSDSDetail.ComplianceID


GO
/****** Object:  View [dbo].[ViewNonComplianceDSDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewNonComplianceDSDetails]
AS
SELECT        dbo.ProfileDSHeader.KebeleID, dbo.ProfileDSHeader.NameOfHouseHoldHead, dbo.ProfileDSHeader.HouseHoldIDNumber, dbo.ProfileDSHeader.Gote, dbo.ProfileDSHeader.Gare, dbo.NonComplianceDSDetail.ProfileDSDetailID, 
                         dbo.ProfileDSDetail.HouseHoldMemberName, dbo.ProfileDSDetail.IndividualID, dbo.ProfileDSDetail.MedicalRecordNumber, dbo.ProfileDSDetail.Age, dbo.ProfileDSDetail.Sex, dbo.ProfileDSDetail.PWL, 
                         dbo.ViewComplianceDSDetails.ServiceID, dbo.NonComplianceDSDetail.NonComplianceReason, dbo.NonComplianceDSDetail.ActionTaken, dbo.NonComplianceDSHeader.SocialWorker, 
                         dbo.NonComplianceDSHeader.ReportingPeriod, dbo.NonComplianceDSHeader.CompletedDate, dbo.ProfileDSHeader.CollectionDate, dbo.ProfileDSHeader.SocialWorker AS Expr1
FROM            dbo.ProfileDSHeader INNER JOIN
                         dbo.ProfileDSDetail ON dbo.ProfileDSHeader.ProfileDSHeaderID = dbo.ProfileDSDetail.ProfileDSHeaderID INNER JOIN
                         dbo.NonComplianceDSHeader ON dbo.NonComplianceDSHeader.KebeleID = dbo.ProfileDSHeader.KebeleID INNER JOIN
                         dbo.NonComplianceDSDetail ON dbo.NonComplianceDSDetail.ProfileDSDetailID = dbo.ProfileDSDetail.ProfileDSDetailID AND 
                         dbo.NonComplianceDSDetail.NonComplianceID = dbo.NonComplianceDSHeader.NonComplianceID INNER JOIN
                         dbo.ViewComplianceDSDetails ON dbo.ViewComplianceDSDetails.KebeleID = dbo.ProfileDSHeader.KebeleID AND dbo.ViewComplianceDSDetails.ProfileDSDetailID = dbo.ProfileDSDetail.ProfileDSDetailID AND 
                         dbo.ViewComplianceDSDetails.Remarks = dbo.NonComplianceDSDetail.NonComplianceReason
GO
/****** Object:  View [dbo].[AdminStructureView]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AdminStructureView]
AS
select Kebele.KebeleID,Kebele.KebeleName,
		Woreda.WoredaID,Woreda.WoredaName, 
		Region.RegionID,Region.RegionName
from Kebele
Inner Join Woreda ON Kebele.WoredaID = Woreda.WoredaID
Inner Join Region ON Woreda.RegionID = Region.RegionID

GO
/****** Object:  View [dbo].[ViewForm1AHouseHoldProfile]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewForm1AHouseHoldProfile]
AS
SELECT        dbo.ProfileDSHeader.ProfileDSHeaderID, dbo.ProfileDSHeader.NameOfHouseHoldHead, dbo.ProfileDSHeader.HouseHoldIDNumber, dbo.ProfileDSHeader.KebeleID, dbo.AdminStructureView.KebeleName, 
                         dbo.AdminStructureView.WoredaID, dbo.AdminStructureView.WoredaName, dbo.AdminStructureView.RegionID, dbo.AdminStructureView.RegionName, dbo.ProfileDSHeader.SocialWorker, 
                         dbo.ProfileDSHeader.CCCCBSPCMember, dbo.ProfileDSHeader.Gote, dbo.ProfileDSHeader.Gare, dbo.ProfileDSHeader.CollectionDate, dbo.ProfileDSHeader.CreatedBy, dbo.ProfileDSHeader.CreatedOn, 
                         dbo.f_GetHouseholdMembers(dbo.ProfileDSHeader.ProfileDSHeaderID) AS RecordCount, dbo.ProfileDSHeader.CBHIMembership
FROM            dbo.ProfileDSHeader INNER JOIN
                         dbo.AdminStructureView ON dbo.ProfileDSHeader.KebeleID = dbo.AdminStructureView.KebeleID
GO
/****** Object:  View [dbo].[viewComplianceDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[viewComplianceDetails]
AS
Select ServiceID,ComplianceSDSHeader.KebeleID,ReportingPeriodID,ProfileDSDetailID ProfileID,Complied, 'Form1A' Form,WoredaID,RegionID
FROM ComplianceSDSHeader
INNER JOIN ComplianceSDSDetail ON ComplianceSDSHeader.ComplianceID = ComplianceSDSDetail.ComplianceID
INNER JOIN AdminStructureView ON ComplianceSDSHeader.KebeleID = AdminStructureView.KebeleID
UNION ALL
Select ServiceID,ComplianceSTDSPLWHeader.KebeleID,ReportingPeriodID,ProfileTDSPLWID ProfileID,Complied, 'Form1B' Form,WoredaID,RegionID
FROM ComplianceSTDSPLWHeader
INNER JOIN ComplianceSTDSPLWDetail ON ComplianceSTDSPLWHeader.ComplianceID = ComplianceSTDSPLWDetail.ComplianceID
INNER JOIN AdminStructureView ON ComplianceSTDSPLWHeader.KebeleID = AdminStructureView.KebeleID
UNION ALL
Select ServiceID,ComplianceSTDSCMCHeader.KebeleID,ReportingPeriodID,ProfileTDSCMCID ProfileID,Complied, 'Form1C' Form,WoredaID,RegionID
FROM ComplianceSTDSCMCHeader
INNER JOIN ComplianceSTDSCMCDetail ON ComplianceSTDSCMCHeader.ComplianceID = ComplianceSTDSCMCDetail.ComplianceID
INNER JOIN AdminStructureView ON ComplianceSTDSCMCHeader.KebeleID = AdminStructureView.KebeleID

GO
/****** Object:  View [dbo].[ViewForm1BDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewForm1BDetails]
AS
SELECT        CONVERT(VARCHAR(100), dbo.ProfileTDSPLW.ProfileTDSPLWID) AS ProfileTDSPLWID, dbo.ProfileTDSPLW.KebeleID, dbo.ProfileTDSPLW.Gote + '/' + dbo.ProfileTDSPLW.Gare AS Gote, dbo.ProfileTDSPLW.Gare, 
                         dbo.ProfileTDSPLW.NameOfPLW, dbo.ProfileTDSPLW.HouseHoldIDNumber, dbo.ProfileTDSPLW.MedicalRecordNumber, dbo.ProfileTDSPLW.PLWAge, dbo.ProfileTDSPLW.StartDateTDS, dbo.ProfileTDSPLW.EndDateTDS, 
                         dbo.ProfileTDSPLWDetail.BabyDateOfBirth, dbo.ProfileTDSPLWDetail.BabyName, dbo.ProfileTDSPLWDetail.BabySex, dbo.ProfileTDSPLW.NutritionalStatusPLW, dbo.ProfileTDSPLWDetail.NutritionalStatusInfant, 
                         dbo.ProfileTDSPLW.Form2Produced, dbo.ProfileTDSPLW.Form3Generated, dbo.ProfileTDSPLW.Form4Produced, dbo.ProfileTDSPLW.Form4Captured, dbo.ProfileTDSPLW.CollectionDate, dbo.ProfileTDSPLW.SocialWorker, 
                         dbo.ProfileTDSPLW.CCCCBSPCMember, dbo.ProfileTDSPLW.CreatedBy, dbo.ProfileTDSPLW.CreatedOn, dbo.ProfileTDSPLW.ApprovedBy, dbo.ProfileTDSPLW.ApprovedOn, dbo.ProfileTDSPLW.FilePath, 
                         dbo.AdminStructureView.KebeleName, dbo.AdminStructureView.WoredaID, dbo.AdminStructureView.WoredaName, dbo.AdminStructureView.RegionID, dbo.AdminStructureView.RegionName, dbo.ProfileTDSPLW.FiscalYear, 
                         dbo.ProfileTDSPLW.ChildProtectionRisk, dbo.ProfileTDSPLW.CBHIMembership
FROM            dbo.ProfileTDSPLW LEFT OUTER JOIN
                         dbo.ProfileTDSPLWDetail ON dbo.ProfileTDSPLWDetail.ProfileTDSPLWID = dbo.ProfileTDSPLW.ProfileTDSPLWID INNER JOIN
                         dbo.AdminStructureView ON dbo.ProfileTDSPLW.KebeleID = dbo.AdminStructureView.KebeleID
GO
/****** Object:  View [dbo].[ViewForm5BDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewForm5BDetails]
AS
SELECT        dbo.ViewForm1BDetails.ProfileTDSPLWID, dbo.ViewForm1BDetails.KebeleID, dbo.ViewForm1BDetails.Gote, dbo.ViewForm1BDetails.Gare, dbo.ViewForm1BDetails.NameOfPLW, dbo.ViewForm1BDetails.HouseHoldIDNumber, 
                         dbo.ViewForm1BDetails.MedicalRecordNumber, dbo.ViewForm1BDetails.PLWAge, dbo.ViewForm1BDetails.StartDateTDS, dbo.ViewForm1BDetails.EndDateTDS, dbo.ViewForm1BDetails.BabyDateOfBirth, 
                         dbo.ViewForm1BDetails.BabyName, dbo.ViewForm1BDetails.BabySex, dbo.ViewForm1BDetails.NutritionalStatusPLW, dbo.ViewForm1BDetails.NutritionalStatusInfant, dbo.ViewForm1BDetails.Form2Produced, 
                         dbo.ViewForm1BDetails.Form3Generated, dbo.ViewForm1BDetails.Form4Produced, dbo.ViewForm1BDetails.Form4Captured, dbo.ViewForm1BDetails.CollectionDate, dbo.ViewForm1BDetails.SocialWorker, 
                         dbo.ViewForm1BDetails.CCCCBSPCMember, dbo.ViewForm1BDetails.CreatedBy, dbo.ViewForm1BDetails.CreatedOn, dbo.ViewForm1BDetails.ApprovedBy, dbo.ViewForm1BDetails.ApprovedOn, dbo.ViewForm1BDetails.FilePath, 
                         dbo.ViewForm1BDetails.KebeleName, dbo.ViewForm1BDetails.WoredaID, dbo.ViewForm1BDetails.WoredaName, dbo.ViewForm1BDetails.RegionID, dbo.ViewForm1BDetails.RegionName, dbo.ViewForm1BDetails.FiscalYear, 
                         dbo.ViewForm1BDetails.ChildProtectionRisk, dbo.ViewForm1BDetails.CBHIMembership, CONVERT(VARCHAR(100), NotComplied.ComplianceID) AS ComplianceID, NotComplied.ServiceID, CONVERT(VARCHAR(100), 
                         NotComplied.ComplianceDtlID) AS ComplianceDtlID, NotComplied.Complied, NotComplied.Remarks, NotComplied.ReportingPeriodID, dbo.f_GetReportPeriod(NotComplied.ReportingPeriodID) AS ReportingPeriodName
FROM            dbo.ViewForm1BDetails INNER JOIN
                             (SELECT        CONVERT(VARCHAR(100), dbo.ComplianceSTDSPLWHeader.ComplianceID) AS ComplianceID, dbo.ComplianceSTDSPLWHeader.ServiceID, dbo.ComplianceSTDSPLWHeader.KebeleID, 
                                                         dbo.ComplianceSTDSPLWHeader.CapturedBy, CONVERT(VARCHAR(100), dbo.ComplianceSTDSPLWDetail.ComplianceDtlID) AS ComplianceDtlID, CONVERT(VARCHAR(100), 
                                                         dbo.ComplianceSTDSPLWDetail.ProfileTDSPLWID) AS ProfileTDSPLWID, dbo.ComplianceSTDSPLWDetail.Complied, dbo.ComplianceSTDSPLWDetail.Remarks, 
                                                         dbo.ComplianceSTDSPLWHeader.ReportingPeriodID
                               FROM            dbo.ComplianceSTDSPLWDetail INNER JOIN
                                                         dbo.ComplianceSTDSPLWHeader ON dbo.ComplianceSTDSPLWDetail.ComplianceID = dbo.ComplianceSTDSPLWHeader.ComplianceID AND ISNULL(dbo.ComplianceSTDSPLWDetail.Complied, 0) = 0) 
                         AS NotComplied ON dbo.ViewForm1BDetails.ProfileTDSPLWID = NotComplied.ProfileTDSPLWID AND dbo.ViewForm1BDetails.KebeleID = NotComplied.KebeleID
GO
/****** Object:  View [dbo].[ViewForm5BDetailsList]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewForm5BDetailsList]
AS
	SELECT DISTINCT ProfileTDSPLWID,HouseHoldIDNumber,NameOfPLW,BabyName,BabySex,BabyDateOfBirth FROM ViewForm5BDetails
GO
/****** Object:  View [dbo].[ViewForm1BPLW]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewForm1BPLW]
AS
SELECT        dbo.ProfileTDSPLW.ProfileTDSPLWID, dbo.ProfileTDSPLW.NameOfPLW, dbo.ProfileTDSPLW.HouseHoldIDNumber, dbo.ProfileTDSPLW.MedicalRecordNumber, dbo.ProfileTDSPLW.PLWAge, dbo.ProfileTDSPLW.StartDateTDS, 
                         dbo.ProfileTDSPLWDetail.BabyDateOfBirth, dbo.ProfileTDSPLWDetail.BabyName, dbo.ProfileTDSPLWDetail.BabySex, dbo.ProfileTDSPLW.EndDateTDS, dbo.ProfileTDSPLW.NutritionalStatusPLW, 
                         dbo.ProfileTDSPLWDetail.NutritionalStatusInfant, dbo.ProfileTDSPLW.KebeleID, dbo.AdminStructureView.KebeleName, dbo.ProfileTDSPLW.Gote, dbo.ProfileTDSPLW.Gare, dbo.AdminStructureView.WoredaID, 
                         dbo.AdminStructureView.WoredaName, dbo.AdminStructureView.RegionID, dbo.AdminStructureView.RegionName, dbo.ProfileTDSPLW.SocialWorker, dbo.ProfileTDSPLW.CCCCBSPCMember, dbo.ProfileTDSPLW.CollectionDate, 
                         dbo.ProfileTDSPLW.CreatedBy, dbo.ProfileTDSPLW.CreatedOn, dbo.f_GetKebeleTDSPLWMembers(dbo.ProfileTDSPLW.KebeleID) AS RecordCount, dbo.ProfileTDSPLW.CBHIMembership, dbo.ProfileTDSPLW.CBHINumber, 
                         dbo.ProfileTDSPLW.ChildProtectionRisk
FROM            dbo.ProfileTDSPLW LEFT OUTER JOIN
                         dbo.ProfileTDSPLWDetail ON dbo.ProfileTDSPLWDetail.ProfileTDSPLWID = dbo.ProfileTDSPLW.ProfileTDSPLWID INNER JOIN
                         dbo.AdminStructureView ON dbo.ProfileTDSPLW.KebeleID = dbo.AdminStructureView.KebeleID
GO
/****** Object:  View [dbo].[ViewForm1BPLWAdmin]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[ViewForm1BPLWAdmin]
AS
SELECT Distinct ProfileTDSPLW.KebeleID,KebeleName,
		AdminStructureView.WoredaID,AdminStructureView.WoredaName,AdminStructureView.RegionID,
		AdminStructureView.RegionName,
		dbo.f_GetKebeleTDSPLWMembers(ProfileTDSPLW.KebeleID) RecordCount
FROM ProfileTDSPLW
INNER JOIN AdminStructureView ON ProfileTDSPLW.KebeleID = AdminStructureView.KebeleID

GO
/****** Object:  View [dbo].[ViewForm1CTDSCMCAdmin]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[ViewForm1CTDSCMCAdmin]
AS
SELECT Distinct ProfileTDSCMC.KebeleID,KebeleName,
		AdminStructureView.WoredaID,AdminStructureView.WoredaName,AdminStructureView.RegionID,
		AdminStructureView.RegionName,
		dbo.f_GetKebeleTDSPLWMembers(ProfileTDSCMC.KebeleID) RecordCount
FROM ProfileTDSCMC
INNER JOIN AdminStructureView ON ProfileTDSCMC.KebeleID = AdminStructureView.KebeleID
GO
/****** Object:  View [dbo].[ViewForm2ADSHeader]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[ViewForm2ADSHeader]
AS
SELECT  CoResponsibilityDSHeader.CoRespDSHeaderID, ProfileDSHeader.ProfileDSHeaderID,ProfileDSHeader.KebeleID,KebeleName,
		AdminStructureView.WoredaID,AdminStructureView.WoredaName,AdminStructureView.RegionID,ApprovedBy,ApprovedOn,
		AdminStructureView.RegionName,ReportFileName,dbo.f_GetCoResponsibilityHouseholds(CoResponsibilityDSHeader.CoRespDSHeaderID) HouseholdCount
FROM CoResponsibilityDSHeader
INNER JOIN CoResponsibilityDSDetail ON CoResponsibilityDSDetail.CoRespDSHeaderID = CoResponsibilityDSHeader.CoRespDSHeaderID
INNER JOIN ProfileDSDetail ON CoResponsibilityDSDetail.ProfileDSDetailID = ProfileDSDetail.ProfileDSDetailID
INNER JOIN ProfileDSHeader ON ProfileDSDetail.ProfileDSHeaderID = ProfileDSHeader.ProfileDSHeaderID
INNER JOIN AdminStructureView ON ProfileDSHeader.KebeleID = AdminStructureView.KebeleID
GO
/****** Object:  View [dbo].[ViewForm2BTDSPLWHeader]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[ViewForm2BTDSPLWHeader]
AS
SELECT  CoResponsibilityTDSPLWHeader.CoRespTDSPLWHeaderID,
		ProfileTDSPLW.KebeleID,
		KebeleName,
		AdminStructureView.WoredaID,
		AdminStructureView.WoredaName,
		AdminStructureView.RegionID,
		AdminStructureView.RegionName,
		CoResponsibilityTDSPLWHeader.PLW,
		ReportFileName, 
		GeneratedBy,
		GeneratedOn,
		CoRespTDSPLWID,
		CoResponsibilityTDSPLW.ProfileTDSPLWID,
		TargetClientID,
		dbo.f_GetCoResponsibilityPLW(CoResponsibilityTDSPLWHeader.CoRespTDSPLWHeaderID) HouseholdCount,
		CoResponsibilityTDSPLWHeader.FiscalYear
FROM CoResponsibilityTDSPLWHeader
INNER JOIN CoResponsibilityTDSPLW ON CoResponsibilityTDSPLWHeader.CoRespTDSPLWHeaderID = CoResponsibilityTDSPLW.CoRespTDSPLWHeaderID
INNER JOIN ProfileTDSPLW ON CoResponsibilityTDSPLW.ProfileTDSPLWID = ProfileTDSPLW.ProfileTDSPLWID
INNER JOIN AdminStructureView ON ProfileTDSPLW.KebeleID = AdminStructureView.KebeleID
WHERE ISNULL(Form2Produced,0) = 1
GO
/****** Object:  View [dbo].[ViewForm3ADSHeader]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[ViewForm3ADSHeader]
AS
SELECT  CheckListDSID, CoResponsibilityDSDetail.CoRespDSHeaderID,ProfileDSHeader.KebeleID,KebeleName,
		WoredaID,WoredaName,RegionID,0 HouseholdCount,
		RegionName,CheckListDS.ReportFileName,CheckListDS.GeneratedBy,CheckListDS.GeneratedOn,CheckListDS.FiscalYear
FROM CheckListDS
INNER JOIN CoResponsibilityDSDetail ON CheckListDS.CoRespDSDetailID = CoResponsibilityDSDetail.CoRespDSDetailID
INNER JOIN ProfileDSDetail ON ProfileDSDetail.ProfileDSDetailID = CoResponsibilityDSDetail.ProfileDSDetailID
INNER JOIN ProfileDSHeader ON ProfileDSHeader.ProfileDSHeaderID = ProfileDSDetail.ProfileDSHeaderID
INNER JOIN AdminStructureView ON ProfileDSHeader.KebeleID = AdminStructureView.KebeleID
WHERE ISNULL(Form2Produced,0) = 1 AND ISNULL(Form3Generated,0) = 1
GO
/****** Object:  View [dbo].[ViewForm3BTDSPLWHeader]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewForm3BTDSPLWHeader]
AS
SELECT DISTINCT 
                         dbo.ProfileTDSPLW.KebeleID, dbo.ViewForm2BTDSPLWHeader.KebeleName, dbo.ViewForm2BTDSPLWHeader.WoredaID, dbo.ViewForm2BTDSPLWHeader.WoredaName, dbo.ViewForm2BTDSPLWHeader.RegionID, 
                         dbo.ViewForm2BTDSPLWHeader.HouseholdCount, dbo.ViewForm2BTDSPLWHeader.RegionName, dbo.CheckListTDSPLW.PLW, dbo.CheckListTDSPLW.ReportFileName, dbo.CheckListTDSPLW.GeneratedBy, 
                         dbo.CheckListTDSPLW.GeneratedOn, dbo.CheckListTDSPLW.FiscalYear, dbo.ProfileTDSPLW.ProfileTDSPLWID
FROM            dbo.CheckListTDSPLW INNER JOIN
                         dbo.CoResponsibilityTDSPLW ON dbo.CheckListTDSPLW.CoRespTDSPLWID = dbo.CoResponsibilityTDSPLW.CoRespTDSPLWID INNER JOIN
                         dbo.ProfileTDSPLW ON dbo.CoResponsibilityTDSPLW.ProfileTDSPLWID = dbo.ProfileTDSPLW.ProfileTDSPLWID INNER JOIN
                         dbo.ViewForm2BTDSPLWHeader ON dbo.ViewForm2BTDSPLWHeader.KebeleID = dbo.ProfileTDSPLW.KebeleID AND dbo.CheckListTDSPLW.PLW = dbo.ViewForm2BTDSPLWHeader.PLW
GO
/****** Object:  View [dbo].[DefaultKebeleView]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[DefaultKebeleView]
AS
	SELECT KebeleID FROM AdminStructureView
	WHERE WoredaID IN(SELECT WoredaID from DefaultWoreda)
GO
/****** Object:  View [dbo].[ViewForm1CDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewForm1CDetails]
AS
SELECT        CONVERT(VARCHAR(100), dbo.ProfileTDSCMC.ProfileTDSCMCID) AS ProfileTDSCMCID, dbo.ProfileTDSCMC.KebeleID, dbo.ProfileTDSCMC.Gote + '/' + dbo.ProfileTDSCMC.Gare AS Gote, dbo.ProfileTDSCMC.Gare, 
                         dbo.ProfileTDSCMC.NameOfCareTaker, dbo.ProfileTDSCMC.HouseHoldIDNumber, dbo.ProfileTDSCMC.CaretakerID, dbo.ProfileTDSCMC.MalnourishedChildName, dbo.ProfileTDSCMC.ChildID, 
                         dbo.ProfileTDSCMC.MalnourishedChildSex, dbo.ProfileTDSCMC.ChildDateOfBirth, dbo.ProfileTDSCMC.DateTypeCertificate, dbo.ProfileTDSCMC.MalnourishmentDegree, dbo.ProfileTDSCMC.StartDateTDS, 
                         dbo.ProfileTDSCMC.NextCNStatusDate, dbo.ProfileTDSCMC.EndDateTDS, dbo.ProfileTDSCMC.Form2Produced, dbo.ProfileTDSCMC.Form3Generated, dbo.ProfileTDSCMC.Form4Produced, dbo.ProfileTDSCMC.Form4Captured, 
                         dbo.ProfileTDSCMC.CollectionDate, dbo.ProfileTDSCMC.SocialWorker, dbo.ProfileTDSCMC.CCCCBSPCMember, dbo.ProfileTDSCMC.CreatedBy, dbo.ProfileTDSCMC.CreatedOn, dbo.ProfileTDSCMC.ApprovedBy, 
                         dbo.ProfileTDSCMC.ApprovedOn, dbo.ProfileTDSCMC.FilePath, dbo.AdminStructureView.KebeleName, dbo.AdminStructureView.WoredaID, dbo.AdminStructureView.WoredaName, dbo.AdminStructureView.RegionID, 
                         dbo.AdminStructureView.RegionName, '' AS ChildCheckup, '' AS GMP, '' AS BCC, dbo.ProfileTDSCMC.FiscalYear, dbo.ProfileTDSCMC.CBHIMembership, dbo.ProfileTDSCMC.CBHINumber, 
                         dbo.ProfileTDSCMC.ChildProtectionRisk
FROM            dbo.ProfileTDSCMC INNER JOIN
                         dbo.AdminStructureView ON dbo.ProfileTDSCMC.KebeleID = dbo.AdminStructureView.KebeleID
GO
/****** Object:  View [dbo].[ViewForm5CDetailsNew]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewForm5CDetailsNew]
AS
SELECT        ProfileTDSCMCID, KebeleID, Gote, Gare, NameOfCareTaker, HouseHoldIDNumber, CaretakerID, MalnourishedChildName, ChildID, MalnourishedChildSex, ChildDateOfBirth, DateTypeCertificate, MalnourishmentDegree, 
                         StartDateTDS, NextCNStatusDate, EndDateTDS, CollectionDate, SocialWorker, CCCCBSPCMember, WoredaID, WoredaName, RegionID, RegionName, ChildCheckup, GMP, BCC, FiscalYear, CBHIMembership, CBHINumber, 
                         ChildProtectionRisk, KebeleName
FROM            dbo.ViewForm1CDetails
GO
/****** Object:  View [dbo].[ViewDashboardComplianceDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewDashboardComplianceDetails]
AS
	select ComplianceSDSHeader.FiscalYear, ReportingIndicatorId, ComplianceSDSHeader.ServiceID, Sex, RegionID, WoredaID, ComplianceSDSHeader.KebeleID, ReportingPeriodID as ReportingPeriod, ComplianceSDSDetail.ProfileDSDetailID as ProfileID, Complied, 'Form1A' as Form
	from ComplianceSDSHeader
	inner join ComplianceSDSDetail on ( ComplianceSDSHeader.ComplianceID = ComplianceSDSDetail.ComplianceID )
	inner join ProfileDSDetail on ( ProfileDSDetail.ProfileDSDetailID = ComplianceSDSDetail.ProfileDSDetailID )
	inner join ReportingIndicatorService on ( ComplianceSDSHeader.ServiceID = ReportingIndicatorService.ServiceID )
	inner join AdminStructureView on ( ComplianceSDSHeader.KebeleID = AdminStructureView.KebeleID )
	inner join ReportingPeriod on ( ReportingPeriod.PeriodID = ComplianceSDSHeader.ReportingPeriodID )
	union
	select ComplianceSTDSPLWHeader.FiscalYear, ReportingIndicatorId, ComplianceSTDSPLWHeader.ServiceID, null as Sex, RegionID, WoredaID, ComplianceSTDSPLWHeader.KebeleID, ReportingPeriodID as ReportingPeriod, ComplianceSTDSPLWDetail.ProfileTDSPLWID as ProfileID, Complied, 'Form1B' as Form
	from ComplianceSTDSPLWHeader
	inner join ComplianceSTDSPLWDetail on ( ComplianceSTDSPLWHeader.ComplianceID = ComplianceSTDSPLWDetail.ComplianceID )
	inner join ProfileTDSPLW on ( ProfileTDSPLW.ProfileTDSPLWID = ComplianceSTDSPLWDetail.ProfileTDSPLWID )
	inner join ReportingIndicatorService on ( ComplianceSTDSPLWHeader.ServiceID = ReportingIndicatorService.ServiceID )
	inner join AdminStructureView on ( ComplianceSTDSPLWHeader.KebeleID = AdminStructureView.KebeleID )
	inner join ReportingPeriod on ( ReportingPeriod.PeriodID = ComplianceSTDSPLWHeader.ReportingPeriodID )
	union
	select ComplianceSTDSCMCHeader.FiscalYear, ReportingIndicatorId, ComplianceSTDSCMCHeader.ServiceID, null as Sex, RegionID, WoredaID, ComplianceSTDSCMCHeader.KebeleID, ReportingPeriodID as ReportingPeriod, ComplianceSTDSCMCDetail.ProfileTDSCMCID as ProfileID, Complied, 'Form1C' as Form
	from ComplianceSTDSCMCHeader
	inner join ComplianceSTDSCMCDetail on ( ComplianceSTDSCMCHeader.ComplianceID = ComplianceSTDSCMCDetail.ComplianceID )
	inner join ProfileTDSCMC on ( ProfileTDSCMC.ProfileTDSCMCID = ComplianceSTDSCMCDetail.ProfileTDSCMCID )
	inner join ReportingIndicatorService on ( ComplianceSTDSCMCHeader.ServiceID = ReportingIndicatorService.ServiceID )
	inner join AdminStructureView on ( ComplianceSTDSCMCHeader.KebeleID = AdminStructureView.KebeleID )
	inner join ReportingPeriod on ( ReportingPeriod.PeriodID = ComplianceSTDSCMCHeader.ReportingPeriodID )
GO
/****** Object:  View [dbo].[ViewGeneratedComplianceTDSPLWDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewGeneratedComplianceTDSPLWDetails]
AS
SELECT        dbo.GenerateCSPTDSPLW.KebeleID, dbo.ViewComplianceTDSPLWDetails.NameOfPLW, dbo.ViewComplianceTDSPLWDetails.HouseHoldIDNumber, dbo.ViewComplianceTDSPLWDetails.Gote, 
                         dbo.ViewComplianceTDSPLWDetails.Gare, dbo.GenerateCSPTDSPLW.ProfileTDSPLWID, dbo.ViewComplianceTDSPLWDetails.MedicalRecordNumber, dbo.ViewComplianceTDSPLWDetails.PLWAge, 
                         dbo.ViewComplianceTDSPLWDetails.BabyName, dbo.ViewComplianceTDSPLWDetails.BabySex, dbo.GenerateCSPTDSPLW.ServiceID, dbo.GenerateCSPTDSPLW.Complied, dbo.GenerateCSPTDSPLW.Remarks, 
                         dbo.GenerateCSPTDSPLW.ReportingPeriodID, dbo.f_GetUserNameByUserID(dbo.GenerateCSPTDSPLW.GeneratedBy) AS CapturedBy, CONVERT(VARCHAR(11), dbo.GenerateCSPTDSPLW.GeneratedOn, 106) AS CapturedOn, 
                         dbo.IntegratedServices.ServiceName, dbo.ProfileTDSPLW.FiscalYear, dbo.ProfileTDSPLW.CollectionDate, dbo.ProfileTDSPLW.SocialWorker
FROM            dbo.ViewComplianceTDSPLWDetails INNER JOIN
                         dbo.GenerateCSPTDSPLW ON dbo.ViewComplianceTDSPLWDetails.KebeleID = dbo.GenerateCSPTDSPLW.KebeleID AND dbo.GenerateCSPTDSPLW.ServiceID = dbo.ViewComplianceTDSPLWDetails.ServiceID AND 
                         dbo.GenerateCSPTDSPLW.ProfileTDSPLWID = dbo.ViewComplianceTDSPLWDetails.ProfileTDSPLWID INNER JOIN
                         dbo.IntegratedServices ON dbo.GenerateCSPTDSPLW.ServiceID = dbo.IntegratedServices.ServiceID INNER JOIN
                         dbo.ProfileTDSPLW ON dbo.GenerateCSPTDSPLW.ProfileTDSPLWID = dbo.ProfileTDSPLW.ProfileTDSPLWID
GO
/****** Object:  View [dbo].[ViewComplianceTDSCMCDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ViewComplianceTDSCMCDetails]
AS
SELECT ProfileTDSCMC.KebeleID,ProfileTDSCMC.NameOfCareTaker,ProfileTDSCMC.HouseHoldIDNumber,
ProfileTDSCMC.MalnourishedChildName,Gote,Gare,ComplianceSTDSCMCDetail.ProfileTDSCMCID,
MalnourishedChildSex,GenerateCSPTDSCMC.ServiceID,GenerateCSPTDSCMC.Complied,GenerateCSPTDSCMC.Remarks,MalnourishmentDegree,
ChildDateOfBirth,GenerateCSPTDSCMC.ReportingPeriodID,
dbo.f_GetUserNameByUserID(ComplianceSTDSCMCHeader.CapturedBy) CapturedBy,
CONVERT(VARCHAR(11), ComplianceSTDSCMCHeader.CapturedOn, 106) CapturedOn
FROM ProfileTDSCMC
INNER JOIN ComplianceSTDSCMCHeader ON ComplianceSTDSCMCHeader.KebeleID = ProfileTDSCMC.KebeleID
INNER JOIN ComplianceSTDSCMCDetail ON ComplianceSTDSCMCDetail.ComplianceID = ComplianceSTDSCMCHeader.ComplianceID
AND ComplianceSTDSCMCDetail.ProfileTDSCMCID = ProfileTDSCMC.ProfileTDSCMCID
INNER JOIN GenerateCSPTDSCMC ON GenerateCSPTDSCMC.KebeleID = ComplianceSTDSCMCHeader.KebeleID
AND GenerateCSPTDSCMC.ProfileTDSCMCID = ComplianceSTDSCMCDetail.ProfileTDSCMCID
AND GenerateCSPTDSCMC.ServiceID = ComplianceSTDSCMCHeader.ServiceID
AND GenerateCSPTDSCMC.ReportingPeriodID = ComplianceSTDSCMCHeader.ReportingPeriodID
GO
/****** Object:  View [dbo].[ViewGeneratedComplianceTDSCMCDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewGeneratedComplianceTDSCMCDetails]
AS
SELECT        dbo.GenerateCSPTDSCMC.KebeleID, dbo.ViewComplianceTDSCMCDetails.NameOfCareTaker, dbo.ViewComplianceTDSCMCDetails.HouseHoldIDNumber, dbo.ViewComplianceTDSCMCDetails.Gote, 
                         dbo.ViewComplianceTDSCMCDetails.Gare, dbo.GenerateCSPTDSCMC.ProfileTDSCMCID, dbo.ViewComplianceTDSCMCDetails.MalnourishedChildName, dbo.ViewComplianceTDSCMCDetails.MalnourishedChildSex, 
                         dbo.GenerateCSPTDSCMC.ServiceID, dbo.GenerateCSPTDSCMC.Complied, dbo.GenerateCSPTDSCMC.Remarks, dbo.GenerateCSPTDSCMC.ReportingPeriodID, 
                         dbo.f_GetUserNameByUserID(dbo.GenerateCSPTDSCMC.GeneratedBy) AS CapturedBy, CONVERT(VARCHAR(11), dbo.GenerateCSPTDSCMC.GeneratedOn, 106) AS CapturedOn, dbo.ProfileTDSCMC.FiscalYear, 
                         dbo.ProfileTDSCMC.CaretakerID, dbo.IntegratedServices.ServiceName, dbo.ProfileTDSCMC.CollectionDate, dbo.ProfileTDSCMC.SocialWorker
FROM            dbo.ViewComplianceTDSCMCDetails INNER JOIN
                         dbo.GenerateCSPTDSCMC ON dbo.ViewComplianceTDSCMCDetails.KebeleID = dbo.GenerateCSPTDSCMC.KebeleID AND dbo.GenerateCSPTDSCMC.ServiceID = dbo.ViewComplianceTDSCMCDetails.ServiceID AND 
                         dbo.GenerateCSPTDSCMC.ProfileTDSCMCID = dbo.ViewComplianceTDSCMCDetails.ProfileTDSCMCID INNER JOIN
                         dbo.ProfileTDSCMC ON dbo.GenerateCSPTDSCMC.ProfileTDSCMCID = dbo.ProfileTDSCMC.ProfileTDSCMCID INNER JOIN
                         dbo.IntegratedServices ON dbo.GenerateCSPTDSCMC.ServiceID = dbo.IntegratedServices.ServiceID
GO
/****** Object:  View [dbo].[ViewNonComplianceTDSCMCDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewNonComplianceTDSCMCDetails]
AS
SELECT        dbo.ProfileTDSCMC.KebeleID, dbo.ProfileTDSCMC.NameOfCareTaker, dbo.ProfileTDSCMC.HouseHoldIDNumber, dbo.ProfileTDSCMC.MalnourishedChildName, dbo.ProfileTDSCMC.Gote, dbo.ProfileTDSCMC.Gare, 
                         dbo.NonComplianceTDSCMCDetail.ProfileTDSCMCID, dbo.ProfileTDSCMC.MalnourishedChildSex, dbo.NonComplianceTDSCMCDetail.NonComplianceReason, dbo.NonComplianceTDSCMCDetail.ActionTaken, 
                         dbo.NonComplianceTDSCMCHeader.SocialWorker, dbo.NonComplianceTDSCMCHeader.ReportingPeriod, dbo.NonComplianceTDSCMCHeader.CompletedDate, dbo.ViewComplianceTDSCMCDetails.ServiceID, 
                         dbo.ProfileTDSCMC.CollectionDate, dbo.ProfileTDSCMC.FiscalYear, dbo.ProfileTDSCMC.CaretakerID
FROM            dbo.ProfileTDSCMC INNER JOIN
                         dbo.NonComplianceTDSCMCHeader ON dbo.NonComplianceTDSCMCHeader.KebeleID = dbo.ProfileTDSCMC.KebeleID INNER JOIN
                         dbo.NonComplianceTDSCMCDetail ON dbo.NonComplianceTDSCMCDetail.NonComplianceID = dbo.NonComplianceTDSCMCHeader.NonComplianceID AND 
                         dbo.NonComplianceTDSCMCDetail.ProfileTDSCMCID = dbo.ProfileTDSCMC.ProfileTDSCMCID INNER JOIN
                         dbo.ViewComplianceTDSCMCDetails ON dbo.ViewComplianceTDSCMCDetails.KebeleID = dbo.ProfileTDSCMC.KebeleID AND dbo.ViewComplianceTDSCMCDetails.ProfileTDSCMCID = dbo.ProfileTDSCMC.ProfileTDSCMCID AND 
                         dbo.ViewComplianceTDSCMCDetails.Remarks = dbo.NonComplianceTDSCMCDetail.NonComplianceReason
GO
/****** Object:  View [dbo].[ViewGeneratedNonComplianceTDSCMCDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewGeneratedNonComplianceTDSCMCDetails]
AS
SELECT        dbo.GenerateNonCCTDSCMC.KebeleID, dbo.ViewNonComplianceTDSCMCDetails.NameOfCareTaker, dbo.ViewNonComplianceTDSCMCDetails.HouseHoldIDNumber, dbo.ViewNonComplianceTDSCMCDetails.Gote, 
                         dbo.ViewNonComplianceTDSCMCDetails.Gare, dbo.GenerateNonCCTDSCMC.ProfileTDSCMCID, dbo.ViewNonComplianceTDSCMCDetails.MalnourishedChildName, dbo.ViewNonComplianceTDSCMCDetails.MalnourishedChildSex,
                          dbo.ViewNonComplianceTDSCMCDetails.NonComplianceReason, dbo.ViewNonComplianceTDSCMCDetails.ActionTaken, dbo.ViewNonComplianceTDSCMCDetails.SocialWorker, dbo.GenerateNonCCTDSCMC.ReportingPeriod, 
                         dbo.ViewNonComplianceTDSCMCDetails.CompletedDate, dbo.ViewNonComplianceTDSCMCDetails.CollectionDate, dbo.ViewNonComplianceTDSCMCDetails.FiscalYear, dbo.ViewNonComplianceTDSCMCDetails.ServiceID, 
                         dbo.ViewNonComplianceTDSCMCDetails.CaretakerID
FROM            dbo.ViewNonComplianceTDSCMCDetails INNER JOIN
                         dbo.GenerateNonCCTDSCMC ON dbo.ViewNonComplianceTDSCMCDetails.KebeleID = dbo.GenerateNonCCTDSCMC.KebeleID AND 
                         dbo.GenerateNonCCTDSCMC.ReportingPeriod = dbo.ViewNonComplianceTDSCMCDetails.ReportingPeriod AND dbo.GenerateNonCCTDSCMC.ProfileTDSCMCID = dbo.ViewNonComplianceTDSCMCDetails.ProfileTDSCMCID
GO
/****** Object:  View [dbo].[ViewCoResponsibilityDS]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewCoResponsibilityDS]
AS
Select CoResponsibilityDSHeader.CoRespDSHeaderID,
		GeneratedBy,GeneratedOn,
		ReportFileName,
		CoRespDSDetailID,
		ProfileDSHeader.ProfileDSHeaderID,
		CoResponsibilityDSDetail.ProfileDSDetailID,
		TargetClientID,
		ProfileDSHeader.KebeleID,
		KebeleName,
		AdminStructureView.WoredaID,
		AdminStructureView.WoredaName,
		AdminStructureView.RegionID,
		ApprovedBy,
		ApprovedOn,
		AdminStructureView.RegionName,
		CoResponsibilityDSHeader.FiscalYear
FROM CoResponsibilityDSHeader
INNER JOIN CoResponsibilityDSDetail ON CoResponsibilityDSDetail.CoRespDSHeaderID = CoResponsibilityDSHeader.CoRespDSHeaderID
INNER JOIN ProfileDSDetail ON ProfileDSDetail.ProfileDSDetailID = CoResponsibilityDSDetail.ProfileDSDetailID
INNER JOIN ProfileDSHeader ON ProfileDSHeader.ProfileDSHeaderID = ProfileDSDetail.ProfileDSHeaderID
INNER JOIN AdminStructureView ON ProfileDSHeader.KebeleID = AdminStructureView.KebeleID
WHERE ISNULL(Form2Produced,0) = 1

GO
/****** Object:  View [dbo].[ViewForm2CTDSCMCHeader]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[ViewForm2CTDSCMCHeader]
AS
SELECT CoResponsibilityTDSCMCHeader.CoRespTDSCMCWHeaderID,
		ProfileTDSCMC.KebeleID,
		KebeleName,
		AdminStructureView.WoredaID,
		AdminStructureView.WoredaName,
		AdminStructureView.RegionID,
		AdminStructureView.RegionName,
		ReportFileName,
		GeneratedBy,
		GeneratedOn,
		CoRespTDSCMCID,
		CoResponsibilityTDSCMC.ProfileTDSCMCID,
		TargetClientID,
		dbo.f_GetCoResponsibilityCMC(CoResponsibilityTDSCMCHeader.CoRespTDSCMCWHeaderID) HouseholdCount,
		CoResponsibilityTDSCMCHeader.FiscalYear
FROM CoResponsibilityTDSCMCHeader
INNER JOIN CoResponsibilityTDSCMC ON CoResponsibilityTDSCMCHeader.CoRespTDSCMCWHeaderID = CoResponsibilityTDSCMC.CoRespTDSCMCWHeaderID
INNER JOIN ProfileTDSCMC ON CoResponsibilityTDSCMC.ProfileTDSCMCID = ProfileTDSCMC.ProfileTDSCMCID
INNER JOIN AdminStructureView ON ProfileTDSCMC.KebeleID = AdminStructureView.KebeleID
WHERE ISNULL(Form2Produced,0) = 1
GO
/****** Object:  View [dbo].[ViewForm3CTDSCMCHeader]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewForm3CTDSCMCHeader]
AS
SELECT DISTINCT 
                         dbo.ProfileTDSCMC.KebeleID, dbo.ViewForm2CTDSCMCHeader.KebeleName, dbo.ViewForm2CTDSCMCHeader.WoredaID, dbo.ViewForm2CTDSCMCHeader.WoredaName, dbo.ViewForm2CTDSCMCHeader.RegionID, 
                         dbo.ViewForm2CTDSCMCHeader.HouseholdCount, dbo.ViewForm2CTDSCMCHeader.RegionName, dbo.CheckListTDSCMC.ReportFileName, dbo.CheckListTDSCMC.GeneratedBy, dbo.CheckListTDSCMC.GeneratedOn, 
                         dbo.CheckListTDSCMC.FiscalYear, dbo.ProfileTDSCMC.ProfileTDSCMCID
FROM            dbo.CheckListTDSCMC INNER JOIN
                         dbo.CoResponsibilityTDSCMC ON dbo.CheckListTDSCMC.CoRespTDSCMCID = dbo.CoResponsibilityTDSCMC.CoRespTDSCMCID INNER JOIN
                         dbo.ProfileTDSCMC ON dbo.CoResponsibilityTDSCMC.ProfileTDSCMCID = dbo.ProfileTDSCMC.ProfileTDSCMCID INNER JOIN
                         dbo.ViewForm2CTDSCMCHeader ON dbo.ViewForm2CTDSCMCHeader.KebeleID = dbo.ProfileTDSCMC.KebeleID
GO
/****** Object:  View [dbo].[ViewForm1ADetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewForm1ADetails]
AS
SELECT        CONVERT(VARCHAR(100), dbo.ProfileDSHeader.ProfileDSHeaderID) AS ProfileDSHeaderID, dbo.ProfileDSHeader.KebeleID, dbo.ProfileDSHeader.Gote, dbo.ProfileDSHeader.Gare, dbo.ProfileDSHeader.NameOfHouseHoldHead, 
                         dbo.ProfileDSHeader.HouseHoldIDNumber, dbo.ProfileDSHeader.CollectionDate, dbo.ProfileDSHeader.SocialWorker, dbo.ProfileDSHeader.CCCCBSPCMember, dbo.ProfileDSHeader.CreatedBy, dbo.ProfileDSHeader.CreatedOn, 
                         dbo.ProfileDSHeader.ApprovedBy, dbo.ProfileDSHeader.ApprovedOn, dbo.ProfileDSHeader.ReportFilePath, dbo.AdminStructureView.KebeleName, dbo.AdminStructureView.WoredaID, dbo.AdminStructureView.WoredaName, 
                         dbo.AdminStructureView.RegionID, dbo.AdminStructureView.RegionName, CONVERT(VARCHAR(100), dbo.ProfileDSDetail.ProfileDSDetailID) AS ProfileDSDetailID, dbo.ProfileDSDetail.HouseHoldMemberName, 
                         dbo.ProfileDSDetail.IndividualID, dbo.ProfileDSDetail.MedicalRecordNumber, dbo.ProfileDSDetail.DateOfBirth, dbo.f_getYearMonth_Form1A(dbo.ProfileDSDetail.DateOfBirth, GETDATE()) AS Age, dbo.ProfileDSDetail.Sex, 
                         dbo.ProfileDSDetail.PWL, dbo.ProfileDSDetail.Handicapped, dbo.ProfileDSDetail.ChronicallyIll, dbo.ProfileDSDetail.NutritionalStatus, dbo.ProfileDSDetail.EnrolledInSchool, dbo.ProfileDSDetail.SchoolName, 
                         dbo.ProfileDSDetail.Form2Produced, dbo.ProfileDSDetail.Form3Generated, dbo.ProfileDSDetail.Form4Produced, dbo.ProfileDSDetail.Form4Captured, dbo.ProfileDSDetail.Pregnant, dbo.ProfileDSDetail.Lactating, 
                         dbo.ProfileDSDetail.childUnderTSForCMAM, dbo.ProfileDSHeader.FiscalYear, dbo.ProfileDSDetail.ChildProtectionRisk, dbo.ProfileDSHeader.CBHIMembership
FROM            dbo.ProfileDSHeader INNER JOIN
                         dbo.AdminStructureView ON dbo.ProfileDSHeader.KebeleID = dbo.AdminStructureView.KebeleID INNER JOIN
                         dbo.ProfileDSDetail ON dbo.ProfileDSHeader.ProfileDSHeaderID = dbo.ProfileDSDetail.ProfileDSHeaderID
GO
/****** Object:  View [dbo].[ViewForm5A1Details]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewForm5A1Details]
AS
SELECT        dbo.ViewForm1ADetails.KebeleID, dbo.ViewForm1ADetails.RegionName, dbo.ViewForm1ADetails.WoredaName, dbo.ViewForm1ADetails.KebeleName, CONVERT(VARCHAR(100), dbo.ViewForm1ADetails.ProfileDSHeaderID) 
                         AS ProfileDSHeaderID, dbo.ViewForm1ADetails.ProfileDSDetailID, dbo.ViewForm1ADetails.HouseHoldMemberName, dbo.ViewForm1ADetails.NameOfHouseHoldHead, 
                         dbo.ViewForm1ADetails.Gote + '/' + dbo.ViewForm1ADetails.Gare AS Gote, dbo.ViewForm1ADetails.HouseHoldIDNumber, NotComplied.ComplianceID, NotComplied.ServiceID, NotComplied.ComplianceDtlID, 
                         NotComplied.Complied, NotComplied.Remarks, dbo.ViewForm1ADetails.IndividualID, dbo.ViewForm1ADetails.Pregnant, dbo.ViewForm1ADetails.Lactating, dbo.ViewForm1ADetails.DateOfBirth, DATEDIFF(YEAR, 
                         dbo.ViewForm1ADetails.DateOfBirth, GETDATE()) AS Age, dbo.ViewForm1ADetails.Sex, 'No' AS EnrolledInSchool, dbo.ViewForm1ADetails.SchoolName, dbo.ViewForm1ADetails.ChildProtectionRisk, 
                         dbo.ViewForm1ADetails.CBHIMembership
FROM            dbo.ViewForm1ADetails INNER JOIN
                             (SELECT        dbo.ComplianceSDSHeader.ComplianceID, dbo.ComplianceSDSHeader.ServiceID, dbo.ComplianceSDSHeader.KebeleID, dbo.ComplianceSDSHeader.GeneratedBy, dbo.ComplianceSDSDetail.ComplianceDtlID, 
                                                         dbo.ComplianceSDSDetail.ProfileDSDetailID, dbo.ComplianceSDSDetail.Complied, dbo.ComplianceSDSDetail.Remarks
                               FROM            dbo.ComplianceSDSDetail INNER JOIN
                                                         dbo.ComplianceSDSHeader ON dbo.ComplianceSDSDetail.ComplianceID = dbo.ComplianceSDSHeader.ComplianceID AND dbo.ComplianceSDSHeader.ServiceID IN (5, 12, 13) AND 
                                                         ISNULL(dbo.ComplianceSDSDetail.Complied, 0) = 0) AS NotComplied ON dbo.ViewForm1ADetails.ProfileDSDetailID = NotComplied.ProfileDSDetailID AND 
                         dbo.ViewForm1ADetails.KebeleID = NotComplied.KebeleID
WHERE        (DATEDIFF(YEAR, dbo.ViewForm1ADetails.DateOfBirth, GETDATE()) BETWEEN 6 AND 18)
GO
/****** Object:  View [dbo].[ViewForm5A2Details]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [dbo].[ViewForm5A2Details]
AS
	SELECT 
		ViewForm1ADetails.*,
		CONVERT(VARCHAR(100), ComplianceID) AS ComplianceID,
		ServiceID,
		CONVERT(VARCHAR(100), ComplianceDtlID) AS ComplianceDtlID,
		Complied,Remarks,
		ReportingPeriodID,
		ReportingPeriodName 
	FROM ViewForm1ADetails
	INNER JOIN (
		Select CONVERT(VARCHAR(100), ComplianceSDSHeader.ComplianceID) AS ComplianceID,ServiceID,KebeleID,GeneratedBy, CONVERT(VARCHAR(100), ComplianceDtlID) AS ComplianceDtlID,ReportingPeriodID,
		dbo.f_GetReportPeriod(ReportingPeriodID) ReportingPeriodName,
		CONVERT(VARCHAR(100), ProfileDSDetailID) AS ProfileDSDetailID,Complied,Remarks 
		FROM ComplianceSDSDetail
		INNER JOIN ComplianceSDSHeader ON ComplianceSDSDetail.ComplianceID = ComplianceSDSHeader.ComplianceID
		AND ServiceID <> 5 AND isnull(Complied,0) = 0
	) NotComplied ON ViewForm1ADetails.ProfileDSDetailID = NotComplied.ProfileDSDetailID
	AND ViewForm1ADetails.KebeleID = NotComplied.KebeleID
	WHERE NotComplied.ProfileDSDetailID NOT IN(Select ProfileDSDetailID FROM ViewForm1ADetails WHERE DATEDIFF(YEAR,DateOfBirth,GETDATE()) BETWEEN 6 AND 18)

GO
/****** Object:  View [dbo].[ViewForm5CDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewForm5CDetails]
AS
SELECT        dbo.ViewForm1CDetails.ProfileTDSCMCID, dbo.ViewForm1CDetails.KebeleID, dbo.ViewForm1CDetails.Gote, dbo.ViewForm1CDetails.Gare, dbo.ViewForm1CDetails.NameOfCareTaker, 
                         dbo.ViewForm1CDetails.HouseHoldIDNumber, dbo.ViewForm1CDetails.CaretakerID, dbo.ViewForm1CDetails.MalnourishedChildName, dbo.ViewForm1CDetails.ChildID, dbo.ViewForm1CDetails.MalnourishedChildSex, 
                         dbo.ViewForm1CDetails.ChildDateOfBirth, dbo.ViewForm1CDetails.DateTypeCertificate, dbo.ViewForm1CDetails.MalnourishmentDegree, dbo.ViewForm1CDetails.StartDateTDS, dbo.ViewForm1CDetails.NextCNStatusDate, 
                         dbo.ViewForm1CDetails.EndDateTDS, dbo.ViewForm1CDetails.Form2Produced, dbo.ViewForm1CDetails.Form3Generated, dbo.ViewForm1CDetails.Form4Produced, dbo.ViewForm1CDetails.Form4Captured, 
                         dbo.ViewForm1CDetails.CollectionDate, dbo.ViewForm1CDetails.SocialWorker, dbo.ViewForm1CDetails.CCCCBSPCMember, dbo.ViewForm1CDetails.CreatedBy, dbo.ViewForm1CDetails.CreatedOn, 
                         dbo.ViewForm1CDetails.ApprovedBy, dbo.ViewForm1CDetails.ApprovedOn, dbo.ViewForm1CDetails.FilePath, dbo.ViewForm1CDetails.KebeleName, dbo.ViewForm1CDetails.WoredaID, dbo.ViewForm1CDetails.WoredaName, 
                         dbo.ViewForm1CDetails.RegionID, dbo.ViewForm1CDetails.RegionName, dbo.ViewForm1CDetails.ChildCheckup, dbo.ViewForm1CDetails.GMP, dbo.ViewForm1CDetails.BCC, dbo.ViewForm1CDetails.FiscalYear, 
                         dbo.ViewForm1CDetails.CBHIMembership, dbo.ViewForm1CDetails.CBHINumber, dbo.ViewForm1CDetails.ChildProtectionRisk, CONVERT(VARCHAR(100), NotComplied.ComplianceID) AS ComplianceID, NotComplied.ServiceID, 
                         CONVERT(VARCHAR(100), NotComplied.ComplianceDtlID) AS ComplianceDtlID, NotComplied.Complied, NotComplied.Remarks, NotComplied.ReportingPeriodID, dbo.f_GetReportPeriod(NotComplied.ReportingPeriodID) 
                         AS ReportingPeriodName
FROM            dbo.ViewForm1CDetails INNER JOIN
                             (SELECT        CONVERT(VARCHAR(100), dbo.ComplianceSTDSCMCHeader.ComplianceID) AS ComplianceID, dbo.ComplianceSTDSCMCHeader.ServiceID, dbo.ComplianceSTDSCMCHeader.KebeleID, 
                                                         dbo.ComplianceSTDSCMCHeader.CapturedBy, CONVERT(VARCHAR(100), dbo.ComplianceSTDSCMCDetail.ComplianceDtlID) AS ComplianceDtlID, CONVERT(VARCHAR(100), 
                                                         dbo.ComplianceSTDSCMCDetail.ProfileTDSCMCID) AS ProfileTDSCMCID, dbo.ComplianceSTDSCMCDetail.Complied, dbo.ComplianceSTDSCMCDetail.Remarks, 
                                                         dbo.ComplianceSTDSCMCHeader.ReportingPeriodID
                               FROM            dbo.ComplianceSTDSCMCDetail INNER JOIN
                                                         dbo.ComplianceSTDSCMCHeader ON dbo.ComplianceSTDSCMCDetail.ComplianceID = dbo.ComplianceSTDSCMCHeader.ComplianceID AND ISNULL(dbo.ComplianceSTDSCMCDetail.Complied, 0) = 0) 
                         AS NotComplied ON dbo.ViewForm1CDetails.ProfileTDSCMCID = NotComplied.ProfileTDSCMCID AND dbo.ViewForm1CDetails.KebeleID = NotComplied.KebeleID
GO
/****** Object:  View [dbo].[ViewGeneratedNonComplianceDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewGeneratedNonComplianceDetails]
AS
SELECT DISTINCT 
                         dbo.ProfileDSHeader.KebeleID, dbo.ProfileDSHeader.NameOfHouseHoldHead, dbo.ProfileDSHeader.HouseHoldIDNumber, dbo.ProfileDSHeader.Gote, dbo.ProfileDSHeader.Gare, dbo.NonComplianceDSDetail.ProfileDSDetailID, 
                         dbo.ProfileDSDetail.HouseHoldMemberName, dbo.ProfileDSDetail.IndividualID, dbo.ProfileDSDetail.MedicalRecordNumber, dbo.ProfileDSDetail.Age, dbo.ProfileDSDetail.Sex, dbo.ProfileDSDetail.PWL, 
                         dbo.NonComplianceDSDetail.NonComplianceReason, dbo.NonComplianceDSDetail.ActionTaken, dbo.NonComplianceDSHeader.SocialWorker, dbo.NonComplianceDSHeader.ReportingPeriod, 
                         dbo.NonComplianceDSHeader.CompletedDate, dbo.NonComplianceDSHeader.TypeOfForm, TheComplianceDSDetails.ServiceID, dbo.ProfileDSHeader.FiscalYear, dbo.ProfileDSHeader.CollectionDate, 
                         dbo.ProfileDSHeader.SocialWorker AS Expr1
FROM            dbo.ProfileDSHeader INNER JOIN
                         dbo.ProfileDSDetail ON dbo.ProfileDSHeader.ProfileDSHeaderID = dbo.ProfileDSDetail.ProfileDSHeaderID INNER JOIN
                         dbo.NonComplianceDSHeader ON dbo.NonComplianceDSHeader.KebeleID = dbo.ProfileDSHeader.KebeleID INNER JOIN
                         dbo.NonComplianceDSDetail ON dbo.NonComplianceDSDetail.ProfileDSDetailID = dbo.ProfileDSDetail.ProfileDSDetailID AND 
                         dbo.NonComplianceDSHeader.NonComplianceID = dbo.NonComplianceDSDetail.NonComplianceID INNER JOIN
                         dbo.GenerateNonCCDS ON dbo.NonComplianceDSHeader.KebeleID = dbo.GenerateNonCCDS.KebeleID AND dbo.GenerateNonCCDS.ReportingPeriod = dbo.NonComplianceDSHeader.ReportingPeriod AND 
                         dbo.GenerateNonCCDS.ProfileDSDetailID = dbo.ProfileDSDetail.ProfileDSDetailID INNER JOIN
                             (SELECT        KebeleID, ProfileDSDetailID, ServiceID
                               FROM            dbo.ViewComplianceDSDetails) AS TheComplianceDSDetails ON TheComplianceDSDetails.KebeleID = dbo.ProfileDSHeader.KebeleID AND 
                         TheComplianceDSDetails.ProfileDSDetailID = dbo.ProfileDSDetail.ProfileDSDetailID
GO
/****** Object:  View [dbo].[ViewGeneratedComplianceDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewGeneratedComplianceDetails]
AS
SELECT        dbo.ProfileDSHeader.KebeleID, dbo.ProfileDSHeader.NameOfHouseHoldHead, dbo.ProfileDSHeader.HouseHoldIDNumber, dbo.ProfileDSHeader.Gote, dbo.ProfileDSHeader.Gare, dbo.ComplianceSDSDetail.ProfileDSDetailID, 
                         dbo.ProfileDSDetail.HouseHoldMemberName, dbo.ProfileDSDetail.IndividualID, dbo.ProfileDSDetail.MedicalRecordNumber, dbo.ProfileDSDetail.Age, dbo.ProfileDSDetail.Sex, dbo.ProfileDSDetail.PWL, 
                         dbo.GenerateCSPDS.ServiceID, dbo.ProfileDSDetail.DateOfBirth, dbo.ComplianceSDSDetail.Complied, dbo.ComplianceSDSDetail.Remarks, dbo.GenerateCSPDS.ReportingPeriodID, 
                         dbo.f_GetUserNameByUserID(dbo.GenerateCSPDS.GeneratedBy) AS CreatedBy, CONVERT(VARCHAR(11), dbo.GenerateCSPDS.GeneratedOn, 106) AS GeneratedOn, dbo.ComplianceSDSHeader.FiscalYear, 
                         dbo.IntegratedServices.ServiceName, dbo.ProfileDSHeader.CollectionDate, dbo.ProfileDSHeader.SocialWorker
FROM            dbo.ProfileDSHeader INNER JOIN
                         dbo.ProfileDSDetail ON dbo.ProfileDSHeader.ProfileDSHeaderID = dbo.ProfileDSDetail.ProfileDSHeaderID INNER JOIN
                         dbo.ComplianceSDSHeader ON dbo.ComplianceSDSHeader.KebeleID = dbo.ProfileDSHeader.KebeleID INNER JOIN
                         dbo.ComplianceSDSDetail ON dbo.ComplianceSDSDetail.ProfileDSDetailID = dbo.ProfileDSDetail.ProfileDSDetailID AND dbo.ComplianceSDSHeader.ComplianceID = dbo.ComplianceSDSDetail.ComplianceID INNER JOIN
                         dbo.GenerateCSPDS ON dbo.ComplianceSDSHeader.ServiceID = dbo.GenerateCSPDS.ServiceID AND dbo.ComplianceSDSHeader.ReportingPeriodID = dbo.GenerateCSPDS.ReportingPeriodID AND 
                         dbo.ComplianceSDSDetail.ProfileDSDetailID = dbo.GenerateCSPDS.ProfileDSDetailID INNER JOIN
                         dbo.IntegratedServices ON dbo.ComplianceSDSHeader.ServiceID = dbo.IntegratedServices.ServiceID AND dbo.GenerateCSPDS.ServiceID = dbo.IntegratedServices.ServiceID
GO
/****** Object:  View [dbo].[viewServiceDetails]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[viewServiceDetails]
AS
SELECT IntegratedServices.ServiceID,IntegratedServices.ServiceName,
ServiceForm.FormID,ServiceForm.ServiceProviderID,ServiceProvider.ServiceProviderName
FROM IntegratedServices
INNER JOIN ServiceForm ON ServiceForm.ServiceID = IntegratedServices.ServiceID
INNER JOIN ServiceProvider ON ServiceProvider.ServiceProviderID = ServiceForm.ServiceProviderID

GO
/****** Object:  View [dbo].[ViewServices]    Script Date: 12/07/2019 11:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewServices]
AS
SELECT ServiceProvider.ServiceProviderID,ServiceProvider.ServiceProviderName 
,ServiceForm.ServiceID,ServiceName
FROM ServiceForm
INNER JOIN ServiceProvider ON ServiceProvider.ServiceProviderID = ServiceForm.ServiceProviderID
INNER JOIN IntegratedServices ON ServiceForm.ServiceID = IntegratedServices.ServiceID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[46] 4[15] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProfileTDSPLW"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 287
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "ProfileTDSPLWDetail"
            Begin Extent = 
               Top = 51
               Left = 551
               Bottom = 273
               Right = 760
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "ComplianceSTDSPLWHeader"
            Begin Extent = 
               Top = 9
               Left = 292
               Bottom = 139
               Right = 480
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ComplianceSTDSPLWDetail"
            Begin Extent = 
               Top = 181
               Left = 208
               Bottom = 311
               Right = 485
            End
            DisplayFlags = 280
            TopColumn = 8
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 5385
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewComplianceTDSPLWDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'      Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewComplianceTDSPLWDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewComplianceTDSPLWDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProfileDSHeader"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 277
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AdminStructureView"
            Begin Extent = 
               Top = 15
               Left = 659
               Bottom = 145
               Right = 829
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProfileDSDetail"
            Begin Extent = 
               Top = 78
               Left = 363
               Bottom = 298
               Right = 589
            End
            DisplayFlags = 280
            TopColumn = 19
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm1ADetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm1ADetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProfileDSHeader"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 278
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "AdminStructureView"
            Begin Extent = 
               Top = 49
               Left = 561
               Bottom = 179
               Right = 731
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm1AHouseHoldProfile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm1AHouseHoldProfile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProfileTDSPLW"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 13
         End
         Begin Table = "ProfileTDSPLWDetail"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "AdminStructureView"
            Begin Extent = 
               Top = 6
               Left = 289
               Bottom = 136
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm1BDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm1BDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -384
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProfileTDSPLW"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 13
         End
         Begin Table = "ProfileTDSPLWDetail"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 247
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "AdminStructureView"
            Begin Extent = 
               Top = 6
               Left = 289
               Bottom = 136
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 5310
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm1BPLW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm1BPLW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProfileTDSCMC"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 278
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 26
         End
         Begin Table = "AdminStructureView"
            Begin Extent = 
               Top = 0
               Left = 741
               Bottom = 130
               Right = 911
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm1CDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm1CDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CheckListTDSPLW"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CoResponsibilityTDSPLW"
            Begin Extent = 
               Top = 88
               Left = 369
               Bottom = 218
               Right = 616
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProfileTDSPLW"
            Begin Extent = 
               Top = 163
               Left = 44
               Bottom = 293
               Right = 257
            End
            DisplayFlags = 280
            TopColumn = 28
         End
         Begin Table = "ViewForm2BTDSPLWHeader"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 261
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm3BTDSPLWHeader'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm3BTDSPLWHeader'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CheckListTDSCMC"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 261
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CoResponsibilityTDSCMC"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 299
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "ProfileTDSCMC"
            Begin Extent = 
               Top = 141
               Left = 503
               Bottom = 271
               Right = 725
            End
            DisplayFlags = 280
            TopColumn = 30
         End
         Begin Table = "ViewForm2CTDSCMCHeader"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 275
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm3CTDSCMCHeader'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm3CTDSCMCHeader'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ViewForm1ADetails"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 278
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 30
         End
         Begin Table = "NotComplied"
            Begin Extent = 
               Top = 6
               Left = 302
               Bottom = 136
               Right = 481
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm5A1Details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm5A1Details'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ViewForm1BDetails"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 278
               Right = 250
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "NotComplied"
            Begin Extent = 
               Top = 32
               Left = 511
               Bottom = 278
               Right = 697
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm5BDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm5BDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ViewForm1CDetails"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 278
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 22
         End
         Begin Table = "NotComplied"
            Begin Extent = 
               Top = 57
               Left = 492
               Bottom = 187
               Right = 678
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm5CDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm5CDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[58] 4[3] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ViewForm1CDetails"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 278
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 27
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm5CDetailsNew'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewForm5CDetailsNew'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProfileDSHeader"
            Begin Extent = 
               Top = 8
               Left = 54
               Bottom = 138
               Right = 276
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "ProfileDSDetail"
            Begin Extent = 
               Top = 11
               Left = 396
               Bottom = 278
               Right = 622
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ComplianceSDSHeader"
            Begin Extent = 
               Top = 136
               Left = 38
               Bottom = 532
               Right = 226
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ComplianceSDSDetail"
            Begin Extent = 
               Top = 140
               Left = 271
               Bottom = 270
               Right = 474
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "GenerateCSPDS"
            Begin Extent = 
               Top = 312
               Left = 673
               Bottom = 548
               Right = 875
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "IntegratedServices"
            Begin Extent = 
               Top = 278
               Left = 264
               Bottom = 391
               Right = 445
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedComplianceDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedComplianceDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedComplianceDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ViewComplianceTDSCMCDetails"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 242
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "GenerateCSPTDSCMC"
            Begin Extent = 
               Top = 26
               Left = 420
               Bottom = 156
               Right = 626
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProfileTDSCMC"
            Begin Extent = 
               Top = 37
               Left = 645
               Bottom = 167
               Right = 867
            End
            DisplayFlags = 280
            TopColumn = 14
         End
         Begin Table = "IntegratedServices"
            Begin Extent = 
               Top = 71
               Left = 124
               Bottom = 184
               Right = 305
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedComplianceTDSCMCDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedComplianceTDSCMCDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedComplianceTDSCMCDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[50] 4[12] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -288
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ViewComplianceTDSPLWDetails"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 341
               Right = 313
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "GenerateCSPTDSPLW"
            Begin Extent = 
               Top = 7
               Left = 338
               Bottom = 341
               Right = 541
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "IntegratedServices"
            Begin Extent = 
               Top = 30
               Left = 569
               Bottom = 143
               Right = 750
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ProfileTDSPLW"
            Begin Extent = 
               Top = 360
               Left = 405
               Bottom = 490
               Right = 618
            End
            DisplayFlags = 280
            TopColumn = 9
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Tabl' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedComplianceTDSPLWDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'e = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedComplianceTDSPLWDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedComplianceTDSPLWDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProfileDSHeader"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 242
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "ProfileDSDetail"
            Begin Extent = 
               Top = 27
               Left = 352
               Bottom = 157
               Right = 578
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "NonComplianceDSHeader"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 249
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "NonComplianceDSDetail"
            Begin Extent = 
               Top = 326
               Left = 615
               Bottom = 456
               Right = 841
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "GenerateNonCCDS"
            Begin Extent = 
               Top = 425
               Left = 290
               Bottom = 555
               Right = 492
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TheComplianceDSDetails"
            Begin Extent = 
               Top = 270
               Left = 287
               Bottom = 383
               Right = 465
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedNonComplianceDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'     Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedNonComplianceDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedNonComplianceDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ViewNonComplianceTDSCMCDetails"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 278
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "GenerateNonCCTDSCMC"
            Begin Extent = 
               Top = 44
               Left = 651
               Bottom = 278
               Right = 857
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedNonComplianceTDSCMCDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedNonComplianceTDSCMCDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ViewNonComplianceTDSPLWDetails"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 278
               Right = 252
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "GenerateNonCCTDSPLW"
            Begin Extent = 
               Top = 12
               Left = 602
               Bottom = 142
               Right = 805
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedNonComplianceTDSPLWDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewGeneratedNonComplianceTDSPLWDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProfileDSHeader"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "ProfileDSDetail"
            Begin Extent = 
               Top = 44
               Left = 458
               Bottom = 174
               Right = 684
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "NonComplianceDSHeader"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 249
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "NonComplianceDSDetail"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ViewComplianceDSDetails"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 664
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewNonComplianceDSDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewNonComplianceDSDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewNonComplianceDSDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[19] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProfileTDSCMC"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 252
               Right = 260
            End
            DisplayFlags = 280
            TopColumn = 19
         End
         Begin Table = "NonComplianceTDSCMCHeader"
            Begin Extent = 
               Top = 42
               Left = 526
               Bottom = 172
               Right = 737
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "NonComplianceTDSCMCDetail"
            Begin Extent = 
               Top = 130
               Left = 987
               Bottom = 260
               Right = 1213
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ViewComplianceTDSCMCDetails"
            Begin Extent = 
               Top = 186
               Left = 525
               Bottom = 316
               Right = 747
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewNonComplianceTDSCMCDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewNonComplianceTDSCMCDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ProfileTDSPLW"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 29
         End
         Begin Table = "ProfileTDSPLWDetail"
            Begin Extent = 
               Top = 51
               Left = 478
               Bottom = 181
               Right = 687
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "NonComplianceTDSPLWHeader"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 249
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "NonComplianceTDSPLWDetail"
            Begin Extent = 
               Top = 402
               Left = 38
               Bottom = 532
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ViewComplianceTDSPLWDetails"
            Begin Extent = 
               Top = 534
               Left = 38
               Bottom = 664
               Right = 250
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewNonComplianceTDSPLWDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'  Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewNonComplianceTDSPLWDetails'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewNonComplianceTDSPLWDetails'
GO
