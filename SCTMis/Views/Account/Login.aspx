﻿
<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.LoginModel>" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>SCT-MIS - version 3.9</title>
	
	<!-- Meta -->
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
	
	<!-- Bootstrap -->
	<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	
	<!-- Bootstrap Extended -->
	<link href="/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet"/>
	<link href="/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap-responsive.min.css" rel="stylesheet"/>
	<link href="/bootstrap/extend/bootstrap-wysihtml5/css/bootstrap-wysihtml5-0.0.2.css" rel="stylesheet"/>
	
	<!-- Select2 -->
	<link rel="stylesheet" href="/theme/scripts/select2/select2.css"/>
	
	<!-- JQueryUI v1.9.2 -->
	<link rel="stylesheet" href="/theme/scripts/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css" />
	
	<!-- Glyphicons -->
	<link rel="stylesheet" href="/theme/css/glyphicons.css" />
	
	<!-- Bootstrap Extended -->
	<link rel="stylesheet" href="/bootstrap/extend/bootstrap-select/bootstrap-select.css" />
	<link rel="stylesheet" href="/bootstrap/extend/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	
	<!-- Uniform -->
	<link rel="stylesheet" media="screen" href="/theme/scripts/pixelmatrix-uniform/css/uniform.default.css" />

	<!-- JQuery v1.8.2 -->
	<script src="/theme/scripts/jquery-1.8.2.min.js"></script>
	
	<!-- Modernizr -->
	<script src="/theme/scripts/modernizr.custom.76094.js"></script>
	<!-- Theme -->
	<link rel="stylesheet" href="/theme/css/style.min.css?1363272417" />
	
	<!-- LESS 2 CSS -->
	<script src="/theme/scripts/less-1.3.3.min.js"></script>
	<!--[if IE]><script type="text/javascript" src="/theme/scripts/excanvas/excanvas.js"></script><![endif]-->
	<!--[if lt IE 8]><script type="text/javascript" src="/theme/scripts/json2.js"></script><![endif]-->
</head>
<body>    
	<!-- Start Content -->
    <div class="innerLR">
	    <div class="widget widget-gray widget-body-white">
            <% using (Html.BeginForm(null, null, new { ReturnUrl = ViewBag.ReturnUrl }, FormMethod.Post, new { data_href = "/Account/Login" }))
               { %>
                <table border="0" style="position:relative; margin-top:10%;height:90%; margin-left:36%">
                    <tr>
                    </tr>
                    <tr>
                        <td style="text-align:center"><h2 class="heading center">Direct support MIS(PSNP)</h2></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="widget widget-4">
                                <div class="widget-head">
                                    <h4 class="heading">Restricted Area</h4>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2 class="glyphicons lock form-signin-heading"><i></i> Kindly sign in</h2> 
                            <div class="uniformjs">
                                <input id="UserName" name="UserName" type="text" class="input-block-level" placeholder="User Name" value="" />
                                <input id="Password" name="Password" type="password" class="input-block-level" placeholder="Password" value="" />
                            </div><br/>
                            <button class="btn btn-large btn-icon btn-primary glyphicons keys" type="submit" ><i></i>Sign in</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%: Html.ValidationSummary(true) %><br />
                            <%: Html.ValidationMessageFor(m => m.UserName) %><br />
                            <%: Html.ValidationMessageFor(m => m.Password) %>
                        </td>
                    </tr>
                </table>
                <% } %>
        </div>
    </div>
	<!-- JQueryUI v1.9.2 -->
	<script src="/theme/scripts/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>
	
	<!-- JQueryUI Touch Punch -->
	<!-- small hack that enables the use of touch events on sites using the jQuery UI user interface library -->
	<script src="/theme/scripts/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- Resize Script -->
	<script src="/theme/scripts/jquery.ba-resize.js"></script>
	
	<!-- Uniform -->
	<script src="/theme/scripts/pixelmatrix-uniform/jquery.uniform.min.js"></script>
	
	<!-- Bootstrap Script -->
	<script src="/bootstrap/js/bootstrap.min.js"></script>
	
	<!-- Bootstrap Extended -->
	<script src="/bootstrap/extend/bootstrap-select/bootstrap-select.js"></script>
	<script src="/bootstrap/extend/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script src="/bootstrap/extend/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"></script>
	<script src="/bootstrap/extend/jasny-bootstrap/js/jasny-bootstrap.min.js" type="text/javascript"></script>
	<script src="/bootstrap/extend/bootbox.js" type="text/javascript"></script>
	
	<!-- Custom Onload Script -->
	<script src="/theme/scripts/load.js"></script>
	<%: Scripts.Render("~/bundles/jqueryval") %>
</body>
</html>
