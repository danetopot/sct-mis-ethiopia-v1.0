﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Standard Reports
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.Partial("_CommonSearch") %>
    
    <!-- <div id="gridbox" style="position:relative; width: 760px; height: 250px"></div> -->
    <div id="gridbox" style="position:relative; width: 935px; height: 250px"></div>

    <span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>

    <div class="form-actions">
        <button id="cmdProduceReport" type="button" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>Generate Standard Reports</button>
    </div>

    <%: Html.Hidden("currentPage", (int)ViewBag.CurrentPage) %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">

    <script type="text/javascript" src="../Scripts/assets/js/app/StandardReportsList.js"></script>

</asp:Content>
