alter table CoResponsibilityTDSPLWHeader add FiscalYear int
go

alter table CoResponsibilityTDSPLWHeader with check add constraint FK_CoResponsibilityTDSPLWHeader_FiscalYear foreign key(FiscalYear)
references FiscalYears (FiscalYear)
go

update CoResponsibilityTDSPLWHeader set FiscalYear = dbo.f_getFiscalYearFromDate(GeneratedOn)
from CoResponsibilityTDSPLW
inner join CoResponsibilityTDSPLWHeader on CoResponsibilityTDSPLWHeader.CoRespTDSPLWHeaderID = CoResponsibilityTDSPLW.CoRespTDSPLWHeaderID

alter table CoResponsibilityTDSPLWHeader alter column FiscalYear int not null
go