ALTER VIEW [dbo].[ViewCoResponsibilityDS]
AS
Select CoResponsibilityDSHeader.CoRespDSHeaderID,
		GeneratedBy,GeneratedOn,
		ReportFileName,
		CoRespDSDetailID,
		ProfileDSHeader.ProfileDSHeaderID,
		CoResponsibilityDSDetail.ProfileDSDetailID,
		TargetClientID,
		ProfileDSHeader.KebeleID,
		KebeleName,
		AdminStructureView.WoredaID,
		AdminStructureView.WoredaName,
		AdminStructureView.RegionID,
		ApprovedBy,
		ApprovedOn,
		AdminStructureView.RegionName,
		CoResponsibilityDSHeader.FiscalYear
FROM CoResponsibilityDSHeader
INNER JOIN CoResponsibilityDSDetail ON CoResponsibilityDSDetail.CoRespDSHeaderID = CoResponsibilityDSHeader.CoRespDSHeaderID
INNER JOIN ProfileDSDetail ON ProfileDSDetail.ProfileDSDetailID = CoResponsibilityDSDetail.ProfileDSDetailID
INNER JOIN ProfileDSHeader ON ProfileDSHeader.ProfileDSHeaderID = ProfileDSDetail.ProfileDSHeaderID
INNER JOIN AdminStructureView ON ProfileDSHeader.KebeleID = AdminStructureView.KebeleID
WHERE ISNULL(Form2Produced,0) = 1

GO


