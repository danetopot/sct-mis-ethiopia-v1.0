﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.ProduceForm2A>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Produce Form 2A
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<h5>Add New Girl Form</h5>   --%>     
<% using (Html.BeginForm("ProduceForm2A", "CoResponsibility", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-4">                
            <label>
                Region <%: Html.ValidationMessageFor(model => model.RegionID) %>
                <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
            </label>
            </div>
            <div class="col-4">                
            <label>
                Woreda  <%: Html.ValidationMessageFor(model => model.WoredaID) %>
                <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
            </label>
            </div>
    <br />
            <div class="col-2">                              
            <label>
                Kebele <%: Html.ValidationMessageFor(model => model.KebeleID) %>  
                <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
            </label>
            </div>
          <br /><br />
            <div class="form-actions">
	            <button id="cmdProduce" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Produce Form 2A</button>
                <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
            </div>
            <br />
            <%: Html.ValidationSummary(true) %>          
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#loading').hide();
            $("#KebeleID").prop("disabled", false);
            $("#cmdBack,#cmdProduce").prop("disabled", false);
            $("#RegionID,#WoredaID").prop("disabled", true);
            $(":input").blur();
            $('#KebeleID').focus();

            $("#cmdBack").click(function () {
                window.location.href = "../CoResponsibility/Form2A";
            });
        });
    </script>
</asp:Content>

