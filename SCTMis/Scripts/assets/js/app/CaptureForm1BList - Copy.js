﻿
var EVENTID = null;
var arrObj = [];
var SerialCount = 0;
var page_count = 4;
var mygrid;
var gridHeader = 'ColumnID,PLW<span class="HeaderChange">_</span>Name,Household<span class="HeaderChange">_</span>ID';
gridHeader = gridHeader + ',Medical<span class="HeaderChange">_</span>RecID,Baby<span class="HeaderChange">_</span>Name';
gridHeader = gridHeader + ',Baby<span class="HeaderChange">_</span>Gender, Edit';
var gridColType = 'ro,ro,ro,ro,ro,ro,img';
var mycalStartTDS, mycalSEndTDS, myBabyDOB, myColDate;
$(document).ready(function () {

    $('#loading').hide();

    $('#CollectionDate').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        //direction: ['2012-08-01', '2012-08-12']
        format: 'd/M/Y'
    });

    $('#BabyDateOfBirth').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        format: 'd/M/Y',
        view: 'years'
    });

    $('#EndDateTDS').Zebra_DatePicker({
        //direction: +1,    // boolean true would've made the date picker future only
        format: 'M/Y',
        view: 'years'
    });

    $('#StartDateTDS').Zebra_DatePicker({
        //direction: 1,    // boolean true would've made the date picker future only
        //direction: -1,
        format: 'M/Y',
        view: 'years'
    });

    $("#KebeleID,#Gote,#Gare,#SocialWorker,#CCCCBSPCMember").prop("disabled", false);
    $("#PLW,#NameOfPLW,#HouseHoldIDNumber,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#BabyDateOfBirth,#BabyName,#BabySex,#EndDateTDS,#NutritionalStatusPLW,#NutritionalStatusInfant,#Remarks").prop("disabled", true);

    $("#RegionID,#WoredaID").attr("disabled", true);
    //$("#RegionID,#WoredaID,#KebeleID").val("0");
    $("#KebeleID,#Gote,#PLW,#NameOfPLW,#HouseHoldIDNumber,#Gare,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#BabyDateOfBirth,#BabyName,#Remarks").val("");
    $("#SocialWorker,#KebeleID").val("0");
    $("#BabySex,#EndDateTDS,#CollectionDate,#CCCCBSPCMember").val("");
    $("#PLW").val("P");
    $("#NutritionalStatusPLW,#NutritionalStatusPLW,#NutritionalStatusInfant,#NutritionalStatusInfant").val("-");
    
    $("#cmdProceed,#cmdSave").prop("disabled", true);

    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,170,110,120,170,100,60");
    mygrid.setColAlign("left,left,right,left,right,right,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();

    dhtmlxError.catchError("ALL", my_error_handler);

    $('#KebeleID').focus();

    $("#cmdBack").click(function () {
        window.location.href = "/Household/Form1B";
    });

    $('#KebeleID,#CollectionDate,#SocialWorker,#CCCCBSPCMember,#PLW').bind('focusout', function (event) {
        if (!isValid($(this).val())) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $(this).off("blur");
            $(this).focus();
        }
    });

    $('#CCCCBSPCMember').change(function (e) {
        if (isValid($(this).val())) {
            $('#RegionID,#WoredaID').prop("disabled", true);
            //$('#Gote,#CollectionDate,#SocialWorker,#CCCCBSPCMember,#Gare').attr("readonly", true);
            $("#Kebele").val($("#KebeleID").val());
            $("#PLW,#NameOfPLW,#HouseHoldIDNumber,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#BabyDateOfBirth,#BabyName,#BabySex,#EndDateTDS,#NutritionalStatusPLW,#NutritionalStatusInfant,#cmdProceed,#Remarks").prop("disabled", false);
            $("#PLW").val("L");
            $('#PLW').focus();
        }
    });

    $('#PLW').change(function (e) {
        if ($("#PLW").val()=="P") {
                $('#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant').attr('disabled', "disabled");                
                $("#BabySex option[value='M']").attr("disabled", "disabled");
                $("#BabySex option[value='F']").attr("disabled", "disabled");

                $("#BabySex option[value='F']").attr("disabled", "disabled");

                $("#NutritionalStatusInfant option[value='N']").attr("disabled", "disabled");
                $("#NutritionalStatusInfant option[value='M']").attr("disabled", "disabled");
                $("#NutritionalStatusInfant option[value='SM']").attr("disabled", "disabled");
            } else {
                $('#BabyDateOfBirth,#BabyName,#BabySex,#NutritionalStatusInfant').attr('disabled', false);
                $("#BabySex option[value='F']").attr("disabled", false);
                $("#BabySex option[value='M']").attr("disabled", false);

                $("#NutritionalStatusInfant option[value='N']").attr("disabled", false);
                $("#NutritionalStatusInfant option[value='M']").attr("disabled", false);
                $("#NutritionalStatusInfant option[value='SM']").attr("disabled", false);
            }        
    });

    $('#CollectionDate').change(function (e) {
        if (isValid($(this).val())) {
            $('#SocialWorker').focus();
        } else { $('#CollectionDate').focus(); }
    });

    $("#CollectionDate").focusout(function () {
        if (isValid($(this).val())) {
            $('#SocialWorker').focus();
        } else { $('#CollectionDate').focus(); }
    });

    $('#StartDateTDS').change(function (e) {
        if (isValid($(this).val())) {
            $('#BabyDateOfBirth').focus();
        } else { $('#StartDateTDS').focus(); }
    });

    $("#StartDateTDS").focusout(function () {
        if (isValid($(this).val())) {
            $('#BabyDateOfBirth').focus();
        } else { $('#StartDateTDS').focus(); }
    });

    $('#EndDateTDS').change(function (e) {
        if (isValid($(this).val())) {
            $('#NutritionalStatusPLW').focus();
        } else { $('#EndDateTDS').focus(); }
    });

    $("#EndDateTDS").focusout(function () {
        if (isValid($(this).val())) {
            $('#NutritionalStatusPLW').focus();
        } else { $('#EndDateTDS').focus(); }
    });

    $("#cmdProceed").click(function () {
        //EVENTID = "ADD";
        var errors = [];
        var html = '<ul>';
        valid = true;
        var errorMsg = '';
        $('#errors2').empty();
        
        if ($('#PLW').val() == '') {
            //errors.push('<li>PLW Name is Required</li>');
            errorMsg = 'Pregnant or Lactating is Required';
            valid = false;
        }

        if ($('#NameOfPLW').val() == '') {
            //errors.push('<li>PLW Name is Required</li>');
            errorMsg = 'PLW Name is Required';
            valid = false;
        }
        
        if ($('#HouseHoldIDNumber').val() == '') {
            //errors.push('<li>Household ID Number is required</li>');
            errorMsg = errorMsg + ' | PLW Name is Required';
            valid = false;
        }

        if ($('#MedicalRecordNumber').val() == '') {
            //errors.push('<li>Medical Record Number Required</li>');
            errorMsg = errorMsg + ' | Medical Record Number Required';
            valid = false;
        }

        if (isNaN($('#PLWAge').val()) == true) {
            //errors.push('<li>Age Of PLW Should be numeric</li>');
            errorMsg = errorMsg + ' | Age Of PLW Should be numeric';
            valid = false;
        }

        if ($('#PLWAge').val() == '') {
            //errors.push('<li>Age of PLW required</li>');
            errorMsg = errorMsg + ' | Age of PLW required';
            valid = false;
        }

        if ($('#PLWAge').val() < 15) {
            //errors.push('<li>Age of PLW required</li>');
            errorMsg = errorMsg + ' | PLW age should be between 15 and 60 years';
            valid = false;
        }

        if ($('#PLWAge').val() > 46) {
            //errors.push('<li>Age of PLW required</li>');
            errorMsg = errorMsg + ' | PLW age should be between 15 and 60 years';
            valid = false;
        }

        if ($('#StartDateTDS').val() == '') {
           // errors.push('<li>Start of being temporary DS client Required</li>');
            errorMsg = errorMsg + ' | Start of being temporary DS client Required';
            valid = false;
        }

        if (jsDateDiff("01/" + $('#StartDateTDS').val(), $('#CollectionDate').val(), "months") > 20) {
            errors.push('<li>TDS start date cannot be earlier than 20 months before date of data collection </li>');
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), "01/" + $('#StartDateTDS').val(), "months") > 1) {
            errors.push('<li>TDS start date cannot be later than 1 month after date of data collection</li>');
            valid = false;
        }

        if ($('#EndDateTDS').val() == '') {
            //errors.push('<li>Date when temporary DS status will end Required</li>');
            errorMsg = errorMsg + ' | Date when temporary DS status will end Required';
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), "01/" + $('#EndDateTDS').val(), "days") > 730) {
            errors.push('<li>Temporary DS end date cannot be more than 2 years</li>');
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), "01/" + $('#EndDateTDS').val(), "days") < 1) {
            errors.push('<li>Temporary DS end date cannot be a past date from collection date</li>');
            valid = false;
        }

        if ($('#NutritionalStatusPLW').val() == '') {
            //errors.push('<li>Nutritional status of PLW required</li>');
            errorMsg = errorMsg + ' | Nutritional status of PLW required';
            valid = false;
        }
        if ($('#CollectionDate').val() == '') {
            //errors.push('<li>Please collected Date</li>');
            errorMsg = errorMsg + ' | Please collected Date';
            valid = false;
        }
        
        if ($('#SocialWorker').val() == '') {
            //errors.push('<li>Please select Social Worker</li>');
            errorMsg = errorMsg + ' | Please select Social Worker';
            valid = false;
        }

        if ($('#CCCCBSPCMember').val() == '') {
            //errors.push('<li>CCC/CBSPC member required</li>');
            errorMsg = errorMsg + ' | CCC/CBSPC member required';
            valid = false;
        }        
        if ($("#PLW").val() == "L") {
            if ($('#BabyDateOfBirth').val() == "") {
                errors.push('<li>Baby Date Of Birth required</li>');
                valid = false;
            }

            if (jsDateDiff($('#BabyDateOfBirth').val(), $('#CollectionDate').val(), "months") > 12) {
                errors.push('<li>Child birth cannot be more than 12 months from collection date</li>');
                valid = false;
            }

            if ($('#BabyName').val() == "") {
                errors.push('<li>Baby Name required</li>');
                valid = false;
            }
            if ($('#BabySex').val() == "") {
                errors.push('<li>Gender Of Baby required</li>');
                valid = false;
            }
        }

        if (!valid) {
            errors.push(errorMsg);
            html += errors.join('') + '</ul>'
            $('#errors2').show();
            $('#errors2').append(html);
            return valid;
        }
        else {
            $('#errors2').hide();
        }

        var theDtls = new Object;
        var babyGender;
        if ($("#PLW").val() == "P") {
            babyGender = '';
        } else {
            babyGender = $("#BabySex").find('option:selected').text();
        }

        if (EVENTID == "EDIT") {

            for (var i in arrObj) {
                if (arrObj.hasOwnProperty(i)) {
                    if (arrObj[i].ColumnID == $("#ColumnID").val()) {                        
                        arrObj[i].PLW = $("#PLW").val();
                        arrObj[i].NameOfPLW = $("#NameOfPLW").val();
                        arrObj[i].HouseHoldIDNumber = $("#HouseHoldIDNumber").val();
                        arrObj[i].MedicalRecordNumber = $("#MedicalRecordNumber").val();
                        arrObj[i].PLWAge = $("#PLWAge").val();
                        arrObj[i].StartDateTDS = $("#StartDateTDS").val();
                        arrObj[i].BabyDateOfBirth = $("#BabyDateOfBirth").val();
                        arrObj[i].BabyName = $("#BabyName").val();
                        arrObj[i].BabySex = $("#BabySex").val();
                        arrObj[i].BabySexName = babyGender;
                        arrObj[i].EndDateTDS = $("#EndDateTDS").val();

                        arrObj[i].NutritionalStatusPLW = $("#NutritionalStatusPLW").val();
                        arrObj[i].NutritionalStatusPLWName = $("#NutritionalStatusPLW").find('option:selected').text();
                        arrObj[i].NutritionalStatusInfant = $("#NutritionalStatusInfant").val();
                        arrObj[i].NutritionalStatusInfantName = $("#NutritionalStatusInfant").find('option:selected').text();
                        arrObj[i].CollectionDate = $("#CollectionDate").val();
                        arrObj[i].SocialWorker = $("#SocialWorker").val();
                        arrObj[i].SocialWorkerName = $("#SocialWorker").find('option:selected').text();
                        arrObj[i].CCCCBSPCMember = $("#CCCCBSPCMember").val();
                        arrObj[i].Remarks = $("#Remarks").val();                        
                    }
                }
            }
        }
        else {
            $('#errors2').hide();
            SerialCount = SerialCount + 1;
            theDtls.ColumnID = SerialCount;            
            theDtls.PLW = $("#PLW").val();
            theDtls.NameOfPLW = $("#NameOfPLW").val();
            theDtls.HouseHoldIDNumber = $("#HouseHoldIDNumber").val();
            theDtls.MedicalRecordNumber = $("#MedicalRecordNumber").val();
            theDtls.PLWAge = $("#PLWAge").val();
            theDtls.StartDateTDS = $("#StartDateTDS").val();
            theDtls.BabyDateOfBirth = $("#BabyDateOfBirth").val();
            theDtls.BabyName = $("#BabyName").val();
            theDtls.BabySex = $("#BabySex").val();
            theDtls.BabySexName = babyGender;
            theDtls.EndDateTDS = $("#EndDateTDS").val();

            theDtls.NutritionalStatusPLW = $("#NutritionalStatusPLW").val();
            theDtls.NutritionalStatusPLWName = $("#NutritionalStatusPLW").find('option:selected').text();
            theDtls.NutritionalStatusInfant = $("#NutritionalStatusInfant").val();
            theDtls.NutritionalStatusInfantName = $("#NutritionalStatusInfant").find('option:selected').text();
            theDtls.CollectionDate = $("#CollectionDate").val();
            theDtls.SocialWorker = $("#SocialWorker").val();
            theDtls.SocialWorkerName = $("#SocialWorker").find('option:selected').text();
            theDtls.CCCCBSPCMember = $("#CCCCBSPCMember").val();
            theDtls.Remarks = $("#Remarks").val();            
            arrObj.push(theDtls);
        }
        var myXml = createXmlstring(arrObj, 1);
        mygrid.clearAll();
        mygrid.parse(myXml);
        $("#cmdSave").prop("disabled", false);
        EVENTID = null;
        $("#PLW,#NameOfPLW,#HouseHoldIDNumber,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#BabyDateOfBirth,#BabyName,#BabySex,#EndDateTDS,#NutritionalStatusPLW,#NutritionalStatusInfant,#Remarks").val('');
        $("#PLW").val("P");
        $("#NutritionalStatusPLW,#NutritionalStatusPLW,#NutritionalStatusInfant,#NutritionalStatusInfant").val("-");
        
        $('#PLW').focus();
    });

});
function createXmlstring(arrObject, inMemory) {
    var xml;
    var gridID;
    xml = '';
    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            xml = xml + '<row id="' + i + '">';
            for (var j in arrObj[i]) {
                if (arrObj[i].hasOwnProperty(j)) {
                    if (j == "ColumnID") {
                        xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                        gridID = arrObj[i][j];
                    }
                }
                if (j == "NameOfPLW" || j == "HouseHoldIDNumber" || j == "MedicalRecordNumber" || j == "BabyName" || j == "BabySexName") {
                    xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                }
            }
            xml = xml + '<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit^javascript:UpdateMembers(' + gridID + ');^_self</cell>';
            xml = xml + '</row>';
        }
    }
    xml = '<rows total_count="' + arrObj.length + '">' + xml + '</rows>';
    $("#MemberXml").val(JSON.stringify(arrObject));
    return xml;
}

function UpdateMembers(_ColumnID) {
    var selectedRow = mygrid.getSelectedId();
    _ColumnID = mygrid.cells(selectedRow, 0).getValue();
    $("#PLW,#NameOfPLW,#HouseHoldIDNumber,#MedicalRecordNumber,#PLWAge,#StartDateTDS,#BabyDateOfBirth,#BabyName,#BabySex,#EndDateTDS,#NutritionalStatusPLW,#NutritionalStatusInfant,#Remarks").val('');

    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            if (arrObj[i].ColumnID == _ColumnID) {
                $("#ColumnID").val(arrObj[i].ColumnID);
                $("#PLW").val(arrObj[i].PLW);
                $("#NameOfPLW").val(arrObj[i].NameOfPLW);
                $("#HouseHoldIDNumber").val(arrObj[i].HouseHoldIDNumber);
                $("#MedicalRecordNumber").val(arrObj[i].MedicalRecordNumber);
                $("#PLWAge").val(arrObj[i].PLWAge);
                $("#StartDateTDS").val(arrObj[i].StartDateTDS);
                $("#BabyDateOfBirth").val(arrObj[i].BabyDateOfBirth);
                $("#BabyName").val(arrObj[i].BabyName);
                $("#BabySex").val(arrObj[i].BabySex);
                $("#EndDateTDS").val(arrObj[i].EndDateTDS);
                $("#NutritionalStatusPLW").val(arrObj[i].NutritionalStatusPLW);
                $("#NutritionalStatusInfant").val(arrObj[i].NutritionalStatusInfant);                
                $("#Remarks").val(arrObj[i].Remarks);
            }
        }
    }

    $("#HouseHoldMemberName,#MedicalRecordNumber,#DateOfBirth,#Sex,#PWL,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#SchoolName,#Remarks").prop("disabled", false);
    //#Age,
    $("#cmdProceed").html('<i></i>Update Details');
    $('#PLW').focus();
    EVENTID = "EDIT";
}

$('#BabyDateOfBirth').focusout(function () {
    var someDate = new Date ($('#BabyDateOfBirth').val());
    var numberOfDaysToAdd = 365;
    someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
    $('#EndDateTDS').val($.datepicker.formatDate("M/yy", someDate));
});
