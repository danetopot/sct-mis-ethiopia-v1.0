ALTER Proc [dbo].[InsertForm4B]
(
	@KebeleID				Int,
	@ServiceID				int,
	@ReportFileName			varchar(50),
	@ReportingPeriodID		Int,		
	@MemberXML				Varchar(max),
	@CreatedBy				varchar(30)
)
AS
BEGIN
SET NOCOUNT ON
	Declare @ComplianceID		varchar(100)
	DECLARE @DetailRecords		XML

	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD	
		SET @DetailRecords = CONVERT(XML,@MemberXML)

		SELECT @ComplianceID = NEWID()

		INSERT INTO ComplianceSTDSPLWHeader(ComplianceID,ServiceID,KebeleID,CapturedBy,CapturedOn,ReportFileName,ReportingPeriodID,FiscalYear)
		VALUES(@ComplianceID,@ServiceID,@KebeleID,dbo.f_getUserIDByUserName(@CreatedBy),GETDATE(),@ReportFileName,@ReportingPeriodID,dbo.f_getFiscalYearFromDate(GETDATE()))
			

		DECLARE @@Form4BCompliance TABLE
		(
			ProfileTDSPLWID			varchar(100)
		)

		INSERT INTO @@Form4BCompliance(ProfileTDSPLWID)
		SELECT
			m.c.value('(ProfileTDSPLWID)[1]', 'varchar(100)')
		FROM @DetailRecords.nodes('/members/row') AS m(c)

		INSERT INTO ComplianceSTDSPLWDetail(ComplianceDtlID,ComplianceID,ProfileTDSPLWID,NameOfPLW,Complied,Remarks)		
		SELECT NEWID(),@ComplianceID,ProfileTDSPLWID,'',0,''
		FROM @@Form4BCompliance

		UPDATE ProfileTDSPLW
		SET Form4Produced = 1
		FROM ProfileTDSPLW
		INNER JOIN @@Form4BCompliance F4BCompliance ON F4BCompliance.ProfileTDSPLWID = ProfileTDSPLW.ProfileTDSPLWID


		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
		  --PRINT 'Error detected, all changes reversed'
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END


