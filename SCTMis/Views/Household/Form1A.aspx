﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Form 1A Listing
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.Partial("_CommonSearch") %>
    <div id="gridbox" style="position:relative; width: 935px; height: 250px"></div>
    <span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>

    <div class="form-actions">
	    <button id="cmdNew" type="button" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i>Add New Household Profile</button>
    </div>
    <%: Html.Hidden("currentPage", (int)ViewBag.CurrentPage) %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">

    <script type="text/javascript" src="../Scripts/assets/js/app/Form1AList.js"></script>

</asp:Content>
