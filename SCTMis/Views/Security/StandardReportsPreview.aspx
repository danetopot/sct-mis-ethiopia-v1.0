﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.StandardReportModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Standard Reports Preview
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%: Html.Partial("_CommonExport") %>
    
    <hr>
    VIEW REPORTS IN HTML TABLE


    <p>@ViewBag.Subtitle</p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">

    
        <script type="text/javascript" src="../Scripts/assets/js/app/StandardReportsPreview.js"></script>

</asp:Content>
