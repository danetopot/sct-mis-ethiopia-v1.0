﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.UserAccessRights>" %>
    <div id="menuInner">
            <ul id="menu-v">
                 <%--Household profile Module--%>
                <% foreach(var menuObj in Model.moduleaccess)  {%>
                   <% if (menuObj.TaskID == 3) //ORIGINAL - <% if (menuObj.TaskID == 3 && menuObj.TaskStatus == 1)
                   {%>
                    <li id="menuHouseholdProfile" style="border-bottom:1px solid #dddddd;"><a class="glyphicons home" href="#"><i></i><span>Household Profile</span></a>
                        <ul class="sub"> 
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                               <% if (menuObj1.TaskID == 4 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Capture Form 1A", "Form1A", "Household") %></li>   
                                <%}%>  
                            <%}%>  
                            
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 7 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Capture Form 1B", "Form1B", "Household") %></li>     
                                <%}%>  
                            <%}%>   
                                                                      
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 10 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Capture Form 1C", "Form1C", "Household") %></li>     
                                <%}%>  
                            <%}%>                                                     
                            
                        </ul>
                    </li>
                    <%}%>
                <%}%>

                 <%--Co-responsibility Module--%>
                <% foreach(var menuObj in Model.moduleaccess)  {%>
                    <% if (menuObj.TaskID == 13 && menuObj.TaskStatus == 1)
                   {%>
                    <li style="border-bottom:1px solid #dddddd;"><a class="glyphicons parents" href="#"><i></i><span>Co-responsibilities</span></a>
                        <ul class="sub"> 
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 14 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 2A", "Form2A", "CoResponsibility") %></li>  
                                <%}%>  
                            <%}%>  
                            
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 16 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 2B1", "Form2B", "CoResponsibility") %></li>     
                                <%}%>  
                            <%}%>  
                            
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 85 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 2B2", "Form2B2", "CoResponsibility") %></li>     
                                <%}%>  
                            <%}%>    
                                                                      
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 18 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 2C", "Form2C", "CoResponsibility") %></li>      
                                <%}%>  
                            <%}%>                            

                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 20 && menuObj1.TaskStatus == 1)
                               {%>   
                                    <li><%: Html.ActionLink("Generate Form 3A", "Form3A", "Checklist") %></li>    
                                <%}%>  
                            <%}%>   

                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 22 && menuObj1.TaskStatus == 1)
                               {%>
                                    <li><%: Html.ActionLink("Generate Form 3B1", "Form3B", "Checklist") %></li>       
                                <%}%>  
                            <%}%>   

                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 22 && menuObj1.TaskStatus == 1)
                               {%>
                                    <li><%: Html.ActionLink("Generate Form 3B2", "Form3B2", "Checklist") %></li>       
                                <%}%>  
                            <%}%>    

                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 24 && menuObj1.TaskStatus == 1)
                               {%>                                                      
                                    <li><%: Html.ActionLink("Generate Form 3C", "Form3C", "Checklist") %></li>     
                                <%}%>  
                            <%}%>   
                        </ul>
                    </li>
                    <%}%>
                <%}%>

                <%--Compliance Reporting Module--%>
                <% foreach(var menuObj in Model.moduleaccess)  {%>
                    <% if (menuObj.TaskID == 26 && menuObj.TaskStatus == 1)
                   {%>
                    <li style="border-bottom:1px solid #dddddd;"><a class="glyphicons database_plus" href="#"><i></i><span>Compliance</span></a>
                        <ul class="sub"> 
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 27 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 4A", "Form4A", "Compliance") %></li> 
                                <%}%>  
                            <%}%>  
                            
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 29 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 4B", "Form4B", "Compliance") %></li>    
                                <%}%>  
                            <%}%>   
                                                                      
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 31 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 4C", "Form4C", "Compliance") %></li>       
                                <%}%>  
                            <%}%>

                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 27 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 4D", "Form4D", "Compliance") %></li>       
                                <%}%>  
                            <%}%>
                            
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 31 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 4D", "Form4C", "Compliance") %></li>       
                                <%}%>  
                            <%}%>
                            

                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 33 && menuObj1.TaskStatus == 1)
                               {%>   
                                    <li><%: Html.ActionLink("Capture Form 4A", "Form4AList", "ComplianceCapture") %></li>    
                                <%}%>  
                            <%}%>   

                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 35 && menuObj1.TaskStatus == 1)
                               {%>
                                    <li><%: Html.ActionLink("Capture Form 4B", "Form4BList", "ComplianceCapture") %></li>        
                                <%}%>  
                            <%}%>   

                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 37 && menuObj1.TaskStatus == 1)
                               {%>                                                      
                                    <li><%: Html.ActionLink("Capture Form 4C", "Form4CList", "ComplianceCapture") %></li>    
                                <%}%>  
                            <%}%> 
                            
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 33 && menuObj1.TaskStatus == 1)
                               {%>   
                                    <li><%: Html.ActionLink("Capture Form 4D", "FormDAList", "ComplianceCapture") %></li>    
                                <%}%>  
                            <%}%>   
                        </ul>
                    </li>
                    <%}%>
                <%}%>

                 <%--Monitoring Reporting Module--%>
                <% foreach(var menuObj in Model.moduleaccess)  {%>
                    <% if (menuObj.TaskID == 39 && menuObj.TaskStatus == 1)
                   {%>
                    <li style="border-bottom:1px solid #dddddd;"><a class="glyphicons stats" href="#"><i></i><span>Monitoring</span></a>
                        <ul class="sub"> 
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 40 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 5A1", "Form5A1", "Monitoring") %></li>
                                <%}%>  
                            <%}%>  
                            
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 42 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 5A2", "Form5A", "Monitoring") %></li>
                                <%}%>  
                            <%}%>  

                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 44 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 5B", "Form5B", "Monitoring") %></li>   
                                <%}%>  
                            <%}%>   
                                                                      
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 46 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Produce Form 5C", "Form5C", "Monitoring") %></li>         
                                <%}%>  
                            <%}%>                                                     
                            
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 48 && menuObj1.TaskStatus == 1)
                               {%>   
                                    <li><%: Html.ActionLink("Capture Form 5A1", "Form5A1List", "MonitoringCapture") %></li>    
                                <%}%>  
                            <%}%> 

                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 50 && menuObj1.TaskStatus == 1)
                               {%>   
                                    <li><%: Html.ActionLink("Capture Form 5A2", "Form5AList", "MonitoringCapture") %></li>    
                                <%}%>  
                            <%}%>   

                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 52 && menuObj1.TaskStatus == 1)
                               {%>
                                    <li><%: Html.ActionLink("Capture Form 5B", "Form5BList", "MonitoringCapture") %></li>         
                                <%}%>  
                            <%}%>   

                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 54 && menuObj1.TaskStatus == 1)
                               {%>                                                      
                                    <li><%: Html.ActionLink("Capture Form 5C", "Form5CList", "MonitoringCapture") %></li>    
                                <%}%>  
                            <%}%>   
                        </ul>
                    </li>
                    <%}%>
                <%}%>

                 <%--Updates Module--%>
<%--                <% foreach(var menuObj in Model.moduleaccess)  {%>
                    <% if (menuObj.TaskID == 66 && menuObj.TaskStatus == 1)
                   {%>
                    <li style="border-bottom:1px solid #dddddd;"><a class="glyphicons display" href="#"><i></i><span>Updates</span></a>
                        <ul class="sub"> 
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 67 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Report Engine", "ReportEngine", "Security") %></li>    
                                <%}%>  
                            <%}%>        
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 67 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Report Export", "ReportExport", "Security") %></li>    
                                <%}%>  
                            <%}%>                                                                                                                                                              
                        </ul>
                    </li>
                    <%}%>
                <%}%>--%>

                <%--Retargeting Module--%>
                <% foreach(var menuObj in Model.moduleaccess)  {%>
                    <% if (menuObj.TaskID == 96)
                   {%>
                    <li style="border-bottom:1px solid #dddddd;"><a class="glyphicons list" href="#"><i></i><span>Re-Targeting</span></a>
                        <ul class="sub"> 
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 97 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Form 7", "Form7Summary", "ReTargeting") %></li>    
                                <%}%>  
                            <%}%>                                                                                                                                                            
                        </ul>
                    </li>
                    <%}%>
                <%}%>

                <%--Administration Module--%>
                <% foreach(var menuObj in Model.moduleaccess)  {%>
                    <% if (menuObj.TaskID == 68 && menuObj.TaskStatus == 1)
                   {%>
                    <li style="border-bottom:1px solid #dddddd;"><a class="glyphicons cogwheel" href="#"><i></i><span>Administration</span></a>
                        <ul class="sub"> 
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 69 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Define Region", "Regions", "Administration") %></li>    
                                <%}%>  
                            <%}%>        
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 72 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Define Woreda", "Woredas", "Administration") %></li>    
                                <%}%>  
                            <%}%>  
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 75 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Define Kebele", "Kebeles", "Administration") %></li>    
                                <%}%>  
                            <%}%>
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 78 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Default Woreda", "DefaultWoreda", "Administration") %></li>    
                                <%}%>  
                            <%}%>        
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 79 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Reporting Period", "ReportingPeriod", "Administration") %></li>    
                                <%}%>  
                            <%}%>     
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 82 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Household Visits", "HouseholdVisits", "Administration") %></li>    
                                <%}%>  
                            <%}%> 
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 89 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Data Export", "DataExport", "Administration") %></li>    
                                <%}%>  
                            <%}%>         
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 91 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Data Import", "DataImport", "Administration") %></li>    
                                <%}%>  
                            <%}%>      
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 94 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Edit Financial Years", "FinancialYears", "Administration") %></li>    
                                <%}%>  
                            <%}%> 
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 95 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Close Financial Year", "CloseFinancialYear", "Administration") %></li>    
                                <%}%>  
                            <%}%>                                                                                                                                                                                                   
                        </ul>
                    </li>
                    <%}%>
                <%}%>

                 <%--Report Engine Module--%>
                <% foreach(var menuObj in Model.moduleaccess)  {%>
                    <% if (menuObj.TaskID == 66 && menuObj.TaskStatus == 1)
                   {%>
                    <li style="border-bottom:1px solid #dddddd;"><a class="glyphicons display" href="#"><i></i><span>Reporting</span></a>
                        <ul class="sub"> 
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 67 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Standard Reports", "StandardReports", "Security") %></li>    
                                <%}%>  
                            <%}%>  
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 67 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Report Engine", "ReportEngine", "Security") %></li>    
                                <%}%>  
                            <%}%>        
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 67 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Report Export", "ReportExport", "Security") %></li>    
                                <%}%>  
                            <%}%>                                                                                                                                                              
                        </ul>
                    </li>
                    <%}%>
                <%}%>

                 <%--Security Module--%>
                <% foreach(var menuObj in Model.moduleaccess)  {%>
                    <% if (menuObj.TaskID == 56 && menuObj.TaskStatus == 1)
                   {%>
                    <li style="border-bottom:1px solid #dddddd;"><a class="glyphicons settings" href="#"><i></i><span>Security</span></a>
                        <ul class="sub"> 
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 57 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Roles", "Roles", "Security") %></li>    
                                <%}%>  
                            <%}%>  
                            
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 61 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Users", "Users", "Security") %></li>     
                                <%}%>  
                            <%}%>   
                                                                      
                            <% foreach(var menuObj1 in Model.moduleaccess)  {%> 
                                <% if (menuObj1.TaskID == 65 && menuObj1.TaskStatus == 1)
                               {%>                              
                                    <li><%: Html.ActionLink("Audit Trail", "AuditTrail", "Security") %></li>     
                                <%}%>  
                            <%}%>                                                     
                            
                        </ul>
                    </li>
                    <%}%>
                <%}%>

            </ul>
            
            <br />
            <br />
            <br />

            <div id="menu-imglogo">
                <img src="../Images/MolasaLogo.png" style="height: 150px;width: 150px" />
            </div>

    </div>


