﻿using SCTMis.Models;
using SCTMis.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.IO.Packaging;
using System.Text;
using System.Web.Security;
using System.Data.SqlClient;

namespace SCTMis.Controllers
{
    [SessionConfig.SessionExpireFilter]
    public class AdministrationController : Controller
    {
        GeneralServices dbService = new GeneralServices();
        AccountServices dbAcService = new AccountServices();
        AdministrationServices dbAdService = new AdministrationServices();
        private const long BUFFER_SIZE = 4096;

        string ip = System.Web.HttpContext.Current.Request.UserHostAddress;
        //
        // GET: /Administration/
        
        [Authorize]
        public ActionResult FinancialYears()
        {

            ViewBag.Subtitle = "Financial Years Listing";
            dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Financial Years List View", ip, "",
            "Financial Years List Access  at " + DateTime.Now.ToString());

            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument FinancialYearsList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = SCTMis.Services.GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = SCTMis.Services.GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = SCTMis.Services.GeneralServices.strQueryString("SearchKeyword", string.Empty);

                Tuple<List<int>, List<FinancialYearGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, FinancialYearGrid>(";Exec GetFiscalYearsAdminList @jtStartIndex, @jtPageSize, @isSearch, @SearchTypeID, @SearchKeyword",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });

                DataTable dt = GeneralServices.ToDataTable<FinancialYearGrid>(resultset.Item2);

                string sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 1);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        [Authorize]
        public ActionResult EditFinancialYear(FinancialYear editmodel)
        {
            ViewBag.Subtitle = "Edit Financial Year";

            editmodel = dbAdService.GetFetchFinancialYear(editmodel);

            return View(editmodel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult UpdateFinancialYear(FinancialYear updatemodel)
        {
            ViewBag.Subtitle = "Update Financial Year";
            string errMsg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbAdService.UpdateFinancialYear(updatemodel, out errMsg);
                    if (string.IsNullOrEmpty(errMsg))
                    {
                        dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Update Financial Year", ip, "",
                        "Update Financial Year " + updatemodel.FiscalYear + "updated successfuly  at " + DateTime.Now.ToString());

                        ViewBag.Subtitle = "Financial Year Listing";
                        return RedirectToAction("FinancialYears");
                    }
                }

                if (string.IsNullOrEmpty(errMsg))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMsg);
                }
                
                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Update Financial Year Error", ip, "",
                "Update Financial Year " + updatemodel.FiscalYear + "Was not added because of error " + errMsg + "  at " + DateTime.Now.ToString());

                return RedirectToAction("EditFinancialYear", updatemodel);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Update Financial Year Error", ip, "",
                "Update Financial Year " + updatemodel.FiscalYear + "Was not added because of error " + errMsg + "  at " + DateTime.Now.ToString());

                GeneralServices.LogError(ex);
                return RedirectToAction("EditFinancialYear", updatemodel);
            }
        }

        [Authorize]
        public ActionResult CloseFinancialYear()
        {
            ViewBag.Subtitle = "Close Financial Year";
            dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "CloseFinancialYear", ip, "",
            "Close Financial Year Viewed  at " + DateTime.Now.ToString());

            var currentFY = dbService.GetCurrentFiscalYear();

            ViewBag.FiscalYearName = currentFY.FiscalYearName;

            return View(currentFY);
        }

        [HttpPost]
        [Authorize]
        public ActionResult CloseFinancialYear(FinancialYear newmodel)
        {
            ViewBag.Subtitle = "Close Financial Year";

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbAdService.CloseFinancialYear(newmodel);
                    
                    dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Close Financial Year", ip, "",
                    "Fiscal Year " + newmodel.FiscalYear + "-" + newmodel.FiscalYearName + " Closed Successfuly  at " + DateTime.Now.ToString());

                    var currentFY = dbService.GetCurrentFiscalYear();
                    Session["CurrentFiscalYear"] = string.Format("Financial Year : {0}", currentFY.FiscalYearName);
                    ViewBag.FiscalYearName = currentFY.FiscalYearName;

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                    
                    dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Close Financial Year", ip, "",
                    "Fiscal Year with Message " + message + " for " + newmodel.FiscalYear + "-" + newmodel.FiscalYearName + " Encountered an error at " + DateTime.Now.ToString());

                    return View("CloseFinancialYear", newmodel);
                }
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Close Financial Year", ip, "",
                "Fiscal Year with Message " + ex.Message + " for " + newmodel.FiscalYear + "-" + newmodel.FiscalYearName + " Encountered an error at " + DateTime.Now.ToString());

                return View("CloseFinancialYear", newmodel);
            }
        }

        [Authorize]
        public ActionResult Regions()
        {

            ViewBag.Subtitle = "Region Listing";
            dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Region List View", ip, "",
            "Region List Access  at " + DateTime.Now.ToString());

            return View();
        }

        [Authorize]
        public ActionResult AddRegion()
        {
            ViewBag.Subtitle = "Add New Region";
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddRegion(RegionAdd newmodel)
        {
            ViewBag.Subtitle = "Add New Region";
            string errMsg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbAdService.InsertRegion(newmodel, out errMsg);
                    if (string.IsNullOrEmpty(errMsg))
                    {
                        ViewBag.Subtitle = "Region Listing";

                        dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Region", ip, "",
                        "New Region " + newmodel.RegionName + "Added successfuly  at " + DateTime.Now.ToString());
                        return RedirectToAction("Regions");
                    }

                }

                if (string.IsNullOrEmpty(errMsg))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMsg);
                }

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Region Error", ip, "",
                "New Region " + newmodel.RegionName + "Was not added because of error " + errMsg + "  at " + DateTime.Now.ToString());

                return View("AddRegion", newmodel);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Region Error", ip, "",
                "New Region " + newmodel.RegionName + "Was not added because of error " + ex.Message + "  at " + DateTime.Now.ToString());

                GeneralServices.LogError(ex);
                return View("AddRegion", newmodel);
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument RegionsList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = SCTMis.Services.GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = SCTMis.Services.GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = SCTMis.Services.GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = SCTMis.Services.GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = SCTMis.Services.GeneralServices.strQueryString("SearchKeyword", string.Empty);

                Tuple<List<int>, List<RegionGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, RegionGrid>(";Exec getRegionList @jtStartIndex, @jtPageSize,@isSearch,@SearchTypeID,@SearchKeyword",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<RegionGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 1);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }
        //Edit Region
        [Authorize]
        public ActionResult EditRegion(RegionModifyModel editmodel)
        {
            ViewBag.Subtitle = "Edit Region";
            editmodel = dbAdService.GetRegionByID(editmodel);
            return View(editmodel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult UpdateRegion(RegionModifyModel updatemodel)
        {
            ViewBag.Subtitle = "Update Region";
            string errMsg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbAdService.UpdateRegion(updatemodel, out errMsg);
                    if (string.IsNullOrEmpty(errMsg))
                    {
                        dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Update Region", ip, "",
                        "Update Region " + updatemodel.RegionName + "updated successfuly  at " + DateTime.Now.ToString());

                        ViewBag.Subtitle = "Region Listing";
                        return RedirectToAction("Regions");
                    }

                }

                if (string.IsNullOrEmpty(errMsg))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMsg);
                }
                //updatemodel.regions = dbService.GetRegions();

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Update Region Error", ip, "",
                "Update Region " + updatemodel.RegionName + "Was not added because of error " + errMsg + "  at " + DateTime.Now.ToString());

                return RedirectToAction("EditRegion", updatemodel);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

                //newmodel.regions = dbService.GetRegions();

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Update Region Error", ip, "",
                "Update Region " + updatemodel.RegionName + "Was not added because of error " + errMsg + "  at " + DateTime.Now.ToString());

                GeneralServices.LogError(ex);
                return RedirectToAction("EditRegion", updatemodel);
            }
        }

        [Authorize]
        public ActionResult EditWoreda(WoredaModifyModel editmodel)
        {
            ViewBag.Subtitle = "Edit Woreda";
            editmodel = dbAdService.GetWoredaByID(editmodel);
            editmodel.regions = dbService.GetRegions();
            return View(editmodel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult UpdateWoreda(WoredaModifyModel updatemodel)
        {
            ViewBag.Subtitle = "Update Woreda";
            string errMsg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbAdService.UpdateWoreda(updatemodel, out errMsg);
                    if (string.IsNullOrEmpty(errMsg))
                    {
                        dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Update Woreda", ip, "",
                        "Update Woreda " + updatemodel.WoredaName + "updated successfuly  at " + DateTime.Now.ToString());

                        ViewBag.Subtitle = "Woreda Listing";
                        return RedirectToAction("Woredas");
                    }
                }

                if (string.IsNullOrEmpty(errMsg))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMsg);
                }
                
                updatemodel.regions = dbService.GetRegions();

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Update Woreda Error", ip, "",
                "Update Woreda " + updatemodel.WoredaName + "Was not added because of error " + errMsg + "  at " + DateTime.Now.ToString());

                return RedirectToAction("EditWoreda", updatemodel);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

                updatemodel.regions = dbService.GetRegions();

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Update Woreda Error", ip, "",
                "Update Woreda " + updatemodel.WoredaName + "Was not added because of error " + errMsg + "  at " + DateTime.Now.ToString());

                GeneralServices.LogError(ex);
                return RedirectToAction("EditWoreda", updatemodel);
            }
        }

        [Authorize]
        public ActionResult EditKebele(KebeleModifyModel editmodel)
        {
            ViewBag.Subtitle = "Edit Kebele";

            editmodel = dbAdService.GetKebeleByID(editmodel);
            editmodel.regions = dbService.GetRegions();
            editmodel.woredas = dbService.GetWoredas(editmodel.RegionID);
            return View(editmodel);
        }



        [HttpPost]
        [Authorize]
        public ActionResult UpdateKebele(KebeleModifyModel updatemodel)
        {
            ViewBag.Subtitle = "Update Kebele";
            string errMsg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbAdService.UpdateKebele(updatemodel, out errMsg);
                    if (string.IsNullOrEmpty(errMsg))
                    {
                        dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Update Kebele", ip, "",
                        "Update Kebele " + updatemodel.KebeleName + "updated successfuly  at " + DateTime.Now.ToString());

                        ViewBag.Subtitle = "Kebele Listing";
                        return RedirectToAction("Kebeles");
                    }
                }

                if (string.IsNullOrEmpty(errMsg))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMsg);
                }

                updatemodel.regions = dbService.GetRegions();
                updatemodel.woredas = dbService.GetWoredas(updatemodel.WoredaID);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Update Kebele Error", ip, "",
                "Update Kebele " + updatemodel.KebeleName + "Was not added because of error " + errMsg + "  at " + DateTime.Now.ToString());

                return RedirectToAction("EditKebele", updatemodel);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

                updatemodel.regions = dbService.GetRegions();
                updatemodel.woredas = dbService.GetWoredas(updatemodel.WoredaID);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Update Kebele Error", ip, "",
                "Update Kebele " + updatemodel.KebeleName + "Was not added because of error " + errMsg + "  at " + DateTime.Now.ToString());

                GeneralServices.LogError(ex);
                return RedirectToAction("EditKebele", updatemodel);
            }
        }
        //End of Regions
        [Authorize]
        public ActionResult Woredas()
        {

            ViewBag.Subtitle = "Woreda Management";
            dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Woreda List View", ip, "",
            "Woreda list Access  at " + DateTime.Now.ToString());

            return View();
        }

        [Authorize]
        public ActionResult AddWoreda()
        {
            ViewBag.Subtitle = "Add New Woreda";
            WoredaAdd myModel = new Models.WoredaAdd();
            myModel.regions = dbService.GetRegions();

            return View(myModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddWoreda(WoredaAdd newmodel)
        {
            ViewBag.Subtitle = "Add Woreda";
            string errMsg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbAdService.InsertWoreda(newmodel, out errMsg);
                    if (string.IsNullOrEmpty(errMsg))
                    {
                        dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Woreda", ip, "",
                        "New Woreda " + newmodel.WoredaName + "Added successfuly  at " + DateTime.Now.ToString());

                        ViewBag.Subtitle = "Woreda Listing";
                        return RedirectToAction("Woredas");
                    }

                }

                if (string.IsNullOrEmpty(errMsg))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMsg);
                }
                newmodel.regions = dbService.GetRegions();

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Woreda Error", ip, "",
                "New Woreda " + newmodel.WoredaName + "Was not added because of error " + errMsg + "  at " + DateTime.Now.ToString());

                return View("AddWoreda", newmodel);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

                newmodel.regions = dbService.GetRegions();

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Woreda Error", ip, "",
                "New Woreda " + newmodel.WoredaName + "Was not added because of error " + ex.Message + "  at " + DateTime.Now.ToString());

                return View("AddWoreda", newmodel);
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument WoredasList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = SCTMis.Services.GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = SCTMis.Services.GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = SCTMis.Services.GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = SCTMis.Services.GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = SCTMis.Services.GeneralServices.strQueryString("SearchKeyword", string.Empty);

                Tuple<List<int>, List<WoredaGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, WoredaGrid>(";Exec getWoredaList @jtStartIndex, @jtPageSize,@isSearch,@SearchTypeID,@SearchKeyword",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<WoredaGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 1);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        [Authorize]
        public ActionResult Kebeles()
        {
            ViewBag.Subtitle = "Kebele Management";
            dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Kebele List View", ip, "",
            "Kebele List Access  at " + DateTime.Now.ToString());

            return View();
        }

        [Authorize]
        public ActionResult AddKebele()
        {
            ViewBag.Subtitle = "Add New Kebele";
            KebeleAdd myModel = new Models.KebeleAdd();
            myModel.regions = dbService.GetRegions();
            myModel.woredas = dbService.GetWoredas(myModel.RegionID);
            return View(myModel);
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddKebele(KebeleAdd newmodel)
        {
            ViewBag.Subtitle = "Add Kebele";
            string errMsg = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbAdService.InsertKebele(newmodel, out errMsg);
                    if (string.IsNullOrEmpty(errMsg))
                    {
                        ViewBag.Subtitle = "Kebele Listing";

                        dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Kebele", ip, "",
                        "New Kebele " + newmodel.KebeleName + "Added successfuly  at " + DateTime.Now.ToString());

                        return RedirectToAction("Kebeles");
                    }

                }

                if (string.IsNullOrEmpty(errMsg))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMsg);
                }

                newmodel.regions = dbService.GetRegions();
                newmodel.woredas = dbService.GetWoredas(newmodel.RegionID);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Kebele Error", ip, "",
                "New Kebele " + newmodel.KebeleName + "Was not added because of error " + errMsg + "  at " + DateTime.Now.ToString());

                return View("AddKebele", newmodel);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                newmodel.regions = dbService.GetRegions();
                newmodel.woredas = dbService.GetWoredas(newmodel.RegionID);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Kebele Error", ip, "",
                "New Kebele " + newmodel.KebeleName + "Was not added because of error " + ex.Message + "  at " + DateTime.Now.ToString());

                return View("AddKebele", newmodel);
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument KebelesList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = SCTMis.Services.GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = SCTMis.Services.GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = SCTMis.Services.GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = SCTMis.Services.GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = SCTMis.Services.GeneralServices.strQueryString("SearchKeyword", string.Empty);

                Tuple<List<int>, List<KebeleGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, KebeleGrid>(";Exec getKebeleList @jtStartIndex, @jtPageSize,@isSearch,@SearchTypeID,@SearchKeyword",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<KebeleGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 1);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }
        //Household Visits
        [Authorize]
        public ActionResult HouseholdVisits()
        {

            ViewBag.Subtitle = "Household Visits";
            dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "View", ip, "",
            "Household Visits Access  at " + DateTime.Now.ToString());

            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument HouseholdVisitsList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = SCTMis.Services.GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = SCTMis.Services.GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = SCTMis.Services.GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = SCTMis.Services.GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = SCTMis.Services.GeneralServices.strQueryString("SearchKeyword", string.Empty);

                Tuple<List<int>, List<HouseholdVisit>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, HouseholdVisit>(";Exec GetHouseholdVisitList @jtStartIndex, @jtPageSize,@isSearch,@SearchTypeID,@SearchKeyword",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<HouseholdVisit>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 1);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("<xml></xml>");
            }
        }

        [Authorize]
        public ActionResult AddHouseholdVisit()
        {
            ViewBag.Subtitle = "Add Household Visit";

            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddHouseholdVisit(HouseholdVisit newmodel)
        {
            ViewBag.Subtitle = "Add Household Visit";
            string strOutMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbService.InsertHouseholdVisit(newmodel, out strOutMessage);
                    ViewBag.Subtitle = "Add Household Visit";
                    if (string.IsNullOrEmpty(strOutMessage))
                    {
                        dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Household Visit", ip, "",
                        "New Household Visit " + newmodel.HouseholdVisitName + "Added successfuly  at " + DateTime.Now.ToString());

                        return RedirectToAction("HouseholdVisits");
                    }

                    return RedirectToAction("HouseholdVisits");

                }
                else
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);

                    dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Household Visit Error", ip, "",
                    "New Household Visit " + newmodel.HouseholdVisitName + "Was not added because of error " + message + "  at " + DateTime.Now.ToString());

                    return View("AddHouseholdVisit", newmodel);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Household Visit Error", ip, "",
                "New Household Visit " + newmodel.HouseholdVisitName + "Was not added because of error " + ex.Message + "  at " + DateTime.Now.ToString());

                return View("AddHouseholdVisit", newmodel);
            }
        }

        //Household Visits Ends

        [Authorize]
        public ActionResult ReportingPeriod()
        {            
            ViewBag.Subtitle = "Reporting Period";
            dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "View", ip, "",
            "Reporting Period Access  at " + DateTime.Now.ToString());

            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument ReportPeriodList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = GeneralServices.strQueryString("SearchKeyword", string.Empty);

                Tuple<List<int>, List<ReportingPeriod>> resultset
                    = GeneralServices.NpocoConnection().FetchMultiple<int, ReportingPeriod>(";Exec GetReportingPeriodList @jtStartIndex, @jtPageSize,@isSearch,@SearchTypeID,@SearchKeyword",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });

                DataTable dt = GeneralServices.ToDataTable<ReportingPeriod>(resultset.Item2);

                string sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 1);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("<xml></xml>");
            }
        }

        [Authorize]
        public ActionResult AddReportingPeriod()
        {
            ViewBag.Subtitle = "Add Reporting Period";

            var currentFY = dbService.GetCurrentFiscalYear();

            ViewBag.PeriodFrom = currentFY.StartDate;
            ViewBag.PeriodTo = currentFY.EndDate;

            var newmodel = new ReportingPeriod();
            newmodel.FiscalYear = currentFY.FiscalYear;

            return View(newmodel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddReportingPeriod(ReportingPeriod newmodel)
        {
            ViewBag.Subtitle = "Add Reporting Period";
            string strOutMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbService.InsertReportingPeriod(newmodel, out strOutMessage);
                    ViewBag.Subtitle = "Add Reporting Period";
                    if (string.IsNullOrEmpty(strOutMessage))
                    {
                        dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Report Period", ip, "",
                        "New Report Period " + newmodel.PeriodName + "Added successfuly  at " + DateTime.Now.ToString());

                        return RedirectToAction("ReportingPeriod");
                    }
                    //return View("Form1A");
                    return RedirectToAction("ReportingPeriod");

                }
                else
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);

                    dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Report Period Error", ip, "",
                    "New Report Period " + newmodel.PeriodName + "Was not added because of error " + message + "  at " + DateTime.Now.ToString());

                    return View("AddReportingPeriod", newmodel);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "New Report Period Error", ip, "",
                "New Report Period " + newmodel.PeriodName + "Was not added because of error " + ex.Message + "  at " + DateTime.Now.ToString());

                GeneralServices.LogError(ex);

                return View("AddReportingPeriod", newmodel);
            }
        }

        [Authorize]
        public ActionResult DefaultWoreda()
        {
            ViewBag.Subtitle = "Default Woreda";

            DefaultWoredaModel models = new DefaultWoredaModel();
            models = dbService.FetchLoggedInWoreda();

            models.regions = dbService.GetRegions();
            models.woredas = dbService.GetWoredas(models.RegionID);
            return View(models);
        }

        [Authorize]
        [HttpPost]
        public ActionResult DefaultWoreda(DefaultWoredaModel updateModel)
        {
            ViewBag.Subtitle = "Default Woreda";
            string strOutMessage = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbService.FetchLoggedInWoreda(updateModel, out strOutMessage);
                    ViewBag.Subtitle = "Default Woreda";
                    if (string.IsNullOrEmpty(strOutMessage))
                    {
                        return RedirectToAction("DefaultWoreda");
                    }
                }

                if (string.IsNullOrEmpty(strOutMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", strOutMessage);
                }

                updateModel.regions = dbService.GetRegions();
                updateModel.woredas = dbService.GetWoredas(updateModel.RegionID);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Modify Woreda Error", ip, "",
                "Modify Woreda " + updateModel.WoredaName + "Was not added because of error " + strOutMessage + "  at " + DateTime.Now.ToString());

                return View("DefaultWoreda", updateModel);
                
            }
            catch (Exception ex)
            {
                updateModel.regions = dbService.GetRegions();
                updateModel.woredas = dbService.GetWoredas(updateModel.RegionID);

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Modify Woreda Error", ip, "",
                "Modify Woreda " + updateModel.WoredaName + "Was not added because of error " + ex.Message + "  at " + DateTime.Now.ToString());

                return View("DefaultWoreda", updateModel);
            }
        }

        [Authorize]
        public ActionResult DataExport()
        {
            ViewBag.Subtitle = "Data Export";
            dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "View", ip, "",
            "Data export Viewed  at " + DateTime.Now.ToString());

            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument DataExportList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = SCTMis.Services.GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = SCTMis.Services.GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = SCTMis.Services.GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = SCTMis.Services.GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = SCTMis.Services.GeneralServices.strQueryString("SearchKeyword", string.Empty);

                Tuple<List<int>, List<DataExportGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, DataExportGrid>(";Exec GetReplicatedData @jtStartIndex, @jtPageSize,@isSearch,@SearchTypeID,@SearchKeyword",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<DataExportGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 4);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        [Authorize]
        public ActionResult AddDataExport()
        {
            ViewBag.Subtitle = "Generate New Export Data";
            dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "View", ip, "",
            "Generate New Export Data  at " + DateTime.Now.ToString());

            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument ImportList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = SCTMis.Services.GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = SCTMis.Services.GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = SCTMis.Services.GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = SCTMis.Services.GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = SCTMis.Services.GeneralServices.strQueryString("SearchKeyword", string.Empty);

                Tuple<List<int>, List<DataImportGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, DataImportGrid>(";Exec GetImportedData @jtStartIndex, @jtPageSize,@isSearch,@SearchTypeID,@SearchKeyword",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<DataImportGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 4);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult PostDataExport()
        {
            ViewBag.Subtitle = "Generate New Export Data";
            string errMessage = string.Empty;
           // ReplicationDetail model = new ReplicationDetail();
            string fileName = fileName = string.Format(User.Identity.Name + "-{0:yyyy-MM-dd_hh-mm-ss-tt}.xml", DateTime.Now);
            string zipfileName = fileName + ".zip";
            Tuple<List<int>, List<DataExportPreviewGrid>> resultset;
            try
            {
                string strFilePath = string.Empty;

                resultset = GeneralServices.NpocoConnection().FetchMultiple<int, DataExportPreviewGrid>(";Exec GetReplicationData");
                GeneralServices.LogError(string.Format("GetReplicationData resultset {0}", resultset));

                DataTable ReplicateGridView = GeneralServices.ToDataTable<DataExportPreviewGrid>(resultset.Item2);

                foreach (DataRow row in ReplicateGridView.Rows)
                {
                    if ((row["xmlString"]).ToString().Length > 1)
                    {
                        string XmlfileName = (row["TableName"]).ToString() + "-" + fileName;

                        System.IO.File.WriteAllText(Server.MapPath("~/tempfile/") + XmlfileName, (row["xmlString"]).ToString(), Encoding.ASCII);
                        strFilePath = @"" + Server.MapPath("~/tempfile/" + XmlfileName.ToString());
                        AddFileToZip(@"" + Server.MapPath("~/Database/") + zipfileName, strFilePath);

                        //If the table has attachments that need to be part of the xip file, we loop them here
                        if ((row["filePath"]) != null)
                        {
                            string strfPath = (row["filePath"]).ToString();
                            if (strfPath.Length > 1)
                            {
                                List<string> rOut = strfPath.Split(',').ToList();
                                foreach (var val in rOut)
                                {
                                    string pdfValstr = string.Empty;
                                    pdfValstr = ReverseString(val);
                                    if (pdfValstr.StartsWith("fdp.") && System.IO.File.Exists(@"" + Server.MapPath("~/ReportFilePath/" + val.ToString())))
                                    {
                                        strFilePath = @"" + Server.MapPath("~/ReportFilePath/" + val.ToString());
                                        AddFileToZip(@"" + Server.MapPath("~/Database/") + zipfileName, strFilePath.ToString());
                                    }
                                }
                            }
                        }
                    }
                }

                var result = dbAdService.AddReplication(resultset.Item2, zipfileName, 1, out errMessage);
                if (!result)
                {
                    ModelState.AddModelError("", errMessage);

                    dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "Add Data Export Error", ip, "",
                    "Data Export Was not added because Error Occured while saving records  at " + DateTime.Now.ToString());

                    GeneralServices.LogError("Data Export Was not added because Error Occured while saving records  at " + DateTime.Now.ToString());

                    return View("AddDataExport");
                }
                else
                {
                    //db.messageKGIS(lblMessage, true, "Records Saved Successfuly!");

                    strFilePath = @"" + Server.MapPath("~/Database/" + zipfileName.ToString());
                    byte[] fileBytes = System.IO.File.ReadAllBytes(strFilePath);
                }
            }
            catch(Exception ee)
            {
                GeneralServices.LogError(ee);
                GeneralServices.LogError("PostDataExport");
            }

            return RedirectToAction("DataExport");
        }

        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        [HttpGet]
        [Authorize]
        public XDocument NewDataExportPreviewList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");
            try
            {
                Tuple<List<int>, List<DataExportPreviewGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, DataExportPreviewGrid>(";Exec GetReplicationData");

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<DataExportPreviewGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], 0, 0, 1);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        [Authorize]
        public ActionResult DataImport()
        {
            ViewBag.Subtitle = "Data Import";

            dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "View", ip, "",
            "Data Import Viewed  at " + DateTime.Now.ToString());

            return View();
        }

        [Authorize]
        public ActionResult Replicate()
        {
            ViewBag.Subtitle = "Data Replication";

            dbAcService.InsertAuditTrail(User.Identity.Name, "Administration", "View", ip, "",
            "Data Replication Viewed  at " + DateTime.Now.ToString());

            return View();
        }

        [HttpPost]
        public ActionResult FileUpload(HttpPostedFileBase file)
        {
            ViewBag.Subtitle = "Import Payment List";
            ImportListModel model = new ImportListModel();
            //DatabaseService dbService = new DatabaseService();
            Boolean hasBeenPosted = false;

            string srcDirectory = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/Uploads/");

            DirectoryInfo d = new DirectoryInfo(srcDirectory);//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.zip"); //Getting Text files
            string PrevfileName = "";

            foreach (FileInfo zipfile in Files)
            {
                PrevfileName = zipfile.Name;

                //if (file.FileName.ToString() == PrevfileName)
                //{
                //    hasBeenPosted = true;
                //}
                if (PrevfileName.IndexOf(file.FileName.ToString()) > 1)
                {
                    hasBeenPosted = true;
                }

            }

            if (hasBeenPosted != true)
            {
                if (ModelState.IsValid)
                {
                    if (file == null)
                    {
                        ModelState.AddModelError("File", "Please Upload Your file");
                    }
                    else if (file.ContentLength > 0)
                    {
                        int MaxContentLength = 1024 * 1024 * 100; //3 MB
                        string[] AllowedFileExtensions = new string[] { ".zip" };

                        if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                        {
                            ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                        }

                        else if (file.ContentLength > MaxContentLength)
                        {
                            ModelState.AddModelError("File", "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                        }
                        else
                        {
                            //TO:DO
                            string errMessage = string.Empty;
                            int ImportID = 0;
                            var fileName = Path.GetFileName(file.FileName);
                            //fileName = DateTime.Now.ToFileTime().ToString() + fileName;
                            string path = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/Uploads/" + fileName);
                            //var path = Path.Combine(Server.MapPath("~/Content/Upload"), fileName);
                            file.SaveAs(path);
                            ModelState.Clear();

                            model.ImportPath = fileName;
                            model.CreatedBy = int.Parse(Membership.GetUser().ProviderUserKey.ToString());
                            
                            var isValid = dbAcService.InsertReplicationList(model, path, User.Identity.Name, out errMessage, out ImportID);
                            if (isValid == true)
                            {
                                dbAcService.InsertAuditTrail(User.Identity.Name, "Import", "File Upload", ip, "",
                                "File Successfuly Uploaded. Original name: " + file.FileName.ToString() + ", archived name: " + fileName + " at " + DateTime.Now.ToString());

                                return View("Replicate");
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(errMessage))
                                {
                                    var message = string.Join(" | ", ModelState.Values
                                        .SelectMany(v => v.Errors)
                                        .Select(e => e.ErrorMessage));

                                    dbAcService.InsertAuditTrail(User.Identity.Name, "Import", "File Upload", ip, "",
                                    "File Upload failed with error: " + message + ". Original filename: " + file.FileName.ToString() + " at " + DateTime.Now.ToString());

                                    ModelState.AddModelError("", message);
                                }
                                else
                                {
                                    dbAcService.InsertAuditTrail(User.Identity.Name, "Import", "File Upload", ip, "",
                                    "File Upload failed with error: " + errMessage + ". Original filename: " + file.FileName.ToString() + " at " + DateTime.Now.ToString());

                                    ModelState.AddModelError("", errMessage);
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                dbAcService.InsertAuditTrail(User.Identity.Name, "Replicate", "File Upload", ip, "",
                "Replication failed with error: File has been replicated Already. Original filename: " + file.FileName.ToString() + " at " + DateTime.Now.ToString());

                ModelState.AddModelError("", "Selected file cannot be replicated! Double Import detected.");

                return View("Replicate");
            }
            //return View("ReplicateList", model);
            return View("DataImport");
        }

        private static void AddFileToZip(string zipFilename, string fileToAdd)
        {
            using (Package zip = System.IO.Packaging.Package.Open(zipFilename, FileMode.OpenOrCreate))
            {
                string destFilename = ".\\" + Path.GetFileName(fileToAdd);
                Uri uri = PackUriHelper.CreatePartUri(new Uri(destFilename, UriKind.Relative));
                if (zip.PartExists(uri))
                {
                    zip.DeletePart(uri);
                }
                PackagePart part = zip.CreatePart(uri, "", CompressionOption.Normal);
                using (FileStream fileStream = new FileStream(fileToAdd, FileMode.Open, FileAccess.Read))
                {
                    using (Stream dest = part.GetStream())
                    {
                        CopyStream(fileStream, dest);
                    }
                }
            }
        }

        private static void CopyStream(System.IO.FileStream inputStream, System.IO.Stream outputStream)
        {
            long bufferSize = inputStream.Length < BUFFER_SIZE ? inputStream.Length : BUFFER_SIZE;
            byte[] buffer = new byte[bufferSize];
            int bytesRead = 0;
            long bytesWritten = 0;
            while ((bytesRead = inputStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                outputStream.Write(buffer, 0, bytesRead);
                bytesWritten += bytesRead;
            }
        }

        [Authorize]
        public FileResult downloadzip()
        {
            string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Database/" + Request.QueryString["GenerationID"].ToString());
            switch (strReportFilePath.Substring(strReportFilePath.Length - 3).ToUpper())
            {
                case "ZIP":
                    byte[] fileBytes = System.IO.File.ReadAllBytes(strReportFilePath);
                    return File(fileBytes, "application/x-compressed-zip", Request.QueryString["GenerationID"].ToString());
                default:
                    return File(strReportFilePath, "application/zip");
            }
        }
    }
}
