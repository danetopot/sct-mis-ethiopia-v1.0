﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.KebeleAdd>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Add New Kebele
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   
<% using (Html.BeginForm(null, null, FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
        <div class="col-2">                
        <label>
            Region Name <%: Html.ValidationMessageFor(model => model.RegionID) %>
            <%: Html.DropDownListFor(model => model.RegionID,
                new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
        </label>
        </div>
        <div class="col-2">                
        <label>
            Woreda Name <%: Html.ValidationMessageFor(model => model.WoredaID) %>
            <%: Html.DropDownListFor(model => model.WoredaID,
                new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2", @class = "alphaonly" })%>
        </label>
        </div>
        <div class="col-2">                
        <label>
            Kebele Code <%: Html.ValidationMessageFor(model => model.KebeleCode) %>
            <%: Html.TextBoxFor(model => model.KebeleCode, new { tabindex = "3", @class = "alphaonly", @maxlength="3" })%>
        </label>
        </div>

        <div class="col-2">                
        <label>
            Kebele Name <%: Html.ValidationMessageFor(model => model.KebeleName) %>
            <%: Html.TextBoxFor(model => model.KebeleName, new { tabindex = "4", @class = "alphaonly", @maxlength="50" })%>
        </label>
        </div>
        <%: Html.ValidationSummary(true) %><br />

    <div class="form-actions">
	    <button id="cmdSave" type="submit" class="btn btn-icon btn-primary glyphicons circle_ok" tabindex = "5"><i></i>Save Kebele</button>
	    <button id="cmdCancel" type="button" class="btn btn-icon btn-default glyphicons circle_remove"><i></i>Back</button>
    </div>
    <% } %>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script>
        $(document).ready(function () {
            $("#cmdCancel").click(function () {
                window.location.href = "/Administration/Kebeles";
            });
            $('#loading').hide();
            $("#RegionID").focus()
        });
    </script>
</asp:Content>
