﻿using SCTMis.Models;
using SCTMis.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace SCTMis.Controllers
{
    [SessionConfig.SessionExpireFilter]
    public class MonitoringController : Controller
    {
        GeneralServices dbService = new GeneralServices();
        MonitoringServices dbCompliance = new MonitoringServices();

        //
        // GET: /Compliance/

        //Form 5A1 Starts
        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form5A1()
        {
            ViewBag.Subtitle = "Form 5A1 Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form5A1ListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");
                                
                Tuple<List<int>, List<Form5A1Grid>> resultset = GeneralServices.NpocoConnection().FetchMultiple<int, Form5A1Grid>(";Exec getForm5A1AdminList @jtStartIndex, @jtPageSize",
                new
                {
                    jtStartIndex = jtStartIndex,
                    jtPageSize = jtPageSize
                });

                DataTable dt = GeneralServices.ToDataTable<Form5A1Grid>(resultset.Item2);

                string sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 2);
               
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        [Authorize]
        public ActionResult ProduceForm5A1()
        {
            ViewBag.Subtitle = "Produce Form 5A1";

            ProduceForm5A1Model models = new Models.ProduceForm5A1Model();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("5A1", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm5A1(ProduceForm5A1Model newmodel)
        {
            ViewBag.Subtitle = "Produce Form 5A1";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.ProduceForm5A1(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Form 5A1 Listing";

                        return RedirectToAction("Form5A1");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("5A1", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                return View("ProduceForm5A1", newmodel);

            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("5A1", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("ProduceForm5A1", newmodel);
            }
        }

        //Form 5A1 Ends
        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form5A()
        {
            ViewBag.Subtitle = "Form 5A2 Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form5AListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");                               

                Tuple<List<int>, List<Form5AGrid>> resultset = GeneralServices.NpocoConnection().FetchMultiple<int, Form5AGrid>(";Exec getForm5AAdminList @jtStartIndex, @jtPageSize",
                new
                {
                    jtStartIndex = jtStartIndex,
                    jtPageSize = jtPageSize
                });

                DataTable dt = GeneralServices.ToDataTable<Form5AGrid>(resultset.Item2);

                string sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 2);

                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        //ProduceForm2A
        [Authorize]
        public ActionResult ProduceForm5A()
        {
            ViewBag.Subtitle = "Produce Form 5A2";

            ProduceForm5AModel models = new Models.ProduceForm5AModel();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("5A2", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm5A(ProduceForm5AModel newmodel)
        {
            ViewBag.Subtitle = "Produce Form 5A2";
            string errMessage = string.Empty;
            
            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.ProduceForm5A(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Form 5A2 Listing";

                        return RedirectToAction("Form5A");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("5A2", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                return View("ProduceForm5A", newmodel);
                
            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("5A2", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("ProduceForm5A", newmodel);
            }
        }

        //FORM 5B

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form5B()
        {
            ViewBag.Subtitle = "Form 5B Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form5BListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");                               

                Tuple<List<int>, List<Form5AGrid>> resultset = GeneralServices.NpocoConnection().FetchMultiple<int, Form5AGrid>(";Exec getForm5BAdminList @jtStartIndex, @jtPageSize",
                new
                {
                    jtStartIndex = jtStartIndex,
                    jtPageSize = jtPageSize
                });

                DataTable dt = GeneralServices.ToDataTable<Form5AGrid>(resultset.Item2);

                string sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 2);

                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }
        //ProduceForm5B
        [Authorize]
        public ActionResult ProduceForm5B()
        {
            ViewBag.Subtitle = "Produce Form 5B";

            ProduceForm5AModel models = new Models.ProduceForm5AModel();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("5B", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm5B(ProduceForm5AModel newmodel)
        {
            ViewBag.Subtitle = "Produce Form 5B";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.ProduceForm5B(newmodel, out errMessage);

                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Form 5B Listing";

                        return RedirectToAction("Form5B");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("5B", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                return View("ProduceForm5B", newmodel);
                
            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("5B", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("ProduceForm5B", newmodel);
            }
        }

        //FORM 5C

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form5C()
        {
            ViewBag.Subtitle = "Form 5C Listing";
            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form5CListAdmin()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = 0;

                if (Request.QueryString["posStart"] != null) { jtStartIndex = int.Parse(Request.QueryString["posStart"].ToString()); }

                int jtPageSize = int.Parse(Request.QueryString["RecCount"].ToString());

                var con = new PetaPoco.Database("conString");                              

                Tuple<List<int>, List<Form5AGrid>> resultset = GeneralServices.NpocoConnection().FetchMultiple<int, Form5AGrid>(";Exec getForm5CAdminList @jtStartIndex, @jtPageSize",
                new
                {
                    jtStartIndex = jtStartIndex,
                    jtPageSize = jtPageSize
                });

                DataTable dt = GeneralServices.ToDataTable<Form5AGrid>(resultset.Item2);

                string sb = GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 2);

                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }
        //ProduceForm2C
        [Authorize]
        public ActionResult ProduceForm5C()
        {
            ViewBag.Subtitle = "Produce Form 5C";

            ProduceForm5AModel models = new Models.ProduceForm5AModel();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetDataKebeles("5C", int.Parse(Session["WoredaID"].ToString()));
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            models.householdvsts = dbService.GetHouseholdVisits();

            return View(models);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProduceForm5C(ProduceForm5AModel newmodel)
        {
            ViewBag.Subtitle = "Produce Form 5C";
            string errMessage = string.Empty;
            
            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.ProduceForm5C(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Form 5C Listing";
                        return RedirectToAction("Form5C");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("5C", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();
                newmodel.householdvsts = dbService.GetHouseholdVisits();

                    return View("ProduceForm5C", newmodel);
                
            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetDataKebeles("5C", int.Parse(Session["WoredaID"].ToString()));
                newmodel.reportgperiod = dbService.GetReportingPeriods();
                newmodel.householdvsts = dbService.GetHouseholdVisits();

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("ProduceForm5C", newmodel);
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult SelectService(ServiceProviders model)
        {
            var ServiceDetails = dbService.GetIntegratedServices(model.ServiceProviderID, model.FormID);
            return Json(ServiceDetails, JsonRequestBehavior.AllowGet);
        }

    }
}
