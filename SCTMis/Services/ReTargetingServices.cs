﻿using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Web.Security;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;
using System.Web.Script.Serialization;

namespace SCTMis.Services
{
    public class ReTargetingServices
    {
        public bool GenerateForm7(GenerateForm7 newModel)
        {
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");                               

                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec GenerateForm7 @KebeleID,@FiscalYear,@CreatedBy",
                    new
                    {
                        KebeleID = newModel.KebeleID,
                        FiscalYear = newModel.FiscalYear,
                        CreatedBy = Membership.GetUser().UserName.ToString()
                    });

                return true;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool Form7Report(string kebeleID, string createdBy, out string errMessage, out string filename)
        {
            string rptFileName = string.Empty;
            string rptSP = string.Empty;
            errMessage = string.Empty;
            filename = string.Empty;
            try
            {
                rptFileName = "PrintForm7";
                rptSP = "PrintForm7";

                Tuple<List<Form7ReportHeader>, List<Form7Report>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<Form7ReportHeader, Form7Report>(";Exec PrintForm7 @KebeleID",
                    new
                    {
                        KebeleID = kebeleID
                    });

                DataTable dtAdminStructure = SCTMis.Services.GeneralServices.ToDataTable<Form7ReportHeader>(resultset.Item1);

                foreach (DataRow dr in dtAdminStructure.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form7Report>(resultset.Item2);

                if (dtDetails.Rows.Count > 0)
                {

                    filename = DateTime.Now.ToFileTime().ToString();
                    filename = filename + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/" + rptFileName + ".rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);

                    myReportDocument.Database.Tables[0].SetDataSource(dtAdminStructure);
                    myReportDocument.Subreports[0].Database.Tables[0].SetDataSource(dtDetails);
                                        
                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();
                }
            }
            catch (Exception ee)
            {
                errMessage = ee.Message;
                GeneralServices.LogError(ee);
                return false;
            }
            return true;
        }

        public Form7Header FetchForm7Header(string retargetingHeaderId)
        {
            Form7Header dtls = new Form7Header();

            List<Form7Header> results
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form7Header>(";Exec FetchForm7Header @RetargetingHeaderID, @LoggedInUser",
                 new
                 {
                     RetargetingHeaderID = retargetingHeaderId,
                     LoggedInUser = Membership.GetUser().UserName.ToString()
                 });

            DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form7Header>(results);

            if (dtDetails.Rows.Count > 0)
            {
                DataRow row = dtDetails.Rows[0];

                dtls.RetargetingHeaderID = row["RetargetingHeaderID"].ToString();
                dtls.RegionID = int.Parse(row["RegionID"].ToString());
                dtls.RegionName = row["RegionName"].ToString();
                dtls.WoredaID = int.Parse(row["WoredaID"].ToString());
                dtls.WoredaName = row["WoredaName"].ToString();
                dtls.KebeleID = int.Parse(row["KebeleID"].ToString());
                dtls.KebeleName = row["KebeleName"].ToString();
                dtls.Gote = row["Gote"].ToString();
                dtls.Gare = row["Gare"].ToString();
                dtls.NameOfHouseHoldHead = row["NameOfHouseHoldHead"].ToString();
                dtls.HouseHoldIDNumber = row["HouseHoldIDNumber"].ToString();
                dtls.FiscalYear = int.Parse(row["FiscalYear"].ToString());
                dtls.DateUpdated = row["DateUpdated"].ToString();
                dtls.SocialWorker = row["SocialWorker"].ToString();
                dtls.CCCCBSPCMember = row["CCCCBSPCMember"].ToString();
                dtls.CreatedBy = row["CreatedBy"].ToString();
                dtls.CreatedOn = row["CreatedOn"].ToString();
                dtls.ApprovalStatus = Boolean.Parse(row["ApprovalStatus"].ToString());
                dtls.ApprovedBy = row["ApprovedBy"].ToString();
                dtls.ApprovedOn = row["ApprovedOn"].ToString();
                dtls.CreatedBy = row["CreatedBy"].ToString();
                dtls.AllowEdit = Boolean.Parse(row["AllowEdit"].ToString());
                //dtls.AllowEdit = true; // Boolean.Parse(row["AllowEdit"].ToString());
            }
            return dtls;
        }

        public bool UpdateForm7(Form7Header newModel)
        {
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");

                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                Form7Header[] MemberList = objJavascript.Deserialize<Form7Header[]>(newModel.MemberXml);

                foreach (var array in MemberList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<RetargetingDetailID>" + array.RetargetingDetailID + "</RetargetingDetailID>";
                    strXML = strXML + "<Status>" + array.Status + "</Status>";
                    strXML = strXML + "<Pregnant>" + array.Pregnant + "</Pregnant>";
                    strXML = strXML + "<Lactating>" + array.Lactating + "</Lactating>";                    
                    strXML = strXML + "<Handicapped>" + array.Handicapped + "</Handicapped>";
                    strXML = strXML + "<ChronicallyIll>" + array.ChronicallyIll + "</ChronicallyIll>";
                    strXML = strXML + "<NutritionalStatus>" + array.NutritionalStatus + "</NutritionalStatus>";
                    strXML = strXML + "<ChildUnderTSForCMAM>" + array.ChildUnderTSForCMAM + "</ChildUnderTSForCMAM>";
                    strXML = strXML + "<EnrolledInSchool>" + array.EnrolledInSchool + "</EnrolledInSchool>";                    
                    strXML = strXML + "<SchoolName>" + array.SchoolName + "</SchoolName>";                    
                    strXML = strXML + "</row>";
                    Console.WriteLine(strXML);
                }

                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec UpdateForm7 @RetargetingHeaderID, @DateUpdated, @SocialWorker, @CCCCBSPCMember, @MemberXML, @CreatedBy",
                     new
                     {
                         RetargetingHeaderID = newModel.RetargetingHeaderID,
                         DateUpdated = newModel.DateUpdated,
                         SocialWorker = newModel.SocialWorker,
                         CCCCBSPCMember = newModel.CCCCBSPCMember,
                         MemberXML = strXML,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool ApproveForm7(Form7Header newModel)
        {
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");

                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec ApproveForm7 @RetargetingHeaderID, @DateUpdated, @SocialWorker, @CCCCBSPCMember, @UpdatedBy",
                    new
                    {
                        RetargetingHeaderID = newModel.RetargetingHeaderID,
                        DateUpdated = newModel.DateUpdated,
                        SocialWorker = newModel.SocialWorker,
                        CCCCBSPCMember = newModel.CCCCBSPCMember,
                        UpdatedBy = Membership.GetUser().UserName.ToString()
                    });

                return true;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return false;
            }
        }
    }
}