﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace SCTMis.Models
{

    public class UserDetailsModel
    {
        public int ColumnID { get; set; }
        public int ID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public string BranchID { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string UserName { get; set; }
        public string HdnUserName { get; set; }

        //[Required(ErrorMessage = "Field is required")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string Mobile { get; set; }

        public string AccountID { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string RoleID { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string IsAgent { get; set; }

        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public string ModifiedBy { get; set; }
    }

    public class RecordCount
    {
        public int TotalRecords { get; set; }
    }

    public class UserAccessRights
    {
        public IEnumerable<ModuleAccess> moduleaccess { get; set; }
    }

    public class ModuleAccess
    {
        public int TaskID { get; set; }
        public int ParentTaskID { get; set; }
        public string TaskName { get; set; }
        public int TaskStatus { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterUserModel
    {
        public int ColumnID { get; set; }
        public int UserID { get; set; }
        [Required(ErrorMessage = "Login ID is required")]
        [Display(Name = "User name")]        
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password   is required")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "First name is required")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        //[Required(ErrorMessage = "Email Address is required")]
        //[Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "The Email address is not Valid")]
        public string Email { get; set; }

        //[Required(ErrorMessage = "Mobile is required")]
        public string Mobile { get; set; }
        [Required(ErrorMessage = "Role is required")]
        [Display(Name = "Role")]
        public int RoleID { get; set; }

        public IEnumerable<AccessRoles> roles { get; set; }
    }

    public class UpdateRegisterUserModel
    {
        public int UserID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        //[Display(Name = "Email")]
        //[Required(ErrorMessage = "Field is required")]
        public string Email { get; set; }

        //[Display(Name = "Mobile")]
        //[Required(ErrorMessage = "Field is required")]
        public string Mobile { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [Display(Name = "Role")]
        public int RoleID { get; set; }

        public IEnumerable<AccessRoles> roles { get; set; }
    }

    public class PasswordUserModel
    {
        public int UserID { get; set; }
        [Required]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

    }

    public class ModifyPasswordModel
    {
        public int UserID { get; set; }
        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

    }

    public class Roles
    {
        public string RoleID { get; set; }
        public string RoleName { get; set; }
    }

    public class AccessRoles
    {
        public string RoleID { get; set; }
        public string RoleName { get; set; }
    }

    public class RegisterRoleModel
    {
        [Display(Name = "User name")]
        public string RoleID { get; set; }

        [Required(ErrorMessage = "Role Name is required")]
        [Display(Name = "Role Name")]
        public string RoleName { get; set; }
    }

    public class EditRoleProfile
    {
        public int TaskID { get; set; }
        public int TaskStatus { get; set; }
    }
    public class ProfileObj
    {
        public string TaskID { get; set; }
        public string TaskStatus { get; set; }
    }
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }

    public class UserList
    {
        public int ColumnID { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
    }
}
