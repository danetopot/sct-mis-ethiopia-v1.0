﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Users
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="gridbox" style="position:relative; width:721px; height: 320px"></div>
    <div id="gridfooter">
        <div><span id="pagingArea"></span>&nbsp;<span id="infoArea"></span></div><span id="recfound"></span>
    </div>
    <div class="form-actions">
	    <button id="cmdNew" onclick="return window.location = '../Security/Register';" type="button" class="btn btn-primary btn-icon glyphicons user_add"><i></i>New</button>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <script src="../Scripts/assets/js/app/UsersView.js"></script>
</asp:Content>
