﻿var EVENTID = null;
var arrObj = [];
var SerialCount = 0;
var page_count = 4;
var mygrid;
var gridHeader = 'ColumnID,Member<span class="HeaderChange">_</span>Name';
gridHeader = gridHeader + ',Individual<span class="HeaderChange">_</span>ID,Age,Gender,Pregnant,Lactating,Handicapped';
gridHeader = gridHeader + ',Chronically<span class="HeaderChange">_</span>Ill,Nutrition';
gridHeader = gridHeader + ',Enrolled,School<span class="HeaderChange">_</span>Name,Child Protection Risk, Edit';
var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,img';

var myDOB,myColDate;
$(document).ready(function () {
    $('#loading').hide();

    $('#CollectionDate').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        format: 'd/M/Y'
    });

    $('#DateOfBirth').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        format: 'd/M/Y',
        //view: 'years',
        onfocusout : function () {
            DobChange();
        }
    });

    DisableEnableGrade("D");
    $("#KebeleID,#Gote,#NameOfHouseHoldHead,#HouseHoldIDNumber,#Gare,#CollectionDate,#SocialWorker,#CCCCBSPCMember,#Remarks").prop("disabled", false);
    $("#HouseHoldMemberName,#IndividualID,#Pregnant,#Lactating,#childUnderTSForCMAM,#Age,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#ChildProtectionRisk,#CBHINumber").prop("disabled", true);

    $("#RegionID,#WoredaID").attr("disabled", true);
    $("#Gote,#NameOfHouseHoldHead,#HouseHoldIDNumber,#Gare,#HouseHoldMemberName,#DateOfBirth,#Age,#SchoolName,#CBHINumber").val("");
    $("#Sex,#IndividualID,#Grade,#CollectionDate,#CCCCBSPCMember,#Remarks").val("");
    $("#SocialWorker,#KebeleID").val("0");
    $("#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#CBHIMembership,#ChildProtectionRisk").val("-");
    $("#cmdProceed,#cmdSave").prop("disabled", true);

    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,150,0,50,80,80,0,0,120,90,90,155,85,80");
    mygrid.setColAlign("left,left,right,left,right,right,left,left,right,left,right,right,right,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    //mygrid.enableAutoHeight(true);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();

    dhtmlxError.catchError("ALL", my_error_handler);

    $('#KebeleID').focus();

    $("#cmdBack").click(function () {
        window.location.href = "/Household/Form1A";
    });

    $('#KebeleID,#NameOfHouseHoldHead,#HouseHoldIDNumber,#CollectionDate,#SocialWorker,#CCCCBSPCMember').bind('focusout', function (event) {
        if (!isValid($(this).val())) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $(this).off("blur");
            $(this).focus();
        }
    });

    $('#HouseHoldMemberName,#DateOfBirth,#Sex,#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#ChildProtectionRisk').bind('focusout', function (event) {
        if (!isValid($(this).val())) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $(this).off("blur");
            $(this).focus();
        }
    });

    
    $('#CCCCBSPCMember').bind('focusout', function (event) {
        if($("#CBHINumber").is(":disabled")){
            if (isValid($('#KebeleID').val()) && isValid($('#NameOfHouseHoldHead').val()) && isValid($('#HouseHoldIDNumber').val()) && isValid($('#CollectionDate').val()) && isValid($('#SocialWorker').val())) {
                $('#RegionID,#WoredaID').prop("disabled", true); 
                $("#Kebele").val($("#KebeleID").val());
                $("#DateOfBirth,#HouseHoldMemberName,#IndividualID,#Sex,#Handicapped,#ChronicallyIll,#ChildProtectionRisk,#cmdProceed").prop("disabled", false);
                $('#HouseHoldMemberName').val($('#NameOfHouseHoldHead').val());            
                $('#HouseHoldMemberName').focus();    
            } else {
                $('#KebeleID').focus();
            }
        }
    });
    

    $('#CBHINumber').bind('focusout', function (event) {
        if (isValid($('#KebeleID').val()) && isValid($('#NameOfHouseHoldHead').val()) && isValid($('#HouseHoldIDNumber').val()) && isValid($('#CollectionDate').val()) && isValid($('#SocialWorker').val())) {
            $('#RegionID,#WoredaID').prop("disabled", true); 
            $("#Kebele").val($("#KebeleID").val());
            $("#DateOfBirth,#HouseHoldMemberName,#IndividualID,#Sex,#Handicapped,#ChronicallyIll,#ChildProtectionRisk,#cmdProceed").prop("disabled", false);
            $('#HouseHoldMemberName').val($('#NameOfHouseHoldHead').val());            
            $('#HouseHoldMemberName').focus();    
        } else {
            $('#KebeleID').focus();
        }
    });

    $("#DateOfBirth").focusout(function () {
        DobChange();
      });
   
    $("#Pregnant,#Lactating").change(function (e) {
        DobChange();
    });

    $('#Sex').change(function (e) {
        $('#Pregnant,#Lactating').prop("disabled", true);

        var TheAge = getAge($('#DateOfBirth').val());
        if ($('#Sex').val() == 'M') {
            $('#Pregnant,#Lactating').val('-');
            $('#Pregnant,#Lactating').prop("disabled", true);

            if (parseInt(TheAge) >= 6) {
                $("#NutritionalStatus option[value='N']").attr("disabled", "disabled");
                $("#NutritionalStatus option[value='M']").attr("disabled", "disabled");
                $("#NutritionalStatus option[value='SM']").attr("disabled", "disabled");
                $('#NutritionalStatus').val('-');
            }

            if (parseInt(TheAge) > 18) {
                $('#childUnderTSForCMAM,#EnrolledInSchool,#Grade,#SchoolName,#NutritionalStatus').prop("disabled", true);
            }

        } else {
            if (parseInt(TheAge) < 12 || parseInt(TheAge) > 50) {
                $('#Pregnant,#Lactating').val('-');
                $('#Pregnant,#Lactating').prop("disabled", true);
            } else {
                $('#Pregnant,#Lactating').prop("disabled", false);
            }
        }
    });

    //#EnrolledInSchool
    $('#EnrolledInSchool').change(function (e) {
        var TheAge = getAge($('#DateOfBirth').val());
        if ($('#EnrolledInSchool').val() !== 'YES') {
            $('#Grade,#SchoolName').prop("disabled", true);
            $('#SchoolName').attr('readonly', true);

            $('#Grade,#SchoolName').val("");
            
        } else {
            $('#Grade,#SchoolName').prop("disabled", false);
            $('#SchoolName').attr('readonly', false);
        }
    });

    //#CBHIMembership
    $('#CBHIMembership').change(function (e) {
        var membership = $('#CBHIMembership').val();
        if (membership== 'YES') {
            $('#CBHINumber').prop("disabled", false);
        } else {
            $('#CBHINumber').prop("disabled", true);
            $('#CBHINumber').val("");
        }
    });

   
    $("#cmdProceed").click(function () {
        var errors = [];
        var html = '<ul>';
        valid = true;
        $('#errors2').empty();
        if ($('#HouseHoldMemberName').val() == '') {
            errors.push('<li>House Hold Member Name Required</li>');
            valid = false;
        }

        if ($('#DateOfBirth').val() == "" ) {
            errors.push('<li>Date Of Birth or age required</li>');
            valid = false;
        }
        if (parseInt($('#Age').val()) == NaN) {
            errors.push('<li>Date Of Birth or age required</li>');
            valid = false;
        }
        if ($('#Sex').val() == '') {
            errors.push('<li>Gender is Required</li>');
            valid = false;
        }
        /*
        if ($('#Pregnant').val() == '-' && $('#Sex').val() == 'F') {
            errors.push('<li>Please specify if Pregnant</li>');
            valid = false;
        }
        if ($('#Lactating').val() == '-' && $('#Sex').val() == 'F') {
            errors.push('<li>Please specify if Lactating</li>');
            valid = false;
        }
        if ($('#Handicapped').val() == '-') {
            errors.push('<li>Please specify if Handicapped</li>');
            valid = false;
        }
        
        if ($('#ChronicallyIll').val() == '-') {
            errors.push('<li>Please specify if Chronically Ill</li>');
            valid = false;
        }
        
        if ($('#NutritionalStatus').val() == '-') {
            errors.push('<li>Please specify Nutritional Status</li>');
            valid = false;
        }

        if ($('#EnrolledInSchool').val() == '-') {
            errors.push('<li>Please specify if Enrolled In School</li>');
            valid = false;
        }
           

        if ($('#ChildProtectionRisk').val() == '-') {
            errors.push('<li>Please specify if Child Potentially at Risk</li>');
            valid = false;
        }
*/

        if ($('#EnrolledInSchool').val() == 'YES' && $('#Grade').val() == '') {
            errors.push('<li>Please specify Grade</li>');
            valid = false;
        }
        if ($('#SchoolName').val() == "" && $('#EnrolledInSchool').val() == 'YES') {
            errors.push('<li>School Name is Required</li>');
            valid = false;
        } 
        if (!valid) {
            html += errors.join('') + '</ul>'
            $('#errors2').show();
            $('#errors2').append(html);
            return valid;
        }
        else {
            $('#errors2').hide();
        }

        var theDtls = new Object;
        if (EVENTID == "EDIT") {

            for (var i in arrObj) {
                if (arrObj.hasOwnProperty(i)) {
                    if (arrObj[i].ColumnID == $("#ColumnID").val()) {
                        arrObj[i].HouseHoldMemberName = $("#HouseHoldMemberName").val();
                        arrObj[i].IndividualID = $("#IndividualID").val();
                        arrObj[i].Age = $("#Age").val();
                        arrObj[i].Sex = $("#Sex").val();
                        arrObj[i].SexName = $("#Sex").find('option:selected').text();
                        arrObj[i].Pregnant = $("#Pregnant").val();
                        arrObj[i].Lactating = $("#Lactating").val();
                        arrObj[i].DateOfBirth = $("#DateOfBirth").val();
                        arrObj[i].Handicapped = $("#Handicapped").val();
                        arrObj[i].ChronicallyIll = $("#ChronicallyIll").val();
                        arrObj[i].NutritionalStatus = $("#NutritionalStatus").val();
                        arrObj[i].NutritionalStatusName = $("#NutritionalStatus").find('option:selected').text();
                        arrObj[i].childUnderTSForCMAM = $("#childUnderTSForCMAM").val();
                        arrObj[i].EnrolledInSchool = $("#EnrolledInSchool").val();
                        arrObj[i].Grade = $("#Grade").val();
                        arrObj[i].SchoolName = $("#SchoolName").val();
                        arrObj[i].ChildProtectionRisk = $("#ChildProtectionRisk").val();
                        //arrObj[i].Remarks = $("#Remarks").val();         
                    }
                }
            }
        }
        else {
            $('#errors2').hide();

            SerialCount = SerialCount + 1;
            theDtls.ColumnID = SerialCount;
            theDtls.HouseHoldMemberName = $("#HouseHoldMemberName").val();
            theDtls.IndividualID = $("#IndividualID").val();
            theDtls.Age = $("#Age").val();
            theDtls.Sex = $("#Sex").val();
            theDtls.SexName = $("#Sex").find('option:selected').text();
            theDtls.Pregnant = $("#Pregnant").val();
            theDtls.Lactating = $("#Lactating").val();
            theDtls.DateOfBirth = $("#DateOfBirth").val();
            theDtls.Handicapped = $("#Handicapped").val();
            theDtls.ChronicallyIll = $("#ChronicallyIll").val();
            theDtls.NutritionalStatus = $("#NutritionalStatus").val();
            theDtls.NutritionalStatusName = $("#NutritionalStatus").find('option:selected').text();
            theDtls.childUnderTSForCMAM = $("#childUnderTSForCMAM").val();
            theDtls.EnrolledInSchool = $("#EnrolledInSchool").val();
            theDtls.Grade = $("#Grade").val();
            theDtls.SchoolName = $("#SchoolName").val();
            theDtls.ChildProtectionRisk = $("#ChildProtectionRisk").val();
            //theDtls.Remarks = $("#Remarks").val();

            arrObj.push(theDtls);
        }
        var myXml = createXmlstring(arrObj, 1);
        mygrid.clearAll();
        mygrid.parse(myXml);

        $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#ChildProtectionRisk,#Grade,#SchoolName").val('');//prop("disabled", false);
        $("#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#ChildProtectionRisk").val("-");
        EVENTID = null;
        $("#cmdProceed").html('<i></i>Proceed');
        $("#cmdSave").prop("disabled", false);
        $('#HouseHoldMemberName').focus();
    });

});

function UpdateMembers(_ColumnID) {
    var selectedRow = mygrid.getSelectedId();
    _ColumnID = mygrid.cells(selectedRow, 0).getValue();
    $("#HouseHoldMemberName,#IndividualID,#DateOfBirth,#Age,#Sex,#Grade,#SchoolName").val('')
    $("#Pregnant,#Lactating,#childUnderTSForCMAM,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#ChildProtectionRisk").val("-");
    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            if (arrObj[i].ColumnID == _ColumnID) {
                $("#ColumnID").val(arrObj[i].ColumnID);
                $("#HouseHoldMemberName").val(arrObj[i].HouseHoldMemberName);
                $("#IndividualID").val(arrObj[i].IndividualID);
                $("#DateOfBirth").val(arrObj[i].DateOfBirth);
                $("#Age").val(arrObj[i].Age);
                $("#Sex").val(arrObj[i].Sex);
                $("#Pregnant").val(arrObj[i].Pregnant);
                $("#Lactating").val(arrObj[i].Lactating);
                $("#Handicapped").val(arrObj[i].Handicapped);
                $("#ChronicallyIll").val(arrObj[i].ChronicallyIll);
                $("#NutritionalStatus").val(arrObj[i].NutritionalStatus);
                $("#childUnderTSForCMAM").val(arrObj[i].childUnderTSForCMAM);
                $("#EnrolledInSchool").val(arrObj[i].EnrolledInSchool);
                $("#Grade").val(arrObj[i].Grade);
                $("#SchoolName").val(arrObj[i].SchoolName);    
                $("#ChildProtectionRisk").val(arrObj[i].ChildProtectionRisk);    
            }
        }
    }

    $("#HouseHoldMemberName,#IndividualID,#Pregnant,#Lactating,#childUnderTSForCMAM,#DateOfBirth,#Sex,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#Grade,#SchoolName,#ChildProtectionRisk").prop("disabled", false);
    //#Age,
    $("#cmdProceed").html('<i></i>Update Member Details');
    EVENTID = "EDIT";
}

function createXmlstring(arrObject, inMemory) {
    var xml;
    var gridID;
    xml = '';
    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            xml = xml + '<row id="' + i + '">';
            for (var j in arrObj[i]) {
                if (arrObj[i].hasOwnProperty(j)) {
                    if (j == "ColumnID") {
                        xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                        gridID = arrObj[i][j];
                    }
                }
                if (j == "HouseHoldMemberName" || j == "IndividualID" || j == "Age" || j == "Sex" || j == "Pregnant"
                || j == "Lactating" || j == "Handicapped" || j == "ChronicallyIll" || j == "NutritionalStatusName"
                || j == "EnrolledInSchool" || j == "SchoolName" || j == "ChildProtectionRisk") {
                xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                 }
            }
            xml = xml + '<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit^javascript:UpdateMembers(' + gridID + ');^_self</cell>';
            xml = xml + '</row>';
        }
    }
    xml = '<rows total_count="' + arrObj.length + '">' + xml + '</rows>';
    
    $("#MemberXml").val(JSON.stringify(arrObject));
    return xml;
}

function isValid(str) {
    if (str === "" || str === "0" || str === 0) {
        return false;
    } else {
        return true;
    }
}

function DobChange() {
    $("#NutritionalStatus option[value='N']").attr("disabled", "disabled");
    $("#NutritionalStatus option[value='M']").attr("disabled", "disabled");
    $("#NutritionalStatus option[value='SM']").attr("disabled", "disabled");
    //$("#ChronicallyIll option[value='-']").attr("disabled", "disabled");
    //$("#Handicapped option[value='-']").attr("disabled", "disabled");
    $("#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
    $("#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");
    $('#EnrolledInSchool,#Grade,#SchoolName,#childUnderTSForCMAM,#NutritionalStatus').prop('disabled', true);

    $("#NutritionalStatus,#EnrolledInSchool,#Grade").val("-");

    if ($('#DateOfBirth').val() == "") {
        $('#DateOfBirth').focus();
    } else {
        var TheAge = getAge($('#DateOfBirth').val());

        $("#Age").val(TheAge);

        //FOR SCHOOL GOING CHILDREN
        if (parseInt(TheAge) > 5 && parseInt(TheAge) < 18) {
            $('#EnrolledInSchool').prop("disabled", false);

            $("#childUnderTSForCMAM").val("-");
            $('#childUnderTSForCMAM').attr('readonly', true);
            $("#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
            $("#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");

            $("#EnrolledInSchool,#Grade option[value='NO']").attr("disabled", false);
            $("#EnrolledInSchool,#Grade option[value='YES']").attr("disabled", false);

            DisableEnableGrade("E");
        }
        
        //THIS IS FOR PREGNANT WOMEN ABOVE 12 YEARS BUT NOT OLDER THAN 50 YEARS
        if (parseInt(TheAge) >= 12 && parseInt(TheAge) <= 50) {
            if ($("#Sex").val() == "F" && ($("#Pregnant").val() == "YES" || $("#Lactating").val() == "YES")) {
                $('#EnrolledInSchool,#Grade,#SchoolName,#childUnderTSForCMAM,#NutritionalStatus').attr('readonly', true);
                $('#EnrolledInSchool,#Grade,#childUnderTSForCMAM,#NutritionalStatus').val('-');

                $("#EnrolledInSchool,#Grade,#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
                $("#EnrolledInSchool,#Grade,#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");

                $('#NutritionalStatus').prop("disabled", false);

                $("#NutritionalStatus option[value='N']").attr("disabled", false);
                $("#NutritionalStatus option[value='M']").attr("disabled", false);
                $("#NutritionalStatus option[value='SM']").attr("disabled", false);
            }
        }
        //FOR INFANTS BELOW 5 YEARS
        if (parseInt(TheAge) <= 5) {
            $('#EnrolledInSchool,#Grade,#SchoolName,#childUnderTSForCMAM,#NutritionalStatus').attr('readonly', true);
            $('#EnrolledInSchool,#Grade,#childUnderTSForCMAM,#NutritionalStatus').val('-');

            $('#NutritionalStatus').prop("disabled", false);

            $("#NutritionalStatus option[value='N']").attr("disabled", false);
            $("#NutritionalStatus option[value='M']").attr("disabled", false);
            $("#NutritionalStatus option[value='SM']").attr("disabled", false);
        }

        $('#Sex').change();
    }
}

$('#NutritionalStatus').change(function (e) {
    var TheAge = getAge($('#DateOfBirth').val());
    $("#childUnderTSForCMAM").val("-");
    $('#childUnderTSForCMAM').attr('readonly', true);
    $("#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
    $("#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");
    $('#SchoolName').prop("disabled", true);

    if (parseInt(TheAge) <= 5){
        if ($("#NutritionalStatus").val() == "M" || $("#NutritionalStatus").val() == "SM") {
            //Not sure if this part should be enabled.
            $('#childUnderTSForCMAM').attr('readonly', false);
            $("#childUnderTSForCMAM option[value='NO']").attr("disabled", false);
            $("#childUnderTSForCMAM option[value='YES']").attr("disabled", false);

            $('#childUnderTSForCMAM').prop("disabled", false);
        } else {
            $("#childUnderTSForCMAM").val("-");
            $('#childUnderTSForCMAM').attr('readonly', true);
            $("#childUnderTSForCMAM option[value='NO']").attr("disabled", "disabled");
            $("#childUnderTSForCMAM option[value='YES']").attr("disabled", "disabled");
        }
    }
});

function DisableEnableGrade(Status) {
    if (Status == "E") {
        $("#Grade option[value='1']").attr("disabled", false);
        $("#Grade option[value='2']").attr("disabled", false);
        $("#Grade option[value='3']").attr("disabled", false);
        $("#Grade option[value='4']").attr("disabled", false);
        $("#Grade option[value='5']").attr("disabled", false);
        $("#Grade option[value='6']").attr("disabled", false);
        $("#Grade option[value='7']").attr("disabled", false);
        $("#Grade option[value='8']").attr("disabled", false);
        $("#Grade option[value='9']").attr("disabled", false);
        $("#Grade option[value='10']").attr("disabled", false);
        $("#Grade option[value='11']").attr("disabled", false);
        $("#Grade option[value='12']").attr("disabled", false);
        $("#Grade option[value='13']").attr("disabled", false);
        $("#Grade option[value='14']").attr("disabled", false);

        $("#SchoolName").prop("disabled", false);

        $("#EnrolledInSchool option[value='NO']").attr("disabled", false);
        $("#EnrolledInSchool option[value='YES']").attr("disabled", false);

    } else {
        $("#Grade option[value='1']").attr("disabled", "disabled");
        $("#Grade option[value='2']").attr("disabled", "disabled");
        $("#Grade option[value='3']").attr("disabled", "disabled");
        $("#Grade option[value='4']").attr("disabled", "disabled");
        $("#Grade option[value='5']").attr("disabled", "disabled");
        $("#Grade option[value='6']").attr("disabled", "disabled");
        $("#Grade option[value='7']").attr("disabled", "disabled");
        $("#Grade option[value='8']").attr("disabled", "disabled");
        $("#Grade option[value='9']").attr("disabled", "disabled");
        $("#Grade option[value='10']").attr("disabled", "disabled");
        $("#Grade option[value='11']").attr("disabled", "disabled");
        $("#Grade option[value='12']").attr("disabled", "disabled");
        $("#Grade option[value='13']").attr("disabled", "disabled");
        $("#Grade option[value='14']").attr("disabled", "disabled");

        $("#SchoolName").prop("disabled", "disabled");

        $("#EnrolledInSchool option[value='NO']").attr("disabled", "disabled");
        $("#EnrolledInSchool option[value='YES']").attr("disabled", "disabled");

    }
}