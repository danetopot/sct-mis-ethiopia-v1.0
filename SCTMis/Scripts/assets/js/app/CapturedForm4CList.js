﻿//var page_count = 10;
//var gridHeader = 'ColumnID,ProfileTDSCMCID,Kebele,Service<span class="HeaderChange">_</span>Provider,Caretaker<span class="HeaderChange">_</span>Name,';
//gridHeader = gridHeader + 'Malnourished<span class="HeaderChange">_</span>Child,HouseHold<span class="HeaderChange">_</span>ID,Capturer,Capture<span class="HeaderChange">_</span>On';
//var gridColType = 'ro,ro,ro,ro,ro,ro,ro,ro,ro';
//var mygrid;

$(document).ready(function () {
    $('#thebox').jtable({
        title: 'Form 4C Compliant Listing',
        paging: true,
        actions: {
            listAction: '/ComplianceCapture/CapturedForm4CMainList',
        },
        fields: {
            ColumnID: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            //CHILD TABLE DEFINITION FOR "PHONE NUMBERS"
            Members: {
                title: '',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                display: function (memberData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="../DHTMLX/codebase/imgs/plus2.gif" title="view Household members" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#thebox').jtable('openChildTable',
                                $img.closest('tr'),
                                {
                                    title: memberData.record.ServiceName + ' - Household members',
                                    paging: true,
                                    actions: {
                                        listAction: '/ComplianceCapture/CapturedForm4CList?ReportingPeriodID=' + memberData.record.ReportingPeriodID + '&ServiceID=' + memberData.record.ServiceID + '&KebeleID=' + memberData.record.KebeleID,
                                    },
                                    fields: {
                                        ProfileTDSCMCID: {
                                            key: true,
                                            create: false,
                                            edit: false,
                                            list: false
                                        },
                                        MalnourishedChildName: {
                                            title: 'Malnourished Child Name',
                                            width: '20%',
                                            create: false,
                                            edit: false
                                        },
                                        HouseHoldIDNumber: {
                                            title: 'PSNP HH #',
                                            width: '10%',
                                            create: false,
                                            edit: false
                                        },
                                        CapturedOn: {
                                            title: 'Record date',
                                            width: '8%',
                                            //type: 'date',
                                            //displayFormat: 'dd-mm-yy',
                                            create: false,
                                            edit: false
                                        }
                                    }
                                }, function (data) { //opened handler
                                    data.childTable.jtable('load');
                                });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            ReportingPeriodID: {
                title: '',
                type: 'hidden',
                create: false,
                edit: false,
                list: false,
            },
            ReportingPeriod: {
                title: 'Reporting Period',
                width: '10%',
                create: false,
                edit: false,
            },
            KebeleName: {
                title: 'Kebele Name',
                width: '10%',
                create: false,
                edit: false,
            },
            ServiceProvider: {
                title: 'Service Provider',
                width: '10%',
                create: false,
                edit: false,
            },
            ServiceID: {
                title: '',
                type: 'hidden',
                create: false,
                edit: false,
                list: false,
            },
            KebeleID: {
                title: '',
                type: 'hidden',
                create: false,
                edit: false,
                list: false,
            },
            ServiceName: {
                title: 'Service Name',
                width: '20%',
                create: false,
                edit: false,
            },
            CreatedBy: {
                title: 'Created By',
                create: false,
                edit: false,
                width: '5%'
            }
        }
    });
    $('#thebox').jtable('load');

    $('#SearchTypeID').append($("<option></option>").attr("value", 0).text('Search All'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 2).text('Household Head Name'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 3).text('Household ID'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 4).text('Gote'));
    $('#SearchTypeID').append($("<option></option>").attr("value", 5).text('Gare'));

    $("#cmdNew").click(function () {
        window.location.href = "../ComplianceCapture/CaptureForm4C";
    });
    $('#loading').hide();
});