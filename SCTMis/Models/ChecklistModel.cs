﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCTMis.Models
{
    public class Form3AGrid
    {
        public int ColumnID { get; set; }
        public Int32 CoRespDSHeaderID { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public string ReportFileName { get; set; }        
    }

    public class ProduceForm3A
    {
        public int ColumnID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
    }

    public class Form3AChecklist
    {
		public string Region				 { get; set; }	
		public string Woreda				 { get; set; }	
		public string Kebele				 { get; set; }	
		public string ClientName			 { get; set; }	
		public string HouseHoldHeadName		 { get; set; }	
		public string HouseHoldIDNumber		 { get; set; }
		public string MedicalRecordNumber		 { get; set; }	
		public int ClientAge { get; set; }	
		public string ClientSex { get; set; }	
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public string Gote { get; set; }	
		public string CoResponsibility1 { get; set; }
		public string CoResponsibility2 { get; set; }	
		public string CoResponsibility3 { get; set; }	
		public string CoResponsibility4 { get; set; }
        public string CoResponsibility5 { get; set; }
        public string CoResponsibility6 { get; set; }
        public string CoResponsibility7 { get; set; }
        public string CreatedBy { get; set; }
    }

    public class ProduceForm3B
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
    }

    public class ProduceForm3B2
    {
        public int ColumnID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
    }

    public class Form3BChecklist
    {
		public string Region	 { get; set; }
		public string Woreda	 { get; set; }
		public string Kebele	 { get; set; }
		public string ClientName { get; set; }
		public int ClientAge { get; set; }
        public string Gote { get; set; }
        public string PLW { get; set; }
		public string HouseHoldIDNumber { get; set; }
		public string MedicalRecordNumber { get; set; }
        //public string ChildProtectionRisk { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
        public string CoResponsibility1 { get; set; }
		public string CoResponsibility2 { get; set; }
		public string CoResponsibility3 { get; set; }
		public string CoResponsibility4 { get; set; }
        public string CoResponsibility5 { get; set; }
        public string CoResponsibility6 { get; set; }
        public string CoResponsibility7 { get; set; }
        public string CreatedBy { get; set; }	
    }

    public class ProduceForm3C
    {
        public int ColumnID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
    }

    public class Form3CChecklist
    {
		public string Region { get; set; }
		public string Woreda { get; set; }
		public string Kebele { get; set; }
		public string CareTakerName { get; set; }
		public string NameOfChild { get; set; }
		public string SexOfChild { get; set; }
		public string ChildBirthDate { get; set; }
		public string HouseHoldIDNumber { get; set; }
		public string MedicalRecordNumber { get; set; }
        public string CBHIMembership { get; set; }
        public string CBHINumber { get; set; }
		public string Gote { get; set; }
        public string CoResponsibility1 { get; set; }
		public string CoResponsibility2 { get; set; }
		public string CoResponsibility3 { get; set; }
		public string CoResponsibility4 { get; set; }
        public string CoResponsibility5 { get; set; }
        public string CoResponsibility6 { get; set; }
        public string CoResponsibility7 { get; set; }
        public string CreatedBy { get; set; }	
    }
}
