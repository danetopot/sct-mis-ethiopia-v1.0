ALTER PROC [dbo].[getForm4AAdminList]
(
	@jtStartIndex	Int = 0,
	@jtPageSize		Int	= 10
)
AS
BEGIN	
	-- 05-June-2018, get only for current FY by default
	declare @CurrentFiscalYear int
	select @CurrentFiscalYear = dbo.f_getFiscalYearFromDate (getdate())

	SELECT COUNT(1) RecCount
	FROM (
		SELECT ComplianceSDSHeader.ComplianceID,KebeleID, ServiceID, ReportFileName,
		GeneratedBy,GeneratedOn,ReportingPeriodID, COUNT(ComplianceDtlID) HouseholdCount
		FROM ComplianceSDSHeader
		INNER JOIN ComplianceSDSDetail ON ComplianceSDSHeader.ComplianceID =  ComplianceSDSDetail.ComplianceID
		WHERE ( ComplianceSDSHeader.FiscalYear = @CurrentFiscalYear )
		GROUP BY ComplianceSDSHeader.ComplianceID,KebeleID, ServiceID, ReportFileName,GeneratedBy,GeneratedOn,ReportingPeriodID
	) ComplianceDS
	INNER JOIN AdminStructureView ON ComplianceDS.KebeleID = AdminStructureView.KebeleID

	SELECT 
		ROW_NUMBER() OVER(ORDER BY ComplianceID) AS ColumnID,
		ComplianceID,
		RegionName,
		WoredaName, 
		KebeleName,
		dbo.f_GetServiceName(ServiceID) ServiceName,
		HouseholdCount,
		ReportFileName, 
		dbo.f_GetUserNameByUserID(GeneratedBy) GeneratedBy,
		CONVERT(VARCHAR(11), GeneratedOn, 106) GeneratedOn,
		GeneratedOn CreatedOn
	FROM (
		SELECT ComplianceSDSHeader.ComplianceID,KebeleID, ServiceID, ReportFileName,
		GeneratedBy,GeneratedOn,ReportingPeriodID, COUNT(ComplianceDtlID) HouseholdCount
		FROM ComplianceSDSHeader
		INNER JOIN ComplianceSDSDetail ON ComplianceSDSHeader.ComplianceID =  ComplianceSDSDetail.ComplianceID
		WHERE ( ComplianceSDSHeader.FiscalYear = @CurrentFiscalYear )
		GROUP BY ComplianceSDSHeader.ComplianceID,KebeleID, ServiceID, ReportFileName,GeneratedBy,GeneratedOn,ReportingPeriodID
	) ComplianceDS
	INNER JOIN AdminStructureView ON ComplianceDS.KebeleID = AdminStructureView.KebeleID
	ORDER BY CreatedOn DESC
	OFFSET @jtStartIndex ROWS
	FETCH NEXT @jtPageSize ROWS ONLY

END

