﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace SCTMis.Services
{
    public class ComplianceServices
    {
        public bool ProduceForm4ASchool(ProduceForm4A newModel, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {
                Tuple<List<ServiceIDModel>, List<Form4AReport>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<ServiceIDModel, Form4AReport>(";Exec form4AReportSchool @KebeleID,@ReportingPeriodID",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID
                     });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form4AReport>(resultset.Item2);
                DataTable dtServices = SCTMis.Services.GeneralServices.ToDataTable<ServiceIDModel>(resultset.Item1);
                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                    //DataRow[] dRresult = dtDetails.Select("ServiceID = " + ds["ServiceID"]);
                    //DataTable dtRpt = dRresult.CopyToDataTable();

                if (dtDetails.Rows.Count > 0)
                    {
                        string filename = DateTime.Now.ToFileTime().ToString();

                        filename = dtDetails.Rows[0]["KebeleName"].ToString() + dtDetails.Rows[0]["ReportingPeriodName"].ToString() + filename + ".pdf";

                        string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form4AComplianceDemo.rpt");
                        string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.FileName = strRptPath;
                        myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                        myReportDocument.Close();
                        myReportDocument.Dispose();

                        string strXML = string.Empty;

                        foreach (DataRow array in dtDetails.Rows)
                        {
                            strXML = strXML + "<row>";
                            strXML = strXML + "<ProfileDSDetailID>" + array["ProfileDSDetailID"] + "</ProfileDSDetailID>";
                            strXML = strXML + "</row>";
                        }

                        strXML = "<members>" + strXML + "</members>";

                        List<RecordCount> isSuccess
                            = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm4A @KebeleID,@ServiceID,@ReportFileName,@ReportingPeriodID,@MemberXML,@CreatedBy",
                             new
                             {
                                 KebeleID = newModel.KebeleID,
                                 ServiceID = dtServices.Rows[0][0],//newModel.ServiceID,
                                 ReportFileName = filename,
                                 ReportingPeriodID = newModel.ReportingPeriodID,
                                 MemberXML = strXML,
                                 CreatedBy = Membership.GetUser().UserName.ToString()
                             });
                    }
                    //else { errMsg = "No Records to produce Report"; }
                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool ProduceForm4A(ProduceForm4A newModel,out string errMsg)
        {
            errMsg = string.Empty;
            try
            {
                Tuple<List<ServiceIDModel>, List<Form4AReport>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<ServiceIDModel, Form4AReport>(";Exec form4AReport @KebeleID,@ReportingPeriodID",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID
                     });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form4AReport>(resultset.Item2);
                DataTable dtServices = SCTMis.Services.GeneralServices.ToDataTable<ServiceIDModel>(resultset.Item1);
                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                foreach (DataRow ds in dtServices.Rows)
                {
                    DataRow[] dRresult = dtDetails.Select("ServiceID = '" + ds["ServiceID"] + "'");

                    if (dRresult.Count() == 0) continue;

                    DataTable dtRpt = dRresult.CopyToDataTable();

                    if (dtRpt.Rows.Count > 0)
                    {
                        string filename = DateTime.Now.ToFileTime().ToString();

                        filename = dtRpt.Rows[0]["KebeleName"].ToString() + dtRpt.Rows[0]["ReportingPeriodName"].ToString() + filename + ".pdf";

                        string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form4AComplianceDemo.rpt");
                        string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                        ReportDocument myReportDocument;

                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.FileName = strRptPath;
                        myReportDocument.Database.Tables[0].SetDataSource(dtRpt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                        myReportDocument.Close();
                        myReportDocument.Dispose();

                        string strXML = string.Empty;

                        foreach (DataRow array in dtRpt.Rows)
                        {
                            strXML = strXML + "<row>";
                            strXML = strXML + "<ProfileDSDetailID>" + array["ProfileDSDetailID"] + "</ProfileDSDetailID>";
                            strXML = strXML + "</row>";
                        }

                        strXML = "<members>" + strXML + "</members>";

                        List<RecordCount> isSuccess
                            = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm4A @KebeleID,@ServiceID,@ReportFileName,@ReportingPeriodID,@MemberXML,@CreatedBy",
                             new
                             {
                                 KebeleID = newModel.KebeleID,
                                 ServiceID = ds["ServiceID"],//newModel.ServiceID,
                                 ReportFileName = filename,
                                 ReportingPeriodID = newModel.ReportingPeriodID,
                                 MemberXML = strXML,
                                 CreatedBy = Membership.GetUser().UserName.ToString()
                             });
                    }
                }
                //else { errMsg = "No Records to produce Report"; }
                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        //ProduceForm4B
        public bool ProduceForm4B(ProduceForm4B newModel, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {

                //Tuple<List<ServiceAdminStructure>, List<Form4BCompliance>> resultset
                //    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<ServiceAdminStructure, Form4BCompliance>(";Exec getForm4BCompliance @KebeleID, @ServiceID,@ReportingPeriodID",
                //    new
                //    {
                //        KebeleID = newModel.KebeleID,
                //        ServiceID = newModel.ServiceID,
                //        ReportingPeriodID = newModel.ReportingPeriodID
                //    });

                Tuple<List<ServiceIDModel>, List<Form4BReport>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<ServiceIDModel, Form4BReport>(";Exec form4BReport @KebeleID,@ReportingPeriodID",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID
                     });
                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form4BReport>(resultset.Item2);
                DataTable dtServices = SCTMis.Services.GeneralServices.ToDataTable<ServiceIDModel>(resultset.Item1);

                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                foreach (DataRow ds in dtServices.Rows)
                {
                    //DataRow[] dRresult = dtDetails.Select("ServiceID = " + ds["ServiceID"]);
                    DataRow[] dRresult = dtDetails.Select("ServiceID = '" + ds["ServiceID"] + "'");

                    if (dRresult.Count() == 0) continue;

                    DataTable dtRpt = dRresult.CopyToDataTable();                                       

                    if (dtRpt.Rows.Count > 0)
                    {
                        string filename = DateTime.Now.ToFileTime().ToString();

                        filename = dtRpt.Rows[0]["KebeleName"].ToString() + dtRpt.Rows[0]["ReportingPeriodName"].ToString() + filename + ".pdf";

                        string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form4BComplianceDemo.rpt");
                        string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.FileName = strRptPath;
                        myReportDocument.Database.Tables[0].SetDataSource(dtRpt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                        myReportDocument.Close();
                        myReportDocument.Dispose();

                        string strXML = string.Empty;

                        foreach (DataRow array in dtRpt.Rows)
                        {
                            strXML = strXML + "<row>";
                            strXML = strXML + "<ProfileTDSPLWID>" + array["ProfileTDSPLWID"] + "</ProfileTDSPLWID>";
                            strXML = strXML + "</row>";
                        }

                        strXML = "<members>" + strXML + "</members>";  

                        List<RecordCount> isSuccess
                            = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm4B @KebeleID,@ServiceID,@ReportFileName,@ReportingPeriodID,@MemberXML,@CreatedBy",
                             new
                             {
                                 KebeleID = newModel.KebeleID,
                                 ServiceID = ds["ServiceID"],//newModel.ServiceID,
                                 ReportFileName = filename,
                                 ReportingPeriodID = newModel.ReportingPeriodID,
                                 MemberXML = strXML,
                                 CreatedBy = Membership.GetUser().UserName.ToString()
                             });
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //ProduceForm4C
        public bool ProduceForm4C(ProduceForm4C newModel, out string errMsg)
        {
            errMsg = string.Empty;
            try
            {

                //Tuple<List<ServiceAdminStructure>, List<Form4CCompliance>> resultset
                //    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<ServiceAdminStructure, Form4CCompliance>(";Exec getForm4CCompliance @KebeleID, @ServiceID,@ReportingPeriodID",
                //    new
                //    {
                //        KebeleID = newModel.KebeleID,
                //        ServiceID = newModel.ServiceID,
                //        ReportingPeriodID = newModel.ReportingPeriodID
                //    });
                Tuple<List<ServiceIDModel>, List<Form4CReport>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<ServiceIDModel, Form4CReport>(";Exec form4CReport @KebeleID,@ReportingPeriodID",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID
                     });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form4CReport>(resultset.Item2);
                DataTable dtServices = SCTMis.Services.GeneralServices.ToDataTable<ServiceIDModel>(resultset.Item1);

                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                foreach (DataRow ds in dtServices.Rows)
                {
                    Int32 intServiceID = Int32.Parse(ds.ItemArray[0].ToString());
                    //DataRow[] dRresult = dtDetails.Select("ServiceID = " + ds["ServiceID"]);
                    

                    var dr = from row in dtDetails.AsEnumerable()
                             where row.Field<string>("ServiceID") == intServiceID.ToString()
                             select row;
                    DataTable dtRpt = dr.CopyToDataTable();

                    if (dtRpt.Rows.Count > 0)
                    {
                        string filename = DateTime.Now.ToFileTime().ToString();

                        filename = dtRpt.Rows[0]["KebeleName"].ToString() + dtRpt.Rows[0]["ReportingPeriodName"].ToString() + filename + ".pdf";

                        string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form4CComplianceDemo.rpt");
                        string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                        ReportDocument myReportDocument;
                        myReportDocument = new ReportDocument();
                        myReportDocument.Load(strRptPath);
                        myReportDocument.FileName = strRptPath;
                        myReportDocument.Database.Tables[0].SetDataSource(dtRpt);
                        myReportDocument.SetParameterValue("GeneratedBy", Membership.GetUser().UserName.ToString());

                        myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                        myReportDocument.Close();
                        myReportDocument.Dispose();

                        string strXML = string.Empty;

                        foreach (DataRow array in dtRpt.Rows)
                        {
                            strXML = strXML + "<row>";
                            strXML = strXML + "<ProfileTDSCMCID>" + array["ProfileTDSCMCID"] + "</ProfileTDSCMCID>";
                            strXML = strXML + "</row>";
                        }

                        strXML = "<members>" + strXML + "</members>";

                        List<RecordCount> isSuccess
                            = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm4C @KebeleID,@ServiceID,@ReportFileName,@ReportingPeriodID,@MemberXML,@CreatedBy",
                             new
                             {
                                 KebeleID = newModel.KebeleID,
                                 ServiceID = ds["ServiceID"],//newModel.ServiceID,
                                 ReportFileName = filename,
                                 ReportingPeriodID = newModel.ReportingPeriodID,
                                 MemberXML = strXML,
                                 CreatedBy = Membership.GetUser().UserName.ToString()
                             });
                    }
                }
                //if (dtDetails.Rows.Count > 0)
                //{
                //string filename = DateTime.Now.ToFileTime().ToString();

                //filename = dtAdminStructure.Rows[0]["Kebele"].ToString() + dtAdminStructure.Rows[0]["ServiceProviderName"].ToString() + filename + ".pdf";

                //string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/Form4CComplianceRptFile.rpt");
                //string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                //ReportDocument myReportDocument;
                //myReportDocument = new ReportDocument();
                //myReportDocument.Load(strRptPath);
                //myReportDocument.FileName = strRptPath;
                //myReportDocument.Database.Tables[0].SetDataSource(dtAdminStructure);
                //myReportDocument.Database.Tables[1].SetDataSource(dtDetails);

                //myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                //myReportDocument.Close();

                ////OTHER SETTINGS TO GO HERE
                ////convert data to xml and pass it to the procedure to save. This is safer

                //string strXML = string.Empty;

                //foreach (DataRow array in dtDetails.Rows)
                //{
                //    strXML = strXML + "<row>";
                //    strXML = strXML + "<ProfileTDSCMCID>" + array["ProfileTDSCMCID"] + "</ProfileTDSCMCID>";
                //    strXML = strXML + "</row>";
                //}

                //strXML = "<members>" + strXML + "</members>";

                //    List<RecordCount> isSuccess
                //        = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm4C @KebeleID,@ServiceID,@ReportFileName,@ReportingPeriodID,@MemberXML,@CreatedBy",
                //         new
                //         {
                //             KebeleID = newModel.KebeleID,
                //             ServiceID = newModel.ServiceID,
                //             ReportFileName = filename,
                //             ReportingPeriodID = newModel.ReportingPeriodID,
                //             MemberXML = strXML,
                //             CreatedBy = Membership.GetUser().UserName.ToString()
                //         });
                //}
                //else { errMsg = "No Records to produce Report"; }
                return true;
            }
            catch (Exception e)
            {
                errMsg = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //Single Form 4A captured Insert
        public bool InsertCapturedForm4ANew(CapturedForm4Model newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm4ANew @KebeleID,@ReportingPeriodID,@ProfileDSDetailID,@ServiceID,@HasComplied,@Remarks,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID,
                         ProfileDSDetailID = newModel.ProfileDSDetailID,
                         ServiceID = newModel.ServiceID,
                         HasComplied=newModel.HasComplied,
                         Remarks=newModel.Remarks,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }

        //Captured Form 4 A Listing details
        public bool InsertCapturedForm4A(CaptureForm4 newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {

                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CapturedForm4XMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm4XMLDetails[]>(newModel.CapturedXml);

                foreach (var array in CapturedList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<UniqueID>" + array.UniqueID + "</UniqueID>";
                    strXML = strXML + "<HasComplied>" + array.HasComplied + "</HasComplied>";
                    strXML = strXML + "<Remarks>" + array.Remarks + "</Remarks>";
                    strXML = strXML + "</row>";
                    Console.WriteLine(strXML);
                }

                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess 
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm4A @KebeleID,@ServiceID,@MemberXML,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.Kebele,
                         ServiceID = newModel.Service,
                         MemberXML = strXML,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //Single Form 4A captured Insert
        public bool InsertCapturedForm4BNew(CapturedForm4BModel newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedFormBANew @KebeleID,@ReportingPeriodID,@ProfileTDSPLWID,@ServiceID,@HasComplied,@Remarks,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID,
                         ProfileTDSPLWID = newModel.ProfileTDSPLWID,
                         ServiceID = newModel.ServiceID,
                         HasComplied = newModel.HasComplied,
                         Remarks = newModel.Remarks,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        //InsertCapturedForm4CNew
        public bool InsertCapturedForm4CNew(CapturedForm4CModel newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {
                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm4CNew @KebeleID,@ReportingPeriodID,@ProfileTDSCMCID,@ServiceID,@HasComplied,@Remarks,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.KebeleID,
                         ReportingPeriodID = newModel.ReportingPeriodID,
                         ProfileTDSCMCID = newModel.ProfileTDSCMCID,
                         ServiceID = newModel.ServiceID,
                         HasComplied = newModel.HasComplied,
                         Remarks = newModel.Remarks,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
        ////Captured Form 4B Listing details
        //public bool InsertCapturedForm4B(CaptureForm4 newModel, out string errMessage)
        //{
        //    errMessage = string.Empty;
        //    try
        //    {

        //        string strXML = string.Empty;
        //        JavaScriptSerializer objJavascript = new JavaScriptSerializer();
        //        CapturedForm4XMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm4XMLDetails[]>(newModel.CapturedXml);

        //        foreach (var array in CapturedList)
        //        {
        //            strXML = strXML + "<row>";
        //            strXML = strXML + "<UniqueID>" + array.UniqueID + "</UniqueID>";
        //            strXML = strXML + "<HasComplied>" + array.HasComplied + "</HasComplied>";
        //            strXML = strXML + "<Remarks>" + array.Remarks + "</Remarks>";
        //            strXML = strXML + "</row>";
        //            Console.WriteLine(strXML);
        //        }

        //        strXML = "<members>" + strXML + "</members>";

        //        List<RecordCount> isSuccess
        //            = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm4B @KebeleID,@ServiceID,@MemberXML,@CreatedBy",
        //             new
        //             {
        //                 KebeleID = newModel.Kebele,
        //                 ServiceID = newModel.Service,
        //                 MemberXML = strXML,
        //                 CreatedBy = Membership.GetUser().UserName.ToString()
        //             });

        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        errMessage = e.Message;
        //        return false;
        //    }
        //}

        //Captured Form 4C Listing details
        public bool InsertCapturedForm4C(CaptureForm4 newModel, out string errMessage)
        {
            errMessage = string.Empty;
            try
            {

                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CapturedForm4XMLDetails[] CapturedList = objJavascript.Deserialize<CapturedForm4XMLDetails[]>(newModel.CapturedXml);

                foreach (var array in CapturedList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<UniqueID>" + array.UniqueID + "</UniqueID>";
                    strXML = strXML + "<HasComplied>" + array.HasComplied + "</HasComplied>";
                    strXML = strXML + "<Remarks>" + array.Remarks + "</Remarks>";
                    strXML = strXML + "</row>";
                    Console.WriteLine(strXML);
                }

                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertCapturedForm4C @KebeleID,@ServiceID,@MemberXML,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.Kebele,
                         ServiceID = newModel.Service,
                         MemberXML = strXML,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                errMessage = e.Message;
                GeneralServices.LogError(e);
                return false;
            }
        }
    }
}
