﻿var EVENTID = null;
var arrObj = [];
var SerialCount = 0;
var page_count = 3;
var mygrid;
var gridHeader = 'ColumnID,Caretaker<span class="HeaderChange">_</span>Name,PSNP<span class="HeaderChange">_</span>Number';
gridHeader = gridHeader + ',Child<span class="HeaderChange">_</span>Name,Child<span class="HeaderChange">_</span>Gender,Child Protection Risk,Edit';
var gridColType = 'ro,ro,ro,ro,ro,ro,img';
var childBirthStartDate = 0;
$(document).ready(function () {
    $('#loading').hide();

    $('#CollectionDate').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        format: 'd/M/Y'
    });

    $('#ChildDateOfBirth').Zebra_DatePicker({
        direction: -1,    //-1 boolean true would've made the date picker future only
        format: 'd/M/Y',
        view: 'years'
    });

    $('#DateTypeCertificate').Zebra_DatePicker({
        direction: -1,    // boolean true would've made the date picker future only
        format: 'd/M/Y',
        view: 'years'
    });

    $('#NextCNStatusDate').Zebra_DatePicker({
        //direction: +1,    // boolean true would've made the date picker future only
        format: 'd/M/Y',
        view: 'years'
    });

    $('#EndDateTDS').Zebra_DatePicker({
        //direction: +1,    // boolean true would've made the date picker future only
        format: 'M/Y',
        view: 'years'
    });

    $('#StartDateTDS').Zebra_DatePicker({
        //direction: 1,    // boolean true would've made the date picker future only
        //direction: -1,
        format: 'M/Y',
        view: 'years'
    });

    $("#RegionID,#WoredaID,#KebeleID,#Gote,#Gare,#SocialWorker,#CCCCBSPCMember,#CBHINumber").prop("disabled", false);
    $("#NameOfCareTaker,#HouseHoldIDNumber,#MalnourishedChildName,#MalnourishedChildSex,#ChildDateOfBirth,#Age,#DateTypeCertificate,#MalnourishmentDegree,#StartDateTDS,#NextCNStatusDate,#EndDateTDS,#ChildID,#CaretakerID,#Remarks,#ChildProtectionRisk,#CBHINumber").prop("disabled", true);
    //$("#RegionID,#WoredaID,#KebeleID").val("0");
    $("#RegionID,#WoredaID").attr("disabled", true);
    $("#KebeleID,#Gote,#NameOfCareTaker,#HouseHoldIDNumber,#Gare,#MalnourishedChildName,#MalnourishedChildSex,#ChildDateOfBirth,#DateTypeCertificate,#Remarks,#CBHIMembership,#CBHINumber").val("");
    $("#CBHIMembership,#ChildProtectionRisk").val("-");
    $("#MalnourishedChildSex,#StartDateTDS,#NextCNStatusDate,#EndDateTDS,#CollectionDate,#CCCCBSPCMember,#ChildID,#CaretakerID").val("");

    $("#KebeleID,#SocialWorker").val("0");
    $("#MalnourishmentDegree").val("-");

    $("#cmdProceed,#cmdSave").prop("disabled", true);

    mygrid = new dhtmlXGridObject('gridbox');
    mygrid.clearAll();
    mygrid.setImagePath("../DHTMLX/codebase/imgs/");
    mygrid.setInitWidths("0,170,120,180,100,100,85");
    mygrid.setColAlign("left,left,right,left,right,left,right");
    mygrid.setHeader(gridHeader);

    mygrid.setColTypes(gridColType);
    mygrid.enablePaging(true, page_count, page_count, "pagingArea", true, "infoArea");
    mygrid.setPagingSkin("bricks");
    mygrid.setSkin("dhx_skyblue");

    mygrid.attachEvent("onXLE", showLoading);
    mygrid.attachEvent("onXLS", function () { showLoading(true) });
    mygrid.init();

    dhtmlxError.catchError("ALL", my_error_handler);

    $('#KebeleID').focus();

    $("#cmdBack").click(function () {
        window.location.href = "/Household/Form1C";
    });

    $('#KebeleID,#SocialWorker,#CCCCBSPCMember').bind('focusout', function (event) {
        if (!isValid($(this).val())) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $(this).off("blur");
            $(this).focus();
        }
    });

    /*
    $('#CCCCBSPCMember').change(function (e) {
        if (isValid($(this).val())) {
            $('#RegionID,#WoredaID').prop("disabled", true);
            $("#Kebele").val($("#KebeleID").val());
            $("#KebeleID,#NameOfCareTaker,#HouseHoldIDNumber,#MalnourishedChildName,#MalnourishedChildSex,#ChildDateOfBirth,#DateTypeCertificate,#MalnourishmentDegree,#StartDateTDS,#NextCNStatusDate,#EndDateTDS,#ChildID,#CaretakerID,#cmdProceed,#Remarks,#CBHIMembership,#ChildProtectionRisk").prop("disabled", false);
            $('#NameOfCareTaker').focus();
        }
    });
    */

    $('#CCCCBSPCMember').bind('focusout', function (event) {
        if (isValid($(this).val())) {
            if($("#CBHINumber").is(":disabled")){
                $('#RegionID,#WoredaID').prop("disabled", true);
                $("#Kebele").val($("#KebeleID").val());
                $("#KebeleID,#NameOfCareTaker,#HouseHoldIDNumber,#MalnourishedChildName,#MalnourishedChildSex,#ChildDateOfBirth,#DateTypeCertificate,#MalnourishmentDegree,#StartDateTDS,#NextCNStatusDate,#EndDateTDS,#ChildID,#CaretakerID,#cmdProceed,#Remarks,#CBHIMembership,#ChildProtectionRisk").prop("disabled", false);
                $('#NameOfCareTaker').focus();
            }
            else
            {
                $('#CBHINumber').focus();
            }
        }
    });

    $('#CBHINumber').bind('focusout', function (event) {
        $('#RegionID,#WoredaID').prop("disabled", true);
        $("#Kebele").val($("#KebeleID").val());
        $("#KebeleID,#NameOfCareTaker,#HouseHoldIDNumber,#MalnourishedChildName,#MalnourishedChildSex,#ChildDateOfBirth,#DateTypeCertificate,#MalnourishmentDegree,#StartDateTDS,#NextCNStatusDate,#EndDateTDS,#ChildID,#CaretakerID,#cmdProceed,#Remarks,#CBHIMembership,#ChildProtectionRisk").prop("disabled", false);
        $('#NameOfCareTaker').focus();
    });


    $('#CollectionDate').bind('focusout', function (event) {
            if (isValid($(this).val())) {
                $('#SocialWorker').focus();
                //$('#ChildDateOfBirth').Zebra_DatePicker({
                //    direction: -1,    //-1 boolean true would've made the date picker future only
                //    format: 'd/M/Y',
                //    view: 'years'
                //});
            } else { $('#CollectionDate').focus(); }
    });

    $('#StartDateTDS').change(function (e) {
        if (isValid($(this).val())) {
            $('#NextCNStatusDate').focus();
        } else { $('#StartDateTDS').focus(); }
    });

    $('#ChildDateOfBirth').change(function (e) {
        if (isValid($(this).val())) {
            $('#DateTypeCertificate').focus();
        } else { $('#ChildDateOfBirth').focus(); }        
    });

    $('#ChildDateOfBirth').focusout(function () {
        var age = getAge($('#ChildDateOfBirth').val());
        $("#Age").val(age);
    });

    //#CBHIMembership
    $('#CBHIMembership').change(function (e) {
        var membership = $('#CBHIMembership').val();
        if (membership== 'YES') {
            $('#CBHINumber').prop("disabled", false);
        } else {
            $('#CBHINumber').prop("disabled", true);
            $('#CBHINumber').val("");
        }
    });

    $("#cmdProceed").click(function () {
        //EVENTID = "ADD";
        var errors = [];
        var html = '<ul>';
        valid = true;
        $('#errors2').empty();

        if ($('#NameOfCareTaker').val() == '') {
            errors.push('<li>Caretaker Name is Required</li>');
            valid = false;
        }

        if ($('#HouseHoldIDNumber').val() == '') {
            errors.push('<li>Household ID Number is required</li>');
            valid = false;
        }
        if ($('#CaretakerID').val() == '') {
            errors.push('<li>Individual ID of caretaker in Health Family Folder required</li>');
            valid = false;
        }

        //if ($('#ChildID').val() == '') {
        //    errors.push('<li>Individual ID of child in Health Family Folder required</li>');
        //    valid = false;
        //}

        if ($('#MalnourishedChildName').val() == '') {
            errors.push('<li>Malnourished Child Name Required</li>');
            valid = false;
        }

        if ($('#MalnourishedChildSex').val() == '') {
            errors.push('<li>Malnourished Child Sex is required</li>');
            valid = false;
        }

        if ($('#ChildDateOfBirth').val() == '') {
            errors.push('<li>Childs Date Of Birth required</li>');
            valid = false;
        }

        if (jsDateDiff($('#ChildDateOfBirth').val(), $('#CollectionDate').val(), "days") > 1826) {
            errors.push('<li>Child cannot be more than 5 years</li>');
            valid = false;
        }

        if ($('#DateTypeCertificate').val() == '') {
            errors.push('<li>Malnourishment start date required</li>');
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), $('#DateTypeCertificate').val(), "days") > 60) {
            errors.push('<li>Malnourishment start date cannot be more than 2 months</li>');
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), $('#DateTypeCertificate').val(), "days") < 1) {
            errors.push('<li>Malnourishment start date cannot be a past date from collection date</li>');
            valid = false;
        }

        if ($('#MalnourishmentDegree').val() == '' || $('#MalnourishmentDegree').val() == '-') {
            errors.push('<li>Malnourishment Degree Required</li>');
            valid = false;
        }

        if ($('#StartDateTDS').val() == '') {
            errors.push('<li>TSF/CMAM start date Required</li>');
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), "01/"+$('#StartDateTDS').val(), "days") > 90) {
            errors.push('<li>TSF/CMAM start date cannot be more than 3 months</li>');
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), "01/" + $('#StartDateTDS').val(), "days") < 1) {
            errors.push('<li>TSF/CMAM start date cannot be a past date from collection date</li>');
            valid = false;
        }

        if ($('#NextCNStatusDate').val() == '') {
            errors.push('<li>Date of next check of nutritional status required</li>');
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), $('#NextCNStatusDate').val(), "days") > 365) {
            errors.push('<li>Next nutrition check cannot be more than 1 years</li>');
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), $('#NextCNStatusDate').val(), "days") < 1) {
            errors.push('<li>Next nutrition check cannot be a past date from collection date</li>');
            valid = false;
        }

        if ($('#EndDateTDS').val() == '') {
            errors.push('<li>Date when temporary DS status has ended required</li>');
            valid = false;
        }

        if ($('#ChildProtectionRisk').val() == '-') {
            errors.push('<li>Please specify if Child Potentially at Risk</li>');
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), "01/" + $('#EndDateTDS').val(), "days") > 730) {
            errors.push('<li>End of treament cannot be more than 2 years</li>');
            valid = false;
        }

        if (jsDateDiff($('#CollectionDate').val(), "01/" + $('#EndDateTDS').val(), "days") < 1) {
            errors.push('<li>End of treament cannot be a past date from collection date</li>');
            valid = false;
        }

        if (!valid) {
            html += errors.join('') + '</ul>'
            $('#errors2').show();
            $('#errors2').append(html);
            return valid;
        }
        else {
            $('#errors2').hide();
        }

        var theDtls = new Object;
        if (EVENTID == "EDIT") {

            for (var i in arrObj) {
                if (arrObj.hasOwnProperty(i)) {
                    if (arrObj[i].ColumnID == $("#ColumnID").val()) {
                        arrObj[i].NameOfCareTaker = $("#NameOfCareTaker").val();
                        arrObj[i].HouseHoldIDNumber = $("#HouseHoldIDNumber").val();
                        arrObj[i].MalnourishedChildName = $("#MalnourishedChildName").val();
                        arrObj[i].MalnourishedChildSex = $("#MalnourishedChildSex").val();
                        arrObj[i].MalnourishedChildSeName = $("#MalnourishedChildSex").find('option:selected').text();
                        arrObj[i].ChildDateOfBirth = $("#ChildDateOfBirth").val();
                        arrObj[i].DateTypeCertificate = $("#DateTypeCertificate").val();
                        arrObj[i].MalnourishmentDegree = $("#MalnourishmentDegree").val();
                        arrObj[i].MalnourishmentDegreeName = $("#MalnourishmentDegree").find('option:selected').text();
                        arrObj[i].StartDateTDS = $("#StartDateTDS").val();
                        arrObj[i].NextCNStatusDate = $("#NextCNStatusDate").val();

                        arrObj[i].CaretakerID = $("#CaretakerID").val();
                        arrObj[i].ChildID = $("#ChildID").val();
                        arrObj[i].EndDateTDS = $("#EndDateTDS").val();
                        arrObj[i].Remarks = $("#Remarks").val();

                        arrObj[i].ChildProtectionRisk = $("#ChildProtectionRisk").val();                        
                    }
                }
            }
        }
        else {
            $('#errors2').hide();
            SerialCount = SerialCount + 1;
            theDtls.ColumnID = SerialCount;
            theDtls.NameOfCareTaker = $("#NameOfCareTaker").val();
            theDtls.HouseHoldIDNumber = $("#HouseHoldIDNumber").val();
            theDtls.MalnourishedChildName = $("#MalnourishedChildName").val();
            theDtls.MalnourishedChildSex = $("#MalnourishedChildSex").val();
            theDtls.MalnourishedChildSeName = $("#MalnourishedChildSex").find('option:selected').text();
            theDtls.ChildDateOfBirth = $("#ChildDateOfBirth").val();
            theDtls.DateTypeCertificate = $("#DateTypeCertificate").val();
            theDtls.MalnourishmentDegree = $("#MalnourishmentDegree").val();
            theDtls.MalnourishmentDegreeName = $("#MalnourishmentDegree").find('option:selected').text();
            theDtls.StartDateTDS = $("#StartDateTDS").val();
            theDtls.NextCNStatusDate = $("#NextCNStatusDate").val();

            theDtls.CaretakerID = $("#CaretakerID").val();
            theDtls.ChildID = $("#ChildID").val();
            theDtls.EndDateTDS = $("#EndDateTDS").val();
            theDtls.Remarks = $("#Remarks").val();

            theDtls.ChildProtectionRisk = $("#ChildProtectionRisk").val();
            
            arrObj.push(theDtls);
        }
        var myXml = createXmlstring(arrObj, 1);
        mygrid.clearAll();
        mygrid.parse(myXml);
        $("#cmdSave").prop("disabled", false);
        $("#NameOfCareTaker,#HouseHoldIDNumber,#MalnourishedChildName,#MalnourishedChildSex,#ChildDateOfBirth,#DateTypeCertificate,#StartDateTDS,#NextCNStatusDate,#ChildID,#CaretakerID,#EndDateTDS,#Remarks,#ChildProtectionRisk").val('');

        $("#MalnourishmentDegree").val("-");
        EVENTID = null;
        $('#NameOfCareTaker').focus();
    });
});

function createXmlstring(arrObject, inMemory) {
    var xml;
    var gridID;
    xml = '';
    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            xml = xml + '<row id="' + i + '">';
            for (var j in arrObj[i]) {
                if (arrObj[i].hasOwnProperty(j)) {
                    if (j == "ColumnID") {
                        xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                        gridID = arrObj[i][j];
                    }
                }
                if (j == "NameOfCareTaker" || j == "HouseHoldIDNumber" || j == "MalnourishedChildName" || j == "MalnourishedChildSeName" || j=="ChildProtectionRisk") {
                    xml = xml + '<cell>' + arrObj[i][j] + '</cell>';
                }
            }
            xml = xml + '<cell>../DHTMLX/codebase/imgs/edit_icon.gif^edit^javascript:UpdateMembers(' + gridID + ');^_self</cell>';
            xml = xml + '</row>';
        }
    }
    xml = '<rows total_count="' + arrObj.length + '">' + xml + '</rows>';
    $("#MemberXml").val(JSON.stringify(arrObject));
    return xml;
}


function UpdateMembers(_ColumnID) {
    var selectedRow = mygrid.getSelectedId();
    _ColumnID = mygrid.cells(selectedRow, 0).getValue();
    $("#NameOfCareTaker,#HouseHoldIDNumber,#MalnourishedChildName,#MalnourishedChildSex,#ChildDateOfBirth,#DateTypeCertificate,#MalnourishmentDegree,#StartDateTDS,#NextCNStatusDate,#ChildID,#CaretakerID,#EndDateTDS,#Remarks,#ChildProtectionRisk").val('');

    for (var i in arrObj) {
        if (arrObj.hasOwnProperty(i)) {
            if (arrObj[i].ColumnID == _ColumnID) {
                $("#ColumnID").val(arrObj[i].ColumnID);
                $("#NameOfCareTaker").val(arrObj[i].NameOfCareTaker);
                $("#HouseHoldIDNumber").val(arrObj[i].HouseHoldIDNumber);
                $("#MalnourishedChildName").val(arrObj[i].MalnourishedChildName);
                $("#MalnourishedChildSex").val(arrObj[i].MalnourishedChildSex);
                $("#ChildDateOfBirth").val(arrObj[i].ChildDateOfBirth);
                $("#DateTypeCertificate").val(arrObj[i].DateTypeCertificate);
                $("#MalnourishmentDegree").val(arrObj[i].MalnourishmentDegree);
                $("#StartDateTDS").val(arrObj[i].StartDateTDS);
                $("#NextCNStatusDate").val(arrObj[i].NextCNStatusDate);
                $("#ChildID").val(arrObj[i].ChildID);
                $("#CaretakerID").val(arrObj[i].CaretakerID);
                $("#EndDateTDS").val(arrObj[i].EndDateTDS);
                $("#Remarks").val(arrObj[i].Remarks);       
                $("#ChildProtectionRisk").val(arrObj[i].ChildProtectionRisk);          
            }
        }
    }
    //$("#HouseHoldMemberName,#MedicalRecordNumber,#DateOfBirth,#Sex,#PWL,#Handicapped,#ChronicallyIll,#NutritionalStatus,#EnrolledInSchool,#SchoolName").prop("disabled", false);
    $("#NameOfCareTaker,#HouseHoldIDNumber,#MalnourishedChildName,#MalnourishedChildSex,#ChildDateOfBirth,#DateTypeCertificate,#MalnourishmentDegree,#StartDateTDS,#NextCNStatusDate,#ChildID,#CaretakerID,#EndDateTDS,#Remarks,#ChildProtectionRisk").prop("disabled", false);
    //#Age,
    $("#cmdProceed").html('<i></i>Update Details');
    $('#NameOfCareTaker').focus();
    EVENTID = "EDIT";
}
