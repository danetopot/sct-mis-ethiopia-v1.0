﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCTMis.Models
{
    public class RoleList
    {
        public int ColumnID { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }
    }

    public class AuditTrailModel
    {
        public int ColumnID { get; set; }
        public string UserName { get; set; }
        public string ScreenName { get; set; }
        public string ComputerName { get; set; }
        public string IpAddress { get; set; }
        public string TransactionDetails { get; set; }
        public string DateCreated { get; set; }
    }

    public class ClientType
    {
        public string ClientTypeID { get; set; }
        public string ClientTypeName { get; set; }
    }

    public class ServiceType
    {
        public string ServiceID { get; set; }
        public string ServiceName { get; set; }
    }

    public class ReportLogModel
    {
        public string Id { get; set; }
        public string ReportName { get; set; }
        public string ReportPeriod { get; set; }
        public string ReportFilePath { get; set; }
        public string GeneratedBy { get; set; }
        public DateTime GeneratedOn { get; set; }
        public bool IsReplicated { get; set; }
        public string ReplicatedBy { get; set; }
        public DateTime ReplicatedOn { get; set; }
    }

    public class ReportType
    {
        public string ReportID { get; set; }
        public string ReportName { get; set; }
    }

    public class ReportDisaggregation
    {
        public string DisaggregationID { get; set; }
        public string DisaggregationName { get; set; }
    }

    public class HouseholdProfilePDSDetailReport
    {
        public string UniqueID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string ProfileDSDetailID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string HouseholdName { get; set; }
        public string HouseholdIDNumber { get; set; }
        public string ClientName { get; set; }
        public string IndividualID { get; set; }
        public string DateOfBirth { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string ChildUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string SchoolName { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string CCCCBSPCMember { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string ApprovedBy { get; set; }
        //public DateTime ApprovedOn { get; set; }
        //public List<HouseholdProfilePDSDetailReport> PdsDetailList { get; set; }
    }

    public class HouseholdProfilePDSSummaryReport
    {
        public string UniqueID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountEnrolledInCBHI { get; set; }
        public int CountOfChildProtectionCases { get; set; }
        public int CountNonMalnuorished { get; set; }
        public int CountMalnourished { get; set; }
        public int CountSeverelyMalnourished { get; set; }
        public int CountHandicapped { get; set; }
        public int CountChronicallyIll { get; set; }
        public int CountLactating { get; set; }
        public int CountPregnant { get; set; }
        public int CountEnrolledInSchool { get; set; }
        public int CountChildUnderTSForCMAM { get; set; }
    }

    public class HouseholdProfileTDSPLWDetailReport
    {
        public string UniqueID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string HouseholdIDNumber { get; set; }
        public string NameOfPLW { get; set; }
        public string MedicalRecordNumber { get; set; }
        public string PLWAge { get; set; }
        public string StartDateTDS { get; set; }
        public string EndDateTDS { get; set; }
        public string BabyDateOfBirth { get; set; }
        public string BabyName { get; set; }
        public string BabySex { get; set; }
        public string NutritionalStatusPLW { get; set; }
        public string NutritionalStatusInfant { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ApproveBy { get; set; }
        public DateTime ApprovedOn { get; set; }
    }

    public class HouseholdProfileTDSPLWSummaryReport
    {
        public string UniqueID { get; set; }
        public string ProfileTDSPLWID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountEnrolledInCBHI { get; set; }
        public int CountOfChildProtectionCases { get; set; }
        public int CountNonMalnuorishedPLW { get; set; }
        public int CountMalnourishedPLW { get; set; }
        public int CountSeverelyMalnourishedPLW { get; set; }
        public int CountNonMalnuorishedInfant { get; set; }
        public int CountMalnourishedInfant { get; set; }
        public int CountSeverelyMalnourishedInfant { get; set; }
    }

    public class HouseholdProfileTDSCMCDetailReport
    {
        public string UniqueID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string CaretakerID { get; set; }
        public string NameOfCareTaker { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string MalnourishedChildName { get; set; }
        public string MalnourishedChildSex { get; set; }
        public string ChildID { get; set; }
        public string ChildDateOfBirth { get; set; }
        public string DateTypeCertificate { get; set; }
        public string MalnourishmentDegree { get; set; }
        public string StartDateTDS { get; set; }
        public string EndDateTDS { get; set; }
        public string NextCNStatusDate { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string CCCCBSPCMember { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public string ApprovedBy { get; set; }
        //public DateTime ApprovedOn { get; set; }
    }

    public class HouseholdProfileTDSCMCSummaryReport
    {
        public string UniqueID { get; set; }
        public string ProfileTDSCMCID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountEnrolledInCBHI { get; set; }
        public int CountOfChildProtectionCases { get; set; }
        public int CountNonMalnuorishedChildren { get; set; }
        public int CountMalnourishedChildren { get; set; }
        public int CountSeverelyMalnourishedChildren { get; set; }
    }

    public class SocialServicesNeedsDetailReport
    {
        public string UniqueID { get; set; }
        public string ProfileHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string IndividualID { get; set; }
        public string ClientType { get; set; }
        public string NameOfHouseHoldMember { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string PreAnteNatalCare { get; set; }
        public string Immunization { get; set; }
        public string SupplementaryFeeding { get; set; }
        public string MonthlyGMPSessions { get; set; }
        public string EnrolmentAttendance { get; set; }
        public string BCCSessions { get; set; }
        public string UnderTSF { get; set; }
        public string BirthRegistration { get; set; }
        public string HealthPostCheckUp { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string PreNatalVisit1 { get; set; }
        public string PreNatalVisit3 { get; set; }
        public string PostNatalVisit { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
    }

    public class SocialServicesNeedsSummaryReport
    {
        public string UniqueID { get; set; }
        public string ProfileHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountPreAnteNatalCare { get; set; }
        public int CountImmunization { get; set; }
        public int CountSupplementaryFeeding { get; set; }
        public int CountMonthlyGMPSessions { get; set; }
        public int CountEnrolmentAttendance { get; set; }
        public int CountBCCSessions { get; set; }
        public int CountUnderTSF { get; set; }
        public int CountBirthRegistration { get; set; }
        public int CountCBHIMembership { get; set; }
        public int CountChildProtectionRisk { get; set; }
        public int CountPreNatalVisit1 { get; set; }
        public int CountPreNatalVisit3 { get; set; }
        public int CountPostNatalVisit { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
    }

    public class ServiceComplianceDetailReport
    {
        public string UniqueID { get; set; }
        public string ProfileDSHeaderID { get; set; }
    public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }

        public string Gote { get; set; }
        public string ClientName { get; set; }
        public string ClientType { get; set; }
        public string IndividualID { get; set; }
        public string Age { get; set; }
        public string Sex { get; set; }
        public string ServiceID { get; set; }
        public string ServiceName { get; set; }
        public string Complied { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
    }

    public class ServiceComplianceSummaryReport
    {
        public string UniqueID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountAntePostNatalCare { get; set; }
        public int CountImmunization { get; set; }
        public int CountTSFCMAM { get; set; }
        public int CountGMPSessions { get; set; }
        public int CountSchoolEnrolment { get; set; }
        public int CountBCCSessions { get; set; }
        public int CountBirthRegistration { get; set; }
        public int Count3PrenatalCareVisits { get; set; }
        public int Count1PrenatalCareVisits { get; set; }
        public int CountCheckUpOfChildAtHealthPost { get; set; }
        public int CountSupplementaryFeeding { get; set; }
        public int CountCBHIMembership { get; set; }
        public int CountChildProtection { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
    }

    public class CaseManagementComplianceDetailReport
    {
        public string UniqueID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }

        public string IndividualID { get; set; }
        public string ClientName { get; set; }
        public string ClientType { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public int AntePostNatal { get; set; }
        public int Immunization { get; set; }
        public int UnderTSFCMAM { get; set; }
        public int GMPSessions { get; set; }
        public int SchoolEnrolment { get; set; }
        public int BCCSessions { get; set; }
        public int BirthRegistration { get; set; }
        public int PrenatalCareVisits3 { get; set; }
        public int PrenatalCareVisits1 { get; set; }
        public int CheckuUpTwiceaMonthAtHealthPost { get; set; }
        public int SupplementaryFeeding { get; set; }
        public int CBHIMembership { get; set; }
        public int ChildProtection { get; set; }
        //public DateTime CollectionDate { get; set; }
        //public string SocialWorker { get; set; }
    }

    public class CaseManagementComplianceSummaryReport
    {
        public string UniqueID { get; set; }
        public string ProfileDSHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountAntePostNatal { get; set; }
        public int CountImmunization { get; set; }
        public int CountUnderTSFCMAM { get; set; }
        public int CountGMPSessions { get; set; }
        public int CountSchoolEnrolment { get; set; }
        public int CountBCCSessions { get; set; }
        public int CountBirthRegistration { get; set; }
        public int CountPrenatalCareVisits3 { get; set; }
        public int CountPrenatalCareVisits1 { get; set; }
        public int CountCheckuUpTwiceaMonthAtHealthPost { get; set; }
        public int CountSupplementaryFeeding { get; set; }
        public int CountCBHIMembership { get; set; }
        public int CountChildProtection { get; set; }
        //public DateTime CollectionDate { get; set; }
        //public string SocialWorker { get; set; }
    }

    public class RetargetingDetailReport
    {
        public string UniqueID { get; set; }
        public string ProfileHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public string HouseHoldIDNumber { get; set; }
        public string IndividualID { get; set; }
        public string ClientType { get; set; }
        public string NameOfHouseHoldMember { get; set; }
        public string Sex { get; set; }
        public string Age { get; set; }
        public string PreAnteNatalCare { get; set; }
        public string Immunization { get; set; }
        public string SupplementaryFeeding { get; set; }
        public string MonthlyGMPSessions { get; set; }
        public string EnrolmentAttendance { get; set; }
        public string BCCSessions { get; set; }
        public string UnderTSF { get; set; }
        public string BirthRegistration { get; set; }
        public string HealthPostCheckUp { get; set; }
        public string CBHIMembership { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string PreNatalVisit1 { get; set; }
        public string PreNatalVisit3 { get; set; }
        public string PostNatalVisit { get; set; }
        public DateTime CollectionDate { get; set; }
        public string SocialWorker { get; set; }
    }

    public class RetargetingSummaryReport
    {
        public string UniqueID { get; set; }
        public string ProfileHeaderID { get; set; }
        public string FiscalYear { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleName { get; set; }
        public int KebeleID { get; set; }
        public string Gote { get; set; }
        public int CountPreAnteNatalCare { get; set; }
        public int CountImmunization { get; set; }
        public int CountSupplementaryFeeding { get; set; }
        public int CountMonthlyGMPSessions { get; set; }
        public int CountEnrolmentAttendance { get; set; }
        public int CountBCCSessions { get; set; }
        public int CountUnderTSF { get; set; }
        public int CountBirthRegistration { get; set; }
        public int CountCBHIMembership { get; set; }
        public int CountChildProtectionRisk { get; set; }
        public int CountPreNatalVisit1 { get; set; }
        public int CountPreNatalVisit3 { get; set; }
        public int CountPostNatalVisit { get; set; }
    }

    public class StandardReportModel
    {
        public int? RegionID { get; set; }
        public int? WoredaID { get; set; }
        public int? KebeleID { get; set; }
        public string ReportName { get; set; }
        public string ReportingPeriod { get; set; }
        public string ReportDisaggregation { get; set; }
        public string SocialWorker { get; set; }

        // RPT 1
        public string Gender { get; set; }
        public string Pregnant { get; set; }
        public string Lactating { get; set; }
        public string Handicapped { get; set; }
        public string ChronicallyIll { get; set; }
        public string NutritionalStatus { get; set; }
        public string ChildUnderTSForCMAM { get; set; }
        public string EnrolledInSchool { get; set; }
        public string ChildProtectionRisk { get; set; }
        public string CBHIMembership { get; set; }

        // RPT 2
        public DateTime StartDateTDSPLW { get; set; }
        public DateTime EndDateTDSPLW { get; set; }
        public string NutritionalStatusPLW { get; set; }
        public string NutritionalStatusInfant { get; set; }

        // RPT 3
        public DateTime StartDateTDSCMC { get; set; }
        public DateTime EndDateTDSCMC { get; set; }
        public string MalnourishmentDegree { get; set; }

        // RPT 4
        public string ServiceType { get; set; }
        public string ClientType { get; set; }

        public DataTable JsonData { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<ReportingPeriod> rptPeriod { get; set; }
        public IEnumerable<ReportType> rptTypes { get; set; }
        public IEnumerable<ReportDisaggregation> rptDisaggregation { get; set; }
        public IEnumerable<ServiceType> services { get; set; }
        public IEnumerable<Gender> sex { get; set; }
        public IEnumerable<SocialWorker> worker { get; set; }
        public IEnumerable<YesNo> yesnos { get; set; }
        public IEnumerable<NutritionalStatus> nutrStatus { get; set; }
        public IEnumerable<ClientType> clients { get; set; }
    }

    public class ReportEngineModel
    {
        public int ColumnID { get; set; }
        public int? RegionID { get; set; }
        public int? WoredaID { get; set; }
        public string ReportName { get; set; }
        public string ReportingPeriod { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public int LocationType { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<ReportingPeriod> reportgperiod { get; set; }
        public IEnumerable<ReportType> rptTypes { get; set; }
    }


    public class ReportExportModel
    {
        public int ColumnID { get; set; }
        public int RegionCodeID { get; set; }
        public int WoredaCodeID { get; set; }
        public int KebeleCodeID { get; set; }
        public int FiscalYear { get; set; }
        [Required]
        public string ReportName { get; set; }
        public string ReportingPeriod { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }

        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
        public IEnumerable<Kebele> kebeles { get; set; }
        public IEnumerable<ReportType> rptTypes { get; set; }
        public IEnumerable<FinancialYear> fiscalYears { get; set; }
    }

    public class Form6ReportHeader
    {
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string ReportingPeriod { get; set; }
        public int DSHousholds { get; set; }
        public int DSMembers { get; set; }
        public int DSHouseholdCo { get; set; }
        public int DsMembersCo { get; set; }
        public int DsHouseholdCBHI { get; set; }
        public int PLWCount { get; set; }
        public int CaretakerCount { get; set; }
    }

    public class Form6Report
    {
        public string ServiceName { get; set; }
        public int NoOfClients { get; set; }
        public int Failedonce { get; set; }
        public int Failedtwice { get; set; }
        public int Failedmore { get; set; }
        public int HouseVisits { get; set; }
        public string Remarks { get; set; }
    }

    public class DefaultWoredaModel
    {
        public int RegionID { get; set; }
        public string RegionName { get; set; }
        public int WoredaID { get; set; }
        public string WoredaName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
    }

    public class RegionGrid
    {
        public int ColumnID { get; set; }
        public int RegionID { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
    }

    public class RegionAdd
    {
        public int ColumnID { get; set; }
        public int RegionID { get; set; }
        [Required(ErrorMessage = "Region Code is Required")]
        public string RegionCode { get; set; }
        [Required(ErrorMessage = "Region Name is Required")]
        public string RegionName { get; set; }
    }

    public class RegionModifyModel
    {
        public int ColumnID { get; set; }
        public int RegionID { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
    }

    public class WoredaAdd
    {
        public int ColumnID { get; set; }
        public int WoredaID { get; set; }
        [Required(ErrorMessage = "Woreda Code is Required")]
        public string WoredaCode { get; set; }
        [Required(ErrorMessage = "Region is Required")]
        public int RegionID { get; set; }
        [Required(ErrorMessage = "Woreda Name is Required")]
        public string WoredaName { get; set; }
        public IEnumerable<Region> regions { get; set; }
    }

    public class WoredaModifyModel
    {
        public int ColumnID { get; set; }
        public int WoredaID { get; set; }
        public string WoredaCode { get; set; }
        public int RegionID { get; set; }
        public string WoredaName { get; set; }
        public IEnumerable<Region> regions { get; set; }
    }

    public class KebeleAdd
    {
        public int ColumnID { get; set; }
        [Required(ErrorMessage = "Region is Required")]
        public int RegionID { get; set; }
        [Required(ErrorMessage = "Woreda is Required")]
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        [Required(ErrorMessage = "Kebele Code is Required")]
        public string KebeleCode { get; set; }
        [Required(ErrorMessage = "Kebele Name is Required")]
        public string KebeleName { get; set; }
        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
    }

    public class KebeleModifyModel
    {
        public int ColumnID { get; set; }
        public int RegionID { get; set; }
        public int WoredaID { get; set; }
        public int KebeleID { get; set; }
        public string KebeleCode { get; set; }
        public string KebeleName { get; set; }
        public IEnumerable<Region> regions { get; set; }
        public IEnumerable<Woreda> woredas { get; set; }
    }

    public class WoredaGrid
    {
        public int ColumnID { get; set; }
        public int WoredaID { get; set; }
        public string WoredaCode { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
    }

    public class KebeleGrid
    {
        public int ColumnID { get; set; }
        public int KebeleID { get; set; }
        public string RegionName { get; set; }
        public string WoredaName { get; set; }
        public string KebeleCode { get; set; }
        public string KebeleName { get; set; }
    }

    public class DataExportGrid
    {
        public int ColumnID { get; set; }
        public string GeneratedBy { get; set; }
        public string GeneratedOn { get; set; }
        public string ZipfileName { get; set; }
    }

    public class DataImportGrid
    {
        public int ColumnID { get; set; }
        public string TableName { get; set; }
        public string ReplicatedBy { get; set; }
        public string ReplicatedOn { get; set; }
        public int ImportedDataID { get; set; }
        public string FileName { get; set; }        
        public string ReplicateXML { get; set; }       
    }

    public class DataExportPreviewGrid
    {
        public int ColumnID { get; set; }
        public string TableName { get; set; }
        public string xmlString { get; set; }
        public string filePath { get; set; }
    }

    public class ImportListModel
    {
        public int ImportID { get; set; }
        public DateTime ImportDate { get; set; }
        public string ImportPath { get; set; }
        public int CreatedBy { get; set; }
    }
    public class ReplicationDetail
    {
        public int GeneratedBy { get; set; }
        public string SourceName { get; set; }
        public string SourceDetails { get; set; }
        public string filePath { get; set; }
    }

    public class Settings
    {
        public int LocationType { get; set; }
    }

    //1 - Woreda, 2 - Regional Office, 3 - Federal HQ
    public enum LocationTypes
    {
        Nana,
        Woreda,
        Regional,
        Federal
    }
}
