﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SCTMis.Models.CaptureForm5>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Capture Form 5B
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("CaptureForm5B", "MonitoringCapture", FormMethod.Post, new { id = "MyForm" }))
   { %>
        <%--The First Row--%>
            <div class="col-7">                
            <label>
                Region <%: Html.ValidationMessageFor(model => model.RegionID) %>
                <%: Html.DropDownListFor(model => model.RegionID,
                    new SelectList(Model.regions, "RegionID", "RegionName"), new { tabindex = "1", @class = "alphaonly" })%>
            </label>
            </div>
            <div class="col-7">                
            <label>
                Woreda  <%: Html.ValidationMessageFor(model => model.WoredaID) %>
                <%: Html.DropDownListFor(model => model.WoredaID,
                    new SelectList(Model.woredas, "WoredaID", "WoredaName"), new { tabindex = "2" })%>
            </label>
            </div>
            <div class="col-4">                              
            <label>
                Kebele <%: Html.ValidationMessageFor(model => model.KebeleID) %>  
                <%: Html.DropDownListFor(model => model.KebeleID,
                    new SelectList(Model.kebeles, "KebeleID", "KebeleName"), new { tabindex = "3" })%>
            </label>
            </div>

            <div class="col-4">                
            <label>
                Reporting Period <%: Html.ValidationMessageFor(model => model.ReportingPeriod) %>
                <%: Html.DropDownListFor(model => model.ReportingPeriod,
                    new SelectList(Model.reportgperiod, "PeriodID", "PeriodName"), new { tabindex = "4"})%>
            </label>
            </div>
            <button id="cmdProceed" type="button" class="btn btn-icon btn-primary glyphicons circle_plus" tabindex="20"><i></i>Load Grid</button>
            <br />
            <div id="thebox" style="position:relative; width: 680px; height: 340px"></div>
            <br />
            <div class="form-actions">
	            <button id="cmdBack" type="button" class="btn btn-icon btn-default glyphicons step_backward"><i></i>Back</button>
            </div>
          <br />
            <%: Html.ValidationSummary(true) %>
            <%: Html.HiddenFor(model => model.Kebele)%>
            <%: Html.HiddenFor(model => model.rptPeriod)%>
            <%: Html.HiddenFor(model => model.CapturedXml)%>
    <% } %>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ScriptsSection" runat="server">
    <link href="../Scripts/jtable/themes/lightcolor/gray/jtable.min.css" rel="stylesheet" />
    <script src="../Scripts/jtable/jquery.jtable.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#KebeleID,#ReportingPeriod,#cmdProceed").prop("disabled", false);
            $("#SocialWorker").attr("readonly", false);
            $("#KebeleID,#ReportingPeriod").val('0');
            $("#cmdBack").prop("disabled", false);
            $("#RegionID,#WoredaID,#cmdProduce").prop("disabled", true);
            $(":input").blur();
            $('#KebeleID').focus();

            $('#thebox').jtable({
                title: 'Case Management',
                paging: true,
                actions: {
                    listAction: '/MonitoringCapture/getForm5BTDSPLWList',
                },
                fields: {
                    ColumnID: {
                        key: true,
                        create: false,
                        edit: false,
                        list: false
                    },
                    ProfileTDSPLWID: {
                        key: true,
                        create: false,
                        edit: false,
                        list: false
                    },
                    MISummary: {
                        title: 'MIS Summary',
                        width: '5%',
                        sorting: false,
                        edit: false,
                        create: false,
                        display: function (MISData) {
                            //Create an image that will be used to open child table
                            var $img = $('<img src="../DHTMLX/codebase/imgs/combo_select_dhx_black.gif" title="view Household members" />');
                            //Open child table when user clicks the image
                            $img.click(function () {
                                $('#thebox').jtable('openChildTable',
                                        $img.closest('tr'),
                                        {
                                            title: MISData.record.NameOfPLW + ' - Tick Summary For MIS',
                                            paging: true,
                                            pageSize: 5,
                                            pageSizes: [5, 10],
                                            actions: {
                                                listAction: '/MonitoringCapture/FetchGridForm5AMISSummary?FormName=5B',
                                                updateAction: '/MonitoringCapture/SaveForm5BMIS?KebeleID=' + $('#KebeleID').val() + '&ReportingPeriodID=' + $('#ReportingPeriod').val() + '&ProfileTDSPLWID=' + MISData.record.ProfileTDSPLWID,
                                            },
                                            fields: {
                                                ID: {
                                                    key: true,
                                                    create: false,
                                                    edit: false,
                                                    list: false
                                                },
                                                Reason: {
                                                    title: 'non-compliance Reason',
                                                    width: '20%',
                                                    create: false,
                                                    edit: false
                                                },
                                                ActionResponse: {
                                                    title: 'Response',
                                                    width: '5%',
                                                    create: false,
                                                    edit: true,
                                                    options: { 'NO': 'NO', 'YES': 'YES' }
                                                },
                                                Action: {
                                                    title: 'Mitigating action suggested',
                                                    width: '40%',
                                                    create: false,
                                                    edit: true,
                                                    options: '/MonitoringCapture/GridFetchForm5AMISSummary?FormName=5B'
                                                },
                                                CompletedDate: {
                                                    title: 'Completed Date',
                                                    width: '15%',
                                                    type: 'date',
                                                    displayFormat: 'mm/dd/yy',
                                                    create: false,
                                                    edit: true,
                                                    list: false
                                                },
                                            }
                                        }, function (data) { //opened handler
                                            data.childTable.jtable('load');
                                        });
                            });
                            //Return image to show on the person row
                            return $img;
                        }
                    },
                    ReportingPeriodID: {
                        title: '',
                        type: 'hidden',
                        create: false,
                        edit: false,
                        list: false,
                    },
                    HouseHoldIDNumber: {
                        title: 'HouseHold ID Number',
                        width: '15%',
                        create: false,
                        edit: false,
                    },
                    NameOfPLW: {
                        title: 'Name Of PLW',
                        width: '20%',
                        create: false,
                        edit: false,
                    }
                }
            });

            $("#cmdBack").click(function () {
                window.location.href = "/MonitoringCapture/Form5BList";
            });

            $("#cmdProceed").click(function () {
                $('#thebox').jtable('load', { KebeleID: $('#KebeleID').val(), ReportingPeriodID: $("#ReportingPeriod").val() });
            });

        });

    </script>
</asp:Content>

