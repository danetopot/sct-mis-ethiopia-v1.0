﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace SCTMis.Services
{
    public class HouseholdServices
    {

        public ModifyForm1C FetchForm1CDetailsByID(string _ProfileTDSCMCID)
        {
            ModifyForm1C dtls = new ModifyForm1C();

            List<ModifyForm1C> ApproveResult
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<ModifyForm1C>(";Exec ProfileTDSCMCID @ProfileTDSCMCID,@LoggedInUser",
                 new
                 {
                     ProfileTDSCMCID = _ProfileTDSCMCID,
                     LoggedInUser = Membership.GetUser().UserName.ToString()
                 });

            DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<ModifyForm1C>(ApproveResult);

            if (dtDetails.Rows.Count > 0)
            {
                DataRow row = dtDetails.Rows[0];

                dtls.ProfileTDSCMCID = row["ProfileTDSCMCID"].ToString();
                dtls.KebeleID = int.Parse(row["KebeleID"].ToString());
                dtls.WoredaID = int.Parse(row["WoredaID"].ToString());
                dtls.RegionID = int.Parse(row["RegionID"].ToString());
                dtls.Gote = row["Gote"].ToString();
                dtls.Gare = row["Gare"].ToString();
                dtls.NameOfCareTaker = row["NameOfCareTaker"].ToString();
                dtls.HouseHoldIDNumber = row["HouseHoldIDNumber"].ToString();
                dtls.CollectionDate = row["CollectionDate"].ToString();
                dtls.SocialWorker = row["SocialWorker"].ToString();
                dtls.CCCCBSPCMember = row["CCCCBSPCMember"].ToString();
                dtls.CreatedBy = row["CreatedBy"].ToString();
                dtls.MalnourishedChildName = row["MalnourishedChildName"].ToString();
                dtls.MalnourishedChildSex = row["MalnourishedChildSex"].ToString();
                dtls.StartDateTDS = row["StartDateTDS"].ToString();
                dtls.DateTypeCertificate = row["DateTypeCertificate"].ToString();
                dtls.MalnourishmentDegree = row["MalnourishmentDegree"].ToString();
                dtls.StartDateTDS = row["StartDateTDS"].ToString();
                dtls.EndDateTDS = row["EndDateTDS"].ToString();
                dtls.CaretakerID = row["CaretakerID"].ToString();
                dtls.ChildDateOfBirth = row["ChildDateOfBirth"].ToString();
                dtls.ChildID = row["ChildID"].ToString();
                dtls.NextCNStatusDate = row["NextCNStatusDate"].ToString();
                dtls.AllowEdit = Boolean.Parse(row["AllowEdit"].ToString());
                dtls.Remarks = row["Remarks"].ToString();
                dtls.ChildProtectionRisk = row["ChildProtectionRisk"].ToString();
                dtls.CBHIMembership = row["CBHIMembership"].ToString();
                dtls.CBHINumber = row["CBHINumber"].ToString();

            }
            return dtls;
        }

        public ModifyForm1B FetchForm1BDetailsByID(string _ProfileTDSPLWID)
        {
            ModifyForm1B dtls = new ModifyForm1B();

            List<CaptureForm1B> ApproveResult
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<CaptureForm1B>(";Exec ProfileTDSPLWID @ProfileTDSPLWID,@LoggedInUser",
                 new
                 {
                     ProfileTDSPLWID = _ProfileTDSPLWID,
                     LoggedInUser = Membership.GetUser().UserName.ToString()
                 });

            DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<CaptureForm1B>(ApproveResult);

            if (dtDetails.Rows.Count > 0)
            {
                DataRow row = dtDetails.Rows[0];

                dtls.ProfileTDSPLWID = row["ProfileTDSPLWID"].ToString();
                dtls.KebeleID = int.Parse(row["KebeleID"].ToString());
                dtls.WoredaID = int.Parse(row["WoredaID"].ToString());
                dtls.RegionID = int.Parse(row["RegionID"].ToString());
                dtls.Gote = row["Gote"].ToString();
                dtls.Gare = row["Gare"].ToString();
                dtls.PLW = row["PLW"].ToString();
                dtls.NameOfPLW = row["NameOfPLW"].ToString();
                dtls.HouseHoldIDNumber = row["HouseHoldIDNumber"].ToString();
                dtls.CollectionDate = row["CollectionDate"].ToString();
                dtls.SocialWorker = row["SocialWorker"].ToString();
                dtls.CCCCBSPCMember = row["CCCCBSPCMember"].ToString();
                dtls.CreatedBy = row["CreatedBy"].ToString();
                dtls.MedicalRecordNumber = row["MedicalRecordNumber"].ToString();
                dtls.PLWAge = row["PLWAge"].ToString();
                dtls.StartDateTDS = row["StartDateTDS"].ToString();
                dtls.EndDateTDS = row["EndDateTDS"].ToString();
                dtls.NutritionalStatusPLW = row["NutritionalStatusPLW"].ToString();
                dtls.PLWAge = row["PLWAge"].ToString();
                dtls.PLWAge = row["PLWAge"].ToString();
                dtls.AllowEdit = Boolean.Parse(row["AllowEdit"].ToString());
                dtls.Remarks = row["Remarks"].ToString();
                dtls.ChildProtectionRisk = row["ChildProtectionRisk"].ToString();
                dtls.CBHIMembership = row["CBHIMembership"].ToString();
                dtls.CBHINumber = row["CBHINumber"].ToString();

            }
            return dtls;
        }

        public CaptureForm1A FetchForm1ADetailsByID(string _ProfileDSHeaderID)
        {
            CaptureForm1A dtls = new CaptureForm1A();

            List<CaptureForm1A> ApproveResult
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<CaptureForm1A>(";Exec FetchForm1ADetailsByID @ProfileDSHeaderID,@LoggedInUser",
                 new
                 {
                     ProfileDSHeaderID = _ProfileDSHeaderID,
                     LoggedInUser = Membership.GetUser().UserName.ToString()
                 });

            DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<CaptureForm1A>(ApproveResult);

            if (dtDetails.Rows.Count > 0)
            {
                DataRow row = dtDetails.Rows[0];

                dtls.ProfileDSHeaderID = row["ProfileDSHeaderID"].ToString();
                dtls.KebeleID = int.Parse(row["KebeleID"].ToString());
                dtls.WoredaID = int.Parse(row["WoredaID"].ToString());
                dtls.RegionID = int.Parse(row["RegionID"].ToString());
                dtls.Gote = row["Gote"].ToString();
                dtls.Gare = row["Gare"].ToString();
                dtls.NameOfHouseHoldHead = row["NameOfHouseHoldHead"].ToString();
                dtls.HouseHoldIDNumber = row["HouseHoldIDNumber"].ToString();
                dtls.CollectionDate = row["CollectionDate"].ToString();
                dtls.SocialWorker = row["SocialWorker"].ToString();
                dtls.CCCCBSPCMember = row["CCCCBSPCMember"].ToString();
                dtls.CBHIMembership = row["CBHIMembership"].ToString();
                dtls.CBHINumber = row["CBHINumber"].ToString();
                dtls.CreatedBy = row["CreatedBy"].ToString();
                dtls.AllowEdit = Boolean.Parse(row["AllowEdit"].ToString());
                dtls.Remarks = row["Remarks"].ToString();
            }
            return dtls;
        }

        public bool InsertForm1A(CaptureForm1A newModel)
        {
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");

                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CaptureForm1A[] MemberList = objJavascript.Deserialize<CaptureForm1A[]>(newModel.MemberXml);

                foreach (var array in MemberList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<HouseHoldMemberName>" + array.HouseHoldMemberName + "</HouseHoldMemberName>";
                    strXML = strXML + "<IndividualID>" + array.IndividualID + "</IndividualID>";
                    strXML = strXML + "<Pregnant>" + array.Pregnant + "</Pregnant>";
                    strXML = strXML + "<Lactating>" + array.Lactating + "</Lactating>";
                    strXML = strXML + "<DateOfBirth>" + array.DateOfBirth + "</DateOfBirth>";
                    strXML = strXML + "<Age>" + array.Age + "</Age>";
                    strXML = strXML + "<Sex>" + array.Sex + "</Sex>";
                    strXML = strXML + "<PWL>" + array.PWL + "</PWL>";
                    strXML = strXML + "<Handicapped>" + array.Handicapped + "</Handicapped>";
                    strXML = strXML + "<ChronicallyIll>" + array.ChronicallyIll + "</ChronicallyIll>";
                    strXML = strXML + "<NutritionalStatus>" + array.NutritionalStatus + "</NutritionalStatus>";
                    strXML = strXML + "<childUnderTSForCMAM>" + array.childUnderTSForCMAM + "</childUnderTSForCMAM>";
                    strXML = strXML + "<EnrolledInSchool>" + array.EnrolledInSchool + "</EnrolledInSchool>";
                    strXML = strXML + "<Grade>" + array.Grade + "</Grade>";
                    strXML = strXML + "<SchoolName>" + array.SchoolName + "</SchoolName>";
                    strXML = strXML + "<ChildProtectionRisk>" + array.ChildProtectionRisk + "</ChildProtectionRisk>";
                    //strXML = strXML + "<Remarks>" + array.Remarks + "</Remarks>";
                    strXML = strXML + "</row>";
                    Console.WriteLine(strXML);
                }

                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec InsertForm1A @KebeleID,@Gote,@Gare,@NameOfHouseHoldHead,@HouseHoldIDNumber,@Remarks,@CollectionDate,@SocialWorker,@CCCCBSPCMember,@CBHIMembership,@CBHINumber,@MemberXML,@CreatedBy",
                    new
                    {
                        KebeleID = newModel.Kebele,
                        Gote = newModel.Gote,
                        Gare = newModel.Gare,
                        NameOfHouseHoldHead = newModel.NameOfHouseHoldHead,
                        HouseHoldIDNumber = newModel.HouseHoldIDNumber,
                        Remarks         = newModel.Remarks,
                        CollectionDate = newModel.CollectionDate,
                        SocialWorker = newModel.SocialWorker,
                        CCCCBSPCMember = newModel.CCCCBSPCMember,
                        CBHIMembership = newModel.CBHIMembership,
                        CBHINumber = newModel.CBHINumber,
                        MemberXML = strXML,
                        CreatedBy = Membership.GetUser().UserName.ToString()
                    });

                return true;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool UpdateForm1A(CaptureForm1A newModel)
        {
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");

                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CaptureForm1A[] MemberList = objJavascript.Deserialize<CaptureForm1A[]>(newModel.MemberXml);

                foreach (var array in MemberList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<HouseHoldMemberName>" + array.HouseHoldMemberName + "</HouseHoldMemberName>";
                    strXML = strXML + "<IndividualID>" + array.IndividualID + "</IndividualID>";
                    strXML = strXML + "<Pregnant>" + array.Pregnant + "</Pregnant>";
                    strXML = strXML + "<Lactating>" + array.Lactating + "</Lactating>";
                    strXML = strXML + "<DateOfBirth>" + array.DateOfBirth + "</DateOfBirth>";
                    strXML = strXML + "<Age>" + array.Age + "</Age>";
                    strXML = strXML + "<Sex>" + array.Sex + "</Sex>";
                    strXML = strXML + "<Handicapped>" + array.Handicapped + "</Handicapped>";
                    strXML = strXML + "<ChronicallyIll>" + array.ChronicallyIll + "</ChronicallyIll>";
                    strXML = strXML + "<NutritionalStatus>" + array.NutritionalStatus + "</NutritionalStatus>";
                    strXML = strXML + "<childUnderTSForCMAM>" + array.childUnderTSForCMAM + "</childUnderTSForCMAM>";
                    strXML = strXML + "<EnrolledInSchool>" + array.EnrolledInSchool + "</EnrolledInSchool>";
                    strXML = strXML + "<Grade>" + array.Grade + "</Grade>";
                    strXML = strXML + "<SchoolName>" + array.SchoolName + "</SchoolName>";
                    strXML = strXML + "<ChildProtectionRisk>" + array.ChildProtectionRisk + "</ChildProtectionRisk>";
                    //strXML = strXML + "<Remarks>" + array.Remarks + "</Remarks>";
                    strXML = strXML + "</row>";
                    Console.WriteLine(strXML);
                }

                strXML = "<members>" + strXML + "</members>";

                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec UpdateForm1A @ProfileDSHeaderID,@KebeleID,@Gote,@Gare,@NameOfHouseHoldHead,@HouseHoldIDNumber,@Remarks,@CollectionDate,@SocialWorker,@CCCCBSPCMember,@CBHIMembership,@CBHINumber,@MemberXML,@CreatedBy",
                     new
                     {
                         ProfileDSHeaderID = newModel.ProfileDSHeaderID,
                         KebeleID = newModel.KebeleID,
                         Gote = newModel.Gote,
                         Gare = newModel.Gare,
                         NameOfHouseHoldHead = newModel.NameOfHouseHoldHead,
                         HouseHoldIDNumber = newModel.HouseHoldIDNumber,
                         Remarks = newModel.Remarks,
                         CollectionDate = newModel.CollectionDate,
                         SocialWorker = newModel.SocialWorker,
                         CCCCBSPCMember = newModel.CCCCBSPCMember,
                         CBHIMembership = newModel.CBHIMembership,
                         CBHINumber = newModel.CBHINumber,
                         MemberXML = strXML,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool InsertForm1B(CaptureForm1B newModel)
        {
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");

                string strXml = string.Empty;
                if (newModel.PLW == "L") //if Lactating the get babies
                {
                    JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                    CaptureForm1B[] MemberList = objJavascript.Deserialize<CaptureForm1B[]>(newModel.MemberXml);

                    foreach (var array in MemberList)
                    {
                        strXml = strXml + "<row>";
                        strXml = strXml + "<BabyDateOfBirth>" + array.BabyDateOfBirth + "</BabyDateOfBirth>";
                        strXml = strXml + "<BabyName>" + array.BabyName + "</BabyName>";
                        strXml = strXml + "<BabySex>" + array.BabySex + "</BabySex>";
                        strXml = strXml + "<NutritionalStatusInfant>" + array.NutritionalStatusInfant + "</NutritionalStatusInfant>";
                        //strXml = strXml + "<ChildProtectionRisk>" + array.ChildProtectionRisk + "</ChildProtectionRisk>";
                        strXml = strXml + "</row>";
                        Console.WriteLine(strXml);
                    }

                    strXml = "<members>" + strXml + "</members>";
                }

                
                List<RecordCount> isSuccess = con.Fetch<RecordCount>(";Exec InsertForm1B @KebeleID,@Gote,@Gare,@CollectionDate,@SocialWorker," +
                                                                     "@CCCCBSPCMember,@PLW,@NameOfPLW,@HouseHoldIDNumber,@MedicalRecordNumber," +
                                                                     "@PLWAge,@NutritionalStatusPLW,@StartDateTDS,@EndDateTDS,@Remarks," +
                                                                     "@ChildProtectionRisk,@CBHIMembership,@CBHINumber,@MemberXML,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.Kebele,
                         Gote = newModel.Gote,
                         Gare = newModel.Gare,
                         CollectionDate = newModel.CollectionDate,
                         SocialWorker = newModel.SocialWorker,
                         CCCCBSPCMember = newModel.CCCCBSPCMember,
                         PLW = newModel.PLW,
                         NameOfPLW = newModel.NameOfPLW,
                         HouseHoldIDNumber = newModel.HouseHoldIDNumber,
                         MedicalRecordNumber = newModel.MedicalRecordNumber,
                         PLWAge = newModel.PLWAge,
                         NutritionalStatusPLW = newModel.NutritionalStatusPLW,
                         StartDateTDS = newModel.StartDateTDS,
                         EndDateTDS = newModel.EndDateTDS,
                         Remarks = newModel.Remarks,
                         ChildProtectionRisk = newModel.ChildProtectionRisk,
                         CBHIMembership = newModel.CBHIMembership,
                         CBHINumber = newModel.CBHINumber,
                         MemberXML = strXml,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool InsertForm1C(CaptureForm1C newModel)
        {
            try
            {
                PetaPoco.Database con = new PetaPoco.Database("conString");

                string strXML = string.Empty;
                JavaScriptSerializer objJavascript = new JavaScriptSerializer();
                CaptureForm1C[] MemberList = objJavascript.Deserialize<CaptureForm1C[]>(newModel.MemberXml);

                foreach (var array in MemberList)
                {
                    strXML = strXML + "<row>";
                    strXML = strXML + "<CaretakerID>" + array.CaretakerID + "</CaretakerID>";
                    strXML = strXML + "<NameOfCareTaker>" + array.NameOfCareTaker + "</NameOfCareTaker>";
                    strXML = strXML + "<HouseHoldIDNumber>" + array.HouseHoldIDNumber + "</HouseHoldIDNumber>";
                    strXML = strXML + "<MalnourishedChildName>" + array.MalnourishedChildName + "</MalnourishedChildName>";
                    strXML = strXML + "<ChildID>" + array.ChildID + "</ChildID>";
                    strXML = strXML + "<MalnourishedChildSex>" + array.MalnourishedChildSex + "</MalnourishedChildSex>";
                    strXML = strXML + "<StartDateTDS>" + array.StartDateTDS + "</StartDateTDS>";
                    strXML = strXML + "<ChildDateOfBirth>" + array.ChildDateOfBirth + "</ChildDateOfBirth>";
                    strXML = strXML + "<DateTypeCertificate>" + array.DateTypeCertificate + "</DateTypeCertificate>";
                    strXML = strXML + "<MalnourishmentDegree>" + array.MalnourishmentDegree + "</MalnourishmentDegree>";
                    strXML = strXML + "<StartDateTDS>" + array.StartDateTDS + "</StartDateTDS>";
                    strXML = strXML + "<NextCNStatusDate>" + array.NextCNStatusDate + "</NextCNStatusDate>";
                    strXML = strXML + "<EndDateTDS>" + array.EndDateTDS + "</EndDateTDS>";
                    strXML = strXML + "<ChildProtectionRisk>" + array.ChildProtectionRisk + "</ChildProtectionRisk>";
                    strXML = strXML + "<Remarks>" + array.Remarks + "</Remarks>";                    

                    strXML = strXML + "</row>";
                    Console.WriteLine(strXML);
                }

                strXML = "<members>" + strXML + "</members>";

                var isSuccess = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<RecordCount>(";Exec InsertForm1C @KebeleID,@Gote,@Gare,@CollectionDate,@SocialWorker,@CCCCBSPCMember,@CBHIMembership,@CBHINumber,@MemberXML,@CreatedBy",
                     new
                     {
                         KebeleID = newModel.Kebele,
                         Gote = newModel.Gote,
                         Gare = newModel.Gare,
                         CollectionDate = newModel.CollectionDate,
                         SocialWorker = newModel.SocialWorker,
                         CCCCBSPCMember = newModel.CCCCBSPCMember,
                         CBHIMembership = newModel.CBHIMembership,
                         CBHINumber = newModel.CBHINumber,
                         MemberXML = strXML,
                         CreatedBy = Membership.GetUser().UserName.ToString()
                     });

                return true;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return false;
            }
        }

        public bool PdfForm1AReport(string _ProfileDSHeaderID, string _createdBy, out string errMessage, out string filename)
        {
            string rptFileName = string.Empty;
            string rptSP = string.Empty;
            errMessage = string.Empty;
            filename = string.Empty;
            try
            {
                rptFileName = "PrintForm1A";
                rptSP = "PrintForm1A";

                Tuple<List<Form1AReportHeader>, List<Form1AReport>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<Form1AReportHeader, Form1AReport>(";Exec " + rptSP + " @ProfileDSHeaderID",
                    new
                    {
                        ProfileDSHeaderID = _ProfileDSHeaderID
                    });

                DataTable dtAdminStructure = SCTMis.Services.GeneralServices.ToDataTable<Form1AReportHeader>(resultset.Item1);
                
                foreach(DataRow dr in dtAdminStructure.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form1AReport>(resultset.Item2);

                if (dtDetails.Rows.Count > 0)
                {

                    filename = DateTime.Now.ToFileTime().ToString();
                    filename = filename + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/" + rptFileName + ".rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.Database.Tables[1].SetDataSource(dtAdminStructure);
                    myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                    //myReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, true, DateTime.Now.ToFileTime().ToString());
                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();
                }
            }
            catch (Exception ee)
            {
                errMessage = ee.Message;
                GeneralServices.LogError(ee);
                return false;
            }
            return true;
        }

        public bool PdfForm1BReport(string _ProfileTDSPLWID, string _createdBy, out string errMessage, out string filename)
        {
            string rptFileName = string.Empty;
            string rptSP = string.Empty;
            errMessage = string.Empty;
            filename = string.Empty;

            try
            {
                rptFileName = "PrintForm1B";
                rptSP = "PrintForm1B";

                var resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form1BReport>(";Exec " + rptSP + " @ProfileTDSPLWID",
                    new
                    {
                        ProfileTDSPLWID = _ProfileTDSPLWID
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form1BReport>(resultset);
                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                if (dtDetails.Rows.Count > 0)
                {
                    filename = DateTime.Now.ToFileTime().ToString();
                    filename = filename + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/" + rptFileName + ".rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                    //myReportDocument.ExportToHttpResponse(ExportFormatType.PortableDocFormat, System.Web.HttpContext.Current.Response, true, DateTime.Now.ToFileTime().ToString());
                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();
                }
            }
            catch (Exception ee)
            {
                errMessage = ee.Message;
                GeneralServices.LogError(ee);
                return false;
            }
            return true;
        }

        public bool PdfForm1CReport(string _ProfileTDSCMCID, string _createdBy, out string errMessage, out string filename)
        {
            string rptFileName = string.Empty;
            string rptSP = string.Empty;
            errMessage = string.Empty;
            filename = string.Empty;

            try
            {
                rptFileName = "PrintForm1C";
                rptSP = "PrintForm1C";

                var resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<Form1CReport>(";Exec " + rptSP + " @ProfileTDSCMCID",
                    new
                    {
                        ProfileTDSCMCID = _ProfileTDSCMCID
                    });

                DataTable dtDetails = SCTMis.Services.GeneralServices.ToDataTable<Form1CReport>(resultset);
                foreach (DataRow dr in dtDetails.Rows)
                {
                    dr["CreatedBy"] = Membership.GetUser().UserName.ToString();
                }

                if (dtDetails.Rows.Count > 0)
                {
                    filename = DateTime.Now.ToFileTime().ToString();
                    filename = filename + ".pdf";

                    string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/rptFolder/" + rptFileName + ".rpt");
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);

                    ReportDocument myReportDocument;
                    myReportDocument = new ReportDocument();
                    myReportDocument.Load(strRptPath);
                    myReportDocument.Database.Tables[0].SetDataSource(dtDetails);
                    myReportDocument.ExportToDisk(ExportFormatType.PortableDocFormat, strReportFilePath);
                    myReportDocument.Close();

                }
            }
            catch (Exception ee)
            {
                errMessage = ee.Message;
                GeneralServices.LogError(ee);
                return false;
            }
            return true;
        }

    }
}
