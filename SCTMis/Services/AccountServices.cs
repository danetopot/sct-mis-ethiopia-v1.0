﻿using SCTMis.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Web.Security;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using System.Configuration;

namespace SCTMis.Services
{
    public class AccountServices
    {
        public IEnumerable<ModuleAccess> GetModuleAccessRights(int _UserID)
        {
            List<ModuleAccess> modulerights =
                SCTMis.Services.GeneralServices.NpocoConnection().Fetch<ModuleAccess>(";Exec getUserMainMenu @UserName", new
                {
                    UserName = Membership.GetUser().UserName.ToString() 
                });

            return modulerights;
        }

        public bool GetTaskAccessRights(int taskId)
        {            
            var con = new PetaPoco.Database("conString");
            con.OneTimeCommandTimeout = 3600;

            bool modulerights = false;
            try
            {
                string userName = Membership.GetUser().UserName.ToString();

                List<RecordCount> list = con.Fetch<RecordCount>("; Exec getTaskAccessRight @UserName, @TaskID",
                    new
                    {
                        UserName = userName,
                        TaskID = taskId
                    });

                DataTable dsResults = SCTMis.Services.GeneralServices.ToDataTable<RecordCount>(list);

                var result = int.Parse(dsResults.Rows[0][0].ToString());
                modulerights = Convert.ToBoolean(result);

            }
            catch(Exception e)
            {
                GeneralServices.LogError(e);
            }
            return modulerights;
        }

        public string GetComputerName(string clientIP)
        {
            try
            {
                var hostEntry = Dns.GetHostEntry(clientIP);
                return hostEntry.HostName;
            }
            catch (Exception e)
            {
                GeneralServices.LogError(e);
                return string.Empty;
            }
        }

        public bool InsertRole(RegisterRoleModel newModel, string createdBy)
        {
            var con = new PetaPoco.Database("conString");
            con.OneTimeCommandTimeout = 3600;

            bool isValidCall = false;
            try
            {
                List<RecordCount> trxs = con.Fetch<RecordCount>(";Exec InsertRole @RoleName,@CreatedBy",
                    new
                    {
                        RoleName = newModel.RoleName,
                        CreatedBy = createdBy
                    });
                isValidCall = true;
            }
            catch (Exception e) { isValidCall = false; }

            return isValidCall;
        }

        public bool InsertRoleProfile(int RoleID, EditRoleProfile[] RoleProfile)
        {
            string strXML = string.Empty;

            foreach (var array in RoleProfile)
            {
                strXML = strXML + "<row>";
                strXML = strXML + "<taskID>" + array.TaskID + "</taskID>";
                strXML = strXML + "<taskStatus>" + array.TaskStatus + "</taskStatus>";
                strXML = strXML + "</row>";
                Console.WriteLine(strXML);
            }

            strXML = "<profile>" + strXML + "</profile>";

            var con = new PetaPoco.Database("conString");
            con.OneTimeCommandTimeout = 3600;

            bool isValidCall = false;
            try
            {
                List<RecordCount> trxs = con.Fetch<RecordCount>(";Exec InsertProfile @RoleID,@Records",
                    new
                    {
                        RoleID = RoleID,
                        Records = strXML
                    });
                isValidCall = true;
            }
            catch (Exception e) { isValidCall = false; }

            return isValidCall;
        }

        public bool InsertReplicationList(ImportListModel newModel, string path, string replicatedBy, out string errMessage, out int ImportID)
        {
            bool isValidCall = false;
            errMessage = string.Empty;
            ImportID = 0;
            DataSet dsResults = new DataSet();

            string importFolder = newModel.ImportPath.Substring(0, newModel.ImportPath.IndexOf(".zip"));
            

            string destDirectory = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/Uploads/" + newModel.ImportPath.Substring(0, newModel.ImportPath.IndexOf(".zip")));
            string srcDirectory = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/Uploads/" + importFolder);

            string targetDirectory = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/");

            var fileName = string.Empty;
            try
            {
                UnZip(path, destDirectory);

                targetDirectory = srcDirectory.Replace(importFolder, ConfigurationManager.AppSettings["backupfile"].ToString());

                //delete all files in target directory before copying new ones -- Added 28-Feb-2018, coz old files were being reimported
                
                DirectoryInfo dirTarget = new DirectoryInfo(targetDirectory);
                foreach (var file in dirTarget.GetFiles())
                {
                    file.Delete();
                }


                CopyFiles(srcDirectory, targetDirectory);

                //Lets loop through the directory to find if there is any new xml file to be populated
                //importFolder = ConfigurationManager.AppSettings["backupfile"].ToString();
                srcDirectory = srcDirectory.Replace(importFolder, ConfigurationManager.AppSettings["backupfile"].ToString());
                DirectoryInfo d = new DirectoryInfo(srcDirectory);//Assuming Test is your Folder
                FileInfo[] Files = d.GetFiles("*.xml"); //Getting Text files

                foreach (FileInfo file in Files)
                {
                    fileName = file.Name;

                    string xmlContent = System.IO.File.ReadAllText(destDirectory + "/" + fileName);
                    string ContentPath = srcDirectory + "/" + fileName;

                    PetaPoco.Database con = new PetaPoco.Database("conString");
                    con.OneTimeCommandTimeout = 3600;

                    string tableName = string.Empty;

                    //00 - Roles
                    if (fileName.Contains("Roles"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportRoles @XMLString", new { XMLString = xmlContent });
                        tableName = "Roles";
                    }

                    //00 - Users
                    if (fileName.Contains("Users"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportUsers @XMLString", new { XMLString = xmlContent });
                        tableName = "Users";
                    }

                    //00 - FiscalYears
                    if (fileName.Contains("FiscalYears"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportFiscalYears @XMLString", new { XMLString = xmlContent });
                        tableName = "FiscalYears";
                    }

                    //00 - Kebele
                    if (fileName.Contains("Kebele"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportKebele @XMLString", new { XMLString = xmlContent });
                        tableName = "Kebele";
                    }

                    //00 - ReportingPeriod
                    if (fileName.Contains("ReportingPeriod"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportReportingPeriod @XMLString", new { XMLString = xmlContent });
                        tableName = "ReportingPeriod";
                    }

                    //01 - ProfileDSHeader
                    if (fileName.Contains("ProfileDSHeader"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportProfileDSHeader @XMLString", new { XMLString = xmlContent });
                        tableName = "ProfileDSHeader";
                    }

                    //02 - ProfileDSDetail
                    if (fileName.Contains("ProfileDSDetail"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportProfileDSDetail @XMLString", new { XMLString = xmlContent });
                        tableName = "ProfileDSDetail";
                    }

                    //03 - ProfileTDSPLW
                    if (fileName.Contains("ProfileTDSPLW"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportProfileTDSPLW @XMLString", new { XMLString = xmlContent });
                        tableName = "ProfileTDSPLW";
                    }

                    //04 - ProfileTDSPLWDetail
                    if (fileName.Contains("ProfileTDSPLWDetail"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportProfileTDSPLWDetail @XMLString", new { XMLString = xmlContent });
                        tableName = "ProfileTDSPLWDetail";
                    }

                    //05 - ProfileTDSCMC
                    if (fileName.Contains("ProfileTDSCMC"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportProfileTDSCMC @XMLString", new { XMLString = xmlContent });
                        tableName = "ProfileTDSCMC";
                    }

                    //06 - RetargetingHeader
                    if (fileName.Contains("RetargetingHeader"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportRetargetingHeader @XMLString", new { XMLString = xmlContent });
                        tableName = "RetargetingHeader";
                    }

                    //07 - RetargetingDetail
                    if (fileName.Contains("RetargetingDetail"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportRetargetingDetail @XMLString", new { XMLString = xmlContent });
                        tableName = "RetargetingDetail";
                    }

                    //08 - CoResponsibilityDSHeader
                    if (fileName.Contains("CoResponsibilityDSHeader"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportCoResponsibilityDSHeader @XMLString", new { XMLString = xmlContent });
                        tableName = "CoResponsibilityDSHeader";
                    }

                    //09 - CoResponsibilityDSDetail
                    if (fileName.Contains("CoResponsibilityDSDetail"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportCoResponsibilityDSDetail @XMLString", new { XMLString = xmlContent });
                        tableName = "CoResponsibilityDSDetail";
                    }

                    //10 - CoResponsibilityTDSPLWHeader
                    if (fileName.Contains("CoResponsibilityTDSPLWHeader"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportCoResponsibilityTDSPLWHeader @XMLString", new { XMLString = xmlContent });
                        tableName = "CoResponsibilityTDSPLWHeader";
                    }

                    //11 - CoResponsibilityTDSPLW
                    if ((fileName.Contains("CoResponsibilityTDSPLW")) & !(fileName.Contains("CoResponsibilityTDSPLWHeader")))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportCoResponsibilityTDSPLW @XMLString", new { XMLString = xmlContent });
                        tableName = "CoResponsibilityTDSPLW";
                    }

                    //12 - CoResponsibilityTDSCMCHeader
                    if (fileName.Contains("CoResponsibilityTDSCMCHeader"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportCoResponsibilityTDSCMCHeader @XMLString", new { XMLString = xmlContent });
                        tableName = "CoResponsibilityTDSCMCHeader";
                    }

                    //13 - CoResponsibilityTDSCMC
                    if ((fileName.Contains("CoResponsibilityTDSCMC")) & !(fileName.Contains("CoResponsibilityTDSCMCHeader")))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportCoResponsibilityTDSCMC @XMLString", new { XMLString = xmlContent });
                        tableName = "CoResponsibilityTDSCMC";
                    }

                    //14 - CheckListDS
                    if (fileName.Contains("CheckListDS"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportCheckListDS @XMLString", new { XMLString = xmlContent });
                        tableName = "CheckListDS";
                    }

                    //15 - CheckListTDSPLW
                    if (fileName.Contains("CheckListTDSPLW"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportCheckListTDSPLW @XMLString", new { XMLString = xmlContent });
                        tableName = "CheckListTDSPLW";
                    }

                    //16 - CheckListTDSCMC
                    if (fileName.Contains("CheckListTDSCMC"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportCheckListTDSCMC @XMLString", new { XMLString = xmlContent });
                        tableName = "CheckListTDSCMC";
                    }

                    //17 - ComplianceSDSHeader
                    if (fileName.Contains("ComplianceSDSHeader"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportComplianceSDSHeader @XMLString", new { XMLString = xmlContent });
                        tableName = "ComplianceSDSHeader";
                    }

                    //18 - ComplianceSDSDetail
                    if (fileName.Contains("ComplianceSDSDetail"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportComplianceSDSDetail @XMLString", new { XMLString = xmlContent });
                        tableName = "ComplianceSDSDetail";
                    }

                    //19 - ComplianceSTDSPLWHeader
                    if (fileName.Contains("ComplianceSTDSPLWHeader"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportComplianceSTDSPLWHeader @XMLString", new { XMLString = xmlContent });
                        tableName = "ComplianceSTDSPLWHeader";
                    }

                    //20 - ComplianceSTDSPLWDetail
                    if (fileName.Contains("ComplianceSTDSPLWDetail"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportComplianceSTDSPLWDetail @XMLString", new { XMLString = xmlContent });
                        tableName = "ComplianceSTDSPLWDetail";
                    }

                    //21 - ComplianceSTDSCMCHeader
                    if (fileName.Contains("ComplianceSTDSCMCHeader"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportComplianceSTDSCMCHeader @XMLString", new { XMLString = xmlContent });
                        tableName = "ComplianceSTDSCMCHeader";
                    }

                    //22 - ComplianceSTDSCMCDetail
                    if (fileName.Contains("ComplianceSTDSCMCDetail"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportComplianceSTDSCMCDetail @XMLString", new { XMLString = xmlContent });
                        tableName = "ComplianceSTDSCMCDetail";
                    }

                    //23 - GenerateCSPDS
                    if (fileName.Contains("GenerateCSPDS"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportGenerateCSPDS @XMLString", new { XMLString = xmlContent });
                        tableName = "GenerateCSPDS";
                    }

                    //24 - GenerateCSPTDSPLW
                    if (fileName.Contains("GenerateCSPTDSPLW"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportGenerateCSPTDSPLW @XMLString", new { XMLString = xmlContent });
                        tableName = "GenerateCSPTDSPLW";
                    }

                    //25 - GenerateCSPTDSCMC
                    if (fileName.Contains("GenerateCSPTDSCMC"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportGenerateCSPTDSCMC @XMLString", new { XMLString = xmlContent });
                        tableName = "GenerateCSPTDSCMC";
                    }

                    //26 - GenerateNonCCDS
                    if (fileName.Contains("GenerateNonCCDS"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportGenerateNonCCDS @XMLString", new { XMLString = xmlContent });
                        tableName = "GenerateNonCCDS";
                    }

                    //27 - GenerateNonCCTDSPLW
                    if (fileName.Contains("GenerateNonCCTDSPLW"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportGenerateNonCCTDSPLW @XMLString", new { XMLString = xmlContent });
                        tableName = "GenerateNonCCTDSPLW";
                    }

                    //28 - GenerateNonCCTDSCMC
                    if (fileName.Contains("GenerateNonCCTDSCMC"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportGenerateNonCCTDSCMC @XMLString", new { XMLString = xmlContent });
                        tableName = "GenerateNonCCTDSCMC";
                    }

                    //29 - NonComplianceDSHeader
                    if (fileName.Contains("NonComplianceDSHeader"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportNonComplianceDSHeader @XMLString", new { XMLString = xmlContent });
                        tableName = "NonComplianceDSHeader";
                    }

                    //30 - NonComplianceDSDetail
                    if (fileName.Contains("NonComplianceDSDetail"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportNonComplianceDSDetail @XMLString", new { XMLString = xmlContent });
                        tableName = "NonComplianceDSDetail";
                    }

                    //31 - NonComplianceTDSPLWHeader
                    if (fileName.Contains("NonComplianceTDSPLWHeader"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportNonComplianceTDSPLWHeader @XMLString", new { XMLString = xmlContent });
                        tableName = "NonComplianceTDSPLWHeader";
                    }

                    //32 - NonComplianceTDSPLWDetail
                    if (fileName.Contains("NonComplianceTDSPLWDetail"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportNonComplianceTDSPLWDetail @XMLString", new { XMLString = xmlContent });
                        tableName = "NonComplianceTDSPLWDetail";
                    }

                    //33 - NonComplianceTDSCMCHeader
                    if (fileName.Contains("NonComplianceTDSCMCHeader"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportNonComplianceTDSCMCHeader @XMLString", new { XMLString = xmlContent });
                        tableName = "NonComplianceTDSCMCHeader";
                    }

                    //34 - NonComplianceTDSCMCDetail
                    if (fileName.Contains("NonComplianceTDSCMCDetail"))
                    {
                        var result = con.Fetch<dynamic>(";EXEC ImportNonComplianceTDSCMCDetail @XMLString", new { XMLString = xmlContent });
                        tableName = "NonComplianceTDSCMCDetail";
                    }

                    if (!string.IsNullOrEmpty(tableName))
                    {
                        InsertReplicatedData(fileName, tableName, xmlContent, replicatedBy);
                    }                    
                }

            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                GeneralServices.LogError(e);

                isValidCall = false;
                errMessage = string.Format("{0} failed to import", fileName);
            }
            
            return isValidCall;
        }

        private static void InsertReplicatedData(string fileName, string tableName, string replicateXML, string replicatedBy)
        {
            PetaPoco.Database con = new PetaPoco.Database("conString");
            con.OneTimeCommandTimeout = 3600;

            var result = con.Fetch<dynamic>(";EXEC InsertImportedData @FileName, @TableName, @ReplicateXML, @ReplicatedBy", new { FileName = fileName, TableName = tableName, ReplicateXML = replicateXML, ReplicatedBy = replicatedBy });
        }

        public static void UnZip(string zipFile, string baseFolder)
        {
            if (!Directory.Exists(baseFolder))
            {
                Directory.CreateDirectory(baseFolder);
            }
            FileStream fr = File.OpenRead(zipFile);
            ZipInputStream ins = new ZipInputStream(fr);
            //ZipFile zf = new ZipFile(zipFile);
            ZipEntry ze = ins.GetNextEntry();
            while (ze != null)
            {
                if (ze.IsDirectory)
                {
                    Directory.CreateDirectory(baseFolder + "\\" + ze.Name);
                }
                else if (ze.IsFile)
                {
                    if (!Directory.Exists(baseFolder + Path.GetDirectoryName(ze.Name)))
                    {
                        Directory.CreateDirectory(baseFolder + Path.GetDirectoryName(ze.Name));
                    }

                    FileStream fs = File.Create(baseFolder + "\\" + ze.Name);

                    byte[] writeData = new byte[ze.Size];
                    int iteration = 0;
                    while (true)
                    {
                        int size = 2048;
                        size = ins.Read(writeData, (int)Math.Min(ze.Size, (iteration * 2048)), (int)Math.Min(ze.Size - (int)Math.Min(ze.Size, (iteration * 2048)), 2048));
                        if (size > 0)
                        {
                            fs.Write(writeData, (int)Math.Min(ze.Size, (iteration * 2048)), size);
                        }
                        else
                        {
                            break;
                        }
                        iteration++;
                    }
                    fs.Close();
                }
                ze = ins.GetNextEntry();
            }
            ins.Close();
            fr.Close();

        }

        public static void CopyFiles(string sourcePath, string targetPath)
        {
            //Check files copied to directory and move them to respective section
            DirectoryInfo d = new DirectoryInfo(sourcePath);//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.xml"); //Getting Text files
            string fileName = "";
            foreach (FileInfo file in Files)
            {
                fileName = file.Name;

                // Use Path class to manipulate file and directory paths.
                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                string destFile = System.IO.Path.Combine(targetPath, fileName);

                // To copy a folder's contents to a new location:
                // Create a new target folder, if necessary.
                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }

                // To copy a file to another location and 
                // overwrite the destination file if it already exists.
                System.IO.File.Copy(sourceFile, destFile, true);


            }
        }

        public bool InsertReportLog(string reportName, string reportPeriod, string reportFilePath, string generatedBy, DateTime generatedOn)
        {
            var con = new PetaPoco.Database("conString");
            con.OneTimeCommandTimeout = 3600;
            bool isValidCall = false;
            try
            {
                List<ReportLogModel> trxs = con.Fetch<ReportLogModel>(";Exec InsertReportingLog @ReportName,@ReportPeriod,@ReportFilePath,@GeneratedBy,@GeneratedOn",
                    new
                    {
                        ReportName = reportName,
                        ReportPeriod = reportPeriod,
                        ReportFilePath = reportFilePath,
                        GeneratedBy = generatedBy,
                        GeneratedOn = generatedOn
                    });
                isValidCall = true;
            }
            catch (Exception ex) {
                isValidCall = false;
                GeneralServices.LogError(ex);
            }

            return isValidCall;
        }


        public bool InsertAuditTrail(string userName, string screenName,
                string functionName, string ipAddress, string macAddress,
            string TransactionDetails)
        {
            string computerName = GetComputerName(ipAddress);

            var con = new PetaPoco.Database("conString");
            con.OneTimeCommandTimeout = 3600;

            bool isValidCall = false;
            try
            {
                List<RecordCount> trxs = con.Fetch<RecordCount>(";Exec InsertUpdateAuditTrail @pUserName,@pScreenName,@pFunctionName,@pComputerName,@pIPAddress,@pMACAddress,@pTransactionDetails",
                    new
                    {
                        pUserName = userName,
                        pScreenName = screenName,
                        pFunctionName = functionName,
                        pComputerName = computerName,
                        pIPAddress = ipAddress,
                        pMACAddress = macAddress,
                        pTransactionDetails = TransactionDetails,
                    });
                isValidCall = true;
            }
            catch (Exception e) { isValidCall = false; }

            return isValidCall;
        }

        public IEnumerable<AccessRoles> GetRoles()
        {
            //DataSet dsResults = new DataSet();
            DataTable dtRecords;
            //bool isValidCall = false;
            List<AccessRoles> roles = new List<AccessRoles>();

            var con = new PetaPoco.Database("conString");
            con.OneTimeCommandTimeout = 3600;

            List<AccessRoles> form1AList = con.Fetch<AccessRoles>(";Exec GetRole");

            DataTable dsResults = SCTMis.Services.GeneralServices.ToDataTable<AccessRoles>(form1AList);

            //isValidCall = DALService.dbGetRole(strSystem, out dsResults);
            //if (isValidCall == true)
            //{
                dtRecords = dsResults;

                foreach (DataRow row in dtRecords.Rows)
                {
                    roles.Add(new AccessRoles()
                    {
                        RoleID = row["RoleID"].ToString(),
                        RoleName = row["RoleName"].ToString()
                    });
                }
            //}
            return roles;
        }

        public UpdateRegisterUserModel GetUserByID(int _UserID)
        {
            //DataSet dsResults = new DataSet();
            DataTable dtRecords;
            //bool isValidCall = false;
            UpdateRegisterUserModel userModel = new UpdateRegisterUserModel();

            List<UpdateRegisterUserModel> form1AList
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<UpdateRegisterUserModel>(";Exec getUserByID @UserID", new { UserID = _UserID });

            DataTable dsResults = SCTMis.Services.GeneralServices.ToDataTable<UpdateRegisterUserModel>(form1AList);


            //isValidCall = DALService.dbGetUserByID(strSystem, _UserID, out dsResults);
            //if (isValidCall == true)
            //{
                dtRecords = dsResults;
                DataRow row = dtRecords.Rows[0];

                userModel.UserID = int.Parse(row["UserID"].ToString());
                userModel.UserName = row["UserName"].ToString();
                userModel.FirstName = row["FirstName"].ToString();
                userModel.LastName = row["LastName"].ToString();
                userModel.Email = row["Email"].ToString();
                userModel.Mobile = row["Mobile"].ToString();
                userModel.RoleID = int.Parse(row["RoleID"].ToString());
            //}
            return userModel;
        }

        public PasswordUserModel GetBasicUserByID(int _UserID)
        {
            DataTable dtRecords;
            PasswordUserModel userModel = new PasswordUserModel();
            List<PasswordUserModel> form1AList 
                = SCTMis.Services.GeneralServices.NpocoConnection().Fetch<PasswordUserModel>(";Exec getUserByID @UserID",
                new { UserID = _UserID });

            DataTable dsResults = SCTMis.Services.GeneralServices.ToDataTable<PasswordUserModel>(form1AList);

                dtRecords = dsResults;
                DataRow row = dtRecords.Rows[0];

                userModel.UserID = int.Parse(row["UserID"].ToString());
                userModel.UserName = row["UserName"].ToString();
           
            return userModel;
        }
    }
}
