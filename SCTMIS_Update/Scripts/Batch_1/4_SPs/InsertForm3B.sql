ALTER Proc [dbo].[InsertForm3B]
(
	@KebeleID				Int,
	@ReportFileName			varchar(50),
	@CreatedBy				varchar(30)
)
AS
BEGIN
SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION INSERTRECORD

		INSERT INTO CheckListTDSPLW(CheckListTDSPLWID,CoRespTDSPLWID,PLW,GeneratedBy,GeneratedOn,ReportFileName,FiscalYear)
		SELECT NEWID(),CoRespTDSPLWID,'P',dbo.f_getUserIDByUserName(@CreatedBy),GETDATE(),@ReportFileName,dbo.f_getFiscalYearFromDate(GETDATE()) 
		FROM ProfileTDSPLW --ON ProfileDSHeader.ProfileDSHeaderID = ProfileTDSPLW.ProfileDSHeaderID
		INNER JOIN CoResponsibilityTDSPLW ON CoResponsibilityTDSPLW.ProfileTDSPLWID = ProfileTDSPLW.ProfileTDSPLWID
		WHERE KebeleID = @KebeleID 
		AND ISNULL(Form2Produced,0) = 1
		AND ISNULL(Form3Generated,0) = 0
		AND ISNULL(ApprovedBy,'') <> ''
		AND ProfileTDSPLW.PLW = 'P'

		UPDATE ProfileTDSPLW
		SET Form3Generated = 1
		FROM ProfileTDSPLW
		WHERE KebeleID = @KebeleID 
		AND ISNULL(Form2Produced,0) = 1
		AND ISNULL(Form3Generated,0) = 0
		AND ISNULL(ApprovedBy,'') <> ''
		AND ProfileTDSPLW.PLW = 'P'

		COMMIT TRANSACTION INSERTRECORD
	END TRY
	BEGIN CATCH 
	  IF (@@TRANCOUNT > 0)
	   BEGIN
		  ROLLBACK TRANSACTION INSERTRECORD
		  --PRINT 'Error detected, all changes reversed'
	   END 

	   Declare @ERROR_MESSAGE	Varchar(100)

		SELECT @ERROR_MESSAGE = ERROR_MESSAGE()

		RAISERROR (@ERROR_MESSAGE,16,1)

	END CATCH

SET NOCOUNT OFF
END
