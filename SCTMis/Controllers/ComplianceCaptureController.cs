﻿using SCTMis.Models;
using SCTMis.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace SCTMis.Controllers
{
    [SessionConfig.SessionExpireFilter]
    public class ComplianceCaptureController : Controller
    {
        GeneralServices dbService = new GeneralServices();
        ComplianceServices dbCompliance = new ComplianceServices();
        //
        // GET: /ComplianceCapture/

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form4AList()
        {
            ViewBag.Subtitle = "Form 4A Compliant Listing";
            return View();
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4AMainList(int jtStartIndex = 0, int jtPageSize = 0)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                Tuple<List<int>, List<CapturedForm4MainAGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4MainAGrid>(";Exec getCapturedForm4AMainList @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4AList(CapturedForm4Models myModel)
        {
            try
            {

                Tuple<List<int>, List<CapturedForm4AGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4AGrid>(";Exec getCapturedForm4AList @jtStartIndex, @jtPageSize,@KebeleID,@ReportingPeriodID,@ServiceID",
                    new
                    {
                        jtStartIndex = myModel.jtStartIndex,
                        jtPageSize = myModel.jtPageSize,
                        KebeleID   = myModel.KebeleID,
                        ReportingPeriodID = myModel.ReportingPeriodID,
                        ServiceID = myModel.ServiceID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        
        //CaptureForm4A
        [Authorize]
        public ActionResult CaptureForm4A()
        {
            ViewBag.Subtitle = "Captured Form 4A Listing";

            CaptureForm4 models = new Models.CaptureForm4();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetForm4AKebeles();
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());
            models.serviceprovids = dbService.GetForm4AServiceProviders(models.KebeleID);
            models.intgrtedservis = dbService.GetForm4AServices(models.KebeleID, models.ServiceProviderID);

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }
        [Authorize]
        [HttpPost]
        public JsonResult CapturedForm4AData(CapturedForm4Model newmodel)
        {
            ViewBag.Subtitle = "Capture Form 4A Details";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4ANew(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        return Json(new { Result = "OK"});
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                return Json(new { Result = "ERROR", Message = errMessage });

            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [Authorize]
        [HttpPost]
        public ActionResult CaptureForm4A(CaptureForm4 newmodel)
        {
            ViewBag.Subtitle = "Capture Form 4A Details";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4A(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Captured Form 4A Listing";

                        return RedirectToAction("Form4AList");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetForm4AKebeles();
                newmodel.serviceprovids = dbService.GetForm4AServiceProviders(newmodel.KebeleID);
                newmodel.intgrtedservis = dbService.GetForm4AServices(newmodel.KebeleID, newmodel.ServiceProviderID);
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                return View("CaptureForm4A", newmodel);

            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetForm4AKebeles();
                newmodel.serviceprovids = dbService.GetForm4AServiceProviders(newmodel.KebeleID);
                newmodel.intgrtedservis = dbService.GetForm4AServices(newmodel.KebeleID, newmodel.ServiceProviderID);
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return View("CaptureForm4A", newmodel);
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm4ADSMainList(Form4ADSMainModel model)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {

                Tuple<List<int>, List<getForm4ADSMainList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm4ADSMainList>(";Exec getForm4ADSMainList @jtStartIndex, @jtPageSize,@KebeleID",
                    new
                    {
                        jtStartIndex = model.jtStartIndex,
                        jtPageSize = model.jtPageSize,
                        KebeleID = model.KebeleID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm4ADSList(getForm4ADSMainList model)
        {
            try
            {
                Tuple<List<int>, List<getForm4ADSList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm4ADSList>(";Exec getForm4ADSList @KebeleID, @ServiceID,@ReportingPeriodID",
                    new
                    {
                        KebeleID = model.KebeleID,
                        ServiceID = model.ServiceID,
                        ReportingPeriodID = model.ReportingPeriodID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form4BList()
        {
            ViewBag.Subtitle = "Form 4B Compliant Listing";
            return View();
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4BMainList(int jtStartIndex = 0, int jtPageSize = 0)
        {
            try
            {
                Tuple<List<int>, List<CapturedForm4MainAGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4MainAGrid>(";Exec getCapturedForm4BMainList @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4BList(CapturedForm4Models myModel)
        {
            try
            {
                Tuple<List<int>, List<CapturedForm4BGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4BGrid>(";Exec getCapturedForm4BList @jtStartIndex, @jtPageSize,@KebeleID,@ReportingPeriodID,@ServiceID",
                    new
                    {
                        jtStartIndex = myModel.jtStartIndex,
                        jtPageSize = myModel.jtPageSize,
                        KebeleID = myModel.KebeleID,
                        ReportingPeriodID = myModel.ReportingPeriodID,
                        ServiceID = myModel.ServiceID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //CaptureForm4B
        [Authorize]
        public ActionResult CaptureForm4B()
        {
            ViewBag.Subtitle = "Capture Form 4B";

            CaptureForm4 models = new Models.CaptureForm4();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetForm4BKebeles();
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());
            models.serviceprovids = dbService.GetForm4BServiceProviders(models.KebeleID);
            models.intgrtedservis = dbService.GetForm4BServices(models.KebeleID, models.ServiceProviderID);

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }

        [Authorize]
        [HttpPost]
        public JsonResult CapturedForm4BData(CapturedForm4BModel newmodel)
        {
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4BNew(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        return Json(new { Result = "OK" });
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                return Json(new { Result = "ERROR", Message = errMessage });

            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);

                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm4BTDSPLWMainList(Form4ADSMainModel model)
        {
            try
            {
                Tuple<List<int>, List<getForm4ADSMainList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm4ADSMainList>(";Exec getForm4BDSMainList @jtStartIndex, @jtPageSize,@KebeleID,@ReportingPeriodID",
                    new
                    {
                        jtStartIndex = model.jtStartIndex,
                        jtPageSize = model.jtPageSize,
                        KebeleID = model.KebeleID,
                        ReportingPeriodID = model.ReportingPeriodID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm4BTDSPLWList(getForm4ADSMainList model)
        {
            try
            {
                Tuple<List<int>, List<getForm4BTDSPLWList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm4BTDSPLWList>(";Exec getForm4BTDSPLWList @KebeleID, @ServiceID,@ReportingPeriodID",
                    new
                    {
                        KebeleID = model.KebeleID,
                        ServiceID = model.ServiceID,
                        ReportingPeriodID = model.ReportingPeriodID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form4CList()
        {
            ViewBag.Subtitle = "Form 4C Compliant Listing";
            return View();
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4CMainList(int jtStartIndex = 0, int jtPageSize = 0)
        {
            try
            {
                Tuple<List<int>, List<CapturedForm4MainAGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4MainAGrid>(";Exec getCapturedForm4CMainList @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //[HttpPost]
        //[Authorize]
        //public JsonResult CapturedForm4BList(CapturedForm4Models myModel)
        //{
        //    try
        //    {
        //        Tuple<List<int>, List<CapturedForm4BGrid>> resultset
        //            = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4BGrid>(";Exec getCapturedForm4BList @jtStartIndex, @jtPageSize,@ReportingPeriodID,@ServiceID",
        //            new
        //            {
        //                jtStartIndex = myModel.jtStartIndex,
        //                jtPageSize = myModel.jtPageSize,
        //                ReportingPeriodID = myModel.ReportingPeriodID,
        //                ServiceID = myModel.ServiceID
        //            });

        //        return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4CList(CapturedForm4Models myModel)
        {
            try
            {
                Tuple<List<int>, List<CapturedForm4CGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4CGrid>(";Exec getCapturedForm4CList @jtStartIndex, @jtPageSize,@KebeleID,@ReportingPeriodID,@ServiceID",
                    new
                    {
                        jtStartIndex = myModel.jtStartIndex,
                        jtPageSize = myModel.jtPageSize,
                        KebeleID = myModel.KebeleID,
                        ReportingPeriodID = myModel.ReportingPeriodID,
                        ServiceID = myModel.ServiceID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult CapturedForm4CData(CapturedForm4CModel newmodel)
        {
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4CNew(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        return Json(new { Result = "OK" });
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                return Json(new { Result = "ERROR", Message = errMessage });

            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //CaptureForm4C
        [Authorize]
        public ActionResult CaptureForm4C()
        {
            ViewBag.Subtitle = "Capture Form 4C";

            CaptureForm4 models = new Models.CaptureForm4();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetForm4CKebeles();
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());
            models.serviceprovids = dbService.GetForm4CServiceProviders(models.KebeleID);
            models.intgrtedservis = dbService.GetForm4CServices(models.KebeleID, models.ServiceProviderID);

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }

        [Authorize]
        [HttpPost]
        public ActionResult CaptureForm4C(CaptureForm4 newmodel)
        {
            ViewBag.Subtitle = "Capture Form 4C Details";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4C(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Captured Form 4C Listing";

                        return RedirectToAction("Form4CList");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetForm4CKebeles();
                newmodel.serviceprovids = dbService.GetForm4CServiceProviders(newmodel.KebeleID);
                newmodel.intgrtedservis = dbService.GetForm4CServices(newmodel.KebeleID, newmodel.ServiceProviderID);
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                return View("CaptureForm4C", newmodel);

            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetForm4CKebeles();
                newmodel.serviceprovids = dbService.GetForm4CServiceProviders(newmodel.KebeleID);
                newmodel.intgrtedservis = dbService.GetForm4CServices(newmodel.KebeleID, newmodel.ServiceProviderID);
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                ModelState.AddModelError("", ex.Message);
                GeneralServices.LogError(ex);
                return View("CaptureForm4C", newmodel);
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm4CTDSCMCMainList(Form4ADSMainModel model)
        {
            try
            {
                Tuple<List<int>, List<getForm4ADSMainList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm4ADSMainList>(";Exec getForm4CTDSCMCMainList @jtStartIndex, @jtPageSize,@KebeleID,@ReportingPeriodID",
                    new
                    {
                        jtStartIndex = model.jtStartIndex,
                        jtPageSize = model.jtPageSize,
                        KebeleID = model.KebeleID,
                        ReportingPeriodID = model.ReportingPeriodID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult getForm4CTDSCMCList(getForm4ADSMainList model)
        {
            try
            {
                Tuple<List<int>, List<getForm4CTDSCMCList>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, getForm4CTDSCMCList>(";Exec getForm4CTDSCMCList @KebeleID,@ServiceID,@ReportingPeriodID",
                    new
                    {
                        KebeleID = model.KebeleID,
                        ServiceID = model.ServiceID,
                        ReportingPeriodID = model.ReportingPeriodID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        
        [Authorize]
        [OutputCache(Duration = 360, VaryByParam = "none")]
        public ActionResult Form4DList()
        {
            ViewBag.Subtitle = "Form 4D Child Protection Listing";
            return View();
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4DMainList(int jtStartIndex = 0, int jtPageSize = 0)
        {
            try
            {
                Tuple<List<int>, List<CapturedForm4MainAGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4MainAGrid>(";Exec getCapturedForm4CMainList @jtStartIndex, @jtPageSize",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult CapturedForm4DList(CapturedForm4Models myModel)
        {
            try
            {
                Tuple<List<int>, List<CapturedForm4CGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, CapturedForm4CGrid>(";Exec getCapturedForm4CList @jtStartIndex, @jtPageSize,@KebeleID,@ReportingPeriodID,@ServiceID",
                    new
                    {
                        jtStartIndex = myModel.jtStartIndex,
                        jtPageSize = myModel.jtPageSize,
                        KebeleID = myModel.KebeleID,
                        ReportingPeriodID = myModel.ReportingPeriodID,
                        ServiceID = myModel.ServiceID
                    });

                return Json(new { Result = "OK", Records = resultset.Item2, TotalRecordCount = resultset.Item1[0] });
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [Authorize]
        [HttpPost]
        public JsonResult CapturedForm4DData(CapturedForm4CModel newmodel)
        {
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4CNew(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        return Json(new { Result = "OK" });
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                return Json(new { Result = "ERROR", Message = errMessage });

            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //CaptureForm4C
        [Authorize]
        public ActionResult CaptureForm4D()
        {
            ViewBag.Subtitle = "Capture Form 4C";

            CaptureForm4 models = new Models.CaptureForm4();

            models.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            models.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            models.kebeles = dbService.GetForm4CKebeles();
            models.RegionID = int.Parse(Session["RegionID"].ToString());
            models.WoredaID = int.Parse(Session["WoredaID"].ToString());
            models.serviceprovids = dbService.GetForm4CServiceProviders(models.KebeleID);
            models.intgrtedservis = dbService.GetForm4CServices(models.KebeleID, models.ServiceProviderID);

            var currentFY = dbService.GetCurrentFiscalYear();
            models.reportgperiod = dbService.GetReportingPeriods(currentFY.FiscalYear);

            return View(models);
        }

        [Authorize]
        [HttpPost]
        public ActionResult CaptureForm4D(CaptureForm4 newmodel)
        {
            ViewBag.Subtitle = "Capture Form 4C Details";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    var result = dbCompliance.InsertCapturedForm4C(newmodel, out errMessage);
                    if (string.IsNullOrEmpty(errMessage))
                    {
                        ViewBag.Subtitle = "Captured Form 4C Listing";

                        return RedirectToAction("Form4CList");
                    }
                }

                if (string.IsNullOrEmpty(errMessage))
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);
                }
                else
                {
                    ModelState.AddModelError("", errMessage);
                }

                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetForm4CKebeles();
                newmodel.serviceprovids = dbService.GetForm4CServiceProviders(newmodel.KebeleID);
                newmodel.intgrtedservis = dbService.GetForm4CServices(newmodel.KebeleID, newmodel.ServiceProviderID);
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                return View("CaptureForm4C", newmodel);

            }
            catch (Exception ex)
            {
                newmodel.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
                newmodel.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
                newmodel.kebeles = dbService.GetForm4CKebeles();
                newmodel.serviceprovids = dbService.GetForm4CServiceProviders(newmodel.KebeleID);
                newmodel.intgrtedservis = dbService.GetForm4CServices(newmodel.KebeleID, newmodel.ServiceProviderID);
                newmodel.reportgperiod = dbService.GetReportingPeriods();

                ModelState.AddModelError("", ex.Message);
                GeneralServices.LogError(ex);
                return View("CaptureForm4C", newmodel);
            }
        }












        //FORM 4 updates
        //Service Providers
        [HttpPost]
        [Authorize]
        public JsonResult Select4AServiceProvider(Kebele model)
        {
            var SPDetails = dbService.GetForm4AServiceProviders(model.KebeleID);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult Select4BServiceProvider(Kebele model)
        {
            var SPDetails = dbService.GetForm4BServiceProviders(model.KebeleID);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult Select4CServiceProvider(Kebele model)
        {
            var SPDetails = dbService.GetForm4CServiceProviders(model.KebeleID);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }
            //Service Names

        [HttpPost]
        [Authorize]
        public JsonResult Select4AServiceNames(int KebeleID, int ServiceProviderID)
        {
            var SPDetails = dbService.GetForm4AServices(KebeleID, ServiceProviderID);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult Select4BServiceNames(int KebeleID, int ServiceProviderID)
        {
            var SPDetails = dbService.GetForm4BServices(KebeleID, ServiceProviderID);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult Select4CServiceNames(int KebeleID, int ServiceProviderID)
        {
            var SPDetails = dbService.GetForm4CServices(KebeleID, ServiceProviderID);
            return Json(SPDetails, JsonRequestBehavior.AllowGet);
        }
    }
}
//models.serviceprovids = dbService.GetForm4AServiceProviders(models.KebeleID);
//models.intgrtedservis = dbService.GetForm4AServices(models.KebeleID, models.ServiceProviderID);
