﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using System.Xml.Linq;
using SCTMis.Models;
using SCTMis.Services;
using System.Linq;
using System.Web.Security;

namespace SCTMis.Controllers
{
    [SessionConfig.SessionExpireFilter]
    public class ReTargetingController : Controller
    {
        GeneralServices dbService = new GeneralServices();
        AccountServices dbAcService = new AccountServices();
        ReTargetingServices reTargetingServices = new ReTargetingServices();

        string ip = System.Web.HttpContext.Current.Request.UserHostAddress;

        static int _currentPage = 0;

        [HttpPost]
        public void UpdateCurrentPage(PageParams pageParam)
        {
            _currentPage = pageParam.CurrentPage;
        }

        //
        // GET: /ReTargeting/
        [Authorize]
        [OutputCache(Duration = 10, VaryByParam = "none")]
        public ActionResult Form7Summary()
        {
            ViewBag.Subtitle = "Form 7 Listing";
            ViewBag.CurrentPage = _currentPage;

            dbAcService.InsertAuditTrail(User.Identity.Name, "Re-Targeting", "Form 7 Summary List View", ip, "",
            "Form 7 Summary List Access  at " + DateTime.Now.ToString());

            var canGenerateForm7 = dbAcService.GetTaskAccessRights(98);
            ViewBag.CanGenerateForm7 = canGenerateForm7;

            return View();
        }

        [HttpGet]
        [Authorize]
        public XDocument Form7SummaryList()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = SCTMis.Services.GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = SCTMis.Services.GeneralServices.intQueryString("RecCount", 10);
                int jtisSearch = SCTMis.Services.GeneralServices.intQueryString("isSearch", 0);
                string jtSearchTypeID = SCTMis.Services.GeneralServices.strQueryString("SearchTypeID", string.Empty);
                string jtSearchKeyword = SCTMis.Services.GeneralServices.strQueryString("SearchKeyword", string.Empty);

                Tuple<List<int>, List<Form7SummaryGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form7SummaryGrid>(";Exec GetForm7SummaryList @jtStartIndex, @jtPageSize,@isSearch,@SearchTypeID,@SearchKeyword",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        isSearch = jtisSearch,
                        SearchTypeID = jtSearchTypeID,
                        SearchKeyword = jtSearchKeyword
                    });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form7SummaryGrid>(resultset.Item2);

                ViewBag.CurrentPage = _currentPage;

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], jtStartIndex, 0, 6);
                
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        [Authorize]
        public ActionResult Form7(string kebeleID)
        {
            ViewBag.Subtitle = "Form 7 Details Listing";
            ViewBag.CurrentPage = _currentPage;                       

            Form7SummaryGrid model = new Form7SummaryGrid();
            model.KebeleID = int.Parse(kebeleID);

            dbAcService.InsertAuditTrail(User.Identity.Name, "Re-Targeting", "Form 7 List View", ip, "",
            "Form 7 List Access  at " + DateTime.Now.ToString());

            return View("Form7List", model);
        }

        [HttpGet]
        [Authorize]
        public XDocument Form7List()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                int jtStartIndex = GeneralServices.intQueryString("posStart", 0);
                int jtPageSize = GeneralServices.intQueryString("RecCount", 10);

                string kebeleID = Request.QueryString["kebeleID"].ToString();

                Tuple<List<int>, List<Form7Grid>> resultset = GeneralServices.NpocoConnection().FetchMultiple<int, Form7Grid>(";Exec GetForm7List @jtStartIndex, @jtPageSize, @KebeleID",
                    new
                    {
                        jtStartIndex = jtStartIndex,
                        jtPageSize = jtPageSize,
                        KebeleID = kebeleID
                    });

                DataTable dt = GeneralServices.ToDataTable<Form7Grid>(resultset.Item2);

                ViewBag.CurrentPage = _currentPage;

                var canApproveForm7 = dbAcService.GetTaskAccessRights(99);

                string sb;
                if (canApproveForm7)
                {
                    sb = GeneralServices.GetDhtmlxXMLForm7(dt, resultset.Item1[0], jtStartIndex, 0, 3);
                }
                else
                {
                    sb = GeneralServices.GetDhtmlxXMLForm7(dt, resultset.Item1[0], jtStartIndex, 0, 6);
                }

                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return XDocument.Parse("xml");
            }
        }

        //CaptureForm1A
        [Authorize]
        public ActionResult GenerateForm7()
        {
            ViewBag.Subtitle = "Generate Form 7";

            GenerateForm7 generateForm7Model = new Models.GenerateForm7();

            generateForm7Model.regions = dbService.GetRegionByIDs(int.Parse(Session["RegionID"].ToString()));
            generateForm7Model.woredas = dbService.GetWoredaByIDs(int.Parse(Session["RegionID"].ToString()), int.Parse(Session["WoredaID"].ToString()));
            generateForm7Model.kebeles = dbService.GetKebeles(int.Parse(Session["WoredaID"].ToString()));
            generateForm7Model.RegionID = int.Parse(Session["RegionID"].ToString());
            generateForm7Model.WoredaID = int.Parse(Session["WoredaID"].ToString());
            generateForm7Model.fiscalYears = dbService.GetFiscalYears(0);
            
            return View(generateForm7Model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult GenerateForm7(GenerateForm7 newmodel)
        {
            ViewBag.Subtitle = "Generate Form 7";

            try
            {
                if (ModelState.IsValid)
                {
                    var result = reTargetingServices.GenerateForm7(newmodel);
                    ViewBag.Subtitle = "Form 7 Listing";
                    dbAcService.InsertAuditTrail(User.Identity.Name, "Re-Targeting", "Generate Form 7", ip, "",
                    "Form1A Details " + newmodel.KebeleID + "-" + newmodel.FiscalYear + " Captured Successfuly  at " + DateTime.Now.ToString());

                    return RedirectToAction("Form7Summary");
                }
                else
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    ModelState.AddModelError("", message);

                    newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                    newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                    newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                    newmodel.fiscalYears = dbService.GetFiscalYears(0);
                    
                    dbAcService.InsertAuditTrail(User.Identity.Name, "Re-Targeting", "Generate Form 7 Error", ip, "",
                    "Form 7 Details with Message " + message + " for " + newmodel.KebeleID + "-" + newmodel.FiscalYear + " Encountered an error at " + DateTime.Now.ToString());

                    return View("GenerateForm7", newmodel);
                }
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                newmodel.regions = dbService.GetRegionByIDs(newmodel.RegionID);
                newmodel.woredas = dbService.GetWoredaByIDs(newmodel.RegionID, newmodel.WoredaID);
                newmodel.kebeles = dbService.GetKebeles(newmodel.WoredaID);
                newmodel.fiscalYears = dbService.GetFiscalYears(0);
                
                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Re-Targeting", "Generate Form 7 Error", ip, "",
                "Form 7 Details with Message " + ex.Message + " for " + newmodel.KebeleID + "-" + newmodel.FiscalYear + " Encountered an error at " + DateTime.Now.ToString());

                return View("GenerateForm7", newmodel);
            }
        }
                
        [Authorize]
        public FileResult PrintForm7()
        {            
            string errMessage = string.Empty;
            string filename = string.Empty;
            try
            {
                string kebeleID = Request.QueryString["kebeleID"].ToString();
               
                var rptGenerated = reTargetingServices.Form7Report(kebeleID, Membership.GetUser().UserName.ToString(), out errMessage, out filename);
                if (rptGenerated == true)
                {
                    string strReportFilePath = System.Web.HttpContext.Current.Server.MapPath("~/ReportFilePath/" + filename);
                    return File(strReportFilePath, "application/pdf");
                }

            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);                
            }

            return null;
        }

        [Authorize]
        public ActionResult ModifyForm7(string retargetingHeaderID)
        {
            ViewBag.Subtitle = "Modify Form 7";

            var retargeting = reTargetingServices.FetchForm7Header(retargetingHeaderID);
            retargeting.yesnos = dbService.GetYesNo();
            retargeting.nutrStatus = dbService.GetNutritionalStatus();
            retargeting.worker = dbService.GetSocialWorkers();
            retargeting.clientStatus = dbService.GetClientStatus();
            retargeting.grades = dbService.GetSchoolGrade();

            //retargeting.Approving = false;

            /*My edits, to remove */
            retargeting.Approving = true;
            retargeting.AllowEdit = true;

            return View(retargeting);
        }

        
        [Authorize]
        public ActionResult ApproveForm7(string retargetingHeaderID)
        {
            ViewBag.Subtitle = "Modify Form 7";

            var retargeting = reTargetingServices.FetchForm7Header(retargetingHeaderID);
            retargeting.yesnos = dbService.GetYesNo();
            retargeting.nutrStatus = dbService.GetNutritionalStatus();
            retargeting.worker = dbService.GetSocialWorkers();
            retargeting.clientStatus = dbService.GetClientStatus();
            retargeting.grades = dbService.GetSchoolGrade();

            retargeting.Approving = true;

            return View("ModifyForm7", retargeting);
        }

        [Authorize]
        [HttpPost]
        public ActionResult UpdateForm7(Form7Header newmodel)
        {
            ViewBag.Subtitle = "Update Form 7";
            string errMessage = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    ViewBag.Subtitle = "Form 7 Listing";

                    if (newmodel.Approving)
                    {
                        if (newmodel.CreatedBy.ToUpper() == Membership.GetUser().UserName.ToString().ToUpper())
                        {
                            errMessage = "You cannot approve a record you created!";
                            throw new ArgumentException(errMessage);
                        }
                        
                        Tuple<List<int>, List<Form7MembersGrid>> resultset =
                            GeneralServices.NpocoConnection().FetchMultiple<int, Form7MembersGrid>(";Exec FetchGridForm7DetailsByID @RetargetingHeaderID, @IsUpdated", 
                            new {
                                    RetargetingHeaderID = newmodel.RetargetingHeaderID,
                                    IsUpdated = false
                            });

                        newmodel.yesnos = dbService.GetYesNo();
                        newmodel.nutrStatus = dbService.GetNutritionalStatus();
                        newmodel.worker = dbService.GetSocialWorkers();
                        newmodel.clientStatus = dbService.GetClientStatus();
                        newmodel.grades = dbService.GetSchoolGrade();

                        var notUpdatedCount = resultset.Item2.Count;
                        
                        if (notUpdatedCount > 0)
                        {
                            var message = string.Format("{0} records in this form have not yet been updated.", notUpdatedCount);

                            ModelState.AddModelError("", message);                           

                            return View("ModifyForm7", newmodel);
                        }

                        //check if DateUpdated, SW and CCC # supplied
                        if (string.IsNullOrEmpty(newmodel.DateUpdated))
                        {
                            ModelState.AddModelError("", "Date updated is required");

                            return View("ModifyForm7", newmodel);
                        }

                        if (string.IsNullOrEmpty(newmodel.SocialWorker) || (int.Parse(newmodel.SocialWorker) <= 0))
                        {
                            ModelState.AddModelError("", "Social worker is required");

                            return View("ModifyForm7", newmodel);
                        }

                        if (string.IsNullOrEmpty(newmodel.CCCCBSPCMember))
                        {
                            ModelState.AddModelError("", "CCCC/BSPC Member # is required");

                            return View("ModifyForm7", newmodel);
                        }

                        var result = reTargetingServices.ApproveForm7(newmodel);

                        dbAcService.InsertAuditTrail(User.Identity.Name, "Re-Targeting", "Approve Form 7", ip, "",
                        "Form7 Details " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Approval Successfuly  at " + DateTime.Now.ToString());
                    }
                    else
                    {
                        var result = reTargetingServices.UpdateForm7(newmodel);

                        dbAcService.InsertAuditTrail(User.Identity.Name, "Re-Targeting", "Update Form 7", ip, "",
                        "Form7 Details " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Update Successfuly  at " + DateTime.Now.ToString());
                    }
                    
                    return RedirectToAction("Form7", new { kebeleID = newmodel.KebeleID });
                }
                else
                {
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));

                    ModelState.AddModelError("", message);
                    
                    newmodel.yesnos = dbService.GetYesNo();
                    newmodel.nutrStatus = dbService.GetNutritionalStatus();
                    newmodel.worker = dbService.GetSocialWorkers();
                    newmodel.clientStatus = dbService.GetClientStatus();
                    newmodel.grades = dbService.GetSchoolGrade();

                    dbAcService.InsertAuditTrail(User.Identity.Name, "Re-Targeting", "Update Form7 Error", ip, "",
                    "Form7 Details with Message " + message + " for " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Encountered an error at " + DateTime.Now.ToString());

                    return View("ModifyForm7", newmodel);
                }
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                                
                newmodel.yesnos = dbService.GetYesNo();
                newmodel.nutrStatus = dbService.GetNutritionalStatus();
                newmodel.worker = dbService.GetSocialWorkers();
                newmodel.clientStatus = dbService.GetClientStatus();
                newmodel.grades = dbService.GetSchoolGrade();

                ModelState.AddModelError("", ex.Message);

                dbAcService.InsertAuditTrail(User.Identity.Name, "Re-Targeting", "Update Form7 Error", ip, "",
                "Form7 Details with Message " + ex.Message + " for " + newmodel.HouseHoldIDNumber + "-" + newmodel.MemberXml + " Encountered an error at " + DateTime.Now.ToString());

                return View("ModifyForm7", newmodel);
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult FetchJsonForm7DetailsByID(Form7Header oldModel)
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                var jtRetargetingHeaderID = oldModel.RetargetingHeaderID;

                Tuple<List<int>, List<Form7MembersGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form7MembersGrid>(";Exec FetchGridForm7DetailsByID @RetargetingHeaderID", new { RetargetingHeaderID = jtRetargetingHeaderID });
               
                return Json(resultset.Item2, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpGet]
        [Authorize]
        public XDocument FetchGridForm7DetailsByID()
        {
            Response.Clear();
            Response.AddHeader("Content-Type", "text/xml");

            try
            {
                var jtRetargetingHeaderID = Request.QueryString["RetargetingHeaderID"].ToString();

                Tuple<List<int>, List<Form7MembersGrid>> resultset
                    = SCTMis.Services.GeneralServices.NpocoConnection().FetchMultiple<int, Form7MembersGrid>(";Exec FetchGridForm7DetailsByID @RetargetingHeaderID", new { RetargetingHeaderID = jtRetargetingHeaderID });

                DataTable dt = SCTMis.Services.GeneralServices.ToDataTable<Form7MembersGrid>(resultset.Item2);

                string sb = SCTMis.Services.GeneralServices.GetDhtmlxXML(dt, resultset.Item1[0], 0, 0, 5);
                var xml = XDocument.Parse(sb);
                return xml;
            }
            catch (Exception ex)
            {
                GeneralServices.LogError(ex);

                return XDocument.Parse("<xml>error</xml>");
            }
        }
    }
}
